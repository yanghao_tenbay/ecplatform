
import { run as tradeRun, emit as tradeEmit } from "./order/trade"
import { run as transferRun, emit as transferEmit } from "./transfer/transfer"
import { run as wxpayRun, emit as wxpayEmit } from "./wxpaydaemon"

let eventMap: Map<string, any>
export function notify(event: string, param?: any) {
    let emit = eventMap.get(event)
    if (!emit) {
        throw new Error("invalid event " + event)
    }

    emit(event, param)
}

export async function run() {
    eventMap = new Map<string, any>()

    eventMap.set("trade", tradeEmit)
    tradeRun()

    eventMap.set("transfer", transferEmit)
    transferRun()

    eventMap.set("wxpay", wxpayEmit)
    wxpayRun()
}