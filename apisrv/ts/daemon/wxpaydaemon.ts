import { selectNewChargeState, finishSmsCharge } from "../dao/pc/daoSms"
export function run() {
    setTimeout(() => emit("wxpay"), 1000)
}

let running = false
let requestCount = 0
export function emit(event: string, param?: any) {
    requestCount++
    if (running) {
        console.log("already running")
        return
    }

    running = true
    process.nextTick(async () => {
        while (requestCount > 0) {
            console.log("start", requestCount)
            requestCount = 0
            await setPlayer()
        }
        running = false
        console.log("done", requestCount)
    })
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

async function setPlayer() {
    try {
        let arr = await selectNewChargeState()
        if (arr.length === 0)
            return

        await finishSmsCharge(arr)
    } catch (e) {
        console.log(e)
    }
}