import { postAsync } from '../../lib/request'
import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { md5sum, randomInt, formatDate, formatDate2 } from '../../lib/utils'
import { log_pc as log } from '../../lib/log'

export async function run() {
    // setTimeout(() => emit("trade"), 1000)
    setImmediate(() => emit("trade"), 3600 * 1000)
}

let running = false
export function emit(event: string, param?: any) {
    log.debug('---run trade-------')
    if (running) {
        log.debug("already running")
        return
    }

    running = true
    process.nextTick(async () => {
        await TraversalDb()
        running = false
        log.debug("trade daemin done")
    })
}

/**********************************************
* 遍历全部商家
* Author：mjq
* Date：2018-11-05
*/
async function TraversalDb() {
    let page = 1
    let size = 100
    while (true) {
        let sql = `select database_name from ec_platform.company where status = 1 and is_del = 0 limit ${(page - 1) * size}, ${size}`
        log.debug(`trade: ${sql}`)
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            return
        }

        //依次处理每个商家
        for (let v of rows) {
            await detail(v.database_name)
        }

        if (rows.length < size) {
            //没有下一页
            return
        }
        page++
    }
}

/**********************************************
* 遍历全部商家
* Author：mjq
* Date：2018-11-05
*/
async function detail(db_name: string) {
    let start = 0       //查询的开始位置
    let length = 100
    while (true) {
        //获取待下单状态的参与记录， 用户信息， 活动信息。
        //rebate_mall: activity_name, activity_id, expenditure, goods_id, after_event
        //user: user_id, openid, phone
        //rebate_mall_record: activity_id, user_id, tid
        let sql = `select mall.activity_name, mall.expenditure, mall.goods_id, mall.after_event, mall.credits, record.id, record.activity_id, record.tid, user.openid, user.phone from ${db_name}.rebate_mall mall, ${db_name}.user, ${db_name}.rebate_mall_record record where (record.status = 1 or record.background = 2) and mall.id = record.activity_id and user.user_id = record.user_id limit ${start}, ${length}`
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            return
        }

        //依次处理每一笔订单
        for (let v of rows) {
            let f = v.tid ? await signEvent(db_name, v) : await firstTime(db_name, v)
            if (false === f) {
                //已有多少个不符合的数据
                start++
            }
        }

        if (rows.length < length) {
            return
        }
    }
}

/**********************************************
* 活动记录没有找到对应订单
* Author：mjq
* Date：2018-10-29
*/
async function firstTime(db_name: string, opt: any): Promise<boolean> {
    let { activity_name, expenditure, goods_id, after_event, credits, id, activity_id, openid, phone } = opt
    let { event, hour } = JSON.parse(after_event)
    try {
        //拿到商品id和用户手机号，查询订单， 未找到订单说明用户未下单
        let sql = `select oid as tid, status, price, pay_time, end_time from ${db_name}.trade_order where num_iid = '${goods_id}' and receiver_mobile = '${phone}' and activity_status = 'false' limit 1`
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            //没有订单
            return false
        }
        let { tid, status, price, pay_time, end_time } = rows[0]

        if ('TRADE_CLOSED' === status) {
            //订单取消
            return false
        }

        //根据活动设置的转账时间限制，进行分类出来
        if ('pay' === event) {
            await transactionAsync(async conn => {
                //匹配出订单之后, 第一种处理方式：可以得出转账时间
                sql = `update ${db_name}.rebate_mall_record set tid = '${tid}', rebate = '${expenditure + price}', status = 2 where id = '${id}' and status = 1`
                log.debug(`firstTime pay_event: ${sql}`)
                await conn.queryAsync(sql)

                //将此订单置为已参与返利商城活动
                sql = `update ${db_name}.trade_order set activity_status = 'rebate_mall' where tid = '${tid}'`
                log.debug(`firstTime pay_event: ${sql}`)
                await conn.queryAsync(sql)

                //支付后X小时转账, 转账时间 = pay_time + hour
                sql = `insert into ${db_name}.transfer (activity, activity_name, record_id, openid, rebate, credits, transfer_time ) value ('rebate_mall', '${activity_name}', '${id}', '${openid}', '${expenditure + price}', '${credits}', date_add('${pay_time}', interval ${hour} hour))`
                log.debug(`firstTime pay_event: ${sql}`)
                await conn.queryAsync(sql)
            })
            return true
        } else if ('signed' === event) {
            if ('TRADE_BUYER_SIGNED' === status || 'TRADE_FINISHED' === status) {
                await transactionAsync(async conn => {
                    //匹配出订单之后, 第一种处理方式：可以得出转账时间
                    sql = `update ${db_name}.rebate_mall_record set tid = '${tid}', rebate = '${expenditure + price}', status = 2 where id = '${id}' and status = 1`
                    log.debug(`firstTime signed_event: ${sql}`)
                    await conn.queryAsync(sql)

                    //将此订单置为已参与返利商城活动
                    sql = `update ${db_name}.trade_order set activity_status = 'rebate_mall' where tid = '${tid}'`
                    log.debug(`firstTime signed_event: ${sql}`)
                    await conn.queryAsync(sql)

                    //确认收货后X小时转账, 转账时间 = end_time + hour
                    sql = `insert into ${db_name}.transfer (activity, activity_name, record_id, openid, rebate, credits, transfer_time ) value ('rebate_mall', '${activity_name}', '${id}', '${openid}', '${expenditure + price}', '${credits}', date_add('${end_time}', interval ${hour} hour))`
                    log.debug(`firstTime signed_event: ${sql}`)
                    await conn.queryAsync(sql)
                })
                return true
            } else {
                await transactionAsync(async conn => {
                    //将此订单置为已参与返利商城活动
                    sql = `update ${db_name}.trade_order set activity_status = 'rebate_mall' where tid = '${tid}'`
                    log.debug(`firstTime signed_event: ${sql}`)
                    await conn.queryAsync(sql)

                    //匹配出订单之后, 第二种处理方式：无法得出转账时间
                    sql = `update ${db_name}.rebate_mall_record set tid = '${tid}', rebate = '${expenditure + price}', status = 2, background = 2 where id = '${id}' and status = 1`
                    log.debug(`firstTime signed_event: ${sql}`)
                    await conn.queryAsync(sql)
                })
                return true
            }
        }

        return false
    } catch (e) {
        log.error(`firstTime: ${e}`)
        return false
    }
}

/**********************************************
* 之前已匹配订单时，不符合转账条件的记录。此次符合条件
* Author：mjq
* Date：2018-11-05
*/
async function signEvent(db_name: string, opt: any): Promise<boolean> {
    let { activity_name, after_event, tid, id, credits, openid, expenditure } = opt
    let { event, hour } = JSON.parse(after_event)
    try {
        //有订单号， 表明这个活动是确认收货转账，确认订单状态
        let sql = `select price, status, end_time from ${db_name}.trade_order where oid = '${tid}'`
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            //应该不会如此
            return false
        }
        let { price, status, end_time } = rows[0]

        //1:买家已签收, 货到付款专用, 2:交易成功
        if ('TRADE_BUYER_SIGNED' === status || 'TRADE_FINISHED' === status) {
            await transactionAsync(async conn => {
                //更改活动记录background状态为1
                sql = `update ${db_name}.rebate_mall_record set background = 1 where id = '${id}' and background = 2`
                log.debug(`signEvent ok: ${sql}`)
                await conn.queryAsync(sql)

                //插入待转账记录表
                sql = `insert into ${db_name}.transfer (activity, activity_name, record_id, openid, rebate, credits, transfer_time ) value ('rebate_mall', '${activity_name}', '${id}', '${openid}', '${expenditure + price}', '${credits}', date_add('${end_time}', interval ${hour} hour))`
                log.debug(`signEvent ok: ${sql}`)
                await conn.queryAsync(sql)
            })
            return true
        }

        //还是不能确认转账时间
        return false
    } catch (e) {
        log.error(`signEvent error: ${e}`)
        return false
    }
}