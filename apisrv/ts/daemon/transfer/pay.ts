
import { postAsync } from '../../lib/request'
import { buildXml, parseXmlAsync } from '../../lib/xml'
import { md5sum, randomInt, formatDate, formatDate2 } from '../../lib/utils'
// import { log_pc as log } from '../../lib/log'

interface IPay {
    mch_key: string       //支付key值
    mch_appid: string       //商户平台关联的appid
    mch_id: string           //商户id
    openid: string          //待转账的openid
    amount: number          //转账金额
    desc: string            //商品描述
    cert: any
    key: any
}

/**********************************************
* 请求微信接口
* Author：mjq
* Date：2018-10-08
*/
export async function pay(opt: IPay) {
    let transfer = {
        mch_appid: opt.mch_appid,
        mchid: opt.mch_id,
        nonce_str: 'mchpay',
        partner_trade_no: `${formatDate(new Date())}${randomInt(1000, 9999)}`,
        openid: opt.openid,
        check_name: 'NO_CHECK',
        amount: opt.amount * 100,
        desc: opt.desc,
        spbill_create_ip: '192.168.0.1',
    }

    let param = await getSign(transfer, opt.mch_key)
    let xml = buildXml(param, { headless: true, rootName: "xml" })
    let postOpt = {
        url: "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers",
        body: xml,
        cert: opt.cert,
        key: opt.key,
    }

    let body = await postAsync(postOpt)
    let obj = await parseXmlAsync(body)
    if (!obj || !obj.xml) {
        throw new Error("invalid result " + body)
    }

    obj = obj.xml
    let res = {} as any
    for (let k in obj) {
        let v = obj[k]
        res[k] = v[0]
    }
    if (res.return_code !== "SUCCESS") {
        throw new Error("invalid result " + body)
    }

    return res
}

export async function getSign(order: any, key: string) {
    let arr = new Array<any>()
    for (let k in order) {
        arr.push(`${k}=${order[k]}`)
    }
    arr.sort()
    arr.push(`key=${key}`)
    order.sign = md5sum(arr.join("&")).toUpperCase()
    return order
}