import { sendMail } from '../../lib/mail'
import { postAsync } from '../../lib/request'
import { md5sum, randomInt, formatDate, formatDate2 } from '../../lib/utils'

interface INoticeOk {
    template_id: string
    access_token: string
    openid: string
    name: string
    trade_order_no: string
    rebate: number
}

/**********************************************
* 转账成功之后， 发生模板消息通知用户
* Author：mjq
* Date：2018-10-08
*/
export async function noticePayOk(data: INoticeOk) {
    let options = {
        method: 'POST',
        url: `https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=${data.access_token}`,
        body: {
            "touser": data.openid,
            "template_id": data.template_id,
            "url": "",
            "data": {
                "first": {
                    "value": "尊敬的用户您好，您的一笔返现已到账。",
                    "color": "#173177"
                },
                "keyword1": {
                    "value": `${data.rebate}元`,
                    "color": "#173177"
                },
                "keyword2": {
                    "value": `订单${data.trade_order_no}已确认完成，返现${data.rebate}元`,
                    "color": "#173177"
                },
                "remark": {
                    "value": "感谢你的使用，谢谢！",
                    "color": "#173177"
                }
            }
        },
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        json: true
    };
    let a = await postAsync(options)
    console.log(a)
}

/**********************************************
* 转账失败之后， 发生邮件通知商家
* Author：mjq
* Date：2018-10-08
*/
export async function noticePayErr(opt: { nickname: string, err_msg: string, mailbox: string }) {
    try {
        let date = formatDate2(new Date())

        await sendMail({
            mailbox: opt.mailbox,
            subject: '转账失败通知',
            text: ` 尊敬的用户：您好！您的电商返利平台账户于${date}转账失败，\n
                    转账失败用户为: ${opt.nickname}，失败原因为：${opt.err_msg}\n
                    平台服务运营团队\n`,
        })
    } catch (e) {
        console.log('err:', e)
    }
}