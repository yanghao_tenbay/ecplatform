import fs = require('fs')
import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'

/**********************************************
* 获取指定商家数据库中的商户平台配置信息,以及注册时的邮箱
* Author：mjq
* Date：2018-10-08
*/
export async function getMchConf(db_name: string) {
    let sql = `select mch_id, mch_appid, mch_key, template_id from ${db_name}.mch_config limit 1`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error("mch_config表，缺少微信支付平台配置")
    }

    //获取转账失败时的通知邮箱地址
    // sql = `select b.email from ${db_name}.account a, ec_platform.company b where b.id = a.supervisor limit 1`
    // let r = await getConnectionAsync(async conn => conn.queryAsync(sql))

    // rows[0].mailbox = r[0].email
    rows[0].cert = fs.readFileSync(`/var/data/cert/${db_name}/apiclient_cert.pem`)
    rows[0].key = fs.readFileSync(`/var/data/cert/${db_name}/apiclient_key.pem`)
    return rows[0]
}
