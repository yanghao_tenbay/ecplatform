
import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { md5sum, randomInt, formatDate, formatDate2 } from '../../lib/utils'
import { log_pc as log } from '../../lib/log'
import { getMchConf } from './conf'
import { noticePayOk, noticePayErr } from './notice'
import { pay } from './pay'


export async function run() {
    // setTimeout(() => emit('transfer'), 1000)
    setImmediate(() => emit("transfer"), 60 * 60 * 1000)
}

let running = false
export function emit(event: string, param?: any) {
    log.debug('---run transfer-------')
    if (running) {
        log.debug("transfer already running")
        return
    }

    running = true
    process.nextTick(async () => {
        await TraversalDb()
        running = false
        log.debug("transfer daemin done")
    })
}

/**********************************************
* 遍历全部商家
* Author：mjq
* Date：2018-11-05
*/
async function TraversalDb() {
    let page = 1
    let size = 100
    while (true) {
        let sql = `select database_name from ec_platform.company where status = 1 and is_del = 0 limit ${(page - 1) * size}, ${size}`
        log.debug(`transfer: ${sql}`)
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            return
        }

        //依次处理商家数据库
        for (let v of rows) {
            await detail(v.database_name)
        }

        if (rows.length < size) {
            return
        }
        page++
    }
}

/**********************************************
* 处理单个商家待转账的交易订单
* Author：mjq
* Date：2018-11-05
*/
async function detail(db_name: string) {
    try {
        let conf = await getMchConf(db_name)
        let { mch_key, mch_appid, mch_id, cert, key } = conf

        while (true) {
            //获取待转账的记录
            let sql = `select a.id, a.activity, a.activity_name, a.record_id, a.openid, a.rebate, a.credits, b.user_id from ${db_name}.transfer a, ${db_name}.user b where a.status = 'new' and a.transfer_time < now() and b.openid = a.openid limit 0, 100`
            let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
            if (0 === rows.length) {
                return
            }

            for (let v of rows) {
                let { openid, rebate, activity, activity_name } = v

                let res = await pay({
                    mch_key: mch_key,
                    mch_appid: mch_appid,
                    mch_id: mch_id,
                    cert: cert,
                    key: key,
                    openid: openid,
                    amount: rebate,
                    desc: activity_name,
                })
                log.debug(`transfer_pay: ${JSON.stringify(res)}`)

                if ('SUCCESS' === res.return_code) {
                    //请求成功，处理转账结果
                    switch (activity) {
                        case 'rebate_mall':
                            await rebateMall(db_name, res, v)
                            break
                        case 'comment_rebate':
                            await commentRebate(db_name, res, v)
                            break
                        case 'order_rebate':
                            await orderRebate(db_name, res, v)
                            break
                        default:
                            log.error(`Ineffectual activity: ${v.activity}`)
                            break
                    }
                }
            }
        }
    } catch (e) {
        log.error(`transfer error: ${e.message}`)
        return
    }
}

/********************************************
* 返利商城处理转账结果
* Author：mjq
* Date：2018-11-07
*/
async function rebateMall(db_name: string, res: any, v: any) {
    let { result_code, err_code_des, partner_trade_no } = res
    let { id, credits, user_id, openid } = v

    //检查活动记录是否还存在
    let sql = `select id from ${db_name}.rebate_mall_record where id = '${v.record_id}' limit 1`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        //若活动记录不存在，删除对应的转账记录（不管是否转账成功）
        sql = `delete from ${db_name}.transfer where id = '${id}'`
        await getConnectionAsync(async conn => await conn.queryAsync(sql))
        return
    }

    if ('FAIL' === result_code) {
        await transactionAsync(async conn => {
            //转账失败, res.err_code_des
            let sql = `update ${db_name}.rebate_mall_record set status = 3, rebate_time = now(), pay_err_desc = '${err_code_des}' where id = '${v.record_id}'`
            log.debug(sql)
            await conn.queryAsync(sql)

            //删除转账记录
            sql = `delete from ${db_name}.transfer where id = '${id}'`
            log.debug(sql)
            await conn.queryAsync(sql)
        })
    } else {
        await transactionAsync(async conn => {
            //转账成功, res.partner_trade_no
            let sql = `update ${db_name}.rebate_mall_record set status = 4, rebate_time = now() where id = '${v.record_id}'`
            await conn.queryAsync(sql)

            //添加转账订单号，更改状态
            sql = `update ${db_name}.transfer set partner_trade_no = '${partner_trade_no}', status = 'fin' where id = '${id}'`
            await conn.queryAsync(sql)

            //更改用户积分
            if (credits > 0) {
                sql = `update ${db_name}.user set credits = credits + ${credits} where openid = '${openid}'`
                await conn.queryArsync(sql)

                sql = `insert into ${db_name}.credits (credits, user_id, reason) value ('${credits}', '${user_id}', '1')`
                await conn.queryArsync(sql)
            }
        })
    }
}

/**********************************************
* 晒评价处理转账结果
* Author：lxx
* Date：2018-11-14
*/
async function commentRebate(db_name: string, res: any, v: any) {
    let { result_code, err_code_des, partner_trade_no } = res
    let { id, credits, user_id, openid } = v

    if ('FAIL' === result_code) {
        await transactionAsync(async conn => {
            //转账失败, res.err_code_des
            let sql = `update ${db_name}.comment_record set status = 6, pay_err_desc = '${err_code_des}' where id = '${v.record_id}'`
            log.debug(sql)
            await conn.queryAsync(sql)

            //删除转账记录
            sql = `delete from ${db_name}.transfer where id = '${id}'`
            log.debug(sql)
            await conn.queryAsync(sql)
        })
    } else {
        await transactionAsync(async conn => {
            //转账成功, res.partner_trade_no
            let sql = `update ${db_name}.comment_record set status = 5 where id = '${v.record_id}' `
            await conn.queryAsync(sql)

            //添加转账订单号，更改状态
            sql = `update ${db_name}.transfer set partner_trade_no = '${partner_trade_no}', status = 'fin' where id = '${id}'`
            await conn.queryAsync(sql)

            //更改用户积分
            if (credits > 0) {
                sql = `update ${db_name}.user set credits = credits + ${credits} where openid = '${openid}'`
                await conn.queryArsync(sql)

                sql = `insert into ${db_name}.credits (credits, user_id, reason) value ('${credits}', '${user_id}', '1')`
                await conn.queryArsync(sql)
            }
        })
    }
}

/**********************************************
* 订单返利处理转账结果
* Author：lxx
* Date：2018-11-14
*/
async function orderRebate(db_name: string, res: any, v: any) {
    let { result_code, err_code_des, partner_trade_no } = res
    let { id, credits, user_id, openid } = v

    if ('FAIL' === result_code) {
        await transactionAsync(async conn => {
            //转账失败, res.err_code_des
            let sql = `update ${db_name}.order_record set status = 6, pay_err_desc = '${err_code_des}' where id = '${v.record_id}'`
            log.debug(sql)
            await conn.queryAsync(sql)

            //删除转账记录
            sql = `delete from ${db_name}.transfer where id = '${id}'`
            log.debug(sql)
            await conn.queryAsync(sql)
        })
    } else {
        await transactionAsync(async conn => {
            //转账成功, res.partner_trade_no
            let sql = `update ${db_name}.order_record set status = 5 where id = '${v.record_id}' `
            await conn.queryAsync(sql)

            //添加转账订单号，更改状态
            sql = `update ${db_name}.transfer set partner_trade_no = '${partner_trade_no}', status = 'fin' where id = '${id}'`
            await conn.queryAsync(sql)

            //更改用户积分
            if (credits > 0) {
                sql = `update ${db_name}.user set credits = credits + ${credits} where openid = '${openid}'`
                await conn.queryArsync(sql)

                sql = `insert into ${db_name}.credits (credits, user_id, reason) value ('${credits}', '${user_id}', '1')`
                await conn.queryArsync(sql)
            }
        })
    }
}