
/**
 * 代码内需要的路径
 */

//商户平台保存路径
export const MCH_CERT = '/var/data/cert/'

//存放用户上传的图片
export const PC_IMAGES = '/var/data/uploads/images/'
export const REQ_IMAGES_prefix = '/uploads/images/'