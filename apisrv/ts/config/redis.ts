export const opt = {
    host: "127.0.0.1"
}

/**
 * redis各个数据库中存储的数据内容
 * 格式：[数据库， 过期时间]
 */
export const rdb = {
    account: [1, 7200],
    user: [2, 86400],
    wxpay: [3, 7200],
    sms: [4, 180],
    mch: [5, 6000],
    wxAuth: [6, 1800]
}