import fs = require('fs')
import { postAsync } from './request'
import { MCH_CERT } from '../config/pwd'
import { buildXml, parseXmlAsync } from './xml'
import { getConnectionAsync, transactionAsync } from './mysqlpool'
import { md5sum, randomInt, formatDate, formatDate2 } from './utils'
// import { log_pc as log } from '../../lib/log'

async function getMchConf(db_name: string) {
    let sql = `select mch_id, mch_appid, mch_key, template_id from ${db_name}.mch_config limit 1`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error("mch_config表，缺少微信支付平台配置")
    }

    rows[0].cert = fs.readFileSync(`${MCH_CERT}${db_name}/apiclient_cert.pem`)
    rows[0].key = fs.readFileSync(`${MCH_CERT}${db_name}/apiclient_key.pem`)
    return rows[0]
}

async function getSign(order: any, key: string) {
    let arr = new Array<any>()
    for (let k in order) {
        arr.push(`${k}=${order[k]}`)
    }
    arr.sort()
    arr.push(`key=${key}`)
    order.sign = md5sum(arr.join("&")).toUpperCase()
    return order
}

/**********************************************
* 请求微信接口
* Author：mjq
* Date：2018-10-08
*/
async function pay(opt: any) {
    let transfer = {
        nonce_str: 'sendredpack',
        mch_billno: `${formatDate(new Date())}${randomInt(1000, 9999)}`,
        mch_id: opt.mch_id,
        wxappid: opt.mch_appid,
        send_name: opt.send_name, //string(32)
        re_openid: opt.openid,
        total_amount: opt.amount * 100,
        total_num: 1,
        wishing: opt.wishing,  //string(128)
        act_name: opt.act_name, //string(32)
        remark: opt.remark, //string(256)
        client_ip: '192.168.0.6',
        // scene_id: 'PRODUCT_3',
    }

    let param = await getSign(transfer, opt.mch_key)
    let xml = buildXml(param, { headless: true, rootName: "xml" })
    let postOpt = {
        url: "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack",
        body: xml,
        cert: opt.cert,
        key: opt.key,
    }

    let body = await postAsync(postOpt)
    let obj = await parseXmlAsync(body)
    if (!obj || !obj.xml) {
        throw new Error("invalid result " + body)
    }

    obj = obj.xml
    let res = {} as any
    for (let k in obj) {
        let v = obj[k]
        res[k] = v[0]
    }
    if (res.return_code !== "SUCCESS") {
        throw new Error("invalid result " + body)
    }

    return res
}

/**********************************************
* 发送微信红包
* Author：mjq
* Date：2018-10-08
*/
export async function sendRedPack(opt: any) {
    let { db_name, openid, rebate, send_name, wishing, act_name, remark } = opt
    if (!db_name || !openid || !rebate || !send_name || !wishing || !act_name || !remark) {
        throw new Error('Invalid parameter')
    }

    let conf = await getMchConf(db_name)
    let { mch_key, mch_appid, mch_id, cert, key } = conf

    let res = await pay({
        mch_key: mch_key,
        mch_appid: mch_appid,
        mch_id: mch_id,
        cert: cert,
        key: key,
        openid: openid,
        amount: rebate,
        send_name: send_name,
        wishing: wishing,
        act_name: act_name,
        remark: remark
    })
    return res
}