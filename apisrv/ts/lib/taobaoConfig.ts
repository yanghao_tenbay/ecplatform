import { md5sum, formatDate2, sleep } from './utils';
import { getConnectionAsync, transactionAsync } from './mysqlpool';
import { getAsync } from './request';
import cheerio = require('cheerio')
import { log_pc as log } from './log'
/**********************************************
 * 获取session_key
 * Author：何语漫
 * Date：2018-11-04
 */
export async function getSessionKey(id: string) {
    let sql = `SELECT session_key FROM ec_platform.authorization WHERE id = '${id}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(result)
    return result[0].session_key
}

/**********************************************
 * 更新session_key
 * Author：何语漫
 * Date：2018-11-04
 */
export async function UpdSessionKey() {
    while (true) {
        try {
            log.debug(`/更新淘宝接口凭证时间: ${formatDate2(new Date())}`)
            let sql = `SELECT id, session_key, refresh_token FROM ec_platform.authorization`
            let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
            let j = 10
            for (let i = 0; i < result.length; i++) {
                await transactionAsync(async conn => {
                    let sign = taoBaoSign()
                    let url = `http://api.taoesoft.com/api.aspx?action=RefreshSession&SessionKey=${result[i].session_key}&refresh_token=${result[i].refresh_token}&sign=${sign}`
                    let tokenStr = await getAsync(url)
                    let tokenMap = await JSON.parse(tokenStr)
                    console.log(tokenMap)
                    if (tokenMap.Top_session && tokenMap.Refresh_token) {
                        let sql = `UPDATE ec_platform.authorization SET session_key = '${tokenMap.Top_session}', refresh_token = '${tokenMap.Refresh_token}', refresh_time = '${formatDate2(new Date())}' WHERE id = ${result[i].id}`
                        console.log(sql)
                        await conn.queryAsync(sql)
                    } else {
                        if (0 == j) {
                            j = 10
                            i++
                        } else {
                            i--
                            j--
                            await sleep(1000)
                        }
                    }
                })
            }
            await sleep(((24 - 1) * 60 * 60) * 1000)
        } catch (e) {
            console.log(e.message)
            log.error(`/淘宝接口凭证出错: ${e.message}`)
            await sleep(2000)
        }
    }
}

/**********************************************
 * 淘宝签名
 * Author：何语漫
 * Date：2018-11-04
 */
export function taoBaoSign() {
    let key = `1b46af52a8dcaa822b98b1be9d447bb2`
    let time = new Date().getTime()
    time = time / 1000 / 60
    let stamp = time.toFixed(0).toString()
    let str = new Buffer(stamp.toString() + key, 'UTF8')
    let a = md5sum(str.toString())
    let a2 = md5sum(a)
    let a3 = new Buffer(a2)
    return a3.toString('base64')
}

/**********************************************
 * 商品信息
 * Author：何语漫
 * Date：2018-11-06
 */
export async function getItemInfo(NumIid: string, id: string) {
    let reqJson = {
        Fields: 'num_iid,price,pic_url,item_imgs,prop_imgs,desc',
        NumIid: NumIid
    }
    let reqstr = JSON.stringify(reqJson)
    let session_key = await getSessionKey(id)
    let sign = taoBaoSign()
    let url = `http://api.taoesoft.com/Api.aspx?action=executeTopApi&methodName=taobao.item.seller.get&requestObjectJson=${reqstr}&SessionKey=${session_key}&sign=${sign}`
    console.log(url)
    let tokenStr = await getAsync(url)
    console.log(tokenStr)
    let tokenMap = await JSON.parse(tokenStr)
    if (tokenMap.ErrCode) {
        throw new Error(`商品信息: ${tokenMap.ErrCode}`)
    }
    console.log(tokenMap.Item.Desc)
    let desc = cheerio.load(tokenMap.Item.Desc)
    let img = desc('p').find('img')
    let img_arr = new Array()
    img.each(function (i: any, elme: any) {
        img_arr.push(elme.attribs.src)
        console.log(elme.attribs.src)
    })
    console.log(img_arr)
    return {
        img: img_arr,
        price: tokenMap.Item.Price,
        numiid: NumIid
    }
}

/**********************************************
 * 评价信息
 * Author：何语漫
 * Date：2018-11-06
 */
export async function getTraderate(tid: string, id: string) {
    let session_key = await getSessionKey(id)
    let sign = taoBaoSign()
    let url = ` http://api.taoesoft.com/api.aspx?action=TraderatesGetRequest&SessionKey=${session_key}&tid=${tid}&PageNo=&RateType=get&Role=buyer&Result=good&start_time=&end_time=&sign=${sign}`
    console.log(url)
    let tokenStr = await getAsync(url)
    console.log(tokenStr)
    if (tokenStr.search('订单不存在')) {
        return {
            content: '',
            time: '',
            result: ''
        }
    }
    console.log(tokenStr)
    let tokenMap = await JSON.parse(tokenStr)
    return {
        content: tokenMap[0].Content,
        time: tokenMap[0].Created,
        result: tokenMap[0].Result
    }
}

/**********************************************
 * 同步订单
 * Author：何语漫
 * Date：2018-11-06
 */
export async function getIncrementTrade() {
    while (true) {
        log.debug(`/增量订单 更新时间：${formatDate2(new Date())} `)
        let tokenMap: any
        let tokenStr: any
        try {
            let sql = `SELECT id, session_key, refresh_token, shop_name, company_id FROM ec_platform.authorization`
            let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
            let j = 1
            let time = new Date()
            let start_time = new Date(time.setMinutes(time.getMinutes() - 30))
            let end_time = new Date()
            for (let i = 0; i < result.length; i++) {
                let company_id = result[i].company_id
                let sign = taoBaoSign()
                let postJson = {
                    Fields: "tid,type,status,payment,orders,created,promotion_details,receiver_mobile,pay_time,buyer_nick,seller_nick",
                    StartModified: `${formatDate2(start_time)}`,
                    EndModified: `${formatDate2(end_time)}`,
                }
                let session_key = result[i].session_key
                let url = `http://api.taoesoft.com/Api.aspx?action=executeTopApi&methodName=taobao.trades.sold.increment.get&requestObjectJson=${JSON.stringify(postJson)}&SessionKey=${session_key}&sign=${sign}`
                tokenStr = await getAsync(url)
                console.log(tokenStr)
                tokenMap = await JSON.parse(tokenStr)
                if (tokenMap.TotalResults == 0) {
                    continue
                }
                await transactionAsync(async conn => {
                    let body = tokenMap.Body
                    console.log(body)
                    body = JSON.parse(body)
                    let TotalResults = body.trades_sold_increment_get_response.total_results
                    let Trades = body.trades_sold_increment_get_response.trades.trade
                    tokenMap = Trades
                    console.log(Trades)
                    //console.log(tokenMap)
                    if (TotalResults != 0) {
                        for (let j = 0; j < Trades.length; j++) {
                            if (Trades[j].orders.order.length != 1) {
                                log.debug(`/增量订单出现两个orders：${JSON.stringify(Trades[j])}`)
                            }
                            if (Trades[j].buyer_rate == 'true') {
                                let commentstr = await getTraderate(Trades[j].tid, result[i].id)
                                Trades[j].buyer_rate = 1
                                Trades[j].comment = commentstr.content
                                Trades[j].result = commentstr.result
                                Trades[j].comment_time = commentstr.time
                            } else {
                                Trades[j].buyer_rate = 0
                                Trades[j].comment = ''
                                Trades[j].result = ''
                            }
                            let sql_pay = ''
                            let value_pay = ''
                            let upd_pay = ''
                            if (Trades[j].pay_time) {
                                sql_pay = ' pay_time,'
                                value_pay = `'${Trades[j].pay_time}',`
                                upd_pay = ` pay_time = '${Trades[j].pay_time}', `
                            }

                            let sql_comment = ''
                            let value_comment = ''
                            let upd_comment = ''
                            if (Trades[j].comment_time) {
                                sql_comment = ', comment_time'
                                value_comment = `, '${Trades[j].comment_time}',`
                                upd_comment = `, comment_time = '${Trades[j].comment_time}'`
                            }
                            console.log(Trades[j].orders)
                            console.log(Trades[j].orders.order)
                            console.log(Trades[j].orders.order.length)
                            for (let k = 0; k < Trades[j].orders.order.length; k++) {
                                let Order = Trades[j].orders.order[k]
                                console.log(Order)
                                let sql_end = ''
                                let value_end = ''
                                let upd_end = ''
                                if (Order.end_time) {
                                    sql_end = ' end_time,'
                                    value_end = `'${Order.end_time}',`
                                    upd_end = `end_time = '${Order.end_time}', `
                                }
                                if (!Order.divide_order_fee) {
                                    Order.divide_order_fee = Trades[j].payment
                                }
                                let sql = `SELECT * FROM company_${company_id}.trade_order WHERE oid = '${Order.oid}'`
                                let trade_result = await conn.queryAsync(sql)
                                if (trade_result.length != 0) {
                                    sql = `UPDATE company_${company_id}.trade_order SET seller_nick = '${Trades[j].seller_nick}', buyer_nick = '${Trades[j].buyer_nick}', title = '${Order.title}', receiver_mobile = '${Trades[j].receiver_mobile}', num_iid = '${Order.num_iid}', status = '${Trades[j].status}', price = ${Number(Order.divide_order_fee).toFixed(2)},  created_time = '${Trades[j].created}', ${upd_pay} ${upd_end} buyer_rate = '${Trades[j].buyer_rate}', comment = '${Trades[j].comment}', result = '${Trades[j].result}' ${upd_comment} WHERE oid = '${Order.oid}'`
                                    console.log(sql)
                                } else {
                                    sql = `INSERT INTO company_${company_id}.trade_order(tid, oid, seller_nick, buyer_nick, title, activity_status, receiver_mobile, num_iid, status, price, created_time, ${sql_pay} ${sql_end} buyer_rate, comment, result ${sql_comment}) VALUES ('${Trades[j].tid}', '${Order.oid}','${Trades[j].seller_nick}', '${Trades[j].buyer_nick}', '${Order.title}', 'false', '${Trades[j].receiver_mobile}', '${Order.num_iid}', '${Trades[j].status}', '${Number(Order.divide_order_fee).toFixed(2)}', '${Trades[j].created}', ${value_pay} ${value_end} '${Trades[j].buyer_rate}', '${Trades[j].comment}', '${Trades[j].result}' ${value_comment})`
                                    console.log(sql)
                                }
                                await conn.queryAsync(sql)
                            }
                        }
                    } else {
                        if (0 == j) {
                            j = 1
                            i++
                        } else {
                            i--
                            j--
                            await sleep(1000)
                        }
                    }
                })
            }
            await sleep(end_time.getTime() - start_time.getTime() - 300000)
        } catch (e) {
            console.log(e.message)
            log.error(`/增量订单: ${e.message}, ${tokenStr},  ${JSON.stringify(tokenMap)}`)
            await sleep(2000)
        }
    }
}