import { getConnectionAsync } from "./mysqlpool";
import { postAsync } from "./request"
import { sleep } from "./utils"
import { log_pc as log } from './log'

/**********************************************
 * 微信公众号第三方平台配置，定时更新预授权码，接口凭证
 * Author：何语漫
 * Date：2018-9-27
 */
export class componentConfig {
    static component_appid: string
    static component_appsecret: string
    static component_verify_ticket: string
    static component_access_token: string
    static pre_auth_code: string
    static token: string
    static encoding_aes_key: string
    static notify_url: string
    static createManue_url: string
    static main_url: string

    private async setConf() {
        let sql = `SELECT * FROM ec_platform.component_config`
        let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
        componentConfig.component_appid = rows[0].component_appid
        componentConfig.component_appsecret = rows[0].component_appsecret
        componentConfig.token = rows[0].token
        componentConfig.encoding_aes_key = rows[0].encoding_aes_key
        componentConfig.component_verify_ticket = rows[0].component_verify_ticket
        componentConfig.component_access_token = rows[0].component_access_token
        componentConfig.pre_auth_code = rows[0].pre_auth_code
        componentConfig.notify_url = rows[0].notify_url
        componentConfig.createManue_url = rows[0].createmanue_url
        componentConfig.main_url = rows[0].main_url
    }

    public async getAuthority() {
        if (!componentConfig.component_appid || !componentConfig.component_appsecret || !componentConfig.token || !componentConfig.encoding_aes_key) {
            await this.setConf()
        }
        return {
            component_appid: componentConfig.component_appid,
            component_appsecret: componentConfig.component_appsecret,
            token: componentConfig.token,
            encoding_aes_key: componentConfig.encoding_aes_key
        }
    }

    //定时更新component_access_token
    public async updCompAccessToken() {
        while (true) {
            try {
                let conf = await this.getAuthority()
                console.log(conf + '    定时更新component_access_token')
                let postOpt = {
                    url: `https://api.weixin.qq.com/cgi-bin/component/api_component_token`,
                    body: JSON.stringify({
                        component_appid: conf.component_appid,
                        component_appsecret: conf.component_appsecret,
                        component_verify_ticket: componentConfig.component_verify_ticket
                    })
                }
                let result = await postAsync(postOpt)
                let rMap = await JSON.parse(result)

                if (rMap.component_access_token && rMap.expires_in) {
                    componentConfig.component_access_token = rMap.component_access_token

                    let sql = `UPDATE ec_platform.component_config SET component_access_token = '${componentConfig.component_access_token}'`
                    await getConnectionAsync(async conn => conn.queryAsync(sql))
                    await sleep((rMap.expires_in - 200) * 1000)
                } else {
                    this.setConf()
                    log.debug(`/定时更新component_access_token:${JSON.stringify(rMap)}`)
                    await sleep(30000)
                }
            } catch (e) {
                log.error(`/定时更新component_access_token: ${e.message}`)
                await sleep(30000)
            }
        }
    }

    //定时更新预授权码
    public async updPreAuthCode() {
        try {
            let conf = await this.getAuthority()
            console.log(conf + '    定时更新预授权码')
            let postOpt = {
                url: `https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=${componentConfig.component_access_token}`,
                body: JSON.stringify({
                    component_appid: conf.component_appid
                })
            }
            let result = await postAsync(postOpt)
            let rMap = await JSON.parse(result)

            if (rMap.pre_auth_code && rMap.expires_in) {
                componentConfig.pre_auth_code = rMap.pre_auth_code

                let sql = `UPDATE ec_platform.component_config SET pre_auth_code = '${componentConfig.pre_auth_code}'`
                await getConnectionAsync(async conn => conn.queryAsync(sql))
            } else {
                this.setConf()
                log.debug(`/获取预授权码: ${JSON.stringify(rMap)}`)
            }
        } catch (e) {
            log.error(`/获取预授权码: ${e.message}`)
        }
    }

    //保存component_verify_ticket
    public async saveCompVerifyTik(component_verify_ticket: string) {
        componentConfig.component_verify_ticket = component_verify_ticket
        let sql = `UPDATE ec_platform.component_config SET component_verify_ticket = '${component_verify_ticket}'`
        await getConnectionAsync(async conn => conn.queryAsync(sql))
    }

    public async getCrypto() {
        if (!componentConfig.component_appid || !componentConfig.token || !componentConfig.encoding_aes_key) {
            await this.setConf()
        }
        return {
            compAppid: componentConfig.component_appid,
            compToken: componentConfig.token,
            commpEncodingAesKey: componentConfig.encoding_aes_key
        }
    }
    public async getAuthorityConf() {
        if (!componentConfig.component_appid || !componentConfig.component_access_token || !componentConfig.notify_url) {
            await this.setConf()
        }
        return {
            compAccessToken: componentConfig.component_access_token,
            compAppid: componentConfig.component_appid,
            notifyUrl: componentConfig.notify_url
        }
    }

    public async getCreateManueUrl() {
        if (!componentConfig.createManue_url) {
            await this.setConf()
        }
        return {
            createManue_url: componentConfig.createManue_url
        }
    }
    public async getPreAuthCode() {
        await updPreAuthCode()
        await this.setConf()
        return {
            preAuthCode: componentConfig.pre_auth_code
        }
    }

    public async getComponentAppidAndHost() {
        if (!componentConfig.component_appid) {
            await this.setConf()
        }
        return {
            appid: componentConfig.component_appid,
            host: componentConfig.main_url
        }
    }

}

export async function getCreateManueUrl() {
    let cpf = new componentConfig()
    return await cpf.getCreateManueUrl()
}

export async function saveCompVerifyTik(component_verify_ticket: string) {
    let cpf = new componentConfig()
    await cpf.saveCompVerifyTik(component_verify_ticket)
}

export async function getCrypto() {
    let cpf = new componentConfig()
    return await cpf.getCrypto()
}

export async function getAuthorityConf() {
    let cpf = new componentConfig()
    return await cpf.getAuthorityConf()
}

export async function getPreAuthcode() {
    let cpf = new componentConfig()
    return await cpf.getPreAuthCode()
}

export async function updCompAccessToken() {
    let cpf = new componentConfig()
    return await cpf.updCompAccessToken()
}

export async function updPreAuthCode() {
    let cpf = new componentConfig()
    return await cpf.updPreAuthCode()
}

export async function getComponentAppidAndHost() {
    let cpf = new componentConfig()
    return await cpf.getComponentAppidAndHost()
}
