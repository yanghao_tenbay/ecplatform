import { createHash, createHmac } from "crypto"
import dateformat = require("dateformat")


export function sendOk(res: any, data: any): void {
    let ret = { status: 0, data: data }
    res.send(JSON.stringify(ret))
}

export function sendDomain(res: any, data: any): void {
    let ret = { status: 2, data: data }
    res.send(JSON.stringify(ret))
}

export function sendOkTable(res: any, count: number, num: number, row: any): void {
    let ret = { status: 0, recordsFiltered: count, recordsTotal: num, data: row }
    res.send(JSON.stringify(ret))
}

export function sendOkStat(res: any, count: number, num: number, total_amount: number, total_number: number, row: any): void {
    let ret = { status: 0, recordsFiltered: count, recordsTotal: num, total_amount: total_amount, total_number: total_number, data: row }
    res.send(JSON.stringify(ret))
}

export function sendError(res: any, errmsg: string): void {
    let ret = { status: 1, msg: errmsg }
    res.send(JSON.stringify(ret))
}

export function md5sum(str: string): string {
    return createHash('md5').update(str).digest("hex")
}

export function sha256(str: string, secret: string): string {
    return createHmac('sha1', secret).update(str).digest("base64")
}

export function randomInt(from: number, to: number) {
    return Math.floor(Math.random() * (to - from) + from)
}

export function formatDate(d: Date) {
    return dateformat(d, "yyyymmddHHMMss")
}

export function formatDate1(d: Date) {
    return dateformat(d, "yyyy-mm-dd'T'HH:MM:ss'Z'")
}

export function formatDate2(d: Date) {
    return dateformat(d, "yyyy-mm-dd HH:MM:ss")
}

export function formatDate3(d: Date) {
    return dateformat(d, "yyyy-mm-dd")
}

export function formatDate4(d: Date) {
    console.log(d.getFullYear().toString() + '-' + (d.getMonth() + 1).toString())
    return d.getFullYear().toString() + '-' + (d.getMonth() + 1).toString()

}
export function getRandStr(str_length: number): string {
    let str = '';
    for (; str.length < str_length; str += Math.random().toString(36).substr(2));
    return str.substr(0, str_length);
}

export function sleep(ms: number) {
    return new Promise(resolve => setTimeout(() => resolve(), ms))
}


export function replaceStr(str: string, rplStr: string): string {
    let valistr = str.replace(/'/g, "''")
    valistr = valistr.replace(/\\/g, "\\\\")
    if (rplStr === '+') {
        valistr = valistr.replace(/\+/g, `${rplStr}+`)
        valistr = valistr.replace(/\//g, `${rplStr}/`)
    } else if (rplStr === '/') {
        valistr = valistr.replace(/\+/g, `${rplStr}+`)
    }
    valistr = valistr.replace(/_/g, `${rplStr}_`)
    valistr = valistr.replace(/%/g, `${rplStr}%`)
    return valistr
}

export function replaceStr2(str: string) {
    function replace(str: string, rplStr: string) {
        let valistr = str.replace(/'/g, "''")
        valistr = valistr.replace(/\\/g, "\\\\")
        if (rplStr === '+') {
            valistr = valistr.replace(/\+/g, `${rplStr}+`)
            valistr = valistr.replace(/\//g, `${rplStr}/`)
        } else if (rplStr === '/') {
            valistr = valistr.replace(/\+/g, `${rplStr}+`)
        }
        valistr = valistr.replace(/_/g, `${rplStr}_`)
        valistr = valistr.replace(/%/g, `${rplStr}%`)
        return {
            valistr: valistr,
            rplStr: rplStr,
        }
    }

    return str ? (str.includes('/') ? replace(str, '+') : replace(str, '/')) : { valistr: '', rplStr: '/' }
}

export function replaceStr3(param: any) {
    for (let k in param) {
        if (param[k] && 'string' === typeof (param[k])) {
            param[k] = param[k].replace(/\\/g, "\\\\").replace(/'/g, "\\\'")
        }
    }
    return param
}

export function replaceToSign(str: string): string {
    let valistr = str.replace(/%/g, "%25")
    valistr = valistr.replace(/=/g, '%3D')
    valistr = valistr.replace(/&/g, '%26')
    return valistr
}

export function replaceUrl(str: string): string {
    str = str.replace(/%/g, '%25')
    str = str.replace(/ /g, '%20')
    str = str.replace(/"/g, "%22")
    str = str.replace(/#/g, '%23')
    str = str.replace(/&/g, '%26')
    str = str.replace(/\(/g, "%28")
    str = str.replace(/\)/g, '%29')
    str = str.replace(/\+/g, '%2B')
    str = str.replace(/,/g, "%2C")
    str = str.replace(/\//g, '%2F')
    str = str.replace(/\:/g, '%3A')
    str = str.replace(/;/g, "%3B")
    str = str.replace(/</g, '%3C')
    str = str.replace(/=/g, '%3D')
    str = str.replace(/>/g, '%3E')
    str = str.replace(/\?/g, '%3F')
    str = str.replace(/@/g, '%40')
    str = str.replace(/\\/g, '%5C')
    str = str.replace(/\|/g, '%7C')
    str = str.replace(/{/g, '%7B')
    str = str.replace(/}/g, '%7D')
    return str
}

export function signString(source: string, secret: string) {
    let hash = sha256(source, secret);
    return hash
}

export function strlen(str: string) {
    let len = 0
    for (let i = 0; i < str.length; i++) {
        let c = str.charCodeAt(i)
        //单字节加1
        if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
            len++
        }
        else {
            len += 2
        }
    }
    return len
}

