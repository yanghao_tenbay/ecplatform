import nodemailer = require('nodemailer')
import { getConnectionAsync } from './mysqlpool';

interface Imail {
    mailbox: string
    subject: string
    text: string
    html?: string
}

export async function sendMail(data: Imail) {
    let sql1 = `SELECT * FROM email_config`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let Email_config = res[0]

    let transporter = nodemailer.createTransport({
        host: Email_config.host,
        port: Email_config.port,
        auth: {
            user: Email_config.user,
            pass: Email_config.pass
        }
    })

    let mailOptions = {
        from: Email_config.user,               //发件人
        to: data.mailbox,                       //收件人，可以设置多个
        subject: data.subject,                  //邮件主题
        text: data.text,                        //邮件文本
        html: data.html ? data.html : ''        //html格式文本
    }

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, function (error: any, info: any) {
            if (error) {
                //console.log('邮件发送失败')
                return reject(error)
            }

            //console.log('邮件发送成功')
            return resolve(info)
        })
    })
}