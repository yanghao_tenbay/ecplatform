import crypto = require('crypto')
import { parseXmlAsync, buildXml } from './xml'
import { getCrypto } from './compconfig'
import { log_pc as log } from './log'

export function sha1_encode(str: string): string {
    return crypto.createHash('sha1').update(str).digest("hex")
}

/**********************************************
 * 将公众号回复用户的消息加密打包
 * Author：何语漫
 * Date：2018-09-21
 */
export async function enCryptMsg(replyMsg: string, nonce: string, timestamp: string) {
    try {
        let cryptoConfig = await getCrypto()
        let encrypt = await encodeCrypt(replyMsg, cryptoConfig.commpEncodingAesKey, cryptoConfig.compAppid)
        let sign = getSHA1(cryptoConfig.compToken, timestamp, nonce, encrypt)
        let xml = {
            'msg_encrypt': encrypt,
            'msg_signaturet': sign,
            'timestamp': timestamp,
            'nonce': nonce,
        }
        return buildXml(xml, { headless: true, rootName: "xml" })
    } catch (e) {
        log.error(`/加密: ${e.message}`)
        throw new Error(e.message)
    }
}

/**********************************************
 * 解密
 * Author：何语漫
 * Date：2018-09-21
 */
export async function deCryptMsg(encryptxml: string, msg_signature: string, timetamp: string, nonce: string) {
    try {
        let cryptoConfig = await getCrypto()
        log.debug(`/解密配置：${JSON.stringify(cryptoConfig)}`)
        let xml = await parseXmlAsync(encryptxml)
        let token = cryptoConfig.compToken //获取数据库内的token
        let sign = await getSHA1(token, timetamp, nonce, xml.xml.Encrypt[0])
        if (sign !== msg_signature) {
            return
        }
        let decryptjson = await decodeCrypto(xml.xml.Encrypt[0], cryptoConfig.commpEncodingAesKey, cryptoConfig.compAppid)
        return await parseXmlAsync(decryptjson)
    } catch (e) {
        log.error(`/解密: ${e.message}`)
        throw new Error(e.message)
    }
}

async function encodeCrypt(xmlencode: string, encodingAesKey: string, compappid: string) {
    let asekey = new Buffer(encodingAesKey + '=', 'base64')
    let iv = asekey.slice(0, 16)
    var random16 = crypto.pseudoRandomBytes(16);
    var msg = new Buffer(xmlencode);
    var msgLength = new Buffer(4);
    msgLength.writeUInt32BE(msg.length, 0);
    let appid = new Buffer(compappid);
    let raw_msg = Buffer.concat([random16, msgLength, msg, appid]);//randomString + msgLength + xmlMsg + this.corpID;
    let encoded = PKCS7Encoder(raw_msg);
    let cipher = crypto.createCipheriv('aes-256-cbc', asekey, iv);
    let cipheredMsg = Buffer.concat([cipher.update(encoded), cipher.final()]);
    return cipheredMsg.toString('base64')
}

async function decodeCrypto(xmldecode: string, encodingAesKey: string, compappid: string) {
    let asekey = new Buffer(encodingAesKey + '=', 'base64')
    let iv = asekey.slice(0, 16)
    let aesCipher = crypto.createDecipheriv('aes-256-cbc', asekey, iv)
    aesCipher.setAutoPadding(false)
    let decipheredBuff = Buffer.concat([aesCipher.update(xmldecode, 'base64'), aesCipher.final()])
    decipheredBuff = PKCS7Decoder(decipheredBuff);
    let len_netOrder_corpid = decipheredBuff.slice(16);
    let msg_len = len_netOrder_corpid.slice(0, 4).readUInt32BE(0);
    let result = len_netOrder_corpid.slice(4, msg_len + 4).toString();
    let appId = len_netOrder_corpid.slice(msg_len + 4).toString();
    if (appId != compappid) throw new Error('appId is invalid');
    return result
}


async function getSHA1(token: string, timestamp: string, nonce: string, enCrypt: string) {
    let arr = new Array()
    arr.push(token)
    arr.push(timestamp)
    arr.push(nonce)
    arr.push(enCrypt)
    arr.sort()
    let str = arr[0] + arr[1] + arr[2] + arr[3]
    return sha1_encode(str)
}

function PKCS7Decoder(buff: any) {
    var pad = buff[buff.length - 1];
    if (pad < 1 || pad > 32) {
        pad = 0;
    }
    return buff.slice(0, buff.length - pad);
}

function PKCS7Encoder(buff: any) {
    var blockSize = 32;
    var strSize = buff.length;
    var amountToPad = blockSize - (strSize % blockSize);
    var pad = new Buffer(amountToPad - 1);
    pad.fill(String.fromCharCode(amountToPad));
    return Buffer.concat([buff, pad]);
}
