import { postAsync } from "./request"
import { getConnectionAsync } from './mysqlpool';
import { getRedisClientAsync } from "./redispool"
import { randomInt } from "./utils"
import { rdb } from "../config/redis"

export class SmsConfig {
    static account: string
    static password: string
    static url: string

    private async setConf() {
        let sql = `select * from sms_config where status = '1'`
        let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
        if (rows.length == 0) {
            throw new Error("sms_config表， 缺少配置")
        }
        SmsConfig.account = rows[0].account
        SmsConfig.password = rows[0].password
        SmsConfig.url = rows[0].url
    }

    public async send(phone: string, code: number) {
        if (!SmsConfig.account || !SmsConfig.password || !SmsConfig.url) {
            await this.setConf()
        }

        let options = {
            method: 'POST',
            url: SmsConfig.url,
            body: {
                'account': SmsConfig.account,
                'password': SmsConfig.password,
                'phone': phone,
                'msg': `【电商平台】您的验证码是${code}。如非本人操作，请忽略。`,
                'report': 'false',
            },
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
            json: true
        };
        let a = await postAsync(options)
        return a
    }
}

/**********************redis********************* */
const [loginDbOpt, loginTimeout] = [{ db: rdb.sms[0] }, rdb.sms[1]]
export async function setLoginAsync(key: string | number, valus: string) {
    await getRedisClientAsync(async client => {
        await client.setAsync(key, valus)
        await client.expireAsync(key, loginTimeout)
    }, loginDbOpt)
}

export async function getLoginAsync(key: string | number): Promise<string> {
    return await getRedisClientAsync(async rds => await rds.getAsync(key), loginDbOpt)
}

export async function delLoginToken(key: string): Promise<void> {
    await getRedisClientAsync(async rds => await rds.delAsync(key), loginDbOpt)
}

/**
 * 发送验证码
 */
export async function sendCode(phone: string): Promise<void> {
    let code = randomInt(1000, 10000)
    let obj = new SmsConfig()
    let r = await obj.send(phone, code) as any
    if (r.code != '0') {
        throw new Error('发送失败')
    }
    let data = {
        code: code,
        time: new Date()
    }
    await setLoginAsync(phone, JSON.stringify(data))
}

/**
 * 检查验证码是否正确
 * @param phone
 */
export async function checkCode(phone: string, code: string): Promise<void> {
    let data = await getLoginAsync(phone)
    if (data == undefined) {
        throw new Error('手机号错误')
    }
    let dataMap = JSON.parse(data)
    if (dataMap.code != code) {
        throw new Error('验证码错误')
    }
    await delLoginToken(phone)
}

/**
 * PC端发送验证码
 */
export async function PcSendCode(phone: string): Promise<void> {
    console.log(`手机号：${phone}`)
    let code = randomInt(100000, 1000000)
    let obj = new SmsConfig()
    let data
    let res = await getLoginAsync(phone)
    console.log('读取已保存的信息:' + res)
    if (res == undefined) {
        let r = await obj.send(phone, code) as any
        if (r.code != '0') {
            throw new Error('发送失败')
        }
        data = {
            code: code,
            time: new Date()
        }
        console.log('未读到，执行if,装入data:' + JSON.stringify(data))
        await setLoginAsync(phone, JSON.stringify(data))
    }
    else {
        let dataMap = JSON.parse(res)
        if ((new Date().getTime() - new Date(dataMap.time).getTime()) / 1000 < 60) {
            throw new Error('发送间隔至少为1分钟!')
        }
        else {
            let r = await obj.send(phone, code) as any
            if (r.code != '0') {
                throw new Error('发送失败')
            }
            data = {
                code: code,
                time: new Date()
            }
            console.log(`读取成功，执行else装入新的data:${JSON.stringify(data)}`)
            await setLoginAsync(phone, JSON.stringify(data))
        }
    }
}

/**
 * pc端检查验证码是否正确
 * @param phone
 */
export async function PccheckCode(phone: string, code: string): Promise<void> {
    console.log('校验验证码，手机号：' + phone)
    let res = await getLoginAsync(phone)
    console.log('读取已保存的信息:' + res)
    if (res == undefined) {
        throw new Error('验证码错误')
    }
    let dataMap = JSON.parse(res)
    if (dataMap.code != code) {
        throw new Error('验证码错误')
    }
    else {
        /**验证通过后清空code */
        dataMap.code = ''
        await setLoginAsync(phone, JSON.stringify(dataMap))
    }
}