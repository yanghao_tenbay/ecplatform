import { getConnectionAsync, transactionAsync } from "./mysqlpool";
import { postAsync } from "./request"
import { sleep } from "./utils"
import { log_pc as log } from './log'

/**********************************************
 * 微信公众号，获取接口凭证
 * Author：何语漫
 * Date：2018-9-27
 */
export async function getaccess_token(appid: string) {
    let sql = `SELECT authorizer_access_token FROM ec_platform.wx_config WHERE appid = '${appid}'`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return rows[0].authorizer_access_token
}

/**********************************************
 * 微信公众号，保存接口凭证
 * Author：何语漫
 * Date：2018-9-27
 */
export async function setAccessToken(appid: string, authorizer_access_token: string, authorizer_refresh_token: string, company_id: string, func_info: string) {
    let sql = `SELECT appid FROM ec_platform.wx_config WHERE appid = '${appid}'`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (rows.length !== 0) {
        sql = `DELETE FROM ec_platform.wx_config WHERE appid = '${appid}'`
        await getConnectionAsync(async conn => conn.queryAsync(sql))
    }
    sql = `INSERT INTO ec_platform.wx_config(appid, authorizer_access_token, authorizer_refresh_token, company_id, func_info) VALUES('${appid}', '${authorizer_access_token}','${authorizer_refresh_token}', '${company_id}', '${func_info}')`
    console.log(sql)
    await getConnectionAsync(async conn => conn.queryAsync(sql))
}

/**********************************************
 * 短信充值调用接口
 * Author：何语漫
 * Date：2018-9-27
 */
export async function getWxpay() {
    let sql = `SELECT * FROM ec_platform.mch_config_platform`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return {
        appid: result[0].mch_appid,
        mch_id: result[0].mch_id,
        notify_url: result[0].notify_url,
        key: result[0].mch_key
    }
}

/**********************************************
 * 定时更新接口凭证
 * Author：何语漫
 * Date：2018-10-08
 */
export async function updAccessToken() {
    while (true) {
        try {
            let expires_in = 7200
            let sql = `SELECT a.appid, a.authorizer_refresh_token, b.component_appid, b.component_access_token FROM ec_platform.wx_config a, ec_platform.component_config b`
            let auth = await getConnectionAsync(async conn => conn.queryAsync(sql))
            let j = 10
            for (let i = 0; i < auth.length; i++) {
                await transactionAsync(async conn => {
                    let postOpt = {
                        url: `https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=${auth[i].component_access_token}`,
                        body: JSON.stringify({
                            component_appid: auth[i].component_appid,
                            authorizer_appid: auth[i].appid,
                            authorizer_refresh_token: auth[i].authorizer_refresh_token,
                        })
                    }
                    let result = await postAsync(postOpt)
                    let rMap = await JSON.parse(result)
                    if (rMap.authorizer_access_token && rMap.authorizer_refresh_token) {
                        let sql = `UPDATE ec_platform.wx_config set authorizer_access_token = '${rMap.authorizer_access_token}',authorizer_refresh_token = '${rMap.authorizer_refresh_token}' where appid = '${auth[i].appid}'`
                        await conn.queryAsync(sql)
                        expires_in = rMap.expires_in
                    } else {
                        j--
                        if (0 == j) {
                            j = 10
                            i++
                        }
                        i--
                        await sleep(2000)
                    }
                }
                )
            }
            await sleep((expires_in - 300) * 1000)
        } catch (e) {
            log.error(`/定时更新接口凭证: ${e.message}`)
        }
    }
}
