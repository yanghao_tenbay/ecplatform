import { UnifiedOrder, SignUnifiedOrder, PrePayUnifiedOrder } from "../entity/unifiedorder"
import { md5sum, randomInt, formatDate } from "./utils"
import { buildXml, parseXmlAsync } from "./xml"
import { postAsync } from "./request"
import { log_pc as log } from './log'

interface unifiedOrderOpt {
    appid: string
    mch_id: string
    body: string
    total_fee: number
    spbill_create_ip: string
    notify_url: string
    timeout: number
    key: string
    product_id: string
}

// 订单起始和失效时间
function getStartExpire(timeout: number) {
    let start = new Date()
    let expire = new Date(start.getTime() + timeout * 1000)
    return { start: formatDate(start), expire: formatDate(expire) }
}

// 生成订单号
function getOutTradeNo() {
    return `${formatDate(new Date())}${randomInt(1000, 9999)}`
}
// 订单签名
function getSign(order: any, key: string) {
    let arr = new Array<any>()
    for (let k in order) {
        arr.push(`${k}=${order[k]}`)
    }
    arr.sort()
    arr.push(`key=${key}`)
    return md5sum(arr.join("&")).toUpperCase()
}

// 统一订单号
function getUnifiedOrder(opt: unifiedOrderOpt): UnifiedOrder {
    let out_trade_no = getOutTradeNo()
    const { start, expire } = getStartExpire(opt.timeout)
    return new UnifiedOrder({
        appid: opt.appid,
        mch_id: opt.mch_id,
        body: opt.body,
        notify_url: opt.notify_url,
        product_id: opt.product_id,
        spbill_create_ip: opt.spbill_create_ip,
        total_fee: opt.total_fee,
        trade_type: "NATIVE",
        device_info: "WEB",
        time_start: start,
        time_expire: expire,
        out_trade_no: out_trade_no,
        attach: out_trade_no,
        nonce_str: md5sum(`${randomInt(100000, 999999)}`),
    })
}

function splitWxResponse(res: any) {
    let sign
    let obj = {} as any
    for (let k in res) {
        if (k === "sign") {
            sign = res[k][0]
        } else {
            obj[k] = res[k][0]
        }
    }
    return { obj: obj, sign: sign }
}

// 请求微信服务器，生成订单
export async function genPrePayUnifiedOrder(opt: unifiedOrderOpt): Promise<PrePayUnifiedOrder> {
    try {
        // 生成请求参数，并加上校验码
        let order = getUnifiedOrder(opt)
        let postOpt = {
            url: "https://api.mch.weixin.qq.com/pay/unifiedorder",
            body: buildXml(new SignUnifiedOrder(order, getSign(order, opt.key)), { headless: true, rootName: "xml" })
        }
        // 请求微信服务器生成订单
        let body = await postAsync(postOpt)
        // 解析结果
        let res = await parseXmlAsync(body)
        if (!res.xml || !res.xml.return_code) {
            console.log("准备订单失败1！", res)
            throw new Error("准备订单失败1！")
        }

        let { obj, sign } = splitWxResponse(res.xml)

        // 校验结果
        validateWxResponse(obj, sign, opt)

        // 返回准备好的订单
        return new PrePayUnifiedOrder(order, obj.prepay_id, obj.code_url)
    } catch (e) {
        log.error(`/短信充值: ${e.message}`)
        throw new Error(e.message)
    }
}

function validateWxResponse(xml: any, sign: string, opt: unifiedOrderOpt) {
    let return_code = xml.return_code
    if (return_code !== "SUCCESS") {
        throw new Error("准备订单失败2！")
    }

    if (!(opt.appid === xml.appid && opt.mch_id == xml.mch_id)) {
        throw new Error("准备订单失败4！")
    }

    let tmp = getSign(xml, opt.key)
    if (tmp !== sign)
        throw new Error("准备订单失败3！")
}

export async function validateNotify(s: string, opt: any) {
    let res = await parseXmlAsync(s)
    let { obj: xml, sign } = splitWxResponse(res.xml)
    let return_code = xml.return_code
    if (return_code !== "SUCCESS") {
        console.log("ERx", xml.return_code, xml.return_code !== "SUCCESS")
        throw new Error("validateNotify")
    }
    let tmp = getSign(xml, opt.key)
    if (tmp === sign)
        return xml
    throw new Error("validateNotify3")
}
