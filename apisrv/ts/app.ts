
import path = require('path')
import logger = require('morgan')
import express = require('express')
import cookieParser = require('cookie-parser')
import bodyParser = require('body-parser')

//mysql
import mysqlPool = require('./lib/mysqlpool')
import mysqlConfig = require('./config/mysql')
mysqlPool.initPool(mysqlConfig.poolOpt)

//redis
import redisPool = require('./lib/redispool')
import redisConfig = require('./config/redis')
redisPool.init(redisConfig.opt)


import { updCompAccessToken } from "./lib/compconfig"
import { updAccessToken } from './lib/wxconfig';
setTimeout(updCompAccessToken, 600000)
setTimeout(updAccessToken, 800000)
import { UpdSessionKey, getIncrementTrade } from "./lib/taobaoConfig"
UpdSessionKey()
getIncrementTrade()
//require("./lib/log")

//设置跨域访问
// let allowCrossDomain = function (req: any, res: any, next: any) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Content-Type,sessionid,appgetname,sess,user,lastpostTimesamp,X-File-Name,Origin");
//     res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
//     res.header("Access-Control-Allow-Credentials", 'true')
//     next();
// };
const app = express()
app.use(logger('dev'))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

//h5
app.use('/wechat/wxapp', require('./routes/h5/wxapp').router)
app.use('/wechat/rebate_mall', require('./routes/h5/rebate_mall').router)
app.use('/wechat/comment', require('./routes/h5/comment').router)
app.use('/wechat/order', require('./routes/h5/order').router)
app.use('/wechat/creditShop', require('./routes/h5/creditsShop').router)
//pc
app.use('/pc/platform', require('./routes/pc/platform').router)
app.use('/pc', require('./routes/pc/login').router)
app.use('/pc/regist', require('./routes/pc/regist').router)
app.use('/pc/check', require('./routes/pc/check').router)
app.use('/pc/account', require('./routes/pc/account').router)
app.use('/pc/position', require('./routes/pc/position').router)
app.use('/pc/personal_homepage', require('./routes/pc/personal_homepage').router)
app.use('/pc/sms', require('./routes/pc/sms').router)
app.use('/pc/userManage', require('./routes/pc/userManage').router)
app.use('/pc/wxapp', require('./routes/pc/wxapp').router)
app.use('/pc/order_info', require('./routes/pc/order_info').router)
app.use('/pc/order_rebate', require('./routes/pc/order_rebate').router)
app.use('/pc/comment_rebate', require('./routes/pc/comment_rebate').router)
app.use('/pc/comment_statistics', require('./routes/pc/comment_statistics').router)
app.use('/pc/order_statistics', require('./routes/pc/order_statistics').router)
app.use('/pc/message', require('./routes/pc/message').router)
app.use('/pc/rebate_mall', require('./routes/pc/rebate_mall').router)
app.use('/pc/mch_conf', require('./routes/pc/mch_conf').router)
app.use('/pc/staticts', require('./routes/pc/staticts').router)
app.use('/pc/creditsManage', require('./routes/pc/creditsManage').router)
app.use('/wechate/creditShop', require('./routes/h5/creditsShop').router)
app.use('/pc/taobaoAuth', require('./routes/pc/taobaoAuth').router)

// catch 404 and forward to error handler
app.use(function (req: express.Request, res: express.Response, next: express.NextFunction) {
    //  res.locals.user = req.session.user;
    // let e = req.session.error;
    res.locals.message = '';
    let err = new Error('Not Found')
    next(err)
})

// error handler
app.use(function (err: any, req: any, res: any, next: any) {
    res.locals.message = err.message
    console.log(err.message)
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    res.status(err.status || 500)
    res.end()
})

module.exports = app
//require('./routes/pc/server').serverstatus()

require('./daemon/daemon').run()
