import common = require("./common")
const { wxcode } = common
const { openid, nickname, headimgurl } = common
const { phone, code } = common
const { uuid, token } = common
const { username, password } = common
const { qrcode } = common
const { weburl, cmd, data } = common
const { page, count } = common

//点击微信菜单，
export const wxCode = {
    wxcode: wxcode
}

export const wxopenid = {
    openid: openid
}

//向微信端请求，获取的玩家信息
export const wxInfo = {
    openid: openid,
    nickname: nickname,
    headimgurl: headimgurl,
}

//手机号注册时，
export const checkPhone = {
    phone: phone
}

//手机号验证
export const checkSmsCode = {
    phone: phone,
    smscode: code
}

//redis_0数据库
export const UserSession = {
    uuid: uuid,
    token: token,
}

//账号绑定
export const checkAccount = {
    username: username,
    password: password,
}

//二维码
export const qrCode = {
    qrcode: qrcode
}

export const Uuid = {
    uuid: uuid,
}

export const webUrl = {
    weburl: weburl,
}

export const scanQrCode = {
    cmd: cmd,
    data: data,
    openid: openid,
    token: token,
}

const sort = {
    require: 0,
    isIn: {
        errmsg: "sort排序值有误",
        param: [["asc", "desc"]]
    }
}

const tag = {
    require: 0,
    isIn: {
        errmsg: "tag字段非法！",
        param: [["vrdevice", "vrpart", "tech_product", "digital_product", "commodity", "toy", "comic", "virtual_good"]]
    }
}

// 列表排序值
export const listSort = {
    sort: sort,
    page: page,
    count: count
}
export const listTag = {
    tag: tag,
    page: page,
    count: count
}