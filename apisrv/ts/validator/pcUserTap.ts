import { User_Id, tap_id, tap_name, start, length, searchphone, start_date, end_date, out_trade_no, search, name, UdJson, Gift_Name, Credits, Status, Gift_Cove, Gift_Type, Num, Create_Time, End_Time, Gift_Id, Record_Id, Rule, shop } from "./pc_common"
import { nickname, package_id, price, phone, redprice } from "./common"

export const userId = {
    userId: User_Id
}

export const Ujson = {
    userId: UdJson
}

export const updTap = {
    tap_id: tap_id,
    tap_name: tap_name,
}

export const addTap = {
    tap_name: tap_name,
}

export const tap_Id = {
    tap_id: tap_id
}

export const nickName = {
    nickname: nickname,
}

export const Phone = {
    phone: searchphone,
}

export const tapName = {
    tap_name: tap_name
}

export const limit = {
    start: start,
    length: length
}

export const order = {
    start: start,
    length: length,
    start_time: start_date,
    end_time: end_date,
}

export const good = {
    package_id: package_id,
    name: name,
    price: price
}

export const outTradeNo = {
    out_trade_no: out_trade_no
}

export const Search = {
    search: search
}



/***********积分管理 */
export const GiftName = {
    gift_name: Gift_Name
}

export const Credit = {
    credits: Credits
}
export const status = {
    status: Status
}

export const NewGift = {
    gift_name: Gift_Name,
    gift_cove: Gift_Cove,
    gift_type: Gift_Type,
    gift_num: Num,
    credits: Credits,
    price: redprice,
    create_time: Create_Time,
    end_time: End_Time
}

export const UpdGift = {
    gift_id: Gift_Id,
    gift_name: Gift_Name,
    gift_cove: Gift_Cove,
    gift_type: Gift_Type,
    gift_num: Num,
    credits: Credits,
    price: redprice,
    create_time: Create_Time,
    end_time: End_Time
}

export const delGift = {
    gift_id: UdJson
}

export const getGiftRecord = {
    gift_name: Gift_Name,
    nickname: nickname,
    status: Status,
    start: start,
    length: length
}

export const delGiftRecord = {
    id: Record_Id
}

export const savePic = {
    data: UdJson,
    credits: Credits
}

export const delPic = {
    data: UdJson
}

export const RuLe = {
    rule: Rule
}

export const SeachDate = {
    date: start_date
}
export const ShopName = {
    shop_name: shop
}
