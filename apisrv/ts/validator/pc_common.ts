export const start = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "start有误!",
        param: { min: 0 }
    }
}

export const length = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "length有误!",
        param: { min: 1 }
    }
}

export const search = {
    tansform: function (v: number) { return "" + v },
    isLength: {
        errmsg: "查询值长度为0到15个字符",
        param: { min: 0, max: 15 }
    }
}

export const tap_name = {
    isLength: {
        errmsg: "标签名字过长",
        param: { min: 0, max: 20 }
    }
}

export const package_fee = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "invalid price",
        param: { min: 0, max: 1000000 }
    }
}

export const tap_id = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "标签id有误!",
        param: { min: 0 }
    }
}

export const package_status = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "User_Id有误!",
        param: ['0', '1']
    }
}

export const package_minute = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "时间有误!",
        param: { min: 0 }
    }
}

export const package_toll = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "invalid toll",
        param: { min: 0, max: 1000000 }
    }
}

export const User_Id = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "User_Id有误!",
        param: { min: 0 }
    }
}

export const action_Id = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "action_Id有误!",
        param: { min: 0 }
    }
}

export const package_size = {
    isLength: {
        errmsg: "大小有误",
        param: { min: 0, max: 6 }
    }
}

export const start_date = {
    require: 0,
    isLength: {
        errmsg: "starttime有误",
        param: [0, 20]
    }
}

export const end_date = {
    require: 0,
    isLength: {
        errmsg: "endtime有误",
        param: [0, 20]
    }
}

export const city = {
    tansform: function (v: number) { return "" + v },
    isLength: {
        errmsg: "city有误",
        param: [0, 20]
    }
}

export const out_trade_no = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "out_trade_no有误!",
        param: { min: 0 }
    },
    isLength: {
        errmsg: "out_trade_no有误",
        param: [0, 18]
    }
}

export const spend_money = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "spend_money有误",
        param: { min: 0, max: 1000000 }
    }
}

export const Fee = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "invalid fee",
        param: { min: 0, max: 1000000 }
    }
}

export const box_no = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "box_no有误!",
        param: { min: 0 }
    },
    isLength: {
        errmsg: "box_no有误",
        param: [0, 11]
    }
}

export const take_time = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isLength: {
        errmsg: "take_time有误",
        param: [0, 20]
    }
}

export const account_name = {
    isLength: {
        errmsg: "请检查账号名长度！",
        param: [6, 32]
    },
}

export const real_name = {
    isLength: {
        errmsg: "请检查姓名长度！",
        param: [0, 10]
    }
}

export const company = {
    isLength: {
        errmsg: "公司名长度不能超过64个字符！",
        param: [0, 64]
    }
}

export const position = {
    isLength: {
        errmsg: "职位长度不能超过24个字符！",
        param: [0, 24]
    }
}

export const shop = {
    isLength: {
        errmsg: "店铺名长度不能超过64个字符！",
        param: [0, 64]
    }
}

export const phone = {
    isMobilePhone: {
        errmsg: "手机号格式错误！",
        param: ["zh-CN"]
    }
}

export const searchphone = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "手机号格式错误！",
        param: [0, 11]
    }
}

export const email = {
    isEmail: {
        errmsg: "邮箱格式错误！",
        param: 1
    }
}

export const remark = {
    isLength: {
        errmsg: "描述信息长度为0到64个字符",
        param: [0, 64]
    }
}

export const province = {
    isLength: {
        errmsg: "参数province长度大于20位",
        param: [0, 20]
    }
}

export const name = {
    isLength: {
        errmsg: "name错误",
        param: [0, 15]
    }
}

export const UdJson = {
    isJSON: {
        errmsg: "格式错误"
    }
}

export const event_name = {
    isLength: {
        errmsg: "活动名长度1-99",
        param: [1, 99]
    }
}

export const order_id = {
    isLength: {
        errmsg: "订单号错误",
        param: [18, 18]
    }
}

/****************积分商城参数 */
export const Gift_Id = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "Gift_Id有误!",
        param: { min: 1 }
    }
}

export const Credits = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "积分输入有误!",
        param: { min: 1, max: 1000000 }
    }
}

export const Num = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "数量输入有误!",
        param: { min: 1, max: 1000000 }
    }
}

export const Record_Id = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "Record_Id有误!",
        param: { min: 0 }
    }
}

export const Gift_Name = {
    isLength: {
        errmsg: "Gift_Name错误",
        param: [0, 32]
    }
}

export const Gift_Cove = {
    isLength: {
        errmsg: "Gift_Cove错误",
        param: [0, 255]
    }
}

export const Gift_Type = {
    isLength: {
        errmsg: "Gift_Type错误",
        param: [0, 4]
    }
}

export const Create_Time = {
    require: 0,
    isLength: {
        errmsg: "Create_Time有误",
        param: [0, 10]
    }
}

export const End_Time = {
    require: 0,
    isLength: {
        errmsg: "End_Time有误",
        param: [0, 10]
    }
}

export const Status = {
    require: 0,
    isLength: {
        errmsg: "Status有误",
        param: [0, 5]
    }
}

export const Rule = {
    require: 0,
    isLength: {
        errmsg: "Status有误",
        param: [0, 4]
    }
}

