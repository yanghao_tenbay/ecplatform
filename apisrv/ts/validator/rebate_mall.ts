
import common = require("./common")
const { phone } = common

export const num = {
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "参数大小错误",
        param: { min: 0, max: 10000000000 }
    }
}

export const str32 = {
    isLength: {
        errmsg: "参数长度错误",
        param: [0, 32]
    }
}

export const str64 = {
    isLength: {
        errmsg: "参数长度错误",
        param: [0, 64]
    }
}

export const str128 = {
    isLength: {
        errmsg: "参数长度错误",
        param: [0, 128]
    }
}

export const rebate = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "金额不符合",
        param: { min: 0.30, max: 20000.00 }
    }
}
export const json = {
    isJSON: {
        errmsg: "参数格式错误"
    }
}
export const uri = {
    isURL: {
        errmsg: "链接格式不正确"
    }
}
export const isAfter = {
    isAfter: {
        errmsg: "链接格式不正确",
        param: [, '']
    }
}

export const ISelectModel = {
    start: num,
    length: num,
}

export const IGoods = {
    goods_url: str128,
    shop_id: num,
}

export const IInsert = {
    activity_name: str128,       //活动名称
    goods_url: uri,               //淘宝链接， 可为空
    goods_id: str128,                 //商品id(1-99)
    goods_img: json,               //商品图片['', '', '']
    goods_price: str128,           //商品价格的区间'10-20'
    // short_link: str128,             //淘口令
    expenditure: num,         //返利金额
    after_event: json,           //返利规则{"event":"支付后","hour":72}
    start_time: str128,             //活动开始时间
    end_time: str128,                 //活动结束时间
    credits: num,         //活动赠送的积分， 默认0
    participation: json,       //参与活动条件，[{"type":"","status":1,"data":500}]
    total: str128,             //活动次数
    // flow: str128,                         //活动流程
    // notes: str128,                       //注意事项
}

export const IUpdete = {
    id: num,
    activity_name: str128,       //活动名称
    goods_url: uri,               //淘宝链接， 可为空
    goods_id: str128,                 //商品id(1-99)
    goods_img: json,               //商品图片['', '', '']
    goods_price: str128,           //商品价格的区间'10-20'
    // short_link: str128,             //淘口令
    expenditure: num,         //返利金额
    after_event: json,           //返利规则{"event":"支付后","hour":72}
    start_time: str128,             //活动开始时间
    end_time: str128,                 //活动结束时间
    credits: num,         //活动赠送的积分， 默认0
    participation: json,       //参与活动条件，[{"type":"","status":1,"data":500}]
    total: str128,             //活动次数
}

export const IUpdete2 = {
    id: num,
    end_time: isAfter,
    total: str128,             //活动次数
}

export const IDelete = {
    id: num,
}

export const IBatch = {
    ids: json,
}

export const ISetCarousel = {
    time: num,
    pic: json
}