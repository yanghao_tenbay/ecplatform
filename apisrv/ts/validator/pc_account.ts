/**********************************************
* 参数校验
* Author：鲁欣欣
* Date：2018-09-08
*/
import { search, account_name, real_name, company, position, shop, phone, province, city, email, remark, event_name, order_id } from "./pc_common";
import { password, code, page, count } from "./common"

export const reqcord = {
    page: page,
    count: count,
}

export const query_value = {
    search: search
}

export const realname_value = {
    real_name: real_name
}

export const accountname_value = {
    account_name: account_name
}

export const passwd = {
    password: password
}

export const phone_value = {
    phone: phone
}

export const email_value = {
    email: email
}

export const company_value = {
    company: company
}

export const position_value = {
    position: position
}

export const remark_value = {
    remark: remark
}

export const shop_value = {
    shop: shop
}

export const phone_code = {
    code: code
}

export const Address = {
    province: province,
    city: city,
}

export const event_value = {
    event_name: event_name
}

export const order_id_value = {
    order_id: order_id
}