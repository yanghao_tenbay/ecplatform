/**
 * 基于validator封装的参数验证库。url: https://www.npmjs.com/package/validator. e.g.
 * export const age = {
 *    require: 0,     // 可选，如果不是必要的，require:0，否则不用写
 *   tansform: function (v: number) { return "" + v },   // 可选，validator是基于字符串的验证，如果是其他类型的参数，要先转换成string
 *    isInt: {        // validator中的函数
 *        errmsg: "年龄非法！",    // 校检出错的返回值
 *        param: { min: 1 }       // isInt的参数，有三种形式 param:1表示isInt(str)；param:[a,b,c]表示isInt(str, a, b, c); param:{min:1}表示isInt(str, {min:1})
 *    }
 * }
 * author: yjs
 * time: 20161224
 */


export const phone = {
    isMobilePhone: {
        errmsg: "手机号格式错误！",
        param: ["zh-CN"]
    }
}
export const token = {
    isLength: {
        errmsg: "token null",
        param: [0, 36]
    }
}
export const password = {
    isLength: {
        errmsg: "密码长度错误！",
        param: [32, 32]
    },
    isAlphanumeric: {
        errmsg: "密码只能包含字母和数字！"
    }
}

export const username = {
    isLength: {
        errmsg: "用户名长度错误！",
        param: [0, 64]
    },
    isLowercase: {
        errmsg: "用户名包含大写字母！",
        param: 1
    }
}

export const code = {
    require: 0,
    isLength: {
        errmsg: "验证码错误",
        param: [6, 6]
    }
}

export const uuid = {
    require: 0,
    isLength: {
        errmsg: "uuid有误",
        param: [36, 36]
    }
}

export const nickname = {
    isLength: {
        errmsg: "nickname有误",
        param: [0, 200]
    }
}

export const page = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "page有误!",
        param: { min: 0 }
    }
}

export const count = {
    require: 0,
    tansform: function (v: number) { return "" + v },
    isInt: {
        errmsg: "count有误!",
        param: { min: 0 }
    }
}

export const wxcode = {
    isLength: {
        errmsg: "请求参数code错误",
        param: [0, 100]
    }
}

export const openid = {
    isLength: {
        errmsg: "openid错误",
        param: [0, 100]
    }
}

export const headimgurl = {
    isLength: {
        errmsg: "nickname错误",
        param: [0, 200]
    }
}

export const qrcode = {
    isLength: {
        errmsg: "二维码内容错误",
        param: [32, 32]
    }
}

export const weburl = {
    isLength: {
        errmsg: "weburl参数错误",
        param: [0, 1024],
    }
}

export const cmd = {
    isLength: {
        errmsg: "cmd参数错误",
        param: [0, 256],
    }
}

export const data = {
    isJSON: {
        errmsg: "data格式有误",
        param: [0, 500]
    }
}


/********************微信充值*******************/
export const user_id = {
    isLength: {
        errmsg: "userid有误！",
        param: [0, 36]
    },
    isInt: {
        errmsg: "invalid package_id",
        param: 1
    }
}

export const package_id = {
    tansform: function (v: number) { return "" + v },
    isLength: {
        errmsg: "invalid package_id",
        param: [0, 11]
    },
    isInt: {
        errmsg: "invalid package_id",
        param: 1
    }
}

export const price = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "invalid price",
        param: [0, 100000000]
    }
}

export const redprice = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "invalid price",
        param: [1, 200]
    }
}

export const timestamp = {
    tansform: function (v: number) { return "" + v },
    isLength: {
        errmsg: "invalid timestamp",
        param: [10, 10]
    },
    isInt: {
        errmsg: "invalid timestamp",
        param: 1
    }
}

export const str = {
    require: 0,
    isLength: {
        errmsg: "参数错误",
        param: [1, 100]
    }
}

export const num = {
    tansform: function (v: number) { return "" + v },
    isFloat: {
        errmsg: "参数错误",
        param: [0, 100000000]
    }
}

export const passwd = {
    isLength: {
        errmsg: "密码长度错误！",
        param: [6, 6]
    },
    isAlphanumeric: {
        errmsg: "密码只能包含字母和数字！"
    }
}