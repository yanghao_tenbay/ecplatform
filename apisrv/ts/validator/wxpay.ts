import common = require("./common")
const { user_id, package_id, price, timestamp } = common

export const postGood = {
    user_id: user_id,
    package_id: package_id,
    price: price,
    timestamp: timestamp,
}