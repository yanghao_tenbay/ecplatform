import common = require("./common")
const { str, num, package_id, code, phone, passwd, page, count } = common
import { start, length, Gift_Id, Num, Credits, Record_Id, UdJson } from "./pc_common"
export const user_id_value = {
    user_id: num
}

export const selectPack = {
    dev_id: str
}

export const saveNotPay = {
    user_id: num,
    dev_id: str,
    package_id: package_id
}

export const getRecord = {
    user_id: num,
    prepay_id: str
}

export const judgeBoxOpen = {
    seq: str
}

export const setRecordPasswd = {
    phone: phone,
    code: code,
    passwd: passwd,
    out_trade_no: str
}

export const setUserPhone = {
    user_id: num,
    phone: phone
}

export const userBox = {
    user_id: num,
    dev_id: str
}

export const openBox = {
    user_id: num,
    out_trade_no: str
}

export const getTakeSeq = {
    out_trade_no: str
}

export const sendCode = {
    phone: phone
}

export const checkCode = {
    user_id: num,
    phone: phone,
    code: code
}

export const userSaveRecord = {
    page: page,
    count: count
}

/************�����̳� ***********/
export const limit = {
    start: start,
    length: length
}

export const AddCart = {
    gift_id: Gift_Id,
    num: Num,
}

export const AddCartNum = {
    gift_id: Gift_Id,
    credits: Credits
}

export const RedCartNum = {
    id: Record_Id,
    credits: Credits
}

export const DelCart = {
    id: Record_Id
}

export const Exchange = {
    gift_id: UdJson,
    num: UdJson,
    credits: UdJson,
}

export const ID = {
    id: UdJson
}
