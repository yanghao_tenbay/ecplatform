/**********************************************
* 微信晒评价
* Author：鲁欣欣
* Date：2018-10-25
*/
import express = require('express')
import { validateCgi } from '../../lib/validator'
import db = require('../../dao/h5/order')
import { sendOk, sendError, sendOkTable, randomInt, replaceStr2, formatDate2 } from '../../lib/utils'
import { CheckUserLogin, getWxInfo, user2Redis } from '../../dao/h5/wxAuth'
import { log_pc as pc } from '../../lib/log'
import acValidator = require('../../validator/pc_account')
export const router: express.Router = express.Router()

/**********************************************
 * 获取用户信息
 * Author：鲁欣欣
 * Date：2018-10-30
 */
router.get('/userInfo/:company_id/:event_id', getWxInfo, user2Redis, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { company_id, event_id } = (req as any).params
    try {
        if (req.query['phone'] == '') {
            return res.redirect(`/H5/index.html#/bindTel?type=order`)
        } else {
            return res.redirect(`/H5/index.html#/orderrebate?company_id=${company_id}&event_id=${event_id}`)
        }
    } catch (e) {
        pc.error(`userInfo-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
 * 获取活动页面信息
 * Author：鲁欣欣
 * Date：2018-10-30
 */
router.post('/get_event', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { company_id, event_id } = (req as any).body
    try {
        let result = await db.getEvent(company_id, event_id)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
 * 提交订单信息
 * Author：鲁欣欣
 * Date：2018-10-25
 */
router.post('/submit', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, phone, user_id, openid } = (req as any).UserSession
    const { event_id, order_id } = (req as any).body

    try {
        validateCgi({ order_id: order_id }, acValidator.order_id_value)
        let status = 1
        let reason = ''
        let orderarr: any[] = []
        /**审核，查活动信息 */
        let event = await db.getEventInfo(database_name, event_id)
        let commodity: string[] = event.commodity.split(",")
        /**新用户限制 */
        let userinfo = await db.getUserInfo(database_name, user_id)
        if (event.newuser === 1) {
            let usernum = await db.getUserRecordNum(database_name, user_id)
            if (usernum > 0) {
                return sendError(res, `该活动为新用户专享，您不是新用户！`)
            }
        }
        /**积分限制 */
        else if (userinfo.credits < event.integral_num) {
            return sendError(res, `用户积分需达到${event.integral_num}才能参与活动`)
        }
        /**一个号参与次数限制 */
        let res2 = await db.getEventRecordInfo(database_name, user_id, event_id)
        if (res2 > event.num) {
            return sendError(res, `一个用户只能参与${event.num}次该活动`)
        }
        /**获取订单信息 */
        let order = await db.getOrderInfo(database_name, order_id)

        if (order.length === 0) {
            status = 2
            reason = '未找到订单信息'
        }
        else if (order[0].receiver_mobile !== phone) {
            return sendError(res, '绑定手机号与订单手机号不一致')
        }
        else if (order[0].activity_status === 'comment_rebate') {
            return sendError(res, `该订单已参与过晒评价活动`)
        }
        else if (order[0].activity_status === 'rebate_mall') {
            return sendError(res, `该订单已参与过返利商城活动`)
        }
        else if (order[0].activity_status === 'order_rebate') {
            return sendError(res, `该订单已参与过订单返利活动`)
        }
        else {
            status = 2
            reason = '商品id不符合活动要求'
            commodity.forEach(function (v: string, i: any, a: any) {
                for (let i = 0; i < order.length; i++) {
                    if (order[i].num_iid == v) {
                        status = 1
                        reason = ''
                        orderarr.push(order[i])
                    }
                }
            })
            for (let b = orderarr.length - 1; b > 0; b--) {
                if (orderarr[b].price > orderarr[b - 1].price) {
                    let temp = orderarr[b]
                    orderarr[b] = orderarr[b - 1]
                    orderarr[b - 1] = temp
                }
            }
        }

        if (status === 1) {
            if (event.rebate_status === '确认收货') {
                if (order[0].status !== 'TRADE_BUYER_SIGNED' &&    //(买家已签收, 货到付款专用)
                    order[0].status !== 'TRADE_FINISHED')//(交易成功)
                {
                    status = 2
                    reason = '订单状态不符合活动要求:确认收货'
                }
            }
            if (event.rebate_status === '已支付') {
                if (order[0].status !== 'TRADE_BUYER_SIGNED' &&    //(买家已签收, 货到付款专用)
                    order[0].status !== 'TRADE_FINISHED' &&         //(交易成功)
                    order[0].status !== 'SELLER_CONSIGNED_PART' &&   //(卖家部分发货)
                    order[0].status !== 'WAIT_SELLER_SEND_GOODS' &&   //(等待卖家发货, 即: 买家已付款)
                    order[0].status !== 'WAIT_BUYER_CONFIRM_GOODS'//(等待买家确认收货, 即: 卖家已发货)
                ) {
                    status = 2
                    reason = '订单状态不符合活动要求:已支付'
                }
            }
        }
        /**返利金额确定*/
        let amount
        if (event.amount === 0) {
            amount = orderarr[0].price * event.percentage / 100
            if (amount < 0.3) {
                amount = 0.3
            }
            if (amount > 20000) {
                amount = 20000
            }
        }
        else {
            amount = event.amount
        }
        let settime
        if (status === 1) {
            /**设定准备返利时间 */
            let date = new Date()
            date.setDate(date.getDate() + event.days)
            settime = formatDate2(date)
            /**活动已参与次数加一 */
            await db.addEventNum(database_name, event_id)
            //更改交易记录状态，从自动审核通过到返利中
            status = 4
        }
        /**更改订单状态，参与过订单返利活动 */
        if (order.length !== 0) {
            await db.updateOrderStatus(database_name, order_id)
        }
        let result = await db.submitOrder(database_name, event_id, orderarr[0].oid, user_id, amount, event.integral, status, reason, settime)
        if (status === 4) {
            /**插入到tranfer表 */
            await db.addTranfer(database_name, event.event, result.insertId, openid, amount, event.integral, settime)
        }
        if (result.affectedRows === 1) {
            return sendOk(res, '提交成功！')
        }
    } catch (e) {
        pc.error(`order_submit-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
 * 我的订单返利
 * Author：鲁欣欣
 * Date：2018-11-8
 */
router.post('/myrebate', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, user_id } = (req as any).UserSession
    const { start, length } = (req as any).body
    try {
        let result = await db.getMyRecord(database_name, user_id, start, length)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        pc.error(`order_myrebate-> ${e.message}`)
        return sendError(res, e.message)
    }
})