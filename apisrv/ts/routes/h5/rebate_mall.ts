import express = require('express')

import { validateCgi } from '../../lib/validator'
import { log_pc as log } from '../../lib/log'
import { sendOk, sendError, sendOkTable } from '../../lib/utils'
import { getWxInfo, user2Redis, CheckUserLogin } from '../../dao/h5/wxAuth'
import db = require('../../dao/h5/rebate_mall')
import { UserSession } from '../../entity/wxUsersession'
export const router: express.Router = express.Router()

/**********************************************
* 分享活动链接， 返回返利商城分享页面
* Author：mjq
* Date：2018-11-08
*/
router.get('/share_entrance/:activity_id', getWxInfo, user2Redis, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { activity_id } = (req as any).params
    const { database_name } = req.query
    try {
        return res.redirect(`/H5/index.html#/detail?id=${activity_id}`)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 分享活动链接， 请求单个商品信息
* Author：mjq
* Date：2018-11-08
*/
router.post('/single_details', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { id } = (req as any).body
    try {
        let database_name = session.getDatabaseName()
        let r = await db.selectRebateMallDetails(database_name, id)
        return sendOk(res, r)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 获取返利商城动图配置
* Author：mjq
* Date：2018-10-31
*/
router.post('/get_carousel', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        const database_name = session.getDatabaseName()
        let r = await db.getRebateMallCarousel(database_name)
        return sendOk(res, r)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 查询返利商城全部活动
* Author：mjq
* Date：2018-10-27
*/
router.post('/select', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length } = req.body
    try {
        const database_name = session.getDatabaseName()
        let r = await db.selectH5RebateMall(database_name, {
            start: start,
            length: length
        })
        return sendOkTable(res, r.recordsFiltered, r.recordsTotal, r.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 查询个人订单
* Author：mjq
* Date：2018-10-27
*/
router.post('/record', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length, status } = req.body
    try {
        const database_name = session.getDatabaseName()
        let r = await db.selectSelfOrder(database_name, {
            start: start,
            length: length,
            user_id: parseInt(session.getUser_id()),
            status: status,
        })
        return sendOkTable(res, r.recordsFiltered, r.recordsTotal, r.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 抢活动资格
* Author：mjq
* Date：2018-10-27
*/
router.post('/rob', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { id } = req.body
    try {
        const database_name = session.getDatabaseName()
        let user_id = parseInt(session.getUser_id())
        await db.robQualification(database_name, user_id, id)
        return sendOk(res, 'ok')
    } catch (e) {
        return sendError(res, e.message)
    }
})