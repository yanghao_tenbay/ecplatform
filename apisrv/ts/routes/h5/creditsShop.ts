import express = require('express')
import { sendOk, sendError, sendOkTable } from '../../lib/utils';
export const router: express.Router = express.Router()
import db = require('../../dao/h5/wxAuth')
import { UserSession } from '../../entity/wxUsersession';
import { validateCgi } from '../../lib/validator';
import { log_h5 as log } from '../../lib/log'
import { getGift, addCart, selectCart, addGiftNum, redGiftNum, cashGift, getCash, getCredits, delGiftRecord, getPict, getCredit } from '../../dao/h5/creditsShop';
import wxValidator = require("../../validator/wwxinfo")

/**********************************************
* 跳转积分商城
* Author：hym
* Date：2018-10-19
*/
router.get('/creditsShop', db.getWxInfo, db.user2Redis, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        return res.redirect(`/H5/index.html#/`)
    } catch (e) {
        log.error(`/creditsShop: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 获取用户积分
* Author：hym
* Date：2018-10-19
*/
router.post('/getUserCredits', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        let result = await getCredits(session)
        sendOk(res, result)
    } catch (e) {
        log.error(`/getUserCredits: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 获取商城配置
* Author：hym
* Date：2018-10-19
*/
router.post('/getShopPic', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        let result = await getPict(session.getDatabaseName())
        sendOk(res, result)
    } catch (e) {
        log.error(`/getShopPic: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 获取礼品
* Author：hym
* Date：2018-10-19
*/
router.post('/getGift', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length } = (req as any).body
    try {
        validateCgi({ start, length }, wxValidator.limit)
        let result = await getGift(session.getDatabaseName(), start, length)
        sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        log.error(`/getGift: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 加入购物车
* Author：hym
* Date：2018-10-19
*/
router.post('/addCart', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { gift_id, num } = (req as any).body
    try {
        validateCgi({ gift_id: gift_id, num: num }, wxValidator.AddCart)
        let result = await addCart(session, gift_id, num)
        sendOk(res, result)
    } catch (e) {
        log.error(`/addCart: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 展示购物车
* Author：hym
* Date：2018-10-19
*/
router.post('/showCart', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length } = (req as any).body
    try {
        validateCgi({ start, length }, wxValidator.limit)
        let result = await selectCart(session, start, length)
        sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        log.error(`/showCart: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 购物车礼品数量加一
* Author：hym
* Date：2018-10-19
*/
router.post('/addCartNum', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { gift_id, credits } = (req as any).body
    try {
        validateCgi({ gift_id: gift_id, credits: credits }, wxValidator.AddCartNum)
        let result = await addGiftNum(session, gift_id, 1, credits)
        sendOk(res, result)
    } catch (e) {
        log.error(`/addCartNum: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 购物车礼品数量减一
* Author：hym
* Date：2018-10-19
*/
router.post('/redCartNum', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { id, credits } = (req as any).body
    try {
        validateCgi({ id: id, credits: credits }, wxValidator.RedCartNum)
        let result = await redGiftNum(session.getDatabaseName(), id, credits)
        sendOk(res, result)
    } catch (e) {
        log.error(`/redCartNum: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 购物车记录删除
* Author：hym
* Date：2018-10-19
*/
router.post('/delCart', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { id } = (req as any).body
    try {
        validateCgi({ id: id }, wxValidator.DelCart)
        let result = await delGiftRecord(session.getDatabaseName(), id)
        sendOk(res, result)
    } catch (e) {
        log.error(`/delCart: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 购物车多选/全选兑换
* Author：hym
* Date：2018-10-19
*/
router.post('/exchange', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { gift_id, num, credits, id } = (req as any).body
    console.log(id)
    try {
        id == '' ? '' : validateCgi({ id }, wxValidator.ID)
        validateCgi({ gift_id: gift_id, num: num, credits: credits }, wxValidator.Exchange)
        let result = await cashGift(session, gift_id, num, credits, id)
        sendOk(res, result)
    } catch (e) {
        log.error(`/exchange: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 展示我的兑换
* Author：hym
* Date：2018-10-19
*/
router.post('/getCash', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length } = (req as any).body
    try {
        validateCgi({ start, length }, wxValidator.limit)
        let result = await getCash(session, start, length)
        sendOk(res, result)
    } catch (e) {
        log.error(`/getCash: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
* 展示我的积分
* Author：hym
* Date：2018-10-19
*/
router.post('/getCrdit', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { start, length } = (req as any).body
    try {
        validateCgi({ start, length }, wxValidator.limit)
        let result = await getCredit(session, start, length)
        sendOk(res, result)
    } catch (e) {
        log.error(`/getCash: ${e.message}`)
        sendError(res, e.message)
    }
})

