import express = require('express')
import { sendOk, sendError } from '../../lib/utils';
export const router: express.Router = express.Router()
import db = require('../../dao/h5/wxAuth')
import { UserSession } from '../../entity/wxUsersession';
import { PccheckCode, PcSendCode } from '../../lib/sms';
import { validateCgi } from '../../lib/validator';
import wxValidator = require("../../validator/wwxapp")
import { log_h5 as log } from '../../lib/log'

/**********************************************
 * H5页面
 * Author：何语漫
 * Date：2018-9-30
 */
router.get('/userInfo', db.getWxInfo, db.user2Redis, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        if (req.query['phone'] == '') {
            return res.redirect(`/h5/index.html#/`)
        } else {
            return res.redirect(`/h5/index.html#/bindTel?type=bind`)
        }
    } catch (e) {
        log.error(`/userInfo: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 我的订单
 * Author：何语漫
 * Date：2018-9-30
 */
router.post('/myTrade', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        sendOk(res, (await db.getMyTrade(session)))
    } catch (e) {
        log.error(`/myTrade: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 发送验证码
 * Author：何语漫
 * Date：2018-9-30
 */
router.post('/sendCheckCode', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { phone } = (req as any).body
    try {
        validateCgi({ phone }, wxValidator.checkPhone)
        let result = await db.cheakSms(session)
        if (result === 1) {
            return sendError(res, '该公司短信余额不足')
        }
        await PcSendCode(phone)
        await db.reduceSms(session)
        sendOk(res, '发送成功')
    } catch (e) {
        log.error(`/sendCheckCode: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 保存手机号
 * Author：何语漫
 * Date：2018-9-30
 */
router.post('/savePhone', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    const { phone, code } = (req as any).body
    try {
        validateCgi({ phone: phone, smscode: code }, wxValidator.checkSmsCode)
        await PccheckCode(phone, code)
        await db.saveUserPhone(session, phone)
        sendOk(res, '绑定成功')
    } catch (e) {
        log.error(`/savePhone: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 用户信息
 * Author：何语漫
 * Date：2018-9-30
 */
router.post('/userInfoDetail', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        let result = await db.getUserInfo(session)
        sendOk(res, result)
    } catch (e) {
        log.error(`/userInfoDetail: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 签到
 * Author：何语漫
 * Date：2018-10-28
 */
router.post('/getAttendence', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        let result = await db.getAttendence(session)
        sendOk(res, result)
    } catch (e) {
        log.error(`/getAttendence: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 签到
 * Author：何语漫
 * Date：2018-10-28
 */
router.post('/setAttendence', db.CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const session = (req as any).UserSession as UserSession
    try {
        let result = await db.setAttendece(session)
        sendOk(res, result)
    } catch (e) {
        log.error(`/setAttendence: ${e.message}`)
        sendError(res, e.message)
    }
})

router.post('/notice/:id', async function (req: express.Request, res: express.Response, next: express.NextFunction) { })