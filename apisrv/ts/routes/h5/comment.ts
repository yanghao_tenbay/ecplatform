/**********************************************
* 微信晒评价
* Author：鲁欣欣
* Date：2018-10-25
*/
import express = require('express')
import db = require('../../dao/h5/comment')
import { sendOk, sendError, randomInt, formatDate2, strlen, sendOkTable } from '../../lib/utils'
import { CheckUserLogin, getWxInfo, user2Redis } from '../../dao/h5/wxAuth'
import { UserSession } from '../../entity/wxUsersession'
import { notify } from '../../daemon/daemon'
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
import { getTraderate } from '../../lib/taobaoConfig';
export const router: express.Router = express.Router()
import { log_pc as pc } from '../../lib/log'
let fs = require('fs')
let multiparty = require('multiparty')


/**********************************************
 * 获取用户信息
 * Author：鲁欣欣
 * Date：2018-10-30
 */
router.get('/userInfo/:company_id/:event_id', getWxInfo, user2Redis, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { company_id, event_id } = (req as any).params
    try {
        console.log('~~~~~')
        console.log(company_id, event_id)
        if (req.query['phone'] == '') {
            return res.redirect(`/H5/index.html#/bindTel?type=comment`)
        } else {
            return res.redirect(`/H5/index.html#/evaluation?company_id=${company_id}&event_id=${event_id}`)
        }
    } catch (e) {
        pc.error(`userInfo-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
 * 获取活动页面信息
 * Author：鲁欣欣
 * Date：2018-10-25
 */
router.post('/get_event', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { company_id, event_id } = (req as any).body
    try {
        let result = await db.getEvent(company_id, event_id)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})
/**********************************************
 * 上传晒评价图片
 * Author：鲁欣欣
 * Date：2018-10-25
 */
router.post('/image_upload', CheckUserLogin, function (req: express.Request, res: express.Response) {

    let form = new multiparty.Form()//新建表单
    //设置编辑
    form.encoding = 'utf-8'
    //设置图片存储路径
    form.uploadDir = "/var/data/uploads/images/"
    form.keepExtensions = true   //保留后缀
    form.maxFieldsSize = 2 * 1024 * 1024  //内存大小
    form.maxFilesSize = 5 * 1024 * 1024  //文件字节大小限制，超出会报错err

    //表单解析
    form.parse(req, function (err: any, fields: any, files: any) {
        //报错处理
        if (err) {
            console.log(err)
            let u = { status: 1, msg: '图片过大' }
            return res.end(JSON.stringify(u))
        }
        //获取路径
        let oldpath = files.file[0]['path']
        let suffix
        //文件后缀处理格式
        if (oldpath.indexOf('.jpg') >= 0) {
            suffix = '.jpg'
        } else if (oldpath.indexOf('.png') >= 0) {
            suffix = '.png'
        } else if (oldpath.indexOf('.jpeg') >= 0) {
            suffix = '.jpeg'
        } else {
            let u = { status: 1, msg: '请上传正确格式:jpg\\png\\jpeg' }
            return res.end(JSON.stringify(u))
        }
        let newpath = form.uploadDir + Date.now() + randomInt(10000, 99999) + suffix
        //给图片修改名称
        fs.renameSync(oldpath, newpath)
        let url = `${newpath.substring(9)}`
        //getResult(res, url, newpath)
        let u = { status: 0, data: url }
        return res.send(JSON.stringify(u))
    })
})

/**********************************************
 * 提交图片和订单信息
 * Author：鲁欣欣
 * Date：2018-10-25
 */
router.post('/submit', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, phone, user_id, openid } = (req as any).UserSession
    const { event_id, order_id, image_url } = (req as any).body
    try {
        validateCgi({ order_id: order_id }, acValidator.order_id_value)
        let status = 1
        let reason = ''
        /**审核，查活动信息 */
        let event = await db.getEventInfo(database_name, event_id)
        console.log('对应活动信息：' + JSON.stringify(event))
        let date = new Date(event.create_time)
        date.setMonth(date.getMonth() - 1)
        let datestr = formatDate2(date)

        let shop: string[] = event.shop.split(",")
        let commodity: string[] = event.commodity.split(",")
        console.log(shop, commodity)
        let res2 = await db.getEventRecordInfo(database_name, user_id, event_id)
        console.log(res2)
        if (res2 > 0) {
            return sendError(res, '一个用户只能参与一次该活动')
        }
        let amount = randomInt(event.s_amount, event.e_amount) //返利金额确定
        console.log(amount)

        /**订单信息 */
        let order = await db.getOrderInfo(database_name, order_id)
        console.log('对应订单信息：' + JSON.stringify(order))

        if (order.length === 0) {
            status = 2
            reason = '未找到订单信息'
        }
        else if (order[0].activity_status === 'comment_rebate') {
            return sendError(res, `该订单已参与过晒评价活动`)
        }
        else if (order[0].activity_status === 'rebate_mall') {
            return sendError(res, `该订单已参与过返利商城活动`)
        }
        else if (order[0].activity_status === 'order_rebate') {
            return sendError(res, `该订单已参与过订单返利活动`)
        }
        else if (phone !== order[0].receiver_mobile) {
            return sendError(res, '绑定的手机号与订单手机号不一致')
        }
        else if (order[0].created_time < datestr || order[0].created_time > event.e_time) {
            status = 2
            reason = '下单时间不符合活动要求'
        }
        else {
            shop.some(function (v: string, i: any, a: any) {
                if (order[0].seller_nick == v) {
                    status = 2
                    reason = '下单店铺不符合活动要求'
                    return order[0].seller_nick == v
                }
            })
            if (status == 1) {
                commodity.some(function (v: string, i: any, a: any) {
                    for (let i = 0; i < order.length; i++) {
                        if (order[i].num_iid == v) {
                            status = 2
                            reason = '商品id不符合活动要求'
                            return order[i].num_iid == v
                        }
                    }
                })
            }
            if (status == 1) {
                /**获取评价信息并审核 */
                let content
                let result
                if (order[0].buyer_rate === '1') {
                    content = order[0].comment
                    result = order[0].result
                }
                if (order[0].buyer_rate === '0') {
                    let shop_id = await db.getShop_id(database_name, order_id)
                    let comment = await getTraderate(order_id, shop_id)
                    if (comment.content === '' && comment.result === '' && comment.time === '') {
                        status = 2
                        reason = '该用户没有评价信息'
                    }
                    else {
                        content = comment.content
                        result = comment.result
                        await db.writeComment(database_name, order_id, comment)
                    }
                }
                if (status === 1) {
                    if (result !== 'good') {
                        status = 2
                        reason = '评价不是好评'
                    }
                    else if (strlen(content) < 20) {
                        status = 2
                        reason = '评论长度小于20个字符(汉字2字符,字母符号1字符)'
                    }
                    else {
                        let keywordstr = ['刷单', '垃圾', '不好', '没办法', '不值', '不匹配', '严重', '此用户没有填写评价', '不灵', '不行', '不好看', '不能用', '差评', '坏了', '坏的', '不太好', '不舒适']
                        for (let i = 0; i < keywordstr.length; i++) {
                            if (content.indexOf(keywordstr[i]) !== -1) {
                                status = 2
                                reason = '评价信息包含敏感词，判定为非好评'
                                break
                            }
                        }
                    }
                }

            }
        }
        console.log(reason)
        if (status === 1) {
            /**活动已参与次数加一 */
            await db.addEventNum(database_name, event_id)
            //更改交易记录状态，从自动审核通过到返利中
            status = 4
        }
        /**更改订单状态，参与过晒评价活动 */
        if (order.length !== 0) {
            await db.updateOrderStatus(database_name, order_id)
        }
        let result = await db.submitComment(database_name, event_id, order[0].oid, user_id, image_url, amount, status, reason)
        if (status === 4) {
            /**插入到tranfer表 */
            await db.addTranfer(database_name, event.event, result.insertId, openid, amount)
        }
        if (result.affectedRows === 1) {
            return sendOk(res, '提交成功！')
        }
    } catch (e) {
        pc.error(`comment_submit-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
 * 我的晒评价返利
 * Author：鲁欣欣
 * Date：2018-11-16
 */
router.post('/myrebate', CheckUserLogin, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, user_id } = (req as any).UserSession
    const { start, length } = (req as any).body
    try {
        let result = await db.getMyRecord(database_name, user_id, start, length)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        pc.error(`comment_myrebate-> ${e.message}`)
        return sendError(res, e.message)
    }
})