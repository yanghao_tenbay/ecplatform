/**********************************************
* 公司账号在总平台上注册
* Author：鲁欣欣
* Date：2018-09-21
*/
import express = require('express')
import { sendOk, sendError, md5sum } from '../../lib/utils';
export const router: express.Router = express.Router()
import db = require('../../dao/pc/regist')
import { validateCgi } from '../../lib/validator'
import acValidator = require("../../validator/pc_account")
import { PccheckCode } from '../../lib/sms'

/**********************************************
* 公司注册
* Author：鲁欣欣
* Date：2018-09-21
*/
router.post('/get_code', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone } = (req as any).body
    validateCgi({ phone: phone }, acValidator.phone_value)
    try {
        let res1 = await db.getCode(phone)
        if (res1 === 0) {
            return sendOk(res, '获取成功')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 点击下一步，验证验证码
* Author：鲁欣欣
* Date：2018-09-21
*/
router.post('/check_code', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone, code } = (req as any).body

    console.log(phone, code)
    try {
        validateCgi({ phone: phone }, acValidator.phone_value)
        validateCgi({ code: code }, acValidator.phone_code)
        await PccheckCode(phone, code)
        return sendOk(res, '验证成功')
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 获取所有一级行政区
* Author：鲁欣欣
* Date：2018-09-30
*/
router.post('/get_province', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        let result = await db.getProvince()
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 获取所有一级行政区对应的二级行政区
* Author：鲁欣欣
* Date：2018-09-21
*/
router.post('/get_city', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { province_id } = (req as any).body
    try {
        let result = await db.getCity(province_id)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 填写公司信息，点击下一步
* Author：鲁欣欣
* Date：2018-09-21
*/
router.post('/write_company', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone, company_name, shop, province, city, real_name, email } = (req as any).body
    try {
        validateCgi({ company: company_name }, acValidator.company_value)
        validateCgi({ shop: shop }, acValidator.shop_value)
        validateCgi({ province: province, city: city }, acValidator.Address)
        validateCgi({ email: email }, acValidator.email_value)
        let result = await db.writeCompany(phone, company_name, shop, province, city, real_name, email)
        if (result == 1) {
            return sendOk(res, '提交成功')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})