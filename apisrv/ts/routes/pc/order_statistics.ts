/**********************************************
* 订单返利数据统计
* Author：鲁欣欣
* Date：2018-11-2
*/

import express = require('express')
import { sendOk, sendError, sendOkStat } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/order_statistics')
export const router: express.Router = express.Router()
import { log_pc as pc } from '../../lib/log'

/**********************************************
* 匹配活动名称
* Author：鲁欣欣
* Date：2018-11-2
*/
router.post('/match_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event } = (req as any).body

    try {
        if (operate_power.rebateStat !== 1) {
            return sendError(res, 'logintimeout')
        }

        let result = await db.matchEvent(database_name, event)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 按天统计
* Author：鲁欣欣
* Date：2018-11-2
*/
router.post('/day_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event } = (req as any).body

    try {
        if (operate_power.rebateStat !== 1) {
            return sendError(res, 'logintimeout')
        }

        let result = await db.dayStatistics(database_name, event)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 按月统计
* Author：鲁欣欣
* Date：2018-11-2
*/
router.post('/month_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event } = (req as any).body

    try {
        if (operate_power.rebateStat !== 1) {
            return sendError(res, 'logintimeout')
        }

        let result = await db.monthStatistics(database_name, event)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 按时间段统计 限制最长30天
* Author：鲁欣欣
* Date：2018-11-5
*/
router.post('/time_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { start_time, end_time, start, length, event } = (req as any).body
    try {
        if (operate_power.rebateStat !== 1) {
            return sendError(res, 'logintimeout')
        }

        let result = await db.timeStatistics(database_name, start_time, end_time, start, length, event)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        pc.error(`time_statistics2-> ${e.message}`)
        return sendError(res, e.message)
    }
})