/**********************************************
* 职位管理
* Author：鲁欣欣
* Date：2018-09-26
*/
import express = require('express')
import { sendOk, sendError, sendOkTable } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/position')
import validator = require("validator")
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
export const router: express.Router = express.Router()
import { log_pc as pc } from '../../lib/log'

/**********************************************
* 职位添加
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/position_add', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name, account_id } = req.query['map']
    const { name, remark, setoperate_power, setshop_power } = (req as any).body

    try {
        if (!validator.isJSON(setoperate_power)) {
            return sendError(res, 'JSON格式异常！')
        }

        validateCgi({ position: name }, acValidator.position_value)
        validateCgi({ remark: remark }, acValidator.remark_value)
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        /**检查职位名是否重复 */
        await db.positionName(database_name, name)
        let result = await db.addPosition(account_id, database_name, name, remark, setoperate_power, setshop_power)
        if (result == 1) {
            return sendOk(res, '新建成功')
        }

    } catch (e) {
        pc.error(`position_add-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 职位查看
* Author：鲁欣欣
* Date：2018-09-27
*/
router.post('/position_select', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name, account_id } = req.query['map']
    const { status, start, length, search } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ page: start, count: length }, acValidator.reqcord)
        validateCgi({ search: search }, acValidator.query_value)
        /**公司管理员查看所有职位，其他人只能查看自己创建的职位 */
        let result
        if (account_id == 100000) {
            result = await db.rootSelectPosition(database_name, search, start, length, status)
        }
        else {
            result = await db.selectPosition(account_id, database_name, search, start, length, status)
        }
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看某职位的详情
* Author：鲁欣欣
* Date：2018-10-15
*/
router.post('/position_info', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.positionInfo(database_name, id)
        return sendOk(res, result)

    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 职位修改
* Author：鲁欣欣
* Date：2018-09-27
*/
router.post('/position_update', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { id, name, remark, setoperate_power, setshop_power } = (req as any).body

    try {
        if (!validator.isJSON(setoperate_power)) {
            return sendError(res, 'JSON格式异常！')
        }

        validateCgi({ position: name }, acValidator.position_value)
        validateCgi({ remark: remark }, acValidator.remark_value)
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        /**检查职位名是否重复 */
        await db.positionName1(database_name, id, name)
        let result = await db.updatePosition(database_name, id, name, remark, setoperate_power, setshop_power)
        if (result == 1) {
            return sendOk(res, '修改成功')
        }

    } catch (e) {
        pc.error(`position_update-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 职位状态修改
* Author：鲁欣欣
* Date：2018-09-27
*/
router.post('/position_status', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { id, status } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.updatePositionStatus(database_name, id, status)
        if (result == 1) {
            return sendOk(res, '修改成功')
        }

    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除职位
* Author：鲁欣欣
* Date：2018-09-27
*/
router.post('/position_delete', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.deletePosition(database_name, id)
        if (result == 1) {
            return sendOk(res, '删除成功')
        }

    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 新建职位或编辑时时展示所有店铺
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/select_shop', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, shop_power, supervisor } = req.query['map']

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        if (shop_power === '') {
            return sendOk(res, [])
        } else {
            let result = await db.selectShop(supervisor, shop_power)
            return sendOk(res, result)
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})