import express = require('express')
export const router: express.Router = express.Router()
import { log_pc as log } from '../../lib/log'
import { taoBaoAuthority, getAuthUrl, getShop } from '../../dao/pc/taobaoAuth';
import { sendError, sendOk, sendOkTable } from '../../lib/utils';
import login = require('../../dao/pc/login')
import { validateCgi } from '../../lib/validator';
import pcValidator = require("../../validator/pcUserTap")

/**********************************************
 * 淘宝授权
 * Author：何语漫
 * Date：2018-11-05
 */
router.get(`/Authority/:id`, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { code } = req.query
    console.log(req.query)
    console.log(req.params)
    try {
        let domian = await taoBaoAuthority(code, req.params.id)
        res.redirect(`http://${domian}/#/shop`)
    } catch (e) {
        log.error(`Authority/${req.params.id}: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取淘宝链接
 * Author：何语漫
 * Date：2018-11-05
 */
router.post('/getAuthUrl', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, supervisor } = req.query['map']
    try {
        if (operate_power.shop !== 1) {
            return sendError(res, '无权限')
        }
        let result = await getAuthUrl(supervisor)
        sendOk(res, result)
    } catch (e) {
        log.error(`Authority/${req.params.id}: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 显示店铺
 * Author：何语漫
 * Date：2018-11-05
 */
router.post('/shop', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { shop_name, start, length } = (req as any).body
    const { operate_power, supervisor } = req.query['map']
    console.log(operate_power)
    try {
        if (operate_power.shop !== 1) {
            return sendError(res, '无权限')
        }
        shop_name == '' ? '' : validateCgi({ shop_name: shop_name }, pcValidator.ShopName)
        validateCgi({ start, length }, pcValidator.limit)
        let result = await getShop(supervisor, shop_name, start, length)
        sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        log.error(`Authority/${req.params.id}: ${e.message}`)
        console.log(e.message)
        sendError(res, e.message)
    }
})
