/**********************************************
* 平台管理，公司审核
* Author：鲁欣欣
* Date：2018-09-25
*/
import express = require('express')
import { sendOk, sendError, randomInt, md5sum, formatDate1, replaceToSign, signString, replaceUrl } from '../../lib/utils';
export const router: express.Router = express.Router()
import db = require('../../dao/pc/check')
import login = require('../../dao/pc/login')
import validator = require("validator")
import { validateCgi } from '../../lib/validator'
import acValidator = require("../../validator/pc_account")
import { getAsync } from '../../lib/request'
import { log_pc as pc } from '../../lib/log'
import { sendMail } from '../../lib/mail';

/**********************************************
* 查看公司信息
* Author：鲁欣欣
* Date：2018-09-25
*/
router.post('/select_company', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { real_name, status, start, length, search } = (req as any).body

    try {
        validateCgi({ page: start, count: length }, acValidator.reqcord)
        validateCgi({ search: search }, acValidator.query_value)
        validateCgi({ real_name: real_name }, acValidator.realname_value)
        let result = await db.selectCompany(search, start, length, status, real_name)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 对审核未通过的公司信息进行编辑
* Author：鲁欣欣
* Date：2018-09-30
*/
router.post('/edite_company', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone, company_name, shop, province, city, real_name, email } = (req as any).body
    try {
        validateCgi({ company: company_name }, acValidator.company_value)
        validateCgi({ shop: shop }, acValidator.shop_value)
        validateCgi({ province: province, city: city }, acValidator.Address)
        validateCgi({ email: email }, acValidator.email_value)
        let result = await db.editeCompany(phone, company_name, shop, province, city, real_name, email)
        if (result == 1) {
            return sendOk(res, '修改成功')
        }
    } catch (e) {
        pc.error(`edite_company-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 点击审核通过，配置密码，确定
* Author：鲁欣欣
* Date：2018-09-25
*/
router.post('/check_company', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { id, phone, real_name } = (req as any).body
    let sendData = {
        mailbox: '',
        subject: '',
        text: ''
    }
    try {
        /**生成6位数密码 */
        let password: string = phone.substring(5)

        /**分配数据库 */
        let database = 'company_' + id

        /**分配二级域名 */
        let DomainName = await db.getDomain()   //获取配置的一级域名
        let record = 'company' + id
        let domain = `${record}.${DomainName}`
        let url = await AddDomainRecord(DomainName, record)

        /**请求分配域名 */
        let res1 = await getAsync(url)
        let res2 = JSON.parse(res1)
        let recordId = res2.RecordId

        /**邮箱通知 */
        let email = await db.getEmail(phone)
        console.log(email)

        /**创建数据库 */
        await db.addDatabase(id, phone, md5sum(password), real_name, database)

        /**写入信息到平台数据库 */
        let result = await db.updateCompany(phone, md5sum(password), domain, recordId, database)
        if (result == 1) {
            sendData = {
                mailbox: email,
                subject: '电商返利平台注册成功',
                text: `亲爱的用户${real_name}：\n恭喜您注册成功！\n您的电商返利平台域名为：${domain}，请使用该域名进行登录!\n账号是：${phone}\n密码是：${password}\n电商返利平台团队`

            }
            validateCgi({ email: sendData.mailbox }, acValidator.email_value)
            await sendMail(sendData)
            return sendOk(res, sendData.text)

        }
    } catch (e) {
        pc.error(`check_company-> ${e.message}`)
        if (e === 'bad code 400') {
            return sendError(res, '域名分配失败，已存在该域名')
        }
        if (e.message === '邮箱格式错误！') {
            let ret = { status: 2, msg: sendData.text }
            return res.send(JSON.stringify(ret))
        }
        else {
            return sendError(res, e.message)
        }
    }
})

/**********************************************
* 点击审核不通过
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/check_not', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone } = (req as any).body
    try {
        /**更改记录状态 */
        let result = await db.updateCompanyStatus(phone)
        if (result == 1) {
            return sendOk(res, '操作成功')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除公司账号
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/company_delete', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { id } = (req as any).body

    try {
        let result = await db.deleteCompany(id)
        if (result == 1) {
            /**删除域名分配记录 */
            /*let recordId = await db.getRecordId(id)
            console.log('预删除记录id：' + recordId)
            let url = DeleteDomainRecord(recordId)
            console.log(url)
            let res1 = await getAsync(url)
            let res2 = JSON.parse(res1)
            let recordId2 = res2.RecordId
            console.log('请求记录结果id：' + recordId2)*/
            return sendOk(res, '删除成功')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 拉黑公司账号
* Author：鲁欣欣
* Date：2018-10-9
*/
router.post('/company_black', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { id } = (req as any).body

    try {
        let result = await db.blackCompany(id)
        if (result == 1) {
            return sendOk(res, '拉黑成功')
        }
    } catch (e) {
        pc.error(`company_black-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 取消拉黑公司账号
* Author：鲁欣欣
* Date：2018-10-9
*/
router.post('/company_white', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { id } = (req as any).body

    try {
        let result = await db.whiteCompany(id)
        if (result == 1) {
            return sendOk(res, '取消拉黑成功')
        }
    } catch (e) {
        pc.error(`company_white-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 更改公司到期时间
* Author：鲁欣欣
* Date：2018-11-14
*/
router.post('/company_time', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { id } = (req as any).body

    try {
        let result = await db.updateCompanyTime(id)
        if (result == 1) {
            return sendOk(res, '续签成功。')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 统计公司的销量数据
* Author：鲁欣欣
* Date：2018-11-15
*/
router.post('/company_stat', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { id } = (req as any).body

    try {
        let result = await db.companyStat(id)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

router.get('/test', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let sendData = {
        mailbox: '',
        subject: '',
        text: ''
    }
    try {

        sendData = {
            mailbox: '1135877970qq.com',
            subject: '电商返利平台注册成功',
            text: `test`
        }
        validateCgi({ email: sendData.mailbox }, acValidator.email_value)
        await sendMail(sendData)
        return sendOk(res, sendData.text)

    } catch (e) {
        console.log(e)
        if (e.message === '邮箱格式错误！') {
            let ret = { status: 2, msg: sendData.text }
            return res.send(JSON.stringify(ret))
        }
        else {
            return sendError(res, e.message)
        }
    }
})

/**添加二级域名解析记录,产生请求url */
async function AddDomainRecord(DomainName: string, record: string): Promise<any> {
    /**获取ali配置信息 */
    let config = await db.getConfig()
    let AccessKeyId = config.AccessKeyId
    let AccessKeySecret = config.AccessKeySecret
    let host = config.host
    let SignatureNonce = randomInt(10000000000000, 100000000000000) //唯一随机数，用于防止网络重放攻击。用户在不同请求间要使用不同的随机数值

    let localTime = new Date()
    localTime.setHours(localTime.getHours() - 8)
    let Timestamp = formatDate1(localTime)
    // let queryString = `AccessKeyId=${AccessKeyId}&Action=AddDomain&DomainName=${DomainName}&Format=JSON&SignatureMethod=HMAC-SHA1&SignatureNonce=${SignatureNonce}&SignatureVersion=1.0&Timestamp=${Timestamp}&Version=2015-01-09`.replace(/:/g, '%3A') //待请求字符串
    let queryString = `AccessKeyId=${AccessKeyId}&Action=AddDomainRecord&DomainName=${DomainName}&Format=JSON&RR=${record}&SignatureMethod=HMAC-SHA1&SignatureNonce=${SignatureNonce}&SignatureVersion=1.0&Timestamp=${Timestamp}&Type=A&Value=${host}&Version=2015-01-09`.replace(/:/g, '%3A') //待请求字符串
    let StringToSign = 'GET&%2F&' + replaceToSign(queryString)                  //待签名字符串
    let Signature = signString(StringToSign, AccessKeySecret + '&')            //签名结果串，关于签名的计算方法，请参见阿里云签名机制
    let url = `https://alidns.aliyuncs.com/?${queryString}&Signature=${replaceUrl(Signature)}`
    return url
}

/**删除二级域名解析记录,产生请求url */
async function DeleteDomainRecord(recordId: string): Promise<any> {
    /**获取ali配置信息 */
    let config = await db.getConfig()
    let AccessKeyId = config.AccessKeyId
    let AccessKeySecret = config.AccessKeySecret
    let host = config.host
    let SignatureNonce = randomInt(10000000000000, 100000000000000) //唯一随机数，用于防止网络重放攻击。用户在不同请求间要使用不同的随机数值

    let localTime = new Date()
    localTime.setHours(localTime.getHours() - 8)
    let Timestamp = formatDate1(localTime)
    let queryString = `AccessKeyId=${AccessKeyId}&Action=DeleteDomainRecord&Format=JSON&RecordId=${recordId}&SignatureMethod=HMAC-SHA1&SignatureNonce=${SignatureNonce}&SignatureVersion=1.0&Timestamp=${Timestamp}&Version=2015-01-09`.replace(/:/g, '%3A') //待请求字符串
    let StringToSign = 'GET&%2F&' + replaceToSign(queryString)                  //待签名字符串
    console.log(StringToSign)
    let Signature = signString(StringToSign, AccessKeySecret + '&')            //签名结果串，关于签名的计算方法，请参见阿里云签名机制
    let url = `https://alidns.aliyuncs.com/?${queryString}&Signature=${replaceUrl(Signature)}`
    return url
}