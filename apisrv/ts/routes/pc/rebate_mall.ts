import express = require('express')
import { PC_IMAGES, REQ_IMAGES_prefix } from '../../config/pwd'
import { validateCgi } from '../../lib/validator'
import { log_pc as log } from '../../lib/log'
import { sendOk, sendError, sendOkTable } from '../../lib/utils'
import { loginCheck } from '../../dao/pc/login'
import db = require('../../dao/pc/rebate_mall')
import { getComponentAppidAndHost } from '../../lib/compconfig'
import { getItemInfo } from '../../lib/taobaoConfig'
import rebate_mall = require("../../validator/rebate_mall")

export const router: express.Router = express.Router()
let multer = require('multer')
var upload = multer({ dest: PC_IMAGES })
const PERMISSION_DENIED = 'loginOut'

/**********************************************
* 查询活动列表（只能看到自己权限店铺商品的活动）
* Author：mjq
* Date：2018-10-27
*/
router.post('/select', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power, account_id } = req.query['map']
    const { start, length, activity_name, status, start_time, end_time } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ start: start, length: length }, rebate_mall.ISelectModel)

        let r = await db.selectRebateMall(database_name, account_id, {
            start: start,
            length: length,
            activity_name: activity_name,
            status: status,
            start_time: start_time,
            end_time: end_time
        })
        return sendOkTable(res, r.recordsFiltered, r.recordsTotal, r.data)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 返回具体某一个活动信息，进行编辑更改
* Author：mjq
* Date：2018-10-27
*/
router.post('/select_detail', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power, account_id } = req.query['map']
    const { id } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }

        let r = await db.selectDetail(database_name, account_id, id)
        return sendOk(res, r)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 根据店铺和商品链接获取商品信息
* Author：mjq
* Date：2018-10-31
*/
router.post('/goods', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { goods_url, shop_id } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ goods_url: goods_url, shop_id: shop_id }, rebate_mall.IGoods)

        let patt = /.*id=(\d*)/
        let mat = patt.exec(goods_url)
        if (!mat) {
            return sendError(res, '无法识别链接')
        }
        let goods_id = mat[1]

        let TBres = await getItemInfo(goods_id, shop_id)

        await db.downloadPic(TBres.img.slice(0, 5), (result: any) => {
            return sendOk(res, {
                goods_id: goods_id,
                goods_img: result,
                goods_price: TBres.price
            })
        })
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 获取属于自己权限的全部店铺信息
* Author：mjq
* Date：2018-11-13
*/
router.post('/shops', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power, account_id } = req.query['map']
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }

        let r = await db.selectShops(database_name, account_id)
        sendOk(res, r)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 新建活动记录
* Author：mjq
* Date：2018-10-27
*/
router.post('/insert', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { activity_name, shop_id, goods_url, goods_id, goods_img, goods_price, short_link, expenditure, after_event, start_time, end_time, credits, participation, total, flow, notes } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi(req.body, rebate_mall.IInsert)

        //检查店铺和商品id是否匹配
        await getItemInfo(goods_id, shop_id)

        let r = await db.insertRebateMall(database_name, {
            activity_name: activity_name,       //活动名称
            shop_id: shop_id,                   //店铺id
            goods_url: goods_url,               //淘宝链接， 可为空
            goods_id: goods_id,                 //商品id
            goods_img: goods_img,               //商品图片['', '', '']
            goods_price: goods_price,           //商品价格的区间'10-20'
            short_link: short_link,             //淘口令
            expenditure: parseFloat(expenditure),         //返利金额
            after_event: after_event,           //返利规则{"event":"支付后","hour":72}
            start_time: start_time,             //活动开始时间
            end_time: end_time,                 //活动结束时间
            credits: parseInt(credits),         //活动赠送的积分， 默认0
            participation: participation,       //参与活动条件，[{"type":"","status":1,"data":500}]
            total: parseInt(total),             //活动次数
            flow: flow,                         //活动流程
            notes: notes,                       //注意事项
        })
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 更改活动记录（只能更改未有人参加的活动）
* Author：mjq
* Date：2018-10-27
*/
router.post('/update', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { id, activity_name, shop_id, goods_url, goods_id, goods_img, goods_price, short_link, expenditure, after_event, start_time, end_time, credits, participation, total, flow, notes } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi(req.body, rebate_mall.IUpdete)

        //检查店铺和商品id是否匹配
        await getItemInfo(goods_id, shop_id)

        let r = await db.updateRebateMall(database_name, {
            id: parseInt(id),
            activity_name: activity_name,       //活动名称
            shop_id: shop_id,
            goods_url: goods_url,               //淘宝链接， 可为空
            goods_id: goods_id,                 //商品id
            goods_img: goods_img,               //商品图片['', '', '']
            goods_price: goods_price,           //商品价格的区间'10-20'
            short_link: short_link,             //淘口令
            expenditure: parseFloat(expenditure),         //返利金额
            after_event: after_event,           //返利规则{"event":"支付后","hour":72}
            start_time: start_time,             //活动开始时间
            end_time: end_time,                 //活动结束时间
            credits: parseInt(credits),         //活动赠送的积分， 默认0
            participation: participation,       //参与活动条件，[{"type":"","status":1,"data":500}]
            total: parseInt(total),             //活动次数
            flow: flow,                         //活动流程
            notes: notes,                       //注意事项
        })
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 更改活动记录（更改有人参加的活动）
* Author：mjq
* Date：2018-10-27
*/
router.post('/update2', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { id, end_time, total } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi(req.body, rebate_mall.IUpdete2)

        let r = await db.updateRebateMall2(database_name, {
            id: parseInt(id),
            end_time: end_time,                 //活动结束时间
            total: parseInt(total),             //活动次数
        })
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除活动记录（只能删除未上线和已上线且未有人参加的活动，不能删除已下线的活动）
* Author：mjq
* Date：2018-10-27
*/
router.post('/del', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { id } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ id: id }, rebate_mall.IDelete)

        await db.delRebateMall(database_name, parseInt(id))
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 查询活动参与记录（查询属于自己店铺权限的参与记录）
* Author：mjq
* Date：2018-10-27
*/
router.post('/select_record', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power, account_id } = req.query['map']
    const { start, length, activity_name, tid, receiver_mobile, order_status, status, created_1, created_2, rebate_time_1, rebate_time_2 } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ start: start, length: length }, rebate_mall.ISelectModel)

        let r = await db.selectRebateMallRecords(database_name, account_id, {
            start: start,                       //开始位置
            length: length,                     //查询个数
            order_status: order_status,         //订单状态
            status: status,                     //记录状态
            activity_name: activity_name,       //活动名称
            tid: tid,                           //交易订单号
            receiver_mobile: receiver_mobile,   //收货人手机号
            created_1: created_1,               //创建时间范围1
            created_2: created_2,               //创建时间范围2
            rebate_time_1: rebate_time_1,       //转账时间范围1
            rebate_time_2: rebate_time_2,       //转账时间范围2
        })
        return sendOkTable(res, r.recordsFiltered, r.recordsTotal, r.data)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 手动操作返利, 返利失败->返利中
* Author：mjq
* Date：2018-10-31
*/
router.post('/update_status', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { ids } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ ids: ids }, rebate_mall.IBatch)

        await db.updateRebateMallDetails(database_name, ids)
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除待下单的记录， 并且模板消息通知
* Author：mjq
* Date：2018-10-27
*/
router.post('/del_record', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { id } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ id: id }, rebate_mall.IDelete)

        await db.delRebateMallDetails(database_name, id)
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 获取返利商城动图配置
* Author：mjq
* Date：2018-10-31
*/
router.post('/get_carousel', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    try {
        if (0 === operate_power.rebateSet) {
            return sendError(res, PERMISSION_DENIED)
        }

        let r = await db.getRebateMallCarousel(database_name)
        return sendOk(res, r)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 配置商城轮播图
* Author：mjq
* Date：2018-10-31
*/
router.post('/set_carousel', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    const { time, pic } = req.body
    try {
        if (0 === operate_power.rebateSet) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ time: time, pic: pic }, rebate_mall.ISetCarousel)

        await db.setRebateMallCarousel(database_name, JSON.stringify({ "time": parseInt(time), "pic": JSON.parse(pic) }))
        return sendOk(res, '操作成功')
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 上传图片， 最多一次上传4个
* Author：mjq
* Date：2018-11-08
*/
router.post('/batch_upload_img', /*loginCheck,*/ upload.array('file', 4), async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    // const { database_name, operate_power } = req.query['map']
    try {
        let files = (req as any).files
        if (!files) {
            return sendError(res, '上传失败')
        }

        let buf = new Array()
        for (let v of files) {
            buf.push(`${REQ_IMAGES_prefix}${v.filename}`)
        }
        sendOk(res, buf)
    } catch (e) {
        log.error(`/mch_conf/cert: ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 分享活动链接， 返回链接
* Author：mjq
* Date：2018-11-08
*/
router.post('/link', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    let { supervisor } = (req as any).cookies
    const { id } = req.body
    try {
        if (0 === operate_power.rebateEvent) {
            return sendError(res, PERMISSION_DENIED)
        }
        validateCgi({ id: id }, rebate_mall.IDelete)

        //获取授权的公众号appid
        let appid = await db.getWxConf(database_name, supervisor)

        //获取开放平台的appid
        let component_appid = await getComponentAppidAndHost()

        let redirect_uri = encodeURIComponent(`${component_appid.host}wechat/rebate_mall/share_entrance/${id}`)
        let link = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appid}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=STATE&component_appid=${component_appid.appid}#wechat_redirect`
        return sendOk(res, link)
    } catch (e) {
        log.error(`${e.message}`)
        return sendError(res, e.message)
    }
})