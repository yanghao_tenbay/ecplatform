/**********************************************
* 平台上账号管理
* Author：鲁欣欣
* Date：2018-09-28
*/

import express = require('express')
import { sendOk, sendError } from "../../lib/utils"
import db = require('../../dao/pc/platform')
import login = require('../../dao/pc/login')
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
import { dayStatistics } from '../../dao/pc/order_statistics';
export const router: express.Router = express.Router()


/**********************************************
* 平台管理员修改密码
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/personal_passwd', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { account_id, passwd, newpasswd } = (req as any).body

    try {
        validateCgi({ password: newpasswd }, acValidator.passwd)
        let res1 = await db.verifyPasswd(account_id)
        if (passwd === res1) {
            let r = await db.updPasswd(account_id, newpasswd)
            if (r == 1) {
                return sendOk(res, '更改密码成功!')
            }
        }
        else {
            return sendError(res, '原密码错误!')
        }

    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
* 平台首页数据统计
* Author：鲁欣欣
* Date：2018-11-6
*/
router.post('/data_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {

    try {
        let result = await db.statistics()
        return sendOk(res, result)

    } catch (e) {
        sendError(res, e.message)
    }
})