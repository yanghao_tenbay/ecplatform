/**********************************************
* 个人密码修改
* Author：鲁欣欣
* Date：2018-09-28
*/

import express = require('express')
import { sendOk, sendError, md5sum } from "../../lib/utils"
import db = require('../../dao/pc/personal_homepage')
export const router: express.Router = express.Router()
import login = require('../../dao/pc/login')
import { validateCgi } from '../../lib/validator'
import acValidator = require("../../validator/pc_account")

/**********************************************
* 获取个人信息
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/personal_info', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { account_id, account_name, database_name } = req.query['map']
    try {
        let result = await db.getPersonalInfo(database_name, account_id)

        return sendOk(res, result.account_name)
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
* 修改密码
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/personal_passwd', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { account_name, database_name, account_id, supervisor } = req.query['map']
    const { passwd, newpasswd } = (req as any).body

    try {
        validateCgi({ password: newpasswd }, acValidator.passwd)
        let res1 = await db.verifyPasswd(database_name, account_id)
        if (passwd === res1) {
            let r = await db.updPersonalPasswd(database_name, account_id, newpasswd)
            if (r == 1) {
                /**如果该账号为公司管理者账号，同时更新账号密码到平台公司账号管理表 */
                await login.updPasswdToPlatform(supervisor, passwd, account_name)
                return sendOk(res, '更改密码成功!')
            }
        }
        else {
            return sendError(res, '原密码错误!')
        }

    } catch (e) {
        sendError(res, e.message)
    }

})