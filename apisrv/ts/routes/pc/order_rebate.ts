/**********************************************
* 订单返利
* Author：鲁欣欣
* Date：2018-10-30
*/

import express = require('express')
import { sendOk, sendError, sendOkTable } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/order_rebate')
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
import { isNull } from 'util';
import { getItemInfo } from '../../lib/taobaoConfig';
import { log_pc as pc } from '../../lib/log'
export const router: express.Router = express.Router()

/**********************************************
* 新建订单返利活动
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/add_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, supervisor, database_name } = req.query['map']
    const { event, start_time, end_time, amount, percentage, rebate_status, days, integral, newuser, integral_num, num, number, commodity, img, shop_id } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ event_name: event }, acValidator.event_value)
        let result = await db.addEvent(supervisor, database_name, event, start_time, end_time, amount, percentage, rebate_status, days, integral, newuser, integral_num, num, number, commodity, img, shop_id)
        if (result === 1) {
            return sendOk(res, '新建活动成功！')
        }
    } catch (e) {
        pc.error(`add_event2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看订单返利活动
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/select_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name, shop_power } = req.query['map']
    const { start, length, event, start_time, end_time, status } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectEvent(shop_power, database_name, start, length, event, start_time, end_time, status)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        pc.error(`select_event2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 编辑订单返利活动
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/update_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id, event, start_time, end_time, amount, percentage, rebate_status, days, integral, newuser, integral_num, num, number, commodity, img, shop_id } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ event_name: event }, acValidator.event_value)
        let result = await db.updateEvent(database_name, event_id, event, start_time, end_time, amount, percentage, rebate_status, days, integral, newuser, integral_num, num, number, commodity, img, shop_id)
        if (result === 1) {
            return sendOk(res, '编辑活动成功！')
        }
    } catch (e) {
        pc.error(`update_event2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 编辑已上线和已下线晒评价返利活动的时间和次数
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/update_event_time', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id, end_time, number } = (req as any).body
    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.updateEventTime(database_name, event_id, end_time, number)
        if (result === 1) {
            return sendOk(res, '编辑活动成功！')
        }
    } catch (e) {
        pc.error(`update_event_time2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除订单返利活动
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/delete_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id } = (req as any).body
    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.deleteEvent(database_name, event_id)
        if (result === 1) {
            return sendOk(res, '删除活动成功！')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看订单返利活动记录
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/select_record', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name, shop_power } = req.query['map']
    const { event, order_id, nickname, status, order_status, start_time, end_time, s_time, e_time, start, length } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectRecord(shop_power, database_name, start, length, event, order_id, nickname, status, order_status, start_time, end_time, s_time, e_time)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        pc.error(`select_record2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 人工审核对自动审核失败的记录给予通过
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/approve', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id, event_id } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.approve(database_name, id)
        if (result === 1) {
            /**活动已参与次数加一 */
            await db.addEventNum(database_name, event_id)
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`approve2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 人工审核确认审核失败
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/unapprove', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.unapprove(database_name, id)
        if (result === 1) {
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`unapprove2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 对单条记录返利
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/rebate', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.rebate(database_name, id)
        if (result === 1) {
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`rebate2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 批量返利
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/batch_rebate', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { ids } = (req as any).body
    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        await db.batchRebate(database_name, ids)
        return sendOk(res, '操作成功！')

    } catch (e) {
        pc.error(`batch_rebate2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看某活动的参与信息
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/event_record', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id, start, length } = (req as any).body

    try {
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectEventRecord(database_name, start, length, event_id)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        pc.error(`event_record2-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 根据链接获取商品图片
* Author：鲁欣欣
* Date：2018-10-30
*/
router.post('/get_image', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power } = req.query['map']
    const { good_url, shop_name } = (req as any).body

    try {
        validateCgi({ shop: shop_name }, acValidator.shop_value)
        if (operate_power.orderRebate !== 1) {
            return sendError(res, 'logintimeout')
        }
        let reg = /id=(\d+)/
        let NumIid = good_url.match(reg)
        console.log(NumIid)
        if (isNull(NumIid)) {
            return sendError(res, '未获取到商品id')
        }
        /**根据店铺名称获取店铺id */
        let res1 = await db.getShop_id(shop_name)
        let result = await getItemInfo(NumIid[1], res1.id)
        let ret = { id: NumIid[1], img: result.img[0] }
        return sendOk(res, ret)
    } catch (e) {
        pc.error(`get_image-> ${e.message}`)
        return sendError(res, e.message)
    }
})