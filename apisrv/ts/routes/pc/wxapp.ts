import express = require('express')
import { deCryptMsg } from '../../lib/crypto';
import { saveCompVerifyTik, updPreAuthCode } from '../../lib/compconfig';
import { authority, getAuthCodeUrl, getAuthInfo, cancelAuth, isWxAuthFinish } from '../../dao/pc/WxAuthority';
import { sendError, sendOk } from '../../lib/utils';
import { loginCheck } from '../../dao/pc/login';
import { log_pc as log } from '../../lib/log'
export const router: express.Router = express.Router()


/**********************************************
 * 判断是否授权
 * Author：何语漫
 * Date：2018-10-08
 */
router.post('/judgeWxAuth', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let { supervisor, operate_power } = req.query['map']
    try {
        if (operate_power.vipcn == 1) {
            let finish = await isWxAuthFinish(supervisor)
            if (finish) {
                sendOk(res, '授权成功')
            } else {
                sendError(res, '用户未授权')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }

})

/**********************************************
 * 公司已授权则获取公司公众号信息，未授权获取授权二维码
 * Author：何语漫
 * Date：2018-10-08
 */
router.post('/getCodeUrl', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let { supervisor, operate_power } = req.query['map']
    try {
        if (operate_power.vipcn == 1) {
            let result = await getAuthInfo(supervisor)
            if (result.length != 0) {
                return sendOk(res, result)
            }
            let codeUrl = await getAuthCodeUrl(supervisor)
            return sendOk(res, codeUrl)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 授权
 * Author：何语漫
 * Date：2018-09-25
 */
router.get(`/Authority/:id`, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { auth_code } = req.query
    try {
        await authority(auth_code, req.params.id)
    } catch (e) {
        log.error(`Authority/${req.params.id}: ${e.message}`)
        sendError(res, e.message)
    }
})

async function readRawBody(req: any, res: any, next: any) {
    let arr = new Array<string>()
    req.on('data', function (chunk: any) {
        arr.push(chunk.toString())
    })

    req.on('end', function (chunk: any) {
        if (chunk) {
            arr.push(chunk.toString())
        }
        req.rawBody = arr.join()
        next()
    })
}

/**********************************************
 * 解密，保存component_verify_ticket，每10分钟一次
 * Author：何语漫
 * Date：2018-09-25
 */
router.post('/notice', readRawBody, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        const { msg_signature, timestamp, nonce } = req.query
        let body = (<any>req).rawBody
        res.send('success')
        // 解析xml，消息解密
        let decrypt = await deCryptMsg(body, msg_signature, timestamp, nonce)
        if (decrypt.xml.InfoType == 'unauthorized') {
            await cancelAuth(decrypt.xml.AuthorizerAppid)
        } else if (decrypt.xml.InfoType == 'authorized') {
            await updPreAuthCode()
        }
        log.debug('/授权信息 ' + JSON.stringify(decrypt))
        //保存component_verify_ticket
        await saveCompVerifyTik(decrypt.xml.ComponentVerifyTicket)
    } catch (e) {
        log.error(`pc/wxapp/notice: ${e.message}`)
    }
})