/**********************************************
* 订单信息
* Author：鲁欣欣
* Date：2018-11-9
*/

import express = require('express')
import { sendError, sendOkTable } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/order_info')
export const router: express.Router = express.Router()

/**********************************************
* 查看参与活动的订单信息
* Author：鲁欣欣
* Date：2018-11-8
*/
router.post('/select_orderinfo', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { phone, order_id, nickname, order_status, start_time, end_time, start, length } = (req as any).body

    try {
        if (operate_power.orderInfo !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectOrder(database_name, start, length, phone, order_id, nickname, order_status, start_time, end_time)
        console.log(result)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        return sendError(res, e.message)
    }
})
