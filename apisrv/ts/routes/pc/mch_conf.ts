import express = require('express')

import { notify } from '../../daemon/daemon'
import { validateCgi } from '../../lib/validator'
import { loginCheck } from '../../dao/pc/login'
import { log_pc as log } from '../../lib/log'
import { sendOk, sendError, sendOkTable } from '../../lib/utils'
import { MCH_CERT } from '../../config/pwd'
import db = require('../../dao/pc/mch_conf')

export const router: express.Router = express.Router()
let multer = require('multer')
let xlsx = require('node-xlsx')
let cmd = require('node-cmd')
const permission_denied = 'loginOut'

/**********************************************
* 返回商户信息
* Author：mjq
* Date：2018-10-08
*/
router.post('/mchinfo', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.vipcn !== 1) {
            sendError(res, permission_denied)
        }
        let result = await db.select(database_name)
        return sendOk(res, result)
    } catch (e) {
        log.error(`/mch_conf/mchinfo: ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 接收商户基础信息
* Author：mjq
* Date：2018-10-08
*/
router.post('/set_mch', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { mch_id, mch_appid, mch_key, template_id } = (req as any).body
    const { database_name, operate_power } = req.query['map']
    try {
        // validateCgi({ mch_id: mch_id, mch_appid: mch_appid, mch_key: mch_key, template_id: template_id }, trade_history.IMchConf)
        if (operate_power.vipcn !== 1) {
            sendError(res, permission_denied)
        }

        await db.update(database_name, {
            mch_id: mch_id,
            mch_appid: mch_appid,
            mch_key: mch_key,
            template_id: template_id
        })
        return sendOk(res, '保存成功')
    } catch (e) {
        log.error(`/mch_conf/set_mch: ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 接收商户上传的证书压缩包
* Author：mjq
* Date：2018-10-08
*/
var upload = multer({ dest: "/tmp/upload" })
router.post('/cert', upload.single('file'), async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.vipcn !== 1) {
            sendError(res, permission_denied)
        }
        let file = (req as any).file
        // log.debug(JSON.stringify(file))

        if (!file || 'application/zip' !== file.mimetype) {
            return sendError(res, '文件错误')
        }

        let cert_file = `${MCH_CERT}${database_name}`
        cmd.get(
            `
            test -d ${cert_file} || mkdir -p ${cert_file}
            rm ${cert_file}/*
            mv /tmp/upload/${file.filename} /tmp/upload/${file.filename}.zip
            unzip -d ${cert_file} /tmp/upload/${file.filename}.zip apiclient_key.pem apiclient_cert.pem
            rm /tmp/upload/${file.filename}.zip
            `,
            function (err: any, data: any, stderr: any) {
                if (!err) {
                    return sendOk(res, '上传成功')
                } else {
                    sendError(res, err)
                }
            }
        )
    } catch (e) {
        log.error(`/mch_conf/cert: ${e.message}`)
        return sendError(res, e.message)
    }
})