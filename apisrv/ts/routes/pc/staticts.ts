import express = require('express')
export const router: express.Router = express.Router()
import { sendError, sendOkTable } from '../../lib/utils';
import { creditsRecord, attendence, exchange } from "../../dao/pc/statistc";
import login = require('../../dao/pc/login')

/**********************************************
 * 积分统计
 * Author：何语漫
 * Date：2018-11-01
 */
router.post('/credits', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { startdate, enddate, start, length, way } = (req as any).body
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.integralStat != 1) {
            throw new Error('无权限')
        }
        let result = await creditsRecord(database_name, way, startdate, enddate)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 签到统计
 * Author：何语漫
 * Date：2018-11-01
 */
router.post('/attendence', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { startdate, enddate, start, length, way } = (req as any).body
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.signStat != 1) {
            throw new Error('无权限')
        }
        let result = await attendence(database_name, way, startdate, enddate)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 兑换统计
 * Author：何语漫
 * Date：2018-11-01
 */
router.post('/exchange', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { search, startdate, enddate, way } = (req as any).body
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.cashStat != 1) {
            throw new Error('无权限')
        }
        let result = await exchange(database_name, way, search, startdate, enddate)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        sendError(res, e.message)
    }
})
