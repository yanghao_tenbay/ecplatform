/**********************************************
* 公司内部账号管理
* Author：鲁欣欣
* Date：2018-09-27
*/

import express = require('express')
import { sendOk, sendError, sendOkTable, randomInt, md5sum } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/account')
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
import { log_pc as pc } from '../../lib/log'
export const router: express.Router = express.Router()

/**********************************************
* 查询账号
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/account_select', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { account_name, real_name, position, status, start, length } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ page: start, count: length }, acValidator.reqcord)
        validateCgi({ search: account_name }, acValidator.query_value)
        validateCgi({ real_name: real_name }, acValidator.realname_value)
        let result = await db.selectAccount(database_name, start, length, account_name, real_name, position, status)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 职位展示，以供选择
* Author：鲁欣欣
* Date：2018-09-27
*/
router.post('/position_show', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name, account_id } = req.query['map']
    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        /**公司管理员账号可分配所有职位，而员工仅能分配自己创建的职位 */
        let result
        if (account_id == 100000) {
            result = await db.rootShowPosition(database_name)
        }
        else {
            result = await db.showPosition(account_id, database_name)
        }
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 添加账号,还需检测账号名是否重复
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/account_add', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name, supervisor } = req.query['map']
    const { account_name, real_name, position, phone } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ real_name: real_name }, acValidator.realname_value)
        validateCgi({ account_name: account_name }, acValidator.accountname_value)
        await db.testAccountName(database_name, account_name, phone)
        let password = `${md5sum(phone.substring(5))}`
        let result = await db.addAccount(account_name, password, real_name, database_name, position, phone, supervisor)
        if (result == 1) {
            return sendOk(res, '新建成功')
        }
    } catch (e) {
        pc.error(`account_add-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 刷新获取某账号的信息
* Author：鲁欣欣
* Date：2018-10-15
*/
router.post('/select_info', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { account_id } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectInfo(database_name, account_id)
        return sendOk(res, result)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 修改账号
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/account_update', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { account_id, real_name, position, phone } = (req as any).body

    try {
        validateCgi({ real_name: real_name }, acValidator.realname_value)
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.updateAccount(database_name, account_id, real_name, position, phone)
        if (result == 1) {
            return sendOk(res, '修改成功')
        }
    } catch (e) {
        pc.error(`account_update-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除账号
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/account_delete', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { account_id } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.deleteAccount(database_name, account_id)
        if (result == 1) {
            return sendOk(res, '删除成功')
        }
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 限制登陆
* Author：鲁欣欣
* Date：2018-09-28
*/
router.post('/account_restrict', login.loginCheck, async function (req: express.Request, res: express.Response) {
    const { operate_power, database_name } = req.query['map']
    const { account_id, status } = (req as any).body

    try {
        if (operate_power.account !== 1) {
            return sendError(res, 'logintimeout')
        }
        let r = await db.restrictAccount(database_name, account_id, status)
        if (r == 1) {
            return sendOk(res, '修改成功')
        }

    } catch (e) {
        return sendError(res, e.message)
    }
})

