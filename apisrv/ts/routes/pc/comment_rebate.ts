/**********************************************
* 晒评价返利
* Author：鲁欣欣
* Date：2018-10-29
*/

import express = require('express')
import { sendOk, sendError, sendOkTable } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/comment_rebate')
import { validateCgi } from '../../lib/validator'
import acValidator = require('../../validator/pc_account')
import { log_pc as pc } from '../../lib/log'
export const router: express.Router = express.Router()

/**********************************************
* 新建活动时展示所有店铺
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/select_shop', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, shop_power, supervisor } = req.query['map']

    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        if (shop_power === '') {
            return sendOk(res, [])
        } else {
            let result = await db.selectShop(supervisor, shop_power)
            return sendOk(res, result)
        }
    } catch (e) {
        pc.error(`select_shop-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 新建晒评价返利活动
* Author：鲁欣欣
* Date：2018-10-23
*/
router.post('/add_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, supervisor, database_name, shop_power } = req.query['map']
    const { event, start_time, end_time, sample_graph, s_amount, e_amount, shop, commodity, shop_id } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ event_name: event }, acValidator.event_value)
        if (s_amount < 1 || e_amount > 20) {
            return sendError(res, '奖励金额须在1-20之间。')
        }

        let str: string[] = []
        if (shop_id !== '') {
            str = JSON.parse(`[${shop_id}]`)
        }
        let res1
        if (shop_power !== '') {

            res1 = await db.selectShop(supervisor, shop_power)
        }
        else {
            return sendError(res, '您没有店铺权限，无法创建活动。')
        }
        let shoparr: string[] = []
        for (let i = 0; i < res1.length; i++) {
            if (str.indexOf(res1[i].id) === -1) {
                shoparr.push(res1[i].id)
            }
        }

        if (shoparr.length === 0) {
            return sendError(res, '该活动没有店铺可参与')
        }
        let result = await db.addEvent(supervisor, database_name, event, start_time, end_time, sample_graph, s_amount, e_amount, shop, commodity, shoparr)
        if (result === 1) {
            return sendOk(res, '新建活动成功！')
        }
    } catch (e) {
        pc.error(`add_event1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看晒评价返利活动
* Author：鲁欣欣
* Date：2018-10-23
*/
router.post('/select_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name, shop_power } = req.query['map']
    const { event, start_time, end_time, start, length } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectEvent(shop_power, database_name, start, length, event, start_time, end_time)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        pc.error(`select_event1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 编辑晒评价返利活动
* Author：鲁欣欣
* Date：2018-10-23
*/
router.post('/update_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id, event, start_time, end_time, sample_graph, s_amount, e_amount, shop, commodity, shop_id } = (req as any).body


    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        validateCgi({ event_name: event }, acValidator.event_value)
        let result = await db.updateEvent(database_name, event_id, event, start_time, end_time, sample_graph, s_amount, e_amount, shop, commodity, shop_id)
        if (result === 1) {
            return sendOk(res, '编辑活动成功！')
        }
    } catch (e) {
        pc.error(`update_event1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 编辑已上线和已下线晒评价返利活动的时间和次数
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/update_event_time', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id, end_time } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.updateEventTime(database_name, event_id, end_time)
        if (result === 1) {
            return sendOk(res, '编辑活动成功！')
        }
    } catch (e) {
        pc.error(`update_event_time1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 删除晒评价返利活动
* Author：鲁欣欣
* Date：2018-10-23
*/
router.post('/delete_event', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { event_id } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.deleteEvent(database_name, event_id)
        if (result === 1) {
            return sendOk(res, '删除活动成功！')
        }
    } catch (e) {
        pc.error(`delete_event1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 查看晒评价返利活动记录
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/select_record', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name, shop_power } = req.query['map']
    const { event, order_id, nickname, shop_name, status, start_time, end_time, start, length } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.selectRecord(shop_power, database_name, start, length, event, order_id, nickname, shop_name, status, start_time, end_time)
        return sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)

    } catch (e) {
        pc.error(`select_record-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 人工审核对自动审核失败的记录给予通过
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/approve', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id, event_id } = (req as any).body


    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.approve(database_name, id)
        if (result === 1) {
            /**活动已参与次数加一 */
            await db.addEventNum(database_name, event_id)
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`approve1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 人工审核确认审核失败
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/unapprove', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body

    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.unapprove(database_name, id)
        if (result === 1) {
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`unapprove1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 对单条记录返利
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/rebate', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { id } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.rebate(database_name, id)
        if (result === 1) {
            return sendOk(res, '操作成功！')
        }
    } catch (e) {
        pc.error(`rebate1-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 批量返利
* Author：鲁欣欣
* Date：2018-10-29
*/
router.post('/batch_rebate', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { ids } = (req as any).body
    try {
        if (operate_power.review !== 1) {
            return sendError(res, 'logintimeout')
        }
        await db.batchRebate(database_name, ids)
        return sendOk(res, '操作成功！')

    } catch (e) {
        pc.error(`batch_rebate1-> ${e.message}`)
        return sendError(res, e.message)
    }
})
