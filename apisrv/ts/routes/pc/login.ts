/**********************************************
* 页面登录, 退出登录，重置密码
* Author：鲁欣欣
* Date：2018-09-26
*/

import express = require('express')
import { sendOk, sendError, sendDomain, md5sum, randomInt } from "../../lib/utils"
import login = require('../../dao/pc/login')
export const router: express.Router = express.Router()
import { Account } from "../../entity/account"
import { PcSendCode, PccheckCode } from '../../lib/sms'
import { validateCgi } from '../../lib/validator'
import acValidator = require("../../validator/pc_account")
import { updPersonalPasswd } from '../../dao/pc/login'
import { log_pc as pc } from '../../lib/log'

let captchapng = require('captchapng');

/**********************************************
* 主域名登录
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/rootlogin', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { username, password } = (req as any).body

    try {

        //读取数据库，检查账号，密码
        let account_info: Account = await login.selectroot(username, password)

        let account_id: number = account_info.getAccountId()
        let account_name: string = account_info.getAccountName()
        let token: string = account_info.getToken()
        //设置token, 存入redis
        login.setLoginAsync(token, JSON.stringify(account_info))
        let expires_time = 7200000
        res.cookie("token", token, { maxAge: expires_time, httpOnly: false })
        res.cookie("account_id", account_id, { maxAge: expires_time, httpOnly: false })
        res.cookie("account_name", account_name, { maxAge: expires_time, httpOnly: false })
        return sendOk(res, account_info)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 公司账号登录
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/login', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { company_name, username, password } = (req as any).body
    console.log(company_name, username, password)
    console.log(req.headers.referer)
    try {

        let domain
        let database_name
        let nick_name
        let head_img
        let account_info: Account


        console.log('登录---------------------')
        /**找公司id */
        let result = await login.selectCompanyId(company_name)
        let company_id = result.id
        domain = result.domain
        database_name = result.database_name

        /**查找公众号信息 */
        console.log('公司id：' + company_id)
        let res1 = await login.selectwx(company_id)
        nick_name = res1.nick_name
        head_img = res1.head_img
        console.log(nick_name, head_img)

        //读取数据库，检查账号，密码
        account_info = await login.selectAccount(username, password, company_id, company_name)
        account_info.database_name = database_name
        console.log(account_info)

        if (username !== 'test77' && req.headers.referer !== `http://${domain}/`) {
            return sendDomain(res, domain + '/#/llogin')
        }

        let account_id: number = account_info.getAccountId()
        let account_name: string = account_info.getAccountName()
        let supervisor: string = account_info.getSupervisor()
        let companyname: string = account_info.getCompany_name()
        let token: string = account_info.getToken()
        let operate_power: string = account_info.getOperate_power()
        let shop_power: string = account_info.getShop_power()

        //设置token, 存入redis
        login.setLoginAsync(token, JSON.stringify(account_info))
        let expires_time = 7200000
        res.cookie("token", token, { maxAge: expires_time, httpOnly: false })
        res.cookie("account_id", account_id, { maxAge: expires_time, httpOnly: false })
        res.cookie("account_name", account_name, { maxAge: expires_time, httpOnly: false })
        res.cookie("nick_name", nick_name, { maxAge: expires_time, httpOnly: false })
        res.cookie("head_img", head_img, { maxAge: expires_time, httpOnly: false })
        res.cookie("supervisor", supervisor, { maxAge: expires_time, httpOnly: false })
        res.cookie("company_name", companyname, { maxAge: expires_time, httpOnly: false })
        res.cookie("operate_power", JSON.stringify(operate_power), { maxAge: expires_time, httpOnly: false })
        res.cookie("shop_power", `[${shop_power}]`, { maxAge: expires_time, httpOnly: false })
        sendOk(res, account_info)
    } catch (e) {
        return sendError(res, e.message)
    }
})


/**********************************************
* 图形验证码
* Author：鲁欣欣
* Date：2018-10-10
*/
router.post('/image', async function (req: any, res: express.Response, next: express.NextFunction) {

    try {
        /*
        * 生成数字随机数图片验证码
        *
        * @description 仅支持生成随机数字图片验证码，
        * */

        //返回统一格式赋值
        //let response = _.extend({},responseData);
        let response = {};

        console.log('开始获取图片验证码。。。。。。');
        let code = randomInt(1000, 10000)
        let capt = new captchapng(80, 30, code); // 图片宽度,图片高度,随机数字
        capt.color(0, 0, 0, 0);  // First color: 图片背景色 (red, green, blue, alpha)
        capt.color(80, 80, 80, 255); // Second color: 数字颜色 (red, green, blue, alpha)
        //转换成base64
        let icodeImg = capt.getBase64();
        response = {
            code: md5sum(String(code)),
            content: `data:image/png;base64,${icodeImg}`
        };
        return sendOk(res, response)

    } catch (e) {
        console.log(e)
        return sendError(res, e.message)
    }
})

/**********************************************
* 登出
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/logout', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { token } = req.query['map']
    try {
        await login.delLoginToken(token)
        return sendOk(res, 'ok')
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 输入账号名用于找回密码
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/forgot_passwd', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { account_name, company_name } = (req as any).body

    try {
        /**找公司id */
        let result = await login.selectCompanyId(company_name)
        let database_name = result.database_name
        let r = await login.selectAccountName(database_name, account_name)
        return sendOk(res, r)
    } catch (e) {
        pc.error(`forgot_passwd-> ${e.message}`)
        return sendError(res, e.message)
    }
})

/**********************************************
* 接收验证码
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/receive_code', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { phone, supervisor } = (req as any).body

    try {
        validateCgi({ phone: phone }, acValidator.phone_value)
        let result = await login.cheakSms(supervisor)
        if (result === 1) {
            return sendError(res, '该公司短信余额不足')
        }
        await PcSendCode(phone)
        await login.reduceSms(supervisor)
        return sendOk(res, '验证码获取成功')

    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 输入新密码和验证码重置密码
* Author：鲁欣欣
* Date：2018-09-26
*/
router.post('/reset_passwd', async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { account_id, password, phone, code, supervisor } = (req as any).body

    try {
        validateCgi({ password: password }, acValidator.passwd)
        validateCgi({ code: code }, acValidator.phone_code)
        await PccheckCode(phone, code)
        let r = await updPersonalPasswd(supervisor, account_id, password)
        if (r == 1) {
            /**如果该账号为公司管理者账号，同时更新账号密码到平台公司账号管理表 */
            await login.updPasswdToPlatform(supervisor, password, phone)
            return sendOk(res, '重置密码成功!')
        }

    } catch (e) {
        pc.error(`reset_passwd-> ${e.message}`)
        return sendError(res, e.message)
    }
})