import { log_pc as pc } from '../../lib/log'
//send memory info per seconds
export function serverstatus() {
    try {
        let WebSocketServer = require('ws').Server
        let wss = new WebSocketServer({ port: 8081 })
        console.log('监听8081端口')

        let spawn = require('child_process').spawn
        let mem: any = {}
        let cpu: any = {}
        let io: any = {}
        function sendMemInfo(ws: any) {
            let free = spawn('free', ['-k'])
            free.stderr.on('data', function (data: any) {
                console.log(`stderr: ${data}`)
            });
            free.stdout.on('data', function (data: any) {
                let memdata = "" + data
                //正则匹配，获取数据
                let re = /Mem: *(\d*) *(\d*) *(\d*) *(\d*) *(\d*) *(\d*)/
                let result = memdata.match(re);

                if (result.length > 0) {
                    mem["total"] = parseInt(result[1])
                    mem["used"] = parseInt(result[2])
                    mem["free"] = parseInt(result[3])
                    mem["shared"] = parseInt(result[4])
                    mem["bufferse"] = parseInt(result[5])

                }
            })

            let iostat = spawn('iostat')
            iostat.stdout.on('data', function (data: any) {
                let iodata = "" + data
                if (iodata.includes('avg-cpu') && iodata.includes('Device') && iodata.length > 300) {
                    iodata = iodata.split("avg-cpu")[1]

                    //正则匹配，获取数据
                    let re1 = /(\d*\.*\d+)/g
                    let ioresult = iodata.match(re1);

                    if (ioresult !== null && ioresult.length > 0) {
                        cpu["user"] = ioresult[0]
                        cpu["nice"] = ioresult[1]
                        cpu["system"] = ioresult[2]
                        cpu["iowait"] = ioresult[3]
                        cpu["steal"] = ioresult[4]
                        cpu["idle"] = ioresult[5]
                        io["kB_read/s"] = ioresult[7]
                        io["kB_wrtn/s"] = ioresult[8]
                    }
                }
            })
            iostat.stderr.on('data', function (data: any) {
                console.log(`stderr: ${data}`)
            });
            iostat.on('close', function (code: any) {
                if (code !== 0) {
                    console.log(`进程退出码：${code}`)
                }
            })
            if (mem && cpu && io) {
                //console.log('#####' + JSON.stringify(mem), JSON.stringify(cpu), JSON.stringify(io))
                let resdata = { mem: mem, cpu: cpu, io: io }
                ws.send(JSON.stringify(resdata))
            }
        }

        wss.on('connection', function (ws: any) {
            let clientMemUpdater: any
            let sendMemUpdates = function (ws: any) {
                if (ws.readyState == 1) {
                    sendMemInfo(ws)
                }
            }
            clientMemUpdater = setInterval(function () {
                sendMemUpdates(ws)
            }, 5000)
            sendMemUpdates(ws)
            ws.on("close", function () {
                if (typeof (clientMemUpdater) != 'undefined') {
                    clearInterval(clientMemUpdater)
                }
            })
        })
    } catch (e) {
        console.log('error:' + e.message)
        pc.error(`8081server-> ${e.message}`)
        return
    }
}
