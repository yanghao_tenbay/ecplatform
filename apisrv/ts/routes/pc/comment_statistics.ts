/**********************************************
* 晒评价数据统计
* Author：鲁欣欣
* Date：2018-11-1
*/

import express = require('express')
import { sendError, sendOkStat } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/comment_statistics')
import { log_pc as pc } from '../../lib/log'
export const router: express.Router = express.Router()

/**********************************************
* 按天统计
* Author：鲁欣欣
* Date：2018-11-1
*/
router.post('/day_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        if (operate_power.reviewStat !== 1) {
            return sendError(res, 'logintimeout')
        }

        let result = await db.dayStatistics(database_name)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})

/**********************************************
* 按月统计
* Author：鲁欣欣
* Date：2018-11-1
*/
router.post('/month_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        if (operate_power.reviewStat !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.monthStatistics(database_name)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        return sendError(res, e.message)
    }
})


/**********************************************
* 按时间段统计 限制最长30天
* Author：鲁欣欣
* Date：2018-11-5
*/
router.post('/time_statistics', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { start_time, end_time, start, length } = (req as any).body
    try {
        if (operate_power.reviewStat !== 1) {
            return sendError(res, 'logintimeout')
        }
        let result = await db.timeStatistics(database_name, start_time, end_time, start, length)
        return sendOkStat(res, result.recordsFiltered, result.recordsTotal, result.total_amount, result.total_number, result.data)
    } catch (e) {
        pc.error(`time_statistics1-> ${e.message}`)
        return sendError(res, e.message)
    }
})