import express = require('express')
import { getTap, joinBlack, userInfo, addTap, updateTap, delTap, searchUser, getUser, getTapInfo, joinTap, outBlack, getRebate, getCredit, getOrder, getComment } from '../../dao/pc/daoUseManage';
import { sendOk, sendError, sendOkTable, formatDate2 } from '../../lib/utils';
import { validateCgi } from "../../lib/validator"
export const router: express.Router = express.Router()
import pcValidator = require("../../validator/pcUserTap")
import login = require('../../dao/pc/login')

/**********************************************
 * 获取标签
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getTap', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        if (operate_power.user == 1) {
            let result = await getTap(database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取用户
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getUser', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { start, length, nickname, phone, tap_name } = (req as any).body
    const { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ start, length }, pcValidator.limit)
            let result
            if (nickname != '' || phone != '' || tap_name != '') {
                nickname === '' ? '' : validateCgi({ nickname }, pcValidator.nickName)
                phone === '' ? '' : validateCgi({ phone }, pcValidator.Phone)
                tap_name === '' ? '' : validateCgi({ tap_name }, pcValidator.tapName)
                result = await searchUser(nickname, phone, tap_name, start, length, database_name)
            } else {
                result = await getUser(start, length, database_name)
            }
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 加入黑名单
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/joinBlack', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId: userId }, pcValidator.Ujson)
            let result = await joinBlack(userId, database_name)
            if (result == 1) {
                sendError(res, '加入黑名单失败')
            } else {
                sendOk(res, '加入黑名单成功')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 解除黑名单
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/outBlack', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId }, pcValidator.userId)
            let result = await outBlack(userId, database_name)
            if (result == 1) {
                sendError(res, '解除黑名单失败')
            } else {
                sendOk(res, '解除黑名单成功')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 加入标签
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/joinTap', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId, tap_id } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId: userId }, pcValidator.Ujson)
            validateCgi({ tap_id }, pcValidator.tap_Id)
            let result = await joinTap(userId, tap_id, database_name)
            if (result == 1) {
                sendError(res, '加入标签失败')
            } else {
                sendOk(res, '加入标签成功')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取用户信息
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/userInfo', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId }, pcValidator.userId)
            let result = await userInfo(userId, database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 用户订单详情
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getComment', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId, start, length } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId }, pcValidator.userId)
            validateCgi({ start, length }, pcValidator.limit)
            let result = await getComment(userId, database_name, start, length)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限查看用户的交易记录')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 用户订单详情
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getOrder', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId, start, length } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId }, pcValidator.userId)
            validateCgi({ start, length }, pcValidator.limit)
            let result = await getOrder(userId, database_name, start, length)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限查看用户的交易记录')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 用户订单详情
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getRebate', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { userId, start, length } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ userId }, pcValidator.userId)
            validateCgi({ start, length }, pcValidator.limit)
            let result = await getRebate(userId, database_name, start, length)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限查看用户的交易记录')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取标签列表
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/getTapInfo', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { start, length, search } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ start, length }, pcValidator.limit)
            validateCgi({ search }, pcValidator.Search)
            let result = await getTapInfo(database_name, start, length, search)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 新建标签
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/newTap', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { tap_name } = (req as any).body
    const { database_name, account_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ tap_name }, pcValidator.addTap)
            let result = await addTap(tap_name, account_name, database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 修改标签
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/updTap', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { tap_id, tap_name } = (req as any).body
    let { database_name, account_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ tap_id, tap_name, }, pcValidator.updTap)
            let result = await updateTap(tap_id, tap_name, account_name, database_name)
            if (result == 1) {
                sendError(res, "修改标签失败")
            } else {
                sendOk(res, '修改标签成功')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 删除标签
 * Author：何语漫
 * Date：2018-09-26
 */
router.post('/delTap', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { tap_id } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user == 1) {
            validateCgi({ tap_id }, pcValidator.tap_Id)
            let result = await delTap(tap_id, database_name)
            if (result == 1) {
                sendError(res, "删除标签失败")
            } else {
                sendOk(res, '删除标签成功')
            }
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

router.post('/getCredit', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { nickname, phone, rule, start_date, end_date, start, length } = (req as any).body
    let { database_name, operate_power } = req.query['map']
    try {
        if (operate_power.user !== 1) {
            return sendError(res, '无权限')
        }
        nickname == '' ? '' : validateCgi({ nickname: nickname }, pcValidator.nickName)
        phone == '' ? '' : validateCgi({ phone: phone }, pcValidator.Phone)
        rule == '' ? '' : validateCgi({ rule }, pcValidator.RuLe)
        start_date == '' ? '' : validateCgi({ date: start_date }, pcValidator.SeachDate)
        end_date == '' ? '' : validateCgi({ date: end_date }, pcValidator.SeachDate)
        validateCgi({ start, length }, pcValidator.limit)
        let result = await getCredit(database_name, nickname, phone, rule, start_date, end_date, start, length)
        sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
    } catch (e) {
        sendError(res, e.message)
    }
})

