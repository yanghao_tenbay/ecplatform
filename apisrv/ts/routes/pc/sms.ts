import express = require('express')
import { sendOk, sendError, sendOkTable } from '../../lib/utils';
import { getSmsPcakge, saveTradeRecord, isTradeFinish, finishWxTrade, getSmsOrder, getRestSms } from '../../dao/pc/daoSms';
import { getWxpay } from '../../lib/wxconfig';
import { genPrePayUnifiedOrder, validateNotify } from '../../lib/wxpay';
import { notify } from '../../daemon/daemon';
import { loginCheck } from '../../dao/pc/login';
import { validateCgi } from '../../lib/validator';
import pcValidator = require("../../validator/pcUserTap")
import { log_pc as log } from '../../lib/log'
export const router: express.Router = express.Router()

/**********************************************
 * 获取短信剩余数量
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/restSms', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let { supervisor, operate_power } = req.query['map']
    try {
        if (operate_power.sms == 1) {
            let result = await getRestSms(supervisor)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取短信套餐
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/smsPackage', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let { operate_power } = req.query['map']
    try {
        if (operate_power.sms == 1) {
            let result = await getSmsPcakge()
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取短信订单
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/smsOrder', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { start, length, start_time, end_time, out_trade_no } = (req as any).body
    let { supervisor, operate_power } = req.query['map']
    try {
        if (operate_power.sms == 1) {
            validateCgi({ start, length, start_time, end_time }, pcValidator.order)
            if (out_trade_no !== undefined && out_trade_no !== '') {
                validateCgi({ out_trade_no }, pcValidator.outTradeNo)
            }
            let result = await getSmsOrder(start, length, supervisor, start_time, end_time, out_trade_no)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        log.error(`/smsOrder: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 下单
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/good', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let { package_id, name, price } = (<any>req).body
    let { account_id, supervisor, operate_power } = req.query['map']
    try {
        if (operate_power.sms == 1) {
            //校验
            validateCgi({ package_id, name, price }, pcValidator.good)
            let wxpay = await getWxpay()
            package_id = parseInt(package_id)
            price = parseFloat(parseFloat(price).toFixed(2))
            let order = await genPrePayUnifiedOrder({
                appid: wxpay.appid,
                mch_id: wxpay.mch_id,
                body: name,
                total_fee: price * 100,
                spbill_create_ip: "192.168.0.6",
                notify_url: wxpay.notify_url,
                product_id: package_id, //商户自定义商品ID
                timeout: 600,
                key: wxpay.key
            })
            //存入数据库
            await saveTradeRecord(order, account_id, supervisor)
            //返回二维码链接给前端
            sendOk(res, { code_url: order.getCode_url(), out_trade_no: order.getOutTradeNo() })
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        log.error(`/good: ${e.message}`)
        sendError(res, e.message)
    }
})

// 读取POST的body
async function readRawBody(req: any, res: any, next: any) {
    let arr = new Array<string>()
    req.on('data', function (chunk: any) {
        arr.push(chunk.toString())
    })

    req.on('end', function (chunk: any) {
        if (chunk) {
            arr.push(chunk.toString())
        }
        req.rawBody = arr.join()
        next()
    })
}

/**********************************************
 * 判断是否支付
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/judgePay', loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { out_trade_no } = (<any>req).body
    let { operate_power } = req.query['map']
    try {
        if (operate_power.sms == 1) {
            validateCgi({ out_trade_no }, pcValidator.outTradeNo)
            let finish = await isTradeFinish(out_trade_no)
            if (finish) {
                sendOk(res, '充值成功')
            } else {
                sendError(res, '用户未充值')
            }
        } else {
            sendError(res, '无权限')
        }
    }
    catch (e) {
        log.error(`/judgePay: ${e.message}`)
        sendError(res, e.message)
    }
})

/**********************************************
 * 微信通知支付成功接口
 * Author：何语漫
 * Date：2018-9-27
 */
router.post('/notify', readRawBody, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        let body = (<any>req).rawBody
        let wxpay = await getWxpay()
        // 校验
        let obj = await validateNotify(body, wxpay)

        // 检查订单是否已经完成
        let out_trade_no = obj.out_trade_no
        let finish = await isTradeFinish(out_trade_no)
        if (finish) {
            return sendTradeOK(res)
        }

        // 设置订单完成
        await finishWxTrade(out_trade_no)

        // 通知后台进程已经充值  充入幻想币和包含的优惠卷
        notify("wxpay")

        return sendTradeOK(res)
    } catch (e) {
        log.error(`/notify: ${e.message}`)
        return sendTradeFail(res)
    }
})

/**
 * 接受异步通知成功时发送xml数据
 */
function sendTradeOK(res: express.Response) {
    res.send(`<xml> <return_code><![CDATA[SUCCESS]]></return_code> <return_msg><![CDATA[OK]]></return_msg> </xml>`)
}

/**
 * 接受异步通知失败时发送xml数据
 */
function sendTradeFail(res: express.Response) {
    res.status(403).send(`<xml> <return_code><![CDATA[FAIL]]></return_code> <return_msg><![CDATA[BAD]]></return_msg> </xml>`)
}