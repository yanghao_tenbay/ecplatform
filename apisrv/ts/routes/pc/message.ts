/**********************************************
* 消息通知
* Author：鲁欣欣
* Date：2018-11-14
*/

import express = require('express')
import { sendOk, sendError, formatDate2 } from "../../lib/utils"
import login = require('../../dao/pc/login')
import db = require('../../dao/pc/message')
import { getAsync } from '../../lib/request'
import { log_pc as pc } from '../../lib/log'
export const router: express.Router = express.Router()
/**********************************************
* 获取消息通知
* Author：鲁欣欣
* Date：2018-11-14
*/
router.post('/get_message', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { supervisor, shop_power, operate_power, database_name, account_id } = req.query['map']

    try {
        let data = []
        let shop_auth = { type: '店铺授权到期通知', msg: `尊敬的用户，您的店铺XXX授权剩余3天，为了保证活动的正常运行，请重新授权。` }
        let sms_msg = { type: '短信套餐不足', msg: `尊敬的用户，您的短信套餐条数仅剩15条，为保证部分功能的正常使用，请及时充值，谢谢。` }
        let update_msg = { type: '更新日志', msg: `` }
        let platform_msg = { type: '平台授权到期通知', msg: `尊敬的用户，您的电商平台授权有效期仅剩X天，请及时完成续费工作。逾期未完成续费，将视为您主动放弃续费。` }
        let login_msg = { type: '账号登录异常', msg: `如果不是您本人操作，密码可能已经泄露，强烈建议立即修改密码。` }
        /**检查自己管理的店铺是否过期 */
        if (shop_power !== '') {
            let res1 = await db.checkShop(supervisor, shop_power)
            if (res1.length > 0) {
                for (let i = 0; i < res1.length; i++) {
                    shop_auth.msg = `尊敬的用户，您的店铺 ${res1[i].shop_name} 授权剩余 ${res1[i].time} 天，为了保证活动的正常运行，请重新授权。`
                    data.push(shop_auth)
                }
            }
        }
        /**检查短信条数 */
        if (operate_power.sms === 1) {
            let res2 = await db.checkSms(supervisor)
            if (res2.length > 0) {
                sms_msg.msg = `尊敬的用户，您的短信套餐条数仅剩 ${res2[0].sms} 条，为保证部分功能的正常使用，请及时充值，谢谢。`
                data.push(sms_msg)
            }
        }
        /** 更新日志*/
        let update_log = `平台基本功能完善，欢迎使用`       //更新内容提示
        let endtime = `2018-12-06 11:30:48`         //提示截至时间
        if (formatDate2(new Date) < endtime) {
            update_msg.msg = update_log
            data.push(update_msg)
        }
        /**平台授权到期通知 */
        let res3 = await db.checkCompany(supervisor)
        if (res3.length > 0) {
            platform_msg.msg = `尊敬的用户，您的电商平台授权有效期仅剩${res3[0].time}天，请及时完成续费工作。逾期未完成续费，将视为您主动放弃续费。`
            data.push(platform_msg)
        }
        /**登录异常通知 */
        let ip = req.headers.realip
        console.log(ip)
        if (ip) {
            let ak = await db.getAk()
            let url = `http://api.map.baidu.com/location/ip?ip=${ip}&ak=${ak}&coor=bd09ll`
            let apiResult = await getAsync(url)
            if (JSON.parse(apiResult).status === 0) {
                let address = JSON.parse(apiResult).content.address
                console.log(address)
                /**读取账号登录信息 */
                let login_address = await db.getLoginAddress(database_name, account_id)
                login_address = JSON.parse(login_address)
                console.log(login_address)
                if (!login_address) {
                    let newlogin = [{ address: address, num: 1 }]
                    console.log(newlogin)
                    /**更新登录信息 */
                    await db.updateLoginAddress(database_name, account_id, JSON.stringify(newlogin))
                }
                else {
                    let flag = 0
                    for (let i = 0; i < login_address.length; i++) {
                        if (login_address[i].address === address) {
                            flag = 1
                            if (i === login_address.length - 1 || login_address[i].num >= 7) {
                                login_address[i].num++
                            }
                            if (login_address[i].num > 7 && i < login_address.length - 1) {
                                login_msg.msg = `如果不是您本人操作，密码可能已经泄露，强烈建议立即修改密码。异常登陆地：${login_address[login_address.length - 1].address}`
                                data.push(login_msg)
                            }
                            for (let j = i; j < login_address.length - 1; j++) {
                                let temp = login_address[j]
                                login_address[j] = login_address[j + 1]
                                login_address[j + 1] = temp
                                if (j === login_address.length - 2 && login_address[j + 1].num < 7) {
                                    login_address[j + 1].num = 1
                                }
                            }
                            break
                        }
                    }
                    if (flag === 0) {
                        login_address.push({ address: address, num: 1 })
                    }
                    /**更新登录信息 */
                    await db.updateLoginAddress(database_name, account_id, JSON.stringify(login_address))
                }
            }
        }
        else {
            console.log('未获取到ip')
            pc.error(`message->未获取到ip`)
        }


        return sendOk(res, data)


    } catch (e) {
        pc.error(`message-> ${e.message}`)
        return sendError(res, e.message)
    }
})