import express = require('express')
import { sendOk, sendError, sendOkTable, randomInt } from '../../lib/utils';
import { validateCgi } from "../../lib/validator"
import pcValidator = require("../../validator/pcUserTap")
export const router: express.Router = express.Router()
import login = require('../../dao/pc/login')
import { getGift, newGift, updGift, delGift, getGiftRecord, savePic, getGiftName, attendenceRule, showPic, delPic, selectPic } from '../../dao/pc/creditsManage';
import fs = require('fs')
let multiparty = require('multiparty')

/**********************************************
 * 获取礼品名字
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/getGiftName', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            let result = await getGiftName(database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取礼品
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/getGift', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { gift_name, startcredits, endcredits, status, start, length } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            gift_name == '' ? '' : validateCgi({ gift_name }, pcValidator.GiftName)
            startcredits == '' ? '' : validateCgi({ credits: startcredits }, pcValidator.Credit)
            endcredits == '' ? '' : validateCgi({ credits: endcredits }, pcValidator.Credit)
            status == '' ? '' : validateCgi({ status: status }, pcValidator.status)
            validateCgi({ start, length }, pcValidator.limit)
            let result = await getGift(database_name, gift_name, startcredits, endcredits, status, start, length)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 新建礼品
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/newGift', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            validateCgi({ gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time }, pcValidator.NewGift)
            let result = await newGift(database_name, gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 修改礼品
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/updGift', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { gift_id, gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            validateCgi({ gift_id, gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time }, pcValidator.UpdGift)
            let result = await updGift(database_name, gift_id, gift_name, gift_cove, gift_type, gift_num, credits, price, create_time, end_time)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 删除礼品
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/delGift', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { gift_id } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            validateCgi({ gift_id }, pcValidator.delGift)
            let result = await delGift(database_name, gift_id)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取礼品记录
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/getGiftRecord', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { gift_name, nickname, status, start, length } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralEvent == 1) {
            gift_name == '' ? '' : validateCgi({ gift_name }, pcValidator.GiftName)
            nickname == '' ? '' : validateCgi({ nickname: nickname }, pcValidator.nickName)
            status == '' ? '' : validateCgi({ status: status }, pcValidator.status)
            validateCgi({ gift_name, nickname, status, start, length }, pcValidator.getGiftRecord)
            let result = await getGiftRecord(database_name, gift_name, nickname, status, start, length)
            sendOkTable(res, result.recordsFiltered, result.recordsTotal, result.data)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 保存礼品
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/savePic', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { data, credits } = (req as any).body
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralSet == 1) {
            validateCgi({ data, credits }, pcValidator.savePic)
            let result = await savePic(database_name, data, credits)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 显示商城配置
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/showPic', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralSet == 1) {
            let result = await showPic(database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 删除商城配置
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/delPic', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    const { data } = (req as any).body
    try {
        //权限检查
        if (operate_power.integralSet == 1) {
            validateCgi({ gift_id: data }, pcValidator.delGift)
            let result = await delPic(database_name, data)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})

/**********************************************
 * 获取商品配置
 * Author：何语漫
 * Date：2018-10-27
 */
router.post('/selectPic', login.loginCheck, async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    const { operate_power, database_name } = req.query['map']
    try {
        //权限检查
        if (operate_power.integralSet == 1) {
            let result = await selectPic(database_name)
            sendOk(res, result)
        } else {
            sendError(res, '无权限')
        }
    } catch (e) {
        sendError(res, e.message)
    }
})


/**********************************************
* 上传图片
* Author：lxx
* Date：2018-09-08
*/
router.post('/image_upload', login.loginCheck, function (req: express.Request, res: express.Response) {

    let form = new multiparty.Form()//新建表单
    //设置编辑
    form.encoding = 'utf-8'
    //设置图片存储路径
    form.uploadDir = "/var/data/uploads/images/"
    form.keepExtensions = true   //保留后缀
    form.maxFieldsSize = 2 * 1024 * 1024  //内存大小
    form.maxFilesSize = 5 * 1024 * 1024  //文件字节大小限制，超出会报错err

    //表单解析
    form.parse(req, function (err: any, fields: any, files: any) {
        //报错处理
        if (err) {
            console.log(err)
            let u = { status: 1, msg: '图片过大' }
            return res.send(JSON.stringify(u))
        }
        //获取路径
        let oldpath = files.file[0]['path']
        let suffix
        //文件后缀处理格式
        if (oldpath.indexOf('.jpg') >= 0) {
            suffix = '.jpg'
        } else if (oldpath.indexOf('.png') >= 0) {
            suffix = '.png'
        } else if (oldpath.indexOf('.jpeg') >= 0) {
            suffix = '.jpeg'
        } else {
            let u = { status: 1, msg: '请上传正确格式:jpg\\png\\jpeg' }
            return res.send(JSON.stringify(u))
        }
        let newpath = form.uploadDir + Date.now() + randomInt(10000, 99999) + suffix
        //给图片修改名称
        fs.renameSync(oldpath, newpath)
        let url = `${newpath.substring(9)}`
        let u = { status: 0, data: url }
        return res.send(JSON.stringify(u))
    })
})
