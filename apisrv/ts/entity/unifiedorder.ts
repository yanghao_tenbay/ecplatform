interface IUnifiedOrder {
    appid: string
    attach: string
    body: string
    mch_id: string
    nonce_str: string
    time_start: string
    time_expire: string
    notify_url: string
    product_id: string
    //    openid: string
    out_trade_no: string
    spbill_create_ip: string
    total_fee: number
    trade_type: string
    device_info: string
}

export class UnifiedOrder {
    protected appid: string
    protected attach: string
    protected body: string
    protected mch_id: string
    protected nonce_str: string
    protected time_start: string
    protected time_expire: string
    protected notify_url: string
    //   protected openid: string
    protected out_trade_no: string
    protected spbill_create_ip: string
    protected total_fee: number
    protected trade_type: string
    protected device_info: string
    protected product_id: string
    protected code_url: string

    constructor(r: IUnifiedOrder) {
        this.appid = r.appid
        this.attach = r.attach
        this.body = r.body
        this.mch_id = r.mch_id
        this.nonce_str = r.nonce_str
        this.time_start = r.time_start
        this.time_expire = r.time_expire
        this.notify_url = r.notify_url
        //       this.openid = r.openid
        this.product_id = r.product_id
        this.out_trade_no = r.out_trade_no
        this.spbill_create_ip = r.spbill_create_ip
        this.total_fee = r.total_fee
        this.trade_type = r.trade_type
        this.device_info = r.device_info
    }
}

export class SignUnifiedOrder extends UnifiedOrder {
    private sign: string
    constructor(r: UnifiedOrder, sign: string) {
        super(r as any)
        this.sign = sign
    }
}


export class PrePayUnifiedOrder extends UnifiedOrder {
    private prepay_id: string
    constructor(r: UnifiedOrder, prepay_id: string, code_url: string) {
        super(r as any)
        this.prepay_id = prepay_id
        this.code_url = code_url
    }

    public getPrepayId() {
        return this.prepay_id
    }

    public getAppid() {
        return this.appid
    }

    public getAttach() {
        return this.attach
    }

    public getBody() {
        return this.body
    }

    public getMchId() {
        return this.mch_id
    }

    public getNonce_str() {
        return this.nonce_str
    }

    public getTime_start() {
        return this.time_start
    }

    public getTime_expire() {
        return this.time_expire
    }

    public getNotify_url() {
        return this.notify_url
    }

    /*     public getOpenid() {
            return this.openid
        } */


    public getOutTradeNo() {
        return this.out_trade_no
    }

    public getProductId(){
        return this.product_id
    }

    public getSpbillCreateIp() {
        return this.spbill_create_ip
    }

    public getTotalFee() {
        return this.total_fee
    }

    public getTrade_type() {
        return this.trade_type
    }

    public getDevice_info() {
        return this.device_info
    }

    public getCode_url() {
        return this.code_url
    }
}

