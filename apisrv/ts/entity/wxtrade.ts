export class WxTrade {
    private account_id: string
    private out_trade_no: string
    private prepay_id: string
    private package_id: string
    private company_id: string
    private state: string
    private chargestate: string
    private appid: string
    private mch_id: string
    private body: string
    private total_fee: number
    private spbill_create_ip: string
    private created: string
    private modified: string
    private code_url: string
    constructor(r: any) {
        this.account_id = r.uuid
        this.out_trade_no = r.out_trade_no
        this.prepay_id = r.prepay_id
        this.state = r.state
        this.chargestate = r.chargestate
        this.appid = r.appid
        this.mch_id = r.mch_id
        this.body = r.body
        this.total_fee = r.total_fee
        this.spbill_create_ip = r.spbill_create_ip
        this.created = r.created
        this.modified = r.modified
        this.code_url = r.coupons_price
        this.package_id = r.package_id
        this.company_id = r.company_id
    }
    public getAccount_id() {
        return this.account_id
    }

    public getCompany_id() {
        return this.company_id
    }

    public getOut_trade_no() {
        return this.out_trade_no
    }

    public getPrepay_id() {
        return this.prepay_id
    }

    public getState() {
        return this.state
    }

    public getChargestate() {
        return this.chargestate
    }

    public getAppid() {
        return this.appid
    }

    public getMch_id() {
        return this.mch_id
    }

    public getBody() {
        return this.body
    }

    public getTotal_fee() {
        return this.total_fee
    }

    public getSpbill_create_ip() {
        return this.spbill_create_ip
    }

    public getCreated() {
        return this.created
    }

    public getModified() {
        return this.modified
    }

    public getCode_url() {
        return this.code_url
    }

    public getPackage_id() {
        return this.package_id
    }
}