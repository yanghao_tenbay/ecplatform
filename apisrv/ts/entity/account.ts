interface IAccount {
    account_id?: number;
    account_name?: string;
    password?: string;
    supervisor?: string;
    company_name?: string;
    real_name?: string;
    position?: string;
    operate_power?: string;
    shop_power?: string;
    phone?: string;
    token?: string

}
export class Account {
    private account_id?: number;        //账号id
    private account_name?: string;      //登录账号
    private password?: string;            //登录密码
    private supervisor?: string;        //账号所属公司id
    private company_name?: string;      //公司名称
    public database_name?: string      //数据库名
    private real_name?: string;         //真实姓名
    private position?: string;          //职位id
    private operate_power?: string;    //操作权限
    private shop_power?: string;        //店铺权限
    private phone?: string;             //手机号
    private token?: string;             //token校验
    constructor(r: IAccount) {
        this.account_id = r.account_id;
        this.account_name = r.account_name;
        this.password = r.password;
        this.operate_power = r.operate_power;
        this.shop_power = r.shop_power;
        this.supervisor = r.supervisor;
        this.company_name = r.company_name;
        this.real_name = r.real_name;
        this.position = r.position;
        this.phone = r.phone;
        this.token = r.token;
    }
    getAccountId(): number { return this.account_id }
    getAccountName(): string { return this.account_name }
    getSupervisor(): string { return this.supervisor }
    getCompany_name(): string { return this.company_name }
    getOperate_power(): string { return this.operate_power }
    getShop_power(): string { return this.shop_power }
    getPhone(): string { return this.phone }
    getToken(): string { return this.token }
}