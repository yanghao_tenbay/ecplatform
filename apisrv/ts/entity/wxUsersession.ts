
export class UserSession {
    private user_id: string
    private token: string
    private openid: string
    private appid: string
    private supervisor: number
    private database_name: string
    private phone: string


    constructor(user_id: string, token: string, openid: string, appid: string, supervisor: number, database_name: string, phone: string) {
        [this.user_id, this.token, this.openid, this.appid, this.supervisor, this.database_name, this.phone] = [user_id, token, openid, appid, supervisor, database_name, phone]
    }

    public static valueOf(s: string): UserSession {
        let obj = JSON.parse(s)
        if (!obj) {
            throw new Error("invalid PlayerInfo format")
        }

        let { user_id, token, openid, appid, supervisor, database_name, phone } = obj
        return new UserSession(user_id, token, openid, appid, supervisor, database_name, phone)
    }

    public getUser_id(): string {
        return this.user_id
    }

    public getToken(): string {
        return this.token
    }

    public getOpenId(): string {
        return this.openid
    }

    public getAppid(): string {
        return this.appid
    }

    public getSupervisor(): number {
        return this.supervisor
    }

    public getDatabaseName(): string {
        return this.database_name
    }
}
