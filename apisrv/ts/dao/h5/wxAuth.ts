import express = require('express')
import { getAsync } from "../../lib/request"
import { validateCgi } from "../../lib/validator"
import wxValidator = require("../../validator/wwxapp")
import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { getRandStr, formatDate3, formatDate2 } from "../../lib/utils"
import { UserSession } from '../../entity/wxUsersession'
import { getRedisClientAsync } from "../../lib/redispool"
import { MyError } from "../../entity/myerror"
import { rdb } from "../../config/redis"
import { getAuthorityConf } from '../../lib/compconfig';

/**************网页授权****************************
 * 入口， 生成token
 */
export async function getWxInfo(req: express.Request, res: express.Response, next: express.NextFunction) {
    const { code, appid } = req.query
    try {
        let mainSQL = 'ec_platform'
        validateCgi({ wxcode: code }, wxValidator.wxCode)
        let sql = `SELECT id, database_name FROM ${mainSQL}.company WHERE appid = '${appid}' AND status = 1`
        let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
        if (result.length == 0) {
            return res.redirect('/h5/index.html#/CompanyBlack')
        }
        let supervisor = result[0].id
        let database_name = result[0].database_name
        let rows = await reqWxInfo(appid, code, database_name)
        if (rows == 1) {
            return res.redirect('/h5/index.html#/error')
        }
        req.query['appid'] = appid
        req.query['supervisor'] = supervisor
        req.query['database_name'] = database_name
        req.query['user_id'] = rows.user_id
        req.query['openid'] = rows.openid
        req.query['phone'] = rows.phone
        req.query["token"] = await getRandStr(32)
        next()
    }
    catch (e) {
        console.log(e)
        return res.status(401).json({ error: e.message })
    }
}

/**********************************************
 * 获取用户信息，新用户插入数据库之后返回
 * Author：何语漫
 * Date：2018-9-30
 */
async function reqWxInfo(appid: string, code: string, database_name: string): Promise<any> {
    let comOpt = await getAuthorityConf()
    let tokenUrl = `https://api.weixin.qq.com/sns/oauth2/component/access_token?appid=${appid}&code=${code}&grant_type=authorization_code&component_appid=${comOpt.compAppid}&component_access_token=${comOpt.compAccessToken}`
    let tokenStr = await getAsync(tokenUrl)
    let tokenMap = await JSON.parse(tokenStr)
    console.log(tokenMap)
    if (tokenMap.errCode !== undefined) {
        throw new Error(`获取微信token错误，微信返回值：${tokenStr}`)
    }

    let sql = `select user_id, is_black, phone from ${database_name}.user where openid = '${tokenMap.openid}'`
    let rows = await await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (rows.length == 1) {
        //已经注册
        if (rows[0].is_black == 1) {
            console.log('您在黑名单内')
            return 1
        }
        if (rows[0].phone !== undefined && rows[0].phone !== null && rows[0] !== '') {
            return {
                user_id: rows[0].user_id,
                openid: tokenMap.openid,
                phone: rows[0].phone
            }
        }
        return {
            user_id: rows[0].user_id,
            openid: tokenMap.openid,
            phone: ''
        }
    }

    //未注册
    let wxInfoUrl = `https://api.weixin.qq.com/sns/userinfo?access_token=${tokenMap.access_token}&openid=${tokenMap.openid}&lang=zh_CN`
    let wxInfoStr = await getAsync(wxInfoUrl)
    let wxInfoMap = await JSON.parse(wxInfoStr)
    console.log(wxInfoMap)
    if (wxInfoMap.errcode !== undefined) {
        throw new Error(`获取微信用户信息错误，微信返回值:${wxInfoStr}`)
    }

    switch (wxInfoMap.sex) {
        case 2: wxInfoMap.sex = '女'; break;
        case 1: wxInfoMap.sex = '男'; break;
        default: wxInfoMap.sex = '未知'
    }

    let str = wxInfoMap.nickname
    let nickname = str.replace(/\\/g, "\\\\")
    nickname = nickname.replace(/'/g, "\\\'")
    let sql2 = `insert into ${database_name}.user(openid, city, province, sex, nickname, headimgurl) values('${wxInfoMap.openid}', '${wxInfoMap.city}','${wxInfoMap.province}','${wxInfoMap.sex}','${nickname}', '${wxInfoMap.headimgurl}')`
    console.log(sql2)
    await getConnectionAsync(async conn => conn.queryAsync(sql2))
    sql2 = `UPDATE ${database_name}.tap SET user_num = user_num + 1 WHERE tap_id = '1'`
    await getConnectionAsync(async conn => conn.queryAsync(sql2))
    sql = `select user_id from ${database_name}.user where openid = '${wxInfoMap.openid}'`
    rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return {
        user_id: rows[0].user_id,
        openid: tokenMap.openid,
        phone: ''
    }
}

/*****************redis***********************
 * 将用户信息存入redis
 * @param session 用户信息类
 */
const [sessionDbOpt, Sessiontimeout] = [{ db: rdb.user[0] }, rdb.user[1]]
async function setLoginAsync(session: UserSession) {
    const [token, content] = [session.getToken(), JSON.stringify(session)]
    await getRedisClientAsync(async client => {
        await client.setAsync(token, content)
        await client.expireAsync(token, Sessiontimeout)
    }, sessionDbOpt)
}

/**********************************************
 * 从redis读取用户信息
 * Author：何语漫
 * Date：2018-9-30
 */
export async function getLoginAsync(user_id: string | number, token: string, noThrow = false): Promise<UserSession> {
    if (!user_id || !token) {
        throw new MyError("用户未登陆！", 401)
    }

    let s = await getRedisClientAsync(async rds => await rds.getAsync(token), sessionDbOpt)
    if (!s) {
        throw new MyError("用户未登陆！", 401)
    }

    let info = UserSession.valueOf(s)
    if (token !== info.getToken()) {
        throw new MyError("用户未登陆！", 401)
    }
    return info
}

/**********************************************
 * 将用户信息存入redis
 * Author：何语漫
 * Date：2018-9-30
 */
export async function user2Redis(req: express.Request, res: express.Response, next: express.NextFunction) {
    const { openid, user_id, token, appid, supervisor, database_name, phone } = req.query
    try {
        let loginUser = new UserSession(user_id, token, openid, appid, supervisor, database_name, phone)
        await setLoginAsync(loginUser)
        res.cookie("user_id", user_id, { maxAge: 1500000, httpOnly: false })
        res.cookie("token", token, { maxAge: 1500000, httpOnly: false })
        return next()
    } catch (e) {
        return res.status(401).json({ error: e.message })
    }
}

/**********************************************
 * 登陆校验
 * Author：何语漫
 * Date：2018-9-30
 */
export async function CheckUserLogin(req: any, res: any, next: any) {
    let { token, user_id } = (req as any).cookies
    console.log(token, user_id)
    try {
        validateCgi({ token: token, user_id: user_id }, wxValidator.UserSession)
        req.UserSession = await getLoginAsync(user_id, token)
        req.query['user_id'] = user_id
        next()
    } catch (e) {
        res.status(403).json({ error: e.message })
    }
}

/*************** 保存用户手机号 **************/

/**********************************************
 * 检查公司短信余额是否充足
 * Author：何语漫
 * Date：2018-9-30
 */
export async function cheakSms(us: UserSession) {
    let sql = `SELECT sms FROM ec_platform.company WHERE id = '${us.getSupervisor()}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result[0].sms <= 0) {
        return 1
    }
    return 0
}

/**********************************************
 * 保存手机号
 * Author：何语漫
 * Date：2018-9-30
 */
export async function saveUserPhone(us: UserSession, phone: string) {
    let sql = `UPDATE ${us.getDatabaseName()}.user SET phone = ${phone} WHERE user_id = '${us.getUser_id()}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectRows == 0) {
        throw new Error('更新失败')
    }
}

/**********************************************
 * 发送短信成功，减少公司短信条数
 * Author：何语漫
 * Date：2018-9-30
 */
export async function reduceSms(us: UserSession) {
    let sql = `UPDATE ec_platform.company SET sms = sms - 1 WHERE id = '${us.getSupervisor()}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
    return
}

/*****************个人中心 *****************/

/**********************************************
 * 获取订单
 * Author：何语漫
 * Date：2018-9-30
 */
export async function getMyTrade(us: UserSession) {
    let sql = `SELECT name, trade_order_no, user_id, rebate, modified FROM ${us.getDatabaseName()}.trade_history WHERE user_id = '${us.getUser_id()}' AND status = 5 `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 获取用户信息
 * Author：何语漫
 * Date：2018-9-30
 */
export async function getUserInfo(us: UserSession) {
    let sql = `SELECT nickname, user_id, phone, headimgurl, credits FROM ${us.getDatabaseName()}.user WHERE user_id = '${us.getUser_id()}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.length == 0) {
        throw new Error('无该用户！')
    }
    return result
}

export async function setAttendece(us: UserSession) {
    let credits: any
    let sql = `SELECT attendence_id FROM ${us.getDatabaseName()}.attendence WHERE user_id = ${us.getUser_id()} AND TO_DAYS(attendence_date)=TO_DAYS(NOW())`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.length !== 0) {
        throw new Error('您已签到！')
    }
    await transactionAsync(async conn => {
        let attend_sql = `SELECT v FROM ${us.getDatabaseName()}.kv WHERE k = 'attendence_rule'`
        credits = await conn.queryAsync(attend_sql)
        console.log(credits)
        let sql = `INSERT INTO ${us.getDatabaseName()}.attendence(attendence_date, user_id, credits) VALUES('${formatDate2(new Date())}', '${us.getUser_id()}', '${credits[0].v}')`
        console.log(sql)
        let result = await conn.queryAsync(sql)
        if (result.affectedRows == 0) {
            throw new Error('签到失败')
        }
        sql = `UPDATE  ${us.getDatabaseName()}.user SET credits = credits + ${credits[0].v} WHERE user_id = ${us.getUser_id()}`
        await conn.queryAsync(sql)
        sql = `INSERT INTO ${us.getDatabaseName()}.credits(credits, user_id, reason) VALUES('${credits[0].v}', '${us.getUser_id()}', '0')`
        await conn.queryAsync(sql)
    })
    let str = `签到成功, 恭喜您获得 ${credits[0].v} 积分`
    return str
}

export async function getAttendence(us: UserSession) {
    let sql = `SELECT * FROM ${us.getDatabaseName()}.attendence WHERE user_id = ${us.getUser_id()} AND DATE_FORMAT(attendence_date,'%Y%m') = DATE_FORMAT(CURDATE(),'%Y%m')`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.length == 0) {
        throw new Error('无签到记录')
    }
    return result
}