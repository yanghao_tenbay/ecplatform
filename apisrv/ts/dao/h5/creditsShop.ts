import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { UserSession } from '../../entity/wxUsersession';
import { formatDate3 } from '../../lib/utils';
import { sendRedPack } from '../../lib/mchpay';
import { log_h5 as log } from '../../lib/log'
/**********************************************
* 获取用户积分
* Author：hym
* Date：2018-10-18
*/
export async function getCredits(us: UserSession): Promise<any> {
    let sql = `SELECT credits FROM ${us.getDatabaseName()}.user WHERE user_id='${us.getUser_id()}'`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (res.length == 0) {
        return
    }
    let credits = res[0].credits
    return credits
}

/**********************************************
* 获取当前活动
* Author：hym
* Date：2018-10-18
*/
export async function getGift(databaseName: string, start: number, length: number) {
    let date = formatDate3(new Date())
    let sql = `SELECT * FROM ${databaseName}.gift WHERE is_del = 0 AND end_time > '${date}' ORDER BY end_time DESC LIMIT ${start}, ${length}  `

    let sql1 = `SELECT COUNT(gift_id) AS num FROM ${databaseName}.gift WHERE is_del = 0 AND end_time >= '${date}'`
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1))
    let num = rowcount[0].num
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    for (let i = 0; i < result.length; i++) {
        if (result[i].used_num === result[i].total_num) {
            result[i].status = 0
        }
    }
    return { recordsFiltered: num, recordsTotal: num, data: result }
}

/**********************************************
* 添加礼品到购物车
* Author：hym
* Date：2018-10-18
*/
export async function addCart(us: UserSession, gift_id: number, num: number) {
    await transactionAsync(async conn => {
        let date = formatDate3(new Date())
        let sql = `SELECT credits FROM ${us.getDatabaseName()}.gift WHERE gift_id = ${gift_id} AND is_del = 0 AND end_time >= '${date}'`
        let credits = await conn.queryAsync(sql)
        if (credits.length == 0) {
            throw new Error("该商品不存在！")
        }

        sql = `SELECT id FROM ${us.getDatabaseName()}.gift_record WHERE gift_id = ${gift_id} AND user_id = ${us.getUser_id()} AND status = 1`
        let result = await conn.queryAsync(sql)
        console.log(result)
        if (result.length !== 0) {
            addGiftNum(us, gift_id, num, credits[0].credits)
        } else {
            sql = `INSERT INTO ${us.getDatabaseName()}.gift_record(user_id, gift_id, num, credits, status) VALUES('${us.getUser_id()}', '${gift_id}', '${num}', '${credits[0].credits * num}',  1) `
            console.log(sql)
            let rows = await conn.queryAsync(sql)
            if (rows.affectedRows === 0) {
                throw Error('添加失败')
            }
        }
    })
    return '添加成功'
}

/**********************************************
* 展示购物车
* Author：hym
* Date：2018-10-18
*/
export async function selectCart(us: UserSession, start: number, length: number) {
    let sql = `SELECT b.*, a.id, a.num, a.user_id, a.status FROM ${us.getDatabaseName()}.gift_record AS a, ${us.getDatabaseName()}.gift AS b WHERE a.user_id = ${us.getUser_id()} AND a.gift_id = b.gift_id AND b.is_del = 0 AND a.is_del = 0 AND a.status = 1`

    let sql1 = `SELECT COUNT(a.id) AS num FROM ${us.getDatabaseName()}.gift_record AS a, ${us.getDatabaseName()}.gift AS b WHERE a.user_id = ${us.getUser_id()} AND a.gift_id = b.gift_id AND b.is_del = 0 AND a.is_del = 0 AND a.status = 1`

    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1))
    let num = rowcount[0].num
    sql += ` ORDER BY create_time DESC LIMIT ${start}, ${length}`
    console.log(sql)
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    for (let i = 0; i < result.length; i++) {
        let date = new Date()
        let end_date = new Date(result[i].end_time)
        if (end_date < date) {
            result[i].status = 0
            if (end_date.getTime() - date.getTime() > (20 * 24 * 60 * 60 * 1000)) {
                sql = `DELETE FROM ${us.getDatabaseName()}.gift_record WHERE id = ${result[i].id}`
                console.log(sql)
                await getConnectionAsync(async conn => await conn.queryAsync(sql))
            }
        }

        if (result[i].used_num === result[i].total_num) {
            result[i].status = 0
        }
    }
    return { recordsFiltered: num, recordsTotal: num, data: result }
}

/**********************************************
* 购物车某礼品数量增加
* Author：hym
* Date：2018-10-19
*/
export async function addGiftNum(us: UserSession, gift_id: number, addnum: number, credits: number) {
    await transactionAsync(async conn => {
        let sql = `SELECT total_num, used_num FROM ${us.getDatabaseName()}.gift WHERE gift_id = ${gift_id} AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
        let num = await conn.queryAsync(sql)
        if (num.length === 0) {
            throw new Error('该礼品已失效')
        }

        if (num[0].total_num === num[0].used_num) {
            throw new Error('该商品数量不足')
        }
        console.log(credits, addnum)
        sql = `UPDATE ${us.getDatabaseName()}.gift_record SET num = num + ${addnum}, credits = credits + ${credits * addnum} WHERE gift_id = ${gift_id} and user_id = ${us.getUser_id()}`
        console.log(sql)
        let result = await conn.queryAsync(sql)
        if (result.affectedRows === 0) {
            throw new Error('添加失败！')
        }
    })
    return '添加成功'
}

/**********************************************
* 购物车某礼品数量减少
* Author：hym
* Date：2018-10-19
*/
export async function redGiftNum(databaseName: string, id: number, credits: number) {
    await transactionAsync(async conn => {
        let sql = `SELECT num FROM ${databaseName}.gift_record WHERE id = ${id}`
        let num = await conn.queryAsync(sql)
        if (num[0].num == 1) {
            sql = `DELETE FROM ${databaseName}.gift_record WHERE id = ${id}`
        } else {
            sql = `UPDATE ${databaseName}.gift_record SET num = num - 1, credits = credits - ${credits} WHERE id = ${id}`
        }
        let result = await conn.queryAsync(sql)
        if (result.affectedRows === 0) {
            throw new Error('减少失败！')
        }
    })
    return '减少成功'
}

/**********************************************
* 购物车删除记录
* Author：hym
* Date：2018-10-19
*/
export async function delGiftRecord(databaseName: string, id: string) {
    let sql = `DELETE FROM ${databaseName}.gift_record WHERE id = ${id}`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        throw new Error('删除失败')
    }
    return '删除成功'
}

/**********************************************
* 购物车全选兑换
* Author：hym
* Date：2018-10-19
*/
export async function cashGift(us: UserSession, gift_id: string, num: string, credits: string, id: string): Promise<any> {
    gift_id = JSON.parse(gift_id)
    num = JSON.parse(num)
    credits = JSON.parse(credits)
    if (id != '') {
        id = JSON.parse(id)
    }
    let total_credits = 0
    let parms = ''
    let sql_rednum = `SELECT COUNT(id) AS num FROM ${us.getDatabaseName()}.gift_record WHERE user_id = ${us.getUser_id()} AND status = 2 AND DATEDIFF(modify_time,'2018-11-10') = 0`
    let total_rednum = (await getConnectionAsync(async conn => conn.queryAsync(sql_rednum)))[0].num
    for (let i = 0; i < credits.length; i++) {
        total_credits += Number(credits[i]) * Number(num[i])
        total_rednum += num[i]
    }
    console.log(total_rednum)
    if (total_rednum >= 10) {
        throw new Error('您今天兑换红包已达到上限！')
    }
    let sql = `SELECT credits, openid FROM ${us.getDatabaseName()}.user WHERE user_id = ${us.getUser_id()}`
    console.log(sql)
    let user_result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (user_result.length != 1) {
        throw new Error('请重新登陆')
    }
    console.log(user_result)
    let user_credits = user_result[0].credits
    let openid = user_result[0].openid
    if (total_credits > Number(user_credits)) {
        throw new Error('您的积分不足！')
    }
    sql = `SELECT gift_id, gift_name, price, total_num, used_num, end_time FROM ${us.getDatabaseName()}.gift WHERE gift_id in (${gift_id.toString()}) AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
    let giftlist = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(giftlist)
    if (giftlist.length === 0) {
        throw new Error('该商品已经过期或数量已兑完！')
    }

    if (giftlist.length !== gift_id.length) {
        throw new Error('该商品已经失效')
    }
    for (let i = 0; i < giftlist.length; i++) {
        console.log(gift_id)
        for (let k = 0; k < giftlist.length; k++) {
            if (gift_id[k] == giftlist[i].gift_id) {
                if ((giftlist[i].total_num - giftlist[i].used_num) < Number(num[k])) {
                    throw new Error('您兑换的数量超过库存量！')
                }
            }
        }
    }
    let value = ''
    let flag = 0
    let cheak = 0
    let wrongstring = ''
    let changenum = ''
    let sql_name = `SELECT nick_name FROM ec_platform.wx_config WHERE company_id = ${us.getSupervisor()}`
    let wxname = (await getConnectionAsync(async conn => conn.queryAsync(sql_name)))[0].nick_name
    let red_id = ''
    let red_giftId = ''
    total_credits = 0
    await transactionAsync(async conn => {
        for (let i = 0; (i < gift_id.length && flag != 1); i++) {
            let price = 0
            let gift_name = ''
            let giftid = ''
            for (let k = 0; k < giftlist.length; k++) {
                if (gift_id[i] == giftlist[k].gift_id) {
                    giftid = gift_id[i]
                    price = giftlist[k].price
                    gift_name = giftlist[k].gift_name
                    break
                }
            }
            let j = 0
            for (; j < Number(num[i]); j++) {
                let res = await sendRedPack({
                    db_name: us.getDatabaseName(),
                    openid: openid,
                    rebate: price,
                    send_name: wxname, //string(32)
                    wishing: '恭喜发财',  //string(128)
                    act_name: gift_name, //string(32)
                    remark: gift_name, //string(256)
                })
                if (res.result_code != 'SUCCESS') {
                    flag = 1
                    log.error(`/:红包发送失败原因: ${JSON.stringify(res)}`);

                    console.log(res)
                    if (j == 0 && i == 0) {
                        throw new Error('红包发送失败！请稍后重试')
                    }

                    if (j == 0) {
                        break
                    }
                    if (j > 0 && j <= Number(num[i])) {
                        let sql = `INSERT INTO ${us.getDatabaseName()}.gift_record(user_id, gift_id, num, credits, status) VALUES('${us.getUser_id()}', '${giftid}', '${j}', '${Number(credits[i]) * Number(j)}', 2)`
                        console.log(sql)
                        await conn.queryAsync(sql)
                        if (id != '') {
                            sql = `UPDATE ${us.getDatabaseName()}.gift_record SET num = num - ${j}, credits = credits - ${Number(credits[i]) * Number(j)} WHERE id = ${id[i]} AND status = 1`
                            console.log(sql)
                            await conn.queryAsync(sql)
                        }
                        wrongstring = `红包兑换出错！您成功的兑换${cheak}个红包`
                        break
                    }
                }
                let sql_record = `INSERT INTO ${us.getDatabaseName()}.gift_history(name, trade_order_no, user_id, price, note) VALUES ('${wxname}', '${res.mch_billno}', ${us.getUser_id()}, ${price}, '${gift_name}')`
                console.log(sql_record)
                await conn.queryAsync(sql_record)
                cheak++
            }
            if (j >= 1) {
                console.log('there')
                total_credits += Number(credits[i]) * Number(j)
                value += ` WHEN ${giftid} THEN used_num + ${j} `
                parms += `('-${Number(credits[i]) * Number(j)}', ${us.getUser_id()}, '2'),`
                red_giftId += ` ${giftid},`
                if (id != '' && !flag) {
                    console.log(id[i])
                    changenum += ` WHEN ${id[i]} THEN ${j}`
                    red_id += ` ${id[i]},`
                }
            }
            if (flag) {
                console.log('break')
                break
            }
        }
        red_giftId = red_giftId.substring(0, red_giftId.length - 1)
        let sql_gift = `UPDATE ${us.getDatabaseName()}.gift SET used_num = CASE gift_id ${value} END WHERE gift_id IN ( ${red_giftId.toString()} )`
        await conn.queryAsync(sql_gift)
        let sql_record
        if (id !== undefined && id !== '' && changenum != '') {
            red_id = red_id.substring(0, red_id.length - 1)
            sql_record = `UPDATE ${us.getDatabaseName()}.gift_record SET status = 2, num = CASE id ${changenum} END WHERE id in (${red_id}) AND status = 1`
            await conn.queryAsync(sql_record)
        } else if (id == '' && flag != 1) {
            sql_record = `INSERT INTO ${us.getDatabaseName()}.gift_record(user_id, gift_id, num, credits, status) VALUES('${us.getUser_id()}', '${gift_id}', '${cheak}', '${Number(credits) * Number(cheak)}', 2)`
            console.log(sql_record)
            await conn.queryAsync(sql_record)
        }
        parms = parms.substring(0, parms.length - 1)
        let sql_credits = `INSERT INTO ${us.getDatabaseName()}.credits(credits, user_id, reason) VALUES${parms}`
        console.log(sql_credits)
        await conn.queryAsync(sql_credits)
        let sql_user = `UPDATE ${us.getDatabaseName()}.user SET credits = credits - ${total_credits} WHERE user_id = ${us.getUser_id()}`
        console.log(sql_user)
        await conn.queryAsync(sql_user)
    })
    if (flag == 1) {
        throw new Error(`${wrongstring}`)
    }
}

/**********************************************
* 展示我的兑换
* Author：hym
* Date：2018-10-19
*/
export async function getCash(us: UserSession, start: number, length: number): Promise<any> {
    let sql = `SELECT b.gift_name, b.gift_cove, a.credits, a.num, a.modify_time FROM ${us.getDatabaseName()}.gift_record AS a, ${us.getDatabaseName()}.gift AS b WHERE a.user_id = '${us.getUser_id()}'  AND a.gift_id = b.gift_id AND a.status = 2  LIMIT ${start}, ${length}`
    console.log(sql)
    let sql_count = `SELECT COUNT(a.id) AS num FROM ${us.getDatabaseName()}.gift_record AS a, ${us.getDatabaseName()}.gift AS b WHERE a.user_id = '${us.getUser_id()}'  AND a.gift_id = b.gift_id AND a.status = 2`

    let num = await getConnectionAsync(async conn => await conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
* 获取商城首页配置
* Author：hym
* Date：2018-10-19
*/
export async function getPict(databaseName: string) {
    let sql = `SELECT gift_id, gift_name, gift_cove FROM ${databaseName}.gift WHERE shop_set = 1 AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
    console.log(sql)
    let sql_turn = `SELECT v FROM ${databaseName}.kv WHERE k = 'turn_time'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(result)
    let turn_time = await getConnectionAsync(async conn => conn.queryAsync(sql_turn))
    if (turn_time.length == 0) {
        return
    }
    return {
        pic: result,
        turn_time: turn_time[0].v
    }
}

/**********************************************
* 展示我的积分
* Author：hym
* Date：2018-11-05
*/
export async function getCredit(us: UserSession, start: number, length: number) {
    let sql_count = `SELECT COUNT(credits_id) FROM ${us.getDatabaseName()}.credits WHERE user_id = ${us.getUser_id()}`
    let sql = `SELECT reason, credits AS credits FROM ${us.getDatabaseName()}.credits WHERE user_id = ${us.getUser_id()} LIMIT ${start}, ${length}`
    console.log(sql)
    let num = await getConnectionAsync(async conn => await conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    console.log(result)
    for (let i = 0; i < result.length; i++) {
        if (result[i].reason == 2) {
            result[i].reason = '积分消耗'
        } else if (result[i].reason == 1) {
            result[i].reason = '订单获得'
        } else if (result[i].reason == 0) {
            result[i].reason = '签到获得'
        }
    }
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}