/**********************************************
* 晒评价
* Author：鲁欣欣
* Date：2018-10-25
*/
import { getConnectionAsync } from '../../lib/mysqlpool'
import { randomInt, formatDate2 } from '../../lib/utils';
import { networkInterfaces } from 'os';
import { data } from '../../validator/common';


/**********************************************
* 获取某公司的活动信息
* Author：鲁欣欣
* Date：2018-10-25
*/
export async function getEvent(company_id: number, event_id: number): Promise<any> {
    let sql = `SELECT * FROM company_${company_id}.comment_event WHERE event_id=${event_id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    console.log(res[0])
    console.log(formatDate2(new Date()))
    if (res.length === 0) {
        throw Error('未找到该活动')
    }
    if (formatDate2(new Date()) < res[0].start_time) {
        throw Error(`starttime:${res[0].start_time}`)
    }
    if (formatDate2(new Date()) > res[0].end_time) {
        throw Error('该活动已结束')
    }
    return res[0]
}

/**********************************************
* 获取活动限制信息
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function getEventInfo(database_name: string, event_id: number): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.comment_event WHERE event_id=${event_id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res[0]
}

/**********************************************
* 获取对应订单信息
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function getOrderInfo(database_name: string, order_id: string): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.trade_order WHERE tid='${order_id}'`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}

/**********************************************
* 更改对应订单状态信息，表参加过某活动
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function updateOrderStatus(database_name: string, order_id: string): Promise<any> {
    let sql = `UPDATE ${database_name}.trade_order set activity_status='comment_rebate' WHERE activity_status='false' and tid='${order_id}'`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.affectedRows === 0) {
        throw Error('该订单号已参与过活动')
    }
    return
}

/**********************************************
* 获取某用户参与该活动的次数
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function getEventRecordInfo(database_name: string, user_id: number, event_id: number): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.comment_record WHERE event_id=${event_id} and user_id=${user_id}`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res.length
}

/**********************************************
* 获取某订单参与该活动的次数
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function getOrderEventRecord(database_name: string, order_id: string): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.comment_record WHERE order_id='${order_id}' `
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res.length
}

/**********************************************
* 提交参与晒评价活动记录
* Author：鲁欣欣
* Date：2018-10-25
*/
export async function submitComment(database_name: string, event_id: number, order_id: string, user_id: number, image_url: string, amount: number, status: number, reason: string): Promise<any> {

    let sql = `INSERT INTO ${database_name}.comment_record(event_id, order_id, user_id, screenshot, amount, status, reason) VALUES (${event_id},'${order_id}',${user_id},'${image_url}',${amount},${status},'${reason}')`
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}

/**************************************
 * 写入评价信息
 * Author：鲁欣欣
 * Date：2018-11-5
 */
export async function writeComment(database_name: string, order_id: string, comment: any): Promise<any> {
    let sql = `UPDATE ${database_name}.trade_order set comment='${comment.content}',result='${comment.result}',comment_time='${comment.time}'  WHERE tid='${order_id}'`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return rows[0]
}

/**********************************************
* 审核成功后更改活动已参与次数
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function addEventNum(database_name: string, event_id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.comment_event set used_number=used_number+1 WHERE event_id='${event_id}'`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return
}

/**********************************************
* 获取店铺id
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getShop_id(database_name: string, order_id: string): Promise<any> {
    let sql = `SELECT b.id FROM ${database_name}.trade_order as a,ec_platform.authorization as b WHERE a.tid='${order_id}' and a.seller_nick=b.shop_name`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return rows[0].id
}

/**********************************************
* 待转账记录插入到tranfer表
* Author：鲁欣欣
* Date：2018-11-9
*/
export async function addTranfer(database_name: string, event: string, record_id: number, openid: string, amount: number): Promise<any> {
    let sql = `INSERT INTO ${database_name}.transfer( activity, activity_name, record_id, openid, rebate, credits, transfer_time) VALUES ( 'comment_rebate', '${event}', ${record_id}, '${openid}', ${amount}, 0,now() );`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return
}

/**********************************************
* 获取某用户参与订单返利的记录(我的返利)
* Author：鲁欣欣
* Date：2018-11-16
*/
export async function getMyRecord(database_name: string, user_id: number, start: number, length: number): Promise<any> {
    let sql = `FROM ${database_name}.comment_record as a,${database_name}.comment_event as b,${database_name}.trade_order as c WHERE user_id=${user_id} and a.status=5 and a.event_id=b.event_id and a.order_id=c.oid `
    let sql1 = `SELECT count(*) as count ` + sql
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `SELECT a.order_id,a.modify_time,a.amount,c.title  ` + sql + `ORDER BY a.create_time DESC limit ${start} , ${length}`
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    return { recordsFiltered: row, recordsTotal: row, data: data }
}