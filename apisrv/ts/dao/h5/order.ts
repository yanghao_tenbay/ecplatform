/**********************************************
* 订单返利
* Author：鲁欣欣
* Date：2018-10-30
*/
import { getConnectionAsync } from '../../lib/mysqlpool'
import { randomInt, formatDate2 } from '../../lib/utils';


/**********************************************
* 获取某公司的活动信息
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getEvent(company_id: number, event_id: number): Promise<any> {
    let sql = `SELECT * FROM company_${company_id}.order_event WHERE event_id=${event_id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    console.log(res[0])

    if (res.length === 0) {
        throw Error('未找到该活动')
    }
    if (formatDate2(new Date()) < res[0].start_time) {
        throw Error(`starttime:${res[0].start_time}`)
    }
    if (formatDate2(new Date()) > res[0].end_time) {
        throw Error('该活动已结束')
    }
    if (res[0].number <= res[0].used_number) {
        throw Error('该活动参与次数已满')
    }
    return res[0]
}

/**********************************************
* 获取活动限制信息
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getEventInfo(database_name: string, event_id: number): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.order_event WHERE event_id=${event_id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res[0]
}

/**********************************************
* 获取对应订单信息
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getOrderInfo(database_name: string, order_id: string): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.trade_order WHERE tid='${order_id}'`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}

/**********************************************
* 审核成功后更改活动已参与次数
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function addEventNum(database_name: string, event_id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_event set used_number=used_number+1 WHERE event_id='${event_id}'`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return
}

/**********************************************
* 更改对应订单状态信息，表参加过某活动
* Author：鲁欣欣
* Date：2018-10-29
*/
export async function updateOrderStatus(database_name: string, order_id: string): Promise<any> {
    let sql = `UPDATE ${database_name}.trade_order set activity_status='order_rebate' WHERE activity_status='false' and tid='${order_id}'`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.affectedRows === 0) {
        throw Error('该订单号已参与过活动')
    }
    return
}

/**********************************************
* 获取用户信息
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getUserInfo(database_name: string, user_id: number): Promise<any> {
    let sql = `SELECT phone FROM ${database_name}.user WHERE user_id=${user_id}`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res[0].phone === undefined) {
        throw Error('请先绑定手机号再参与活动')
    }
    return res[0]
}

/**********************************************
* 获取某用户参与该活动的次数
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getEventRecordInfo(database_name: string, user_id: number, event_id: number): Promise<any> {
    let sql = `SELECT * FROM ${database_name}.order_record WHERE event_id=${event_id} and user_id=${user_id}`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res.length
}

/**********************************************
* 获取某用户参与所有活动的次数
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function getUserRecordNum(database_name: string, user_id: number): Promise<any> {
    let sql1 = `SELECT count(*) as num FROM ${database_name}.order_record WHERE user_id=${user_id}`
    let res1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let sql2 = `SELECT count(*) as num FROM ${database_name}.comment_record WHERE user_id=${user_id}`
    let res2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    let sql3 = `SELECT count(*) as num FROM ${database_name}.rebate_mall_record WHERE user_id=${user_id}`
    let res3 = await getConnectionAsync(async conn => await conn.queryAsync(sql3)) as any
    return res1[0].num + res2[0].num + res3[0].num
}

/**********************************************
* 提交参与订单返利活动记录
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function submitOrder(database_name: string, event_id: number, order_id: string, user_id: number, amount: number, integral: number, status: number, reason: string, settime?: string): Promise<any> {
    let sql: string
    if (settime === undefined) {
        sql = `INSERT INTO ${database_name}.order_record(event_id, order_id, user_id, amount, integral, status, reason) VALUES (${event_id},'${order_id}',${user_id},${amount},${integral},${status},'${reason}')`
    }
    else {
        sql = `INSERT INTO ${database_name}.order_record(event_id, order_id, user_id, amount, integral, status, reason, settime) VALUES (${event_id},'${order_id}',${user_id},${amount},${integral},${status},'${reason}','${settime}')`
    }
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}

/**********************************************
* 获取某用户参与订单返利的记录(我的返利)
* Author：鲁欣欣
* Date：2018-11-16
*/
export async function getMyRecord(database_name: string, user_id: number, start: number, length: number): Promise<any> {
    let sql = `FROM ${database_name}.order_record as a,${database_name}.order_event as b,${database_name}.trade_order as c WHERE user_id=${user_id} and a.status=5 and a.event_id=b.event_id and a.order_id=c.oid `
    let sql1 = `SELECT count(*) as count ` + sql
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `SELECT a.order_id,a.modify_time,a.amount,a.integral,b.img,c.title  ` + sql + `ORDER BY a.create_time DESC limit ${start} , ${length}`
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    return { recordsFiltered: row, recordsTotal: row, data: data }
}

/**********************************************
* 待转账记录插入到tranfer表
* Author：鲁欣欣
* Date：2018-11-9
*/
export async function addTranfer(database_name: string, event: string, record_id: number, openid: string, amount: number, credits: number, transfer_time: string): Promise<any> {
    let sql = `INSERT INTO ${database_name}.transfer( activity, activity_name, record_id, openid, rebate, credits, transfer_time) VALUES ('order_rebate', '${event}', ${record_id}, '${openid}', ${amount}, ${credits},'${transfer_time}' );`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return
}