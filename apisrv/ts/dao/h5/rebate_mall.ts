import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { getRedisClientAsync } from "../../lib/redispool"
import { rdb } from "../../config/redis"
import { replaceStr2 } from "../../lib/utils"
import { log_pc as log } from '../../lib/log'

/**********************************************
* 获取商城配置
* Author：mjq
* Date：2018-10-31
*/
export async function getRebateMallCarousel(db_name: string): Promise<any> {
    let sql = `select v from ${db_name}.kv where k = 'rebate_mall_carousel'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        return { "time": "3", "pic": [] }
    }
    return JSON.parse(rows[0].v)
}

/**********************************************
* 展示活动记录
* Author：mjq
* Date：2018-10-29
*/
interface H5RebateMall {
    start: number               //开始位置
    length: number              //查询个数
}
export async function selectH5RebateMall(db_name: string, opt: H5RebateMall): Promise<any> {
    let sql_from = `from ${db_name}.rebate_mall where start_time < now() and end_time > now()`

    let sql_num = `select count(*) as num ${sql_from}`
    let sql_data = `SELECT *, (select count(*) from ${db_name}.rebate_mall_record where activity_id = rebate_mall.id) Participate_number ${sql_from} order by id desc limit ${opt.start}, ${opt.length}`

    let rows_num = await getConnectionAsync(async conn => conn.queryAsync(sql_num))
    let rows_data = await getConnectionAsync(async conn => conn.queryAsync(sql_data))
    for (let v of rows_data) {
        v.goods_img = JSON.parse(v.goods_img)
        v.after_event = JSON.parse(v.after_event)
        v.participation = JSON.parse(v.participation)
    }

    return {
        recordsFiltered: rows_num[0].num,
        recordsTotal: rows_num[0].num,
        data: rows_data
    }
}

/**********************************************
* 展示活动参与记录
* Author：mjq
* Date：2018-10-29
*/
interface WxSearch {
    start: number               //开始位置
    length: number              //查询个数
    user_id: number             //用户id
    status: string             //按照状态搜索
}
export async function selectSelfOrder(db_name: string, opt: WxSearch): Promise<any> {
    let wh_status = opt.status ? `and c.status = '${opt.status}'` : ''

    let tb_1 = `select a.activity_name, a.goods_img, a.after_event, b.nickname, c.id, c.tid, c.rebate, c.created, c.rebate_time, c.status from ${db_name}.rebate_mall a, ${db_name}.user b, ${db_name}.rebate_mall_record c where b.user_id = '${opt.user_id}' ${wh_status} and a.id = c.activity_id and b.user_id = c.user_id limit ${opt.start}, ${opt.length}`

    let tb_2 = `select a2.activity_name, a2.goods_img, a2.after_event, a2.nickname, a2.id, a2.tid, a2.rebate, a2.created, a2.rebate_time, a2.status, b2.receiver_mobile, b2.status as order_status from (${tb_1}) a2 LEFT JOIN ${db_name}.trade_order b2 ON b2.tid = a2.tid`

    let sql_num = `select count(*) as num from (${tb_2}) tb`
    let sql_data = `select * from (${tb_2}) tb order by id desc`

    let rows_num = await getConnectionAsync(async conn => conn.queryAsync(sql_num))
    let rows_data = await getConnectionAsync(async conn => conn.queryAsync(sql_data))
    for (let v of rows_data) {
        v.goods_img = JSON.parse(v.goods_img)
        v.after_event = JSON.parse(v.after_event)
    }

    return {
        recordsFiltered: rows_num[0].num,
        recordsTotal: rows_num[0].num,
        data: rows_data
    }
}

/**********************************************
* 抢资格
* Author：mjq
* Date：2018-10-29
*/
export async function robQualification(db_name: string, user_id: number, activity_id: number) {
    //是否绑定手机号
    let sql = `select phone, credits from ${db_name}.user where user_id = '${user_id}'`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error('无效的用户')
    }
    if (!rows[0].phone) {
        throw new Error('unbind')
    }
    let credits = rows[0].credits

    //获取活动信息
    sql = `select * from ${db_name}.rebate_mall where id = '${activity_id}'`
    rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error('无效的活动')
    }
    //`[{ "type": "new_user", "status": 1, "data": "" }, { "type": "limit_credits", "status": 1, "data": 500 }, { "type": "limit_time", "status": 1, "data": 3 }]`
    let { total, participation, start_time, end_time, short_link } = rows[0]

    //检查用户是否可以抢资格
    for (let v of JSON.parse(participation)) {
        switch (v.type) {
            case 'new_user':
                if (0 == v.status) {
                    //未勾选限制新用户
                    continue
                }
                //查询返利商城参与记录，晒评价参与记录，订单返利参与记录。有数据就不算新用户
                sql = `select id from ${db_name}.rebate_mall_record where user_id = '${user_id}' limit 1`
                rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
                if (0 !== rows.length) {
                    throw new Error('该活动限制新注册用户参与')
                }
                sql = `select id from ${db_name}.comment_record where user_id = '${user_id}' limit 1`
                rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
                if (0 !== rows.length) {
                    throw new Error('该活动限制新注册用户参与')
                }
                sql = `select id from ${db_name}.order_record where user_id = '${user_id}' limit 1`
                rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
                if (0 !== rows.length) {
                    throw new Error('该活动限制新注册用户参与')
                }
                continue

            case 'limit_credits':
                if (0 == v.status) {
                    //未勾选积分限制
                    continue
                }
                if (credits < v.data) {
                    throw new Error('积分不足，无法参与')
                }
                continue

            case 'limit_time':
                if (0 == v.status) {
                    //未勾选次数限制
                    continue
                }
                sql = `select count(*) as num from ${db_name}.rebate_mall_record where activity_id = '${activity_id}' and user_id = '${user_id}'`
                rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
                if (0 === rows.length) {
                    throw new Error('数据库异常')
                }
                if (v.data > rows[0].num) {
                    continue
                } else {
                    throw new Error(`该活动一个用户只能参与${v.data}次`)
                }
        }
    }

    //检查剩余个数
    sql = `select count(*) as num from ${db_name}.rebate_mall_record where activity_id = '${activity_id}'`
    rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error('意外错误')
    }
    if (total > rows[0].num) {
        //插入新纪录
        sql = `insert into ${db_name}.rebate_mall_record (activity_id, user_id) value ('${activity_id}', '${user_id}')`
        await getConnectionAsync(async conn => conn.queryAsync(sql))
        return
    }
    throw new Error('活动名额已被抢光')
}

/**********************************************
* 展示分享链接商品的具体内容
* Author：mjq
* Date：2018-10-29
*/
export async function selectRebateMallDetails(db_name: string, activity_id: number): Promise<any> {
    let sql_data = `SELECT *, (select count(*) from ${db_name}.rebate_mall_record where activity_id = id) Participate_number from ${db_name}.rebate_mall where id = '${activity_id}'`
    let rows_data = await getConnectionAsync(async conn => conn.queryAsync(sql_data))
    for (let v of rows_data) {
        v.goods_img = JSON.parse(v.goods_img)
        v.after_event = JSON.parse(v.after_event)
        v.participation = JSON.parse(v.participation)
    }

    return rows_data[0]
}