import { transactionAsync, getConnectionAsync } from "../../lib/mysqlpool";
import { formatDate2, replaceStr } from "../../lib/utils";
import { taoBaoSign } from "../../lib/taobaoConfig";
import { getAsync } from "../../lib/request";
import { updateShop_power } from "./position";

/**********************************************
 * 获取授权链接
 * Author：何语漫
 * Date：2018-11-05
 */
export async function getAuthUrl(company_id: string) {
    let sql = `SELECT domain FROM ec_platform.company WHERE id = ${company_id}`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(result)
    let redirect_uri = `http://${result[0].domain}/pc/taobaoAuth/Authority/${company_id}`
    console.log(redirect_uri)
    redirect_uri = await encodeURIComponent(redirect_uri)
    let url = `https://oauth.taobao.com/authorize?response_type=code&client_id=12253109&redirect_uri=http://api.taoesoft.com/api.aspx&view=web&state=${redirect_uri}`
    console.log(url)
    return url
}

/**********************************************
 * 淘宝授权
 * Author：何语漫
 * Date：2018-11-05
 */
export async function taoBaoAuthority(code: string, company_id: string) {
    let sign = taoBaoSign()
    let url = `http://api.taoesoft.com/api.aspx?action=getSessionByCode&code=${code}&sign=${sign}`
    let tokenStr = await getAsync(url)
    let tokenMap = await JSON.parse(tokenStr)
    console.log(tokenMap)
    let taobao_user_nick = await decodeURIComponent(tokenMap.taobao_user_nick)
    console.log(taobao_user_nick)

    let sql = `SELECT id FROM ec_platform.authorization WHERE shop_name = '${taobao_user_nick}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(result)
    if (result.length != 0) {
        sql = `UPDATE ec_platform.authorization SET shop_name = '${taobao_user_nick}', taobao_user_id = '${tokenMap.taobao_user_id}', session_key = '${tokenMap.access_token}', refresh_token = '${tokenMap.refresh_token}', auth_time = '${formatDate2(new Date())}', company_id = '${company_id}'  WHERE id = ${result[0].id} `
        await getConnectionAsync(async conn => conn.queryAsync(sql))
    } else {
        sql = `INSERT INTO ec_platform.authorization(shop_name, taobao_user_id, session_key, refresh_token, company_id) VALUES('${taobao_user_nick}', '${tokenMap.taobao_user_id}', '${tokenMap.access_token}', '${tokenMap.refresh_token}','${company_id}')`
        let row = await getConnectionAsync(async conn => conn.queryAsync(sql))
        await updateShop_power(company_id, row.insertId)
    }
    sql = `SELECT domain FROM ec_platform.company WHERE id = ${company_id}`
    result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.length != 0) {
        return result[0].domain
    }
}

/**********************************************
 * 显示店铺
 * Author：何语漫
 * Date：2018-11-05
 */
export async function getShop(company_id: string, shop_name: string, start: string, length: string) {
    let search = ''
    if (shop_name !== '') {
        let rplstr = '/'
        if (shop_name.includes('/')) {
            rplstr = '+'
        }
        shop_name = replaceStr(shop_name, rplstr)
        search = `AND shop_name LIKE '%${shop_name}%' ESCAPE '${rplstr}'`
    }
    let sql_count = `SELECT COUNT(id) AS num FROM ec_platform.authorization WHERE company_id = '${company_id}' ${search}`
    let sql = `SELECT shop_name, first_auth_time, auth_time FROM ec_platform.authorization WHERE company_id = '${company_id}' ${search} LIMIT ${start}, ${length}`
    console.log(sql)
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    for (let i = 0; i < result.length; i++) {
        let time = new Date(result[i].auth_time)
        let end_date = new Date(time.setDate(time.getDate() + 31))
        result[i].end_date = formatDate2(end_date)
        result[i].status = end_date.getTime() > (new Date().getTime()) ? '已授权' : '重新授权'
    }
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}