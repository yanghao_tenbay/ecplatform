/**********************************************
* 订单信息
* Author：鲁欣欣
* Date：2018-11-9
*/
import { getConnectionAsync } from "../../lib/mysqlpool"
import { replaceStr } from "../../lib/utils"

/**************************************
 * 查看订单信息
 * Author：鲁欣欣
 * Date：2018-11-9
 */
export async function selectOrder(database_name: string, start: number, length: number, phone: string, order_id: string, nickname: string, order_status: string, start_time: string, end_time: string): Promise<any> {

    let nicknamestr = '/'
    if (nickname.includes('/')) {
        nicknamestr = '+'
    }
    nickname = replaceStr(nickname, nicknamestr)

    let phonesql
    if (phone === '') {
        phonesql = ``
    }
    else {
        phonesql = `and receiver_mobile like '%${phone}%' `
    }
    /**订单状态语句 */
    let order_statussql
    if (order_status === '') {
        order_statussql = ``
    }
    if (order_status === '等待买家付款') {
        order_statussql = `and status= 'WAIT_BUYER_PAY' `
    }
    if (order_status === '买家已付款') {
        order_statussql = `and status in ('WAIT_SELLER_SEND_GOODS','WAIT_BUYER_CONFIRM_GOODS') `
    }
    if (order_status === '交易成功') {
        order_statussql = `and status in ('TRADE_BUYER_SIGNED','TRADE_FINISHED') `
    }

    let nicknamesql
    if (nickname === '') {
        nicknamesql = ``
    }
    else {
        nicknamesql = `and nickname like '%${nickname}%' ESCAPE '${nicknamestr}' `
    }

    let ordersql
    if (order_id === '') {
        ordersql = ``
    }
    else {
        ordersql = `and tid like '%${order_id}%' `
    }

    let create_timesql
    if (start_time === '' && end_time === '') {
        create_timesql = `where 1 `
    }
    if (start_time === '' && end_time !== '') {
        create_timesql = `where created_time<'${end_time}' `
    }
    if (start_time !== '' && end_time === '') {
        create_timesql = `where created_time>'${start_time}' `
    }
    if (start_time !== '' && end_time !== '') {
        create_timesql = `where created_time between '${start_time}' and '${end_time}' `
    }

    let sql = `FROM ( SELECT event,headimgurl,nickname,b.* FROM ${database_name}.order_record as a JOIN ${database_name}.trade_order as b on a.order_id=b.oid JOIN  ${database_name}.user as c on a.user_id=c.user_id JOIN ${database_name}.order_event as d on a.event_id=d.event_id union SELECT event,headimgurl,nickname,b.* FROM ${database_name}.comment_record as a JOIN ${database_name}.trade_order as b on a.order_id=b.oid JOIN ${database_name}.user as c on a.user_id=c.user_id JOIN  ${database_name}.comment_event as d on a.event_id=d.event_id union SELECT activity_name,headimgurl,nickname,b.* FROM ${database_name}.rebate_mall_record as a JOIN ${database_name}.trade_order as b on a.tid=b.oid JOIN ${database_name}.user as c on a.user_id=c.user_id JOIN ${database_name}.rebate_mall as d on a.activity_id=d.id) as order_info ${create_timesql} ${phonesql}${ordersql}${nicknamesql}${order_statussql} `
    let sql1 = `SELECT count(*) as count ` + sql
    console.log(sql1)
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `SELECT *  ` + sql + `ORDER BY created_time DESC limit ${start} , ${length}`
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    for (let v of data) {
        switch (v.status) {
            case 'TRADE_NO_CREATE_PAY':
                v.status = '没有创建支付宝交易'
                break;
            case 'WAIT_BUYER_PAY':
                v.status = '等待买家付款'
                break;
            case 'SELLER_CONSIGNED_PART':
                v.status = '卖家部分发货'
                break;
            case 'WAIT_SELLER_SEND_GOODS':
                v.status = '买家已付款'
                break;
            case 'WAIT_BUYER_CONFIRM_GOODS':
                v.status = '卖家已发货'
                break;
            case 'TRADE_BUYER_SIGNED':
                v.status = '买家已签收,货到付款专用'
                break;
            case 'TRADE_FINISHED':
                v.status = '交易成功'
                break;
            case 'TRADE_CLOSED':
                v.status = '交易自动关闭'
                break;
            case 'TRADE_CLOSED_BY_TAOBAO':
                v.status = '主动关闭交易'
                break;
            case 'PAY_PENDING':
                v.status = '国际信用卡支付付款确认中'
                break;
            case 'WAIT_PRE_AUTH_CONFIRM':
                v.status = '0元购合约中'
                break;
            case 'PAID_FORBID_CONSIGN':
                v.status = '已付款但禁止发货'
                break;
        }
    }
    return { recordsFiltered: row, recordsTotal: row, data: data }
}