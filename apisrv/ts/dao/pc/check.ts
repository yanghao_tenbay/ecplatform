/**********************************************
* 审核公司账号
* Author：鲁欣欣
* Date：2018-09-25
*/
import { getConnectionAsync, transactionAsync } from "../../lib/mysqlpool"
import { replaceStr } from "../../lib/utils"

/**********************************************
* 查看公司账号信息
* Author：鲁欣欣
* Date：2018-09-25
*/
export async function selectCompany(search: string, start: number, length: number, status?: string, real_name?: string): Promise<any> {
    let rplstr = '/'
    if (search.includes('/')) {
        rplstr = '+'
    }
    search = replaceStr(search, rplstr)
    let rplstr1 = '/'
    if (real_name.includes('/')) {
        rplstr1 = '+'
    }
    real_name = replaceStr(real_name, rplstr1)
    let sql = `FROM company WHERE company_name like '%${search}%' ESCAPE '${rplstr}'  and real_name like '%${real_name}%' ESCAPE '${rplstr1}' and is_del=0 `
    let sql1: string
    let sql2: string
    if (status !== '') {
        sql1 = `SELECT count(*) as count ` + sql + `and status=${status} `
        sql2 = `SELECT id,company_name,real_name,phone,shop,province,city,email,status,end_time ` + sql + `and status=${status} ORDER BY modify_time DESC limit ${start} , ${length}`
    }
    else {
        sql1 = `SELECT count(*) as count ` + sql + `and status<4`
        sql2 = `SELECT id,company_name,real_name,phone,shop,province,city,email,status,end_time ` + sql + `and status<4 ORDER BY modify_time DESC limit ${start} , ${length}`
    }
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    return { recordsFiltered: row, recordsTotal: row, data: data }
}

/**********************************************
* 获取邮箱地址
* Author：鲁欣欣
* Date：2018-09-25
*/
export async function getEmail(phone: string): Promise<any> {
    let sql = `SELECT email from company WHERE phone='${phone}' and (status=0 or status=2) and is_del=0`
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.length == 0) {
        throw Error('该账号无需审核')
    }
    return res[0].email
}

/**********************************************
* 对审核未通过的公司信息进行编辑
* Author：鲁欣欣
* Date：2018-09-30
*/
export async function editeCompany(phone: string, company_name: string, shop: string, province: string, city: string, real_name: string, email: string): Promise<any> {
    let sql1 = `SELECT * FROM company WHERE company_name='${company_name}' and phone!=${phone} and is_del=0;`
    let rows1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    if (rows1.length > 0) {
        throw Error('该公司名已注册')
    }
    let sql = `UPDATE company SET company_name='${company_name}', shop='${shop}', province='${province}', city='${city}', real_name='${real_name}', email='${email}' WHERE phone='${phone}' and (status=2 or status=0) and is_del=0`
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.affectedRows == 0) {
        throw Error('审核成功的不能编辑')
    }
    return res.affectedRows
}

/**********************************************
* 获取待分配的主域名
* Author：鲁欣欣
* Date：2018-11-20
*/
export async function getDomain(): Promise<any> {
    let sql = `select * from ali_config`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.length == 0) {
        throw Error('未获取到配置信息')
    }
    return res[0].domain
}

/**********************************************
* 审核通过写入配置信息
* Author：鲁欣欣
* Date：2018-09-25
*/
export async function updateCompany(phone: string, password: string, domain: string, recordId: string, database: string): Promise<any> {
    let sql = `UPDATE company SET password='${password}',domain='${domain}',recordId='${recordId}',database_name='${database}',end_time=DATE_ADD(now(), INTERVAL 1 YEAR),status=1 WHERE phone='${phone}' and (status=0 or status=2) and is_del=0`
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res.affectedRows
}

/**********************************************
* 审核不通过
* Author：鲁欣欣
* Date：2018-09-25
*/
export async function updateCompanyStatus(phone: string): Promise<any> {
    let sql = `UPDATE company SET status=2 WHERE phone='${phone}' and status=0 and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.affectedRows == 0) {
        throw Error('操作失败')
    }
    return res.affectedRows
}

/**********************************************
* 删除公司账号，修改状态
* Author：鲁欣欣
* Date：2018-10-9
*/
export async function deleteCompany(id: number): Promise<any> {
    let sql = `UPDATE company SET is_del=1 WHERE id=${id} and is_del=0`
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (row.affectedRows == 0) {
        throw Error('删除失败')
    }
    /**删除数据库 */
    /*let sql2 = `SELECT database_name FROM company WHERE id=${id} and is_del=0`
    let res2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    let sql3 = `DROP DATABASE IF EXISTS ${res2[0].database_name}`
    await getConnectionAsync(async conn => await conn.queryAsync(sql3)) as any*/
    return row.affectedRows
}

/**********************************************
* 获取公司账号记录id
* Author：鲁欣欣
* Date：2018-10-16
*/
export async function getRecordId(id: number): Promise<any> {
    let sql = `SELECT recordId FROM company WHERE id=${id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.length == 0) {
        throw Error('未找到域名分配记录')
    }
    return res[0].recordId
}

/**********************************************
* 拉黑公司账号，修改状态
* Author：鲁欣欣
* Date：2018-10-9
*/
export async function blackCompany(id: number): Promise<any> {
    let sql = `UPDATE company SET status=3 WHERE id=${id} and status=1 and is_del=0`
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (row.affectedRows == 0) {
        throw Error('拉黑失败')
    }
    return row.affectedRows
}

/**********************************************
* 取消拉黑公司账号，修改状态
* Author：鲁欣欣
* Date：2018-10-9
*/
export async function whiteCompany(id: number): Promise<any> {
    let sql = `UPDATE company SET status=1 WHERE id=${id} and status=3 and is_del=0`
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (row.affectedRows == 0) {
        throw Error('取消拉黑失败')
    }
    return row.affectedRows
}

/**********************************************
* 修改公司到期时间
* Author：鲁欣欣
* Date：2018-11-14
*/
export async function updateCompanyTime(id: number): Promise<any> {
    let sql = `UPDATE company SET end_time=DATE_ADD(end_time, INTERVAL 1 YEAR) WHERE id=${id} and is_del=0`
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (row.affectedRows == 0) {
        throw Error('续签失败')
    }
    return row.affectedRows
}

/**********************************************
* 公司统计数据展示
* Author：鲁欣欣
* Date：2018-11-15
*/
export async function companyStat(id: number): Promise<any> {
    /**总粉丝数 */
    let sql1 = `SELECT count(*) as count FROM company_${id}.user`
    let res1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1))
    /**昨日新增粉丝数 */
    let sql2 = `SELECT count(*) as count FROM company_${id}.user WHERE date(create_time)=CURDATE()-INTERVAL 1 DAY`
    let res2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    /**月销量 */
    let sql3 = `SELECT count(*) as count FROM company_${id}.trade_order WHERE status='TRADE_FINISHED' and PERIOD_DIFF(date_format(now(),'%Y%m'),date_format(end_time,'%Y%m')) =1`
    let res3 = await getConnectionAsync(async conn => await conn.queryAsync(sql3))
    /**昨日销量 */
    let sql4 = `SELECT count(*) as count FROM company_${id}.trade_order WHERE status='TRADE_FINISHED' and date(end_time)=CURDATE()-INTERVAL 1 DAY`
    let res4 = await getConnectionAsync(async conn => await conn.queryAsync(sql4))
    /**晒评价总数 */
    let sql5 = `SELECT count(*) as count FROM company_${id}.comment_record WHERE status=5 `
    let res5 = await getConnectionAsync(async conn => await conn.queryAsync(sql5))
    /**昨日晒评价数 */
    let sql6 = `SELECT count(*) as count FROM company_${id}.comment_record WHERE status=5 and date(modify_time)=CURDATE()-INTERVAL 1 DAY `
    let res6 = await getConnectionAsync(async conn => await conn.queryAsync(sql6))
    return { users: res1[0].count, yesterday_users: res2[0].count, sales: res3[0].count, yesterday_sales: res4[0].count, comment_number: res5[0].count, yesterday_comment: res6[0].count }
}

/**********************************************
* 获取ali配置信息
* Author：鲁欣欣
* Date：2018-09-25
*/
export async function getConfig(): Promise<any> {
    let sql = `select * from ali_config`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (res.length == 0) {
        throw Error('未获取到配置信息')
    }
    return res[0]
}

/**********************************************
* 添加商家数据库
* Author：鲁欣欣
* Date：2018-10-13
*/
export async function addDatabase(id: number, phone: string, password: string, real_name: string, database: string): Promise<any> {

    try {
        await transactionAsync(async conn => {
            /**创建数据库company_id */
            console.log(`创建数据库company_${id}`)
            let sql1 = `create DATABASE IF NOT EXISTS ${database} CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';`
            await conn.queryAsync(sql1)

            /**创建account表 */
            console.log(`创建account表`)
            let sql200 = `DROP TABLE IF EXISTS ${database}.account`
            await conn.queryAsync(sql200)
            let sql2 = `CREATE TABLE IF NOT EXISTS ${database}.account (
                account_id int(11) NOT NULL AUTO_INCREMENT COMMENT '账号id',
                account_name varchar(255) NOT NULL DEFAULT '' COMMENT '账号名',
                password varchar(255) NOT NULL DEFAULT '' COMMENT 'MD5之后的密码',
                real_name varchar(255) NOT NULL DEFAULT '' COMMENT '姓名',
                phone varchar(255) DEFAULT NULL COMMENT '手机号',
                position varchar(255) DEFAULT NULL COMMENT '职位',
                supervisor int(11) DEFAULT NULL COMMENT '所属公司id',
                status tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态0：失效；1：有效',
                login_address text COMMENT '记录登陆地址',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '0：未删除；1：已删除',
                PRIMARY KEY (account_id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='个人账号信息表';`
            await conn.queryAsync(sql2)

            /**创建post表 */
            console.log(`创建post表`)
            let sql300 = `DROP TABLE IF EXISTS ${database}.post`
            await conn.queryAsync(sql300)
            let sql3 = `CREATE TABLE IF NOT EXISTS ${database}.post (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '职位id，自增主键',
                name varchar(255) NOT NULL DEFAULT '' COMMENT '职位名称',
                remark varchar(255) DEFAULT NULL COMMENT '职位描述',
                shop_power text NOT NULL COMMENT '店铺权限',
                operate_power text NOT NULL COMMENT '操作权限',
                creator int(11) DEFAULT NULL COMMENT '创建者',
                status tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态0：无效；1：有效',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除0：未删除；1：已删除',
                PRIMARY KEY (id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='职位信息表';`
            await conn.queryAsync(sql3)

            /**创建mch_config表 */
            console.log(`创建mch_config表`)
            let sql4 = `CREATE TABLE IF NOT EXISTS ${database}.mch_config (
                id int(11) NOT NULL AUTO_INCREMENT,
                mch_id varchar(255) NOT NULL DEFAULT '' COMMENT '商户平台id',
                mch_appid varchar(255) NOT NULL DEFAULT '' COMMENT '商户平台对应的公众号appid',
                mch_key varchar(255) NOT NULL DEFAULT '' COMMENT '支付key',
                template_id varchar(255) NOT NULL DEFAULT '' COMMENT '转账成功的微信模板id',
                PRIMARY KEY (id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;`
            await conn.queryAsync(sql4)

            /**创建attendence表 */
            console.log(`创建attendence表`)
            let sql5 = `CREATE TABLE IF NOT EXISTS ${database}.attendence (
                attendence_id int(11) NOT NULL AUTO_INCREMENT COMMENT '签到ID',
                attendence_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '签到日期',
                user_id int(11) DEFAULT NULL COMMENT '用户ID',
                credits float(11,0) DEFAULT NULL COMMENT '积分',
                PRIMARY KEY (attendence_id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;`
            await conn.queryAsync(sql5)

            /**创建tap表 */
            console.log(`创建tap表`)
            let sql600 = `DROP TABLE IF EXISTS ${database}.tap`
            await conn.queryAsync(sql600)
            let sql6 = `CREATE TABLE IF NOT EXISTS ${database}.tap (
                tap_id int(11) NOT NULL AUTO_INCREMENT,
                tap_name varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '标签名字',
                user_num int(11) DEFAULT NULL COMMENT '用户数',
                account_name varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '操作人',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                PRIMARY KEY (tap_id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ;`
            await conn.queryAsync(sql6)

            /**创建trade_order表 */
            console.log(`创建trade_order表`)
            let sql7 = `CREATE TABLE IF NOT EXISTS ${database}.trade_order (
                tid varchar(255) NOT NULL COMMENT '交易编号 (父订单的交易编号)',
                oid varchar(255) NOT NULL COMMENT '子订单号',
                seller_nick varchar(255) DEFAULT NULL COMMENT '卖家昵称（店铺名）',
                buyer_nick varchar(255) DEFAULT NULL COMMENT '买家昵称',
                title varchar(255) DEFAULT NULL COMMENT '商品名称',
                activity_status varchar(255) NOT NULL DEFAULT '0' COMMENT '商品参与活动状态。false:未参与活动;晒评价返利：comment_rebate；返利商城：rebate_mall；订单返利：order_rebate',
                receiver_mobile varchar(255) DEFAULT NULL COMMENT '收货人的手机号码',
                num_iid varchar(255) DEFAULT NULL COMMENT '商品数字编号',
                status varchar(255) DEFAULT NULL COMMENT '交易状态。可选值: \r\n* TRADE_NO_CREATE_PAY(没有创建支付宝交易)\r\n * WAIT_BUYER_PAY(等待买家付款)\r\n * SELLER_CONSIGNED_PART(卖家部分发货)\r\n * WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款) \r\n* WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货)\r\n* TRADE_BUYER_SIGNED(买家已签收,货到付款专用)\r\n * TRADE_FINISHED(交易成功)\r\n* TRADE_CLOSED(付款以后用户退款成功，交易自动关闭) \r\n* TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易) \r\n* PAY_PENDING(国际信用卡支付付款确认中)\r\n* WAIT_PRE_AUTH_CONFIRM(0元购合约中)	\r\n* PAID_FORBID_CONSIGN(拼团中订单或者发货强管控的订单，已付款但禁止发货)',
                price float(11,2) DEFAULT NULL COMMENT '商品价格。精确到2位小数；单位：元。如：200.07，表示：200元7分',
                created_time datetime DEFAULT NULL COMMENT '下单时间',
                pay_time datetime DEFAULT NULL COMMENT '付款时间。格式:yyyy-MM-dd HH:mm:ss',
                end_time datetime DEFAULT NULL COMMENT '交易结束时间。交易成功时间或者交易关闭时间 。格式:yyyy-MM-dd HH:mm:ss',
                buyer_rate varchar(255) DEFAULT NULL COMMENT '买家是否已评价。可选值:1(已评价),0(未评价)。如买家只评价未打分，此字段仍返回false',
                comment varchar(255) DEFAULT NULL COMMENT '评价信息',
                result varchar(255) DEFAULT NULL COMMENT '评价等级',
                comment_time datetime DEFAULT NULL COMMENT '评价时间',
                PRIMARY KEY (tid)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
            await conn.queryAsync(sql7)

            /**创建user表 */
            console.log(`创建user表`)
            let sql8 = `CREATE TABLE IF NOT EXISTS ${database}.user (
                user_id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
                openid varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '用户唯一标示',
                phone char(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '玩家手机号码',
                city varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '城市',
                province varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '省份',
                sex varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '性别(男、女、未知)',
                nickname varchar(255) NOT NULL COMMENT '微信昵称',
                headimgurl text CHARACTER SET utf8 NOT NULL COMMENT '头像地址',
                credits float(11,2) NOT NULL DEFAULT '0.00' COMMENT '积分值',
                tap_id int(11) DEFAULT '1' COMMENT '标签ID',
                is_black tinyint(4) DEFAULT '0' COMMENT '加入黑名单',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次修改时间',
                PRIMARY KEY (user_id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户信息表';`
            await conn.queryAsync(sql8)
            /**创建wxpay表 */
            console.log(`创建wxpay表`)
            let sql9 = `CREATE TABLE IF NOT EXISTS ${database}.wxpay (
                id int(11) NOT NULL COMMENT '递增主键',
                partner_trade_no int(11) NOT NULL COMMENT '转账记录号',
                fee float(11,2) NOT NULL DEFAULT '0.00' COMMENT '转账费用',
                openid varchar(64) NOT NULL COMMENT '转账的openid',
                trade_order_no varchar(32) NOT NULL DEFAULT '' COMMENT '关联的交易记录',
                pay_desc varchar(255) NOT NULL DEFAULT '' COMMENT '入账详情，企业付款，备注的地方微信展示给用户的信息',
                status int(4) NOT NULL DEFAULT '0' COMMENT '转账状态（0： 成功， 1：失败）',
                res varchar(255) NOT NULL DEFAULT '' COMMENT '转账接口返回值',
                created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                PRIMARY KEY (id)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='企业转账给个人的记录表';`
            await conn.queryAsync(sql9)
            console.log(`创建comment_event表`)
            let sql10 = `CREATE TABLE IF NOT EXISTS ${database}.comment_event (
                event_id int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                event varchar(255) DEFAULT NULL COMMENT '活动名称',
                sample_graph varchar(255) DEFAULT NULL COMMENT '示例图',
                start_time datetime DEFAULT NULL COMMENT '开始时间',
                end_time datetime DEFAULT NULL COMMENT '结束时间',
                s_amount float(11,2) DEFAULT NULL COMMENT '返利金额下限',
                e_amount float(11,2) DEFAULT NULL COMMENT '返利金额上限',
                shop text COMMENT '店铺限制',
                shop_id text COMMENT '限制的店铺的id(为了权限判断)',
                commodity text COMMENT '商品限制',
                url varchar(500) DEFAULT NULL COMMENT '推广链接',
                used_number int(11) NOT NULL DEFAULT '0' COMMENT '已参与人数',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除，1：已删除；0：未删除',
                PRIMARY KEY (event_id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='晒评价活动信息表';`
            await conn.queryAsync(sql10)
            console.log(`创建comment_record表`)
            let sql11 = `CREATE TABLE IF NOT EXISTS ${database}.comment_record (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                event_id int(11) DEFAULT NULL COMMENT '活动id',
                user_id int(11) DEFAULT NULL COMMENT '用户id',
                order_id varchar(255) DEFAULT NULL COMMENT '订单号',
                screenshot varchar(255) DEFAULT NULL COMMENT '好评截图',
                amount float(11,2) DEFAULT '0.00' COMMENT '已返金额',
                status tinyint(4) DEFAULT '0' COMMENT '状态:1审核成功；2自动审核失败；3人工审核失败；4返利中；5已返利；6返利失败',
                reason varchar(255) DEFAULT NULL COMMENT '审核失败原因',
                pay_err_desc varchar(255) DEFAULT NULL COMMENT '返利失败原因',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                PRIMARY KEY (id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='晒评价活动参与记录表';`
            await conn.queryAsync(sql11)
            console.log(`创建order_event表`)
            let sql12 = `CREATE TABLE IF NOT EXISTS ${database}.order_event (
                event_id int(11) NOT NULL AUTO_INCREMENT COMMENT '活动id',
                event varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
                start_time datetime DEFAULT NULL COMMENT '活动开始时间',
                end_time datetime DEFAULT NULL COMMENT '结束时间',
                used_amount float(255,2) DEFAULT '0.00' COMMENT '已返金额',
                amount float(11,2) DEFAULT NULL COMMENT '固定返利金额',
                percentage float(11,2) DEFAULT NULL COMMENT '返利金额百分比%',
                rebate_status varchar(255) DEFAULT NULL COMMENT '返利标准，订单状态  已支付，已收货',
                days tinyint(4) DEFAULT NULL COMMENT '返利标准，多少天后',
                integral int(11) DEFAULT NULL COMMENT '赠送积分',
                newuser tinyint(4) DEFAULT NULL COMMENT '是否新用户专享1：是0不是',
                integral_num int(11) DEFAULT NULL COMMENT '积分限制',
                num int(11) DEFAULT NULL COMMENT '一个号限参与几次',
                number int(11) DEFAULT NULL COMMENT '活动总次数',
                used_number int(11) DEFAULT '0' COMMENT '已参与次数',
                shop_id text COMMENT '限制的店铺的id',
                commodity text COMMENT '活动包含商品',
                img text COMMENT '商品图片url',
                url varchar(500) DEFAULT NULL COMMENT '推广链接',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '活动创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '活动更改时间',
                is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除',
                PRIMARY KEY (event_id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='订单返利活动信息表';`
            await conn.queryAsync(sql12)
            console.log(`创建order_record表`)
            let sql13 = `CREATE TABLE IF NOT EXISTS ${database}.order_record (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                event_id int(11) DEFAULT NULL COMMENT '活动id',
                user_id int(11) DEFAULT NULL COMMENT '微信用户id',
                order_id varchar(255) DEFAULT NULL COMMENT '订单号',
                amount float(11,2) DEFAULT NULL COMMENT '返利金额',
                integral int(11) DEFAULT NULL COMMENT '赠送积分',
                status tinyint(4) DEFAULT NULL COMMENT '状态:1审核成功；2自动审核失败；3人工审核失败；4返利中；5已返利；6返利失败',
                reason varchar(255) DEFAULT NULL COMMENT '审核失败原因',
                pay_err_desc varchar(255) DEFAULT NULL COMMENT '返利失败原因',
                settime datetime DEFAULT NULL COMMENT '准备返利时间',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                PRIMARY KEY (id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='订单返利活动参与记录表';`
            await conn.queryAsync(sql13)
            console.log(`创建kv表`)
            let sql14 = `CREATE TABLE IF NOT EXISTS ${database}.kv (
                k varchar(255) NOT NULL COMMENT '积分商城/返利商城/签到配置',
                v varchar(255) DEFAULT NULL COMMENT '{time: x,pic:[{name:xxx,url:...},{...},{..}]} || X',
                PRIMARY KEY (k)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`
            await conn.queryAsync(sql14)
            console.log(`创建rebate_mall表`)
            let sql15 = `CREATE TABLE IF NOT EXISTS ${database}.rebate_mall (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
                activity_name varchar(255) NOT NULL COMMENT '活动名称，不能重复',
                shop_id int(11) NOT NULL COMMENT '商品所属店铺id',
                goods_url varchar(255) DEFAULT NULL COMMENT '淘宝链接， 可为空',
                goods_id varchar(255) NOT NULL COMMENT '商品id',
                goods_img varchar(255) NOT NULL COMMENT '商品图片【{}， {}， {}】',
                goods_price varchar(255) NOT NULL DEFAULT '0-0' COMMENT '商品价格的区间',
                short_link varchar(255) NOT NULL DEFAULT '' COMMENT '淘口令',
                expenditure float(11,2) NOT NULL DEFAULT '0.00' COMMENT '返利金额',
                after_event varchar(255) NOT NULL DEFAULT '{}' COMMENT '返利标准，1、支付后天数， 2、确认收货后, 天数',
                start_time datetime NOT NULL COMMENT '活动开始时间',
                end_time datetime NOT NULL COMMENT '活动结束时间',
                credits int(11) NOT NULL DEFAULT '0' COMMENT '活动赠送的积分， 默认0',
                participation varchar(255) NOT NULL DEFAULT '' COMMENT '参与活动条件，{新用户， 积分满X, 一个用户只能参与X次}',
                total int(11) NOT NULL DEFAULT '0' COMMENT '活动次数',
                flow longtext COMMENT '活动流程',
                notes longtext COMMENT '注意事项',
                created datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modified datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                PRIMARY KEY (id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='返利商城活动列表';`
            await conn.queryAsync(sql15)
            console.log(`创建rebate_mall_record表`)
            let sql16 = `CREATE TABLE IF NOT EXISTS ${database}.rebate_mall_record (
                id int(11) NOT NULL AUTO_INCREMENT,
                activity_id int(11) DEFAULT NULL COMMENT '活动id',
                user_id int(11) DEFAULT NULL COMMENT '用户id',
                tid varchar(255) DEFAULT '' COMMENT '交易订单号',
                rebate varchar(255) DEFAULT NULL COMMENT '返利金额 = 订单价格 +活动返利金额',
                rebate_time datetime DEFAULT NULL COMMENT '返利时间',
                pay_err_desc varchar(255) DEFAULT NULL COMMENT '返利失败原因',
                status varchar(255) DEFAULT NULL COMMENT '活动状态（1、待下单，2 返利中，3返利失败， 4返利成功）',
                created datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modified datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                background int(11) DEFAULT '1' COMMENT '后台业务状态（1、同status，2、待收货 ）',
                PRIMARY KEY (id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;`
            await conn.queryAsync(sql16)
            console.log(`创建tranfer表`)
            let sql17 = `CREATE TABLE IF NOT EXISTS ${database}.transfer (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
                partner_trade_no varchar(64) DEFAULT NULL COMMENT '转账记录号',
                activity varchar(255) DEFAULT NULL COMMENT '关联的活动（晒评价， 订单返利， 返利商城rebate_mall）',
                activity_name varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
                record_id int(11) NOT NULL DEFAULT '0' COMMENT '关联的活动记录',
                openid varchar(64) NOT NULL COMMENT '转账的openid',
                rebate float(11,2) NOT NULL DEFAULT '0.00' COMMENT '转账费用',
                credits int(11) NOT NULL DEFAULT '0' COMMENT '赠送的积分',
                status varchar(255) DEFAULT 'new' COMMENT '是否已转账, new, fin',
                transfer_time datetime DEFAULT NULL COMMENT '待转账的转账时间',
                created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modified datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间， 具体的转账时间',
                PRIMARY KEY (id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='企业转账给个人的记录表';`
            await conn.queryAsync(sql17)
            console.log(`创建credits表`)
            let sql18 = `CREATE TABLE IF NOT EXISTS ${database}.credits (
                credits_id int(11) NOT NULL AUTO_INCREMENT,
                credits int(11) DEFAULT NULL COMMENT '积分变化',
                user_id int(11) DEFAULT NULL COMMENT '用户ID',
                reason varchar(255) DEFAULT NULL COMMENT '来源 0:签到获取，1:订单积分，2:积分消耗',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (credits_id)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;`
            await conn.queryAsync(sql18)
            console.log(`创建gift表`)
            let sql19 = `CREATE TABLE IF NOT EXISTS ${database}.gift (
                gift_id int(11) NOT NULL AUTO_INCREMENT COMMENT '礼品ID',
                gift_name varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '礼品名称',
                gift_cove text CHARACTER SET utf8 COMMENT '封面地址',
                gift_type varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '礼品类型：微信红包',
                credits int(255) DEFAULT NULL COMMENT '所需积分',
                price float(10,2) DEFAULT NULL COMMENT '价格',
                total_num int(11) DEFAULT NULL COMMENT '礼品数量',
                used_num int(11) DEFAULT '0' COMMENT '已使用的数量',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                end_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '活动结束时间',
                shop_set tinyint(4) DEFAULT '0' COMMENT '商城首页，0不是，1是',
                is_del tinyint(4) DEFAULT '0' COMMENT '删除状态0:未删除；1:已删除',
                PRIMARY KEY (gift_id,gift_name) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;`
            await conn.queryAsync(sql19)
            console.log(`创建gift_record表`)
            let sql20 = `CREATE TABLE IF NOT EXISTS ${database}.gift_record (
                id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
                user_id int(11) NOT NULL COMMENT '用户id',
                gift_id int(11) NOT NULL COMMENT '礼品id',
                num int(11) NOT NULL DEFAULT '0' COMMENT '数量',
                credits varchar(255) NOT NULL DEFAULT '0' COMMENT '所需积分',
                status tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:已失效；1:购物车；2:已兑换',
                create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
                is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '0：未删除；1：已删除',
                PRIMARY KEY (id) USING BTREE
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='礼品兑换记录表';`
            await conn.queryAsync(sql20)
            /**初始化数据 */
            console.log('初始化数据')
            let sql30 = `INSERT INTO ${database}.account(account_id, account_name, password, real_name, phone, position, supervisor) VALUES (100000, '${phone}', '${password}', '${real_name}', '${phone}', '1', ${id});`
            await conn.queryAsync(sql30)
            let sql31 = `INSERT INTO ${database}.tap(tap_name, user_num, account_name) VALUES ('未分类', 0, 'root');`
            await conn.queryAsync(sql31)
            let sql32 = `INSERT INTO ${database}.post( name, remark, shop_power, operate_power,creator) VALUES ('root', '所有权限', '', '{\"review\":1,\"orderInfo\":1,\"orderRebate\":1,\"rebateEvent\":1,\"rebateSet\":1,\"integralEvent\":1,\"integralSet\":1,\"account\":1,\"user\":1,\"reviewStat\":1,\"rebateStat\":1,\"signStat\":1,\"cashStat\":1,\"integralStat\":1,\"vipcn\":1,\"shop\":1,\"sms\":1}',100000);`
            await conn.queryAsync(sql32)
            let sql33 = `INSERT INTO ${database}.kv(k, v) VALUES ('turn_time', '3');`
            await conn.queryAsync(sql33)
        })
    } catch (e) {
        console.log(e)
        throw Error('数据库初始化失败，' + e)
    }


}


