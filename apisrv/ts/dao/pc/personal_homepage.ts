/**********************************************
* 个人主页
* Author：鲁欣欣
* Date：2018-09-07
*/

import { getConnectionAsync } from "../../lib/mysqlpool"

/**********************************************
* 获取个人信息
* Author：鲁欣欣
* Date：2018-09-08
*/
export async function getPersonalInfo(database_name: string, account_id: number): Promise<any> {
    let sql = `SELECT account_name FROM ${database_name}.account WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return res[0]
}

/**********************************************
* 原密码验证
* Author：鲁欣欣
* Date：2018-09-08
*/
export async function verifyPasswd(database_name: string, account_id: number): Promise<any> {
    let sql = `SELECT password FROM ${database_name}.account WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return res[0].password
}

/**********************************************
* 修改密码
* Author：鲁欣欣
* Date：2018-09-08
*/
export async function updPersonalPasswd(database_name: number, account_id: number, passwd: string): Promise<any> {
    let sql = `UPDATE ${database_name}.account set password='${passwd}' WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (res.affectedRows === 0) {
        throw Error('修改失败')
    }
    return res.affectedRows
}