/**********************************************
* 职位管理
* Author：鲁欣欣
* Date：2018-09-27
*/

import { getConnectionAsync } from "../../lib/mysqlpool"
import { replaceStr } from "../../lib/utils"

/**********************************************
* 添加职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function addPosition(account_id: number, database_name: string, name: string, remark: string, operate_power: string, shop_power: string): Promise<any> {
    let sql = `insert into ${database_name}.post(name, remark, operate_power, shop_power,creator) values ('${name}', '${remark}', '${operate_power}', '${shop_power}',${account_id})`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows == 0) {
        throw Error('添加失败')
    }
    return rows.affectedRows
}

/**********************************************
* 公司管理者查看职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function rootSelectPosition(database_name: string, search: string, start: number, length: number, status?: string): Promise<any> {
    let rplstr = '/'
    if (search.includes('/')) {
        rplstr = '+'
    }
    search = replaceStr(search, rplstr)
    let sta
    if (status === '') {
        sta = ``
    }
    else {
        sta = `status=${status} and `
    }
    let sql = `FROM ${database_name}.post WHERE ${sta} name like '%${search}%' ESCAPE '${rplstr}' and id<>1 and is_del=0 `
    let sql1 = `SELECT count(*) as count ` + sql
    let sql2 = `SELECT * ` + sql + `ORDER BY modify_time DESC limit ${start} , ${length}`
    console.log(sql1)
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    for (let i = 0; i < data.length; i++) {
        data[i].operate_power = JSON.parse(data[i].operate_power)
    }
    return { recordsFiltered: row, recordsTotal: row, data: data }
}

/**********************************************
* 员工查看职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function selectPosition(account_id: number, database_name: string, search: string, start: number, length: number, status?: string): Promise<any> {
    let rplstr = '/'
    if (search.includes('/')) {
        rplstr = '+'
    }
    search = replaceStr(search, rplstr)
    let sta
    if (status === '') {
        sta = ``
    }
    else {
        sta = `status=${status} and `
    }
    let sql = `FROM ${database_name}.post WHERE creator=${account_id} and ${sta} name like '%${search}%' ESCAPE '${rplstr}' and is_del=0 `
    let sql1 = `SELECT count(*) as count ` + sql
    let sql2 = `SELECT * ` + sql + `ORDER BY modify_time DESC limit ${start} , ${length}`
    console.log(sql1)
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    for (let i = 0; i < data.length; i++) {
        data[i].operate_power = JSON.parse(data[i].operate_power)
    }
    return { recordsFiltered: row, recordsTotal: row, data: data }
}


/**********************************************
* 展示某职位的信息
* Author：鲁欣欣
* Date：2018-10-15
*/
export async function positionInfo(database_name: string, id: number): Promise<any> {
    let sql = `select * FROM ${database_name}.post where id=${id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return rows[0]
}

/**********************************************
* 修改职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function updatePosition(database_name: string, id: number, name: string, remark: string, operate_power: string, shop_power: string): Promise<any> {
    let sql = `update ${database_name}.post set name='${name}', remark='${remark}', operate_power='${operate_power}', shop_power='${shop_power}' where id=${id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows == 0) {
        throw Error('修改失败')
    }
    return rows.affectedRows
}

/**********************************************
* 修改职位状态
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function updatePositionStatus(database_name: string, id: number, status: number): Promise<any> {
    /**下架职位时检查是否还有在使用该职位的账号 */
    if (status == 0) {
        let sql1 = `SELECT count(*) as count FROM ${database_name}.account,${database_name}.post WHERE account.position=post.id and post.id=${id} and post.status=1 and post.is_del=0 and account.is_del=0`
        let res1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
        if (res1[0].count > 0) {
            throw Error('有账号在使用该职位，不能禁用')
        }
    }
    let sql2 = `update ${database_name}.post set status=${status} where id=${id} and is_del=0`
    let rows2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    if (rows2.affectedRows == 0) {
        throw Error('修改失败')
    }
    return rows2.affectedRows
}

/**********************************************
* 删除职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function deletePosition(database_name: string, id: number): Promise<any> {
    let sql = `update ${database_name}.post set is_del=1 where id=${id} and status=0 and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows == 0) {
        throw Error('职位必须禁用才能删除')
    }
    return rows.affectedRows
}

/**********************************************
* 添加时检查职位名是否重复
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function positionName(database_name: string, name: string): Promise<any> {
    let sql = `select * from ${database_name}.post where name='${name}' and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length > 0) {
        throw Error('该职位名已存在')
    }
    return rows.affectedRows
}

/**********************************************
* 修改时检查职位名是否重复
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function positionName1(database_name: string, id: number, name: string): Promise<any> {
    let sql = `select * from ${database_name}.post where name='${name}' and id<>${id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length > 0) {
        throw Error('该职位名已存在')
    }
    return rows.affectedRows
}

/**************************************
 * 查询所有已授权店铺
 * Author：鲁欣欣
 * Date：2018-11-6
 */
export async function selectShop(company_id: string, shop_power: string): Promise<any> {
    let sql = `select id,shop_name from authorization where company_id=${company_id} and now() > auth_time and id in (${shop_power}) `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**********************************************
* 店铺产生时，添加权限到公司主账号
* Author：鲁欣欣
* Date：2018-11-6
*/
export async function updateShop_power(company_id: string, insert_id: string): Promise<any> {
    let sql1 = `select shop_power from company_${company_id}.post WHERE id=1`
    let rows1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let shop_power: string[] = JSON.parse(`[${rows1[0].shop_power}]`)
    shop_power.push(insert_id)
    console.log(shop_power)
    let sql2 = `update company_${company_id}.post set shop_power='${shop_power}'  WHERE id=1`
    console.log(sql2)
    let rows2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    return rows2.affectedRows
}
