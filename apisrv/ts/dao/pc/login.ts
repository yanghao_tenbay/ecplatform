/**********************************************
* 登录检查
* Author：鲁欣欣
* Date：2018-09-26
*/

import express = require('express')
import { getConnectionAsync } from "../../lib/mysqlpool"
import { getRedisClientAsync } from "../../lib/redispool"
import { Account } from "../../entity/account"
import { sendError, md5sum } from "../../lib/utils"
import { rdb } from "../../config/redis"
/*************redis********************
 * 账号登录session
 */
const [loginDbOpt, loginTimeout] = [{ db: rdb.account[0] }, rdb.account[1]]
export async function setLoginAsync(key: string | number, valus: string) {
    await getRedisClientAsync(async client => {
        await client.setAsync(key, valus)
        await client.expireAsync(key, loginTimeout)
    }, loginDbOpt)
}

export async function getLoginAsync(key: string | number): Promise<any> {
    return await getRedisClientAsync(async rds => await rds.getAsync(key), loginDbOpt)
}

export async function delLoginToken(key: string): Promise<void> {
    await getRedisClientAsync(async rds => await rds.delAsync(key), loginDbOpt)
}

/**************************************
 * 检查是否登录，token是否过期
 */
export async function loginCheck(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
        let { token, account_id } = (req as any).cookies
        if (!token || !account_id) {
            console.log('登录超时1！')
            throw new Error('logintimeout')
        }
        let redis_data = await getLoginAsync(token)
        console.log(redis_data)
        if (!redis_data) {
            throw new Error('logintimeout')
        }
        let map = JSON.parse(redis_data)
        req.query['map'] = map
        if (!map.token || map.token !== token) {
            console.log('登录超时2！')
            throw new Error('logintimeout')
        }

        next()
    } catch (e) {
        return sendError(res, e.message)
    }
}

/**************************************
 * 查询管理员账号密码对应的信息
 * Author：鲁欣欣
 * Date：2018-09-26
 */
export async function selectroot(account_name: string, password: string): Promise<any> {
    let sql = `select * from account where account_name = '${account_name}' `
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length === 0) {
        throw Error('账号错误')
    }
    if (rows[0].status === 0) {
        throw Error('账号已被禁用')
    }
    if (rows[0].password !== password) {
        throw Error('密码错误')
    }
    rows[0].token = md5sum(`${new Date().getTime()}_${Math.random()}`);  //getRandStr(32)
    return new Account(rows[0])
}


/**************************************
 * 查询账号密码对应的用户信息
 * @param username
 * @param password
 */
export async function selectAccount(account_name: string, password: string, company_id: number, company_name: string): Promise<any> {
    let sql = `select account_id,account_name,password,real_name,phone,supervisor,post.name as position,account.status as status,operate_power,shop_power from company_${company_id}.account,company_${company_id}.post where account_name = '${account_name}' and account.is_del=0 and post.id=account.position and post.status=1`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length === 0) {
        throw Error('账号错误')
    }
    if (rows[0].status === 0) {
        throw Error('账号已被禁用')
    }
    if (rows[0].password !== password) {
        throw Error('密码错误')
    }
    rows[0].token = md5sum(`${new Date().getTime()}_${Math.random()}`);
    rows[0].operate_power = JSON.parse(rows[0].operate_power)
    rows[0].shop_power = rows[0].shop_power
    rows[0].company_name = company_name
    return new Account(rows[0])
}

/**********************************************
* 重置密码检测输入账号名是否存在
* Author：鲁欣欣
* Date：2018-09-26
*/
export async function selectAccountName(database_name: string, account_name: string): Promise<any> {
    let sql = `select account_id, phone, supervisor from ${database_name}.account where account_name = '${account_name}' and is_del=0;`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length === 0) {
        throw Error('该账号名不存在')
    }
    return rows[0]
}

/**********************************************
 * 检查公司短信余额是否充足
 * Author：鲁欣欣
 * Date：2018-10-18
 */
export async function cheakSms(id: number): Promise<any> {
    let sql = `SELECT sms FROM company WHERE id = ${id}`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result[0].sms <= 0) {
        throw Error('该公司短信余额不足')
    }
}

/**********************************************
 * 发送短信成功，减少公司短信条数
 * Author：鲁欣欣
 * Date：2018-10-18
 */
export async function reduceSms(id: number): Promise<any> {
    let sql = `UPDATE company SET sms = sms - 1 WHERE id = ${id}`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
    return
}

/**********************************************
* 修改密码
* Author：鲁欣欣
* Date：2018-09-26
*/
export async function updPersonalPasswd(company_id: number, account_id: number, password: string): Promise<any> {
    let sql = `UPDATE company_${company_id}.account set password='${password}' WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (res.affectedRows === 0) {
        throw Error('修改失败')
    }
    return res.affectedRows
}

/**********************************************
* 公司管理者账号更新密码到平台公司账号管理表
* Author：鲁欣欣
* Date：2018-09-26
*/
export async function updPasswdToPlatform(supervisor: number, password: string, phone: string): Promise<any> {
    let sql = `UPDATE company set password='${password}' WHERE id=${supervisor} and phone='${phone}' and status=1`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
}

/**************************************
 * 查询公司账号对应的公司信息
 * Author：鲁欣欣
 * Date：2018-09-28
 */
export async function selectCompanyId(company_name: string): Promise<any> {
    let sql = `select id,company_name,domain,database_name from company where company_name = '${company_name}' and company.status=1 and is_del=0 `
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length == 0) {
        throw Error('未找到该公司')
    }
    return rows[0]
}

/**************************************
 * 查询公司账号对应的公众号
 * Author：鲁欣欣
 * Date：2018-09-28
 */
export async function selectwx(id: number): Promise<any> {
    let sql = `select nick_name,head_img from wx_config where ${id}=company_id `
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length == 0) {
        rows.nick_name = ''
        rows.head_img = ''
        return rows
    }
    return rows[0]
}