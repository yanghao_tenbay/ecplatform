import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'

/**********************************************
* 查询
* Author：mjq
* Date：2018-10-08
*/
export async function select(db_name: string) {
    let sql = `select mch_id, mch_key, template_id from ${db_name}.mch_config`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        return {
            mch_id: '',
            mch_key: '',
            template_id: '',
        }
    }
    return rows[0]
}

/**********************************************
* 更改
* Author：mjq
* Date：2018-10-08
*/
interface Imch {
    mch_id: string
    mch_appid: string
    mch_key: string
    template_id: string
}
export async function update(db_name: string, opt: Imch) {
    let sql = `select count(*) as num from ${db_name}.mch_config`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows[0].num) {
        sql = `insert into ${db_name}.mch_config (mch_id, mch_key, mch_appid, template_id) values
        ('${opt.mch_id}', '${opt.mch_key}', '${opt.mch_appid}', '${opt.template_id}')`
    } else {
        sql = `update ${db_name}.mch_config set mch_id = '${opt.mch_id}', mch_appid = '${opt.mch_appid}',
        mch_key = '${opt.mch_key}',  template_id = '${opt.template_id}'`
    }
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
}
