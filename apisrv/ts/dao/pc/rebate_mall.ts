var async = require('async');
let fs = require('fs')
var request = require('request');
import { PC_IMAGES, REQ_IMAGES_prefix } from '../../config/pwd'
import { getConnectionAsync, transactionAsync } from '../../lib/mysqlpool'
import { getRedisClientAsync } from "../../lib/redispool"
import { rdb } from "../../config/redis"
import { replaceStr2, replaceStr3, randomInt } from "../../lib/utils"
import { log_pc as log } from '../../lib/log'


/**********************************************
* 下载图片
* Author：mjq
* Date：2018-10-29
*/
export function downloadPic(buf: any, callback: any) {
    var download = (src: any, dest: any) => {
        request(src).pipe(fs.createWriteStream(dest)).on('close', function () {
            // console.log('pic saved!')
        })
    }

    let result = new Array()
    async.mapLimit(buf, 5, (item: any, callback: any) => {
        setTimeout(() => {
            let name = (new Date()).getTime() + randomInt(10000, 99999) + '.jpg'
            result.push(`${REQ_IMAGES_prefix}${name}`)
            download(item, `${PC_IMAGES}${name}`)
            callback(null, item);
        }, 10);
    }, () => {
        callback(result)
    })
}

/**********************************************
* 根据account_id查询可以管理的店铺， 返回值格式：‘4,5,6,7’
* Author：mjq
* Date：2018-10-29
*/
async function getShopPowerByAccountId(db_name: string, account_id: number): Promise<string> {
    //根据account查询职位对应的权限
    let sql = `select b.shop_power from ${db_name}.account a, ${db_name}.post b where a.account_id = '${account_id}' and b.id = a.position limit 1`
    let rows = await getConnectionAsync(async (conn) => conn.queryAsync(sql))
    if (0 === rows.length) {
        return ''
    }
    return rows[0].shop_power
}

/**********************************************
* 查询授权的店铺
* Author：mjq
* Date：2018-10-29
*/
export async function selectShops(db_name: string, account_id: number) {
    let shop_power = await getShopPowerByAccountId(db_name, account_id)
    if (!shop_power) {
        return new Array()
    }

    let sql = `select id, shop_name from ec_platform.authorization where id in (${shop_power})`
    let rows = await getConnectionAsync(async (conn) => conn.queryAsync(sql))
    return rows
}

/**********************************************
* 查询
* Author：mjq
* Date：2018-10-29
*/
interface SearchRebateMall {
    start: number               //开始位置
    length: number              //查询个数
    activity_name: string      //按照活动名称搜索
    status: string             //按照活动状态搜索
    start_time: string         //活动开始时间
    end_time: string           //活动结束时间
    simplify?: string           //导出要求简化数据当此字段为simplify时，表示为导出表格请求
}
export async function selectRebateMall(db_name: string, account_id: number, opt: SearchRebateMall): Promise<any> {
    let shop_power = await getShopPowerByAccountId(db_name, account_id)
    if (!shop_power) {
        return {
            recordsFiltered: 0,
            recordsTotal: 0,
            data: new Array()
        }
    }

    let activity_name = replaceStr2(opt.activity_name)

    let wh_status = opt.status === '未上线' ? `and a.start_time > now()` : opt.status === '已上线' ? `and (a.start_time < now() and a.end_time > now())` : opt.status === '已下线' ? `and a.end_time < now()` : ''

    let wh_time = ''
    if (opt.start_time && opt.end_time) {
        wh_time = `and ( NOT((a.start_time < '${opt.start_time}' AND a.end_time < '${opt.start_time}') OR ('${opt.end_time}' < a.start_time and '${opt.end_time}' < a.end_time)))`
    } else if (opt.start_time) {
        //结束时间小于查询的开始时间，表示有交集
        wh_time = opt.start_time ? `and a.end_time < '${opt.start_time}'` : ''
    } else if (opt.end_time) {
        //开始时间大于查询的结束时间
        wh_time = opt.end_time ? `and a.start_time > '${opt.end_time}'` : ''
    }

    let sql_from = `from ${db_name}.rebate_mall a, ec_platform.authorization b where a.shop_id in (${shop_power}) and a.activity_name LIKE '%${activity_name.valistr}%' ESCAPE '${activity_name.rplStr}' ${wh_status} ${wh_time} and b.id = a.shop_id`

    let sql_num = `select count(*) as num ${sql_from}`
    let sql_data = `SELECT a.activity_name, a.after_event, a.created, a.credits, a.end_time, a.expenditure, a.flow, a.goods_id, a.goods_img, a.goods_price,  a.goods_url, a.id, a.notes, a.participation, a.short_link, a.start_time, a.total, a.shop_id, b.shop_name, (select count(*) from ${db_name}.rebate_mall_record where activity_id = a.id) Participate_number, (select ifnull(SUM(rebate), 0) from ${db_name}.rebate_mall_record where activity_id = a.id) rebate_total ${sql_from} order by a.id desc limit ${opt.start}, ${opt.length}`

    let rows_num = await getConnectionAsync(async conn => conn.queryAsync(sql_num))
    let rows_data = await getConnectionAsync(async conn => conn.queryAsync(sql_data))
    for (let v of rows_data) {
        v.goods_img = JSON.parse(v.goods_img)
        v.after_event = JSON.parse(v.after_event)
        v.participation = JSON.parse(v.participation)
        let start_time = new Date(v.start_time)
        let end_time = new Date(v.end_time)
        let now = new Date()
        if (start_time > now) {
            v.status = '未上线'
        } else if (now > end_time) {
            v.status = '已下线'
        } else {
            v.status = '已上线'
        }
    }

    return {
        recordsFiltered: rows_num[0].num,
        recordsTotal: rows_num[0].num,
        data: rows_data
    }
}

/**********************************************
* 查询单个信息
* Author：mjq
* Date：2018-10-29
*/
export async function selectDetail(db_name: string, account_id: number, id: number) {
    let shop_power = await getShopPowerByAccountId(db_name, account_id)
    if (!shop_power) {
        return {}
    }

    let sql = `SELECT a.activity_name, a.after_event, a.created, a.credits, a.end_time, a.expenditure, a.flow, a.goods_id, a.goods_img, a.goods_price,  a.goods_url, a.id, a.notes, a.participation, a.short_link, a.start_time, a.total, a.shop_id, b.shop_name, (select count(*) from ${db_name}.rebate_mall_record where activity_id = a.id) Participate_number, (select ifnull(SUM(rebate), 0) from ${db_name}.rebate_mall_record where activity_id = a.id) rebate_total from ${db_name}.rebate_mall a, ec_platform.authorization b where a.id = '${id}' and a.shop_id in (${shop_power}) and b.id = a.shop_id limit 1`
    let rows = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (0 === rows.length) {
        return {}
    }
    return rows[0]
}

/**********************************************
* 添加
* Author：mjq
* Date：2018-10-29
*/
interface IRebateMallActivity {
    activity_name: string       //活动名称，不能重复
    shop_id: string
    goods_url: string           //淘宝链接， 可为空
    goods_id: string            //商品id
    goods_img: string           //商品图片【{}， {}， {}】
    goods_price: string         //商品价格的区间
    short_link: string          //淘口令
    expenditure: number              //返利规则， {百分比， 固定金额， 两个固定金额}
    after_event: string         //返利时间几天后
    start_time: string          //活动开始时间
    end_time: string            //活动结束时间
    credits: number             //活动赠送的积分， 默认0
    participation: string       //参与活动条件，{新用户， 积分满X, 一个用户只能参与X次}
    total: number               //活动次数
    flow: string                //活动流程
    notes: string               //注意事项
}
export async function insertRebateMall(db_name: string, opt: IRebateMallActivity): Promise<void> {
    opt = replaceStr3(opt)

    //开始时间大于当前时间， 结束时间大于当前时间
    let start_time = new Date(opt.start_time)
    let end_time = new Date(opt.end_time)
    let now = new Date()
    if (!(start_time > now && end_time > start_time)) {
        throw new Error('活动时间不正确')
    }

    //检查活动名称是否重复，
    let sql = `select id from ${db_name}.rebate_mall where activity_name = '${opt.activity_name}'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('活动名称重复')
    }

    //检查商品id是否与未过期的活动重复
    sql = `select id from ${db_name}.rebate_mall where goods_id = '${opt.goods_id}' and end_time > now()`
    rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('该商品已有未下线活动，请勿重复')
    }

    sql = `insert into ${db_name}.rebate_mall (activity_name, shop_id, goods_url, goods_id, goods_img, goods_price, short_link, expenditure, after_event,
         start_time, end_time, credits, participation, total, flow, notes) value ('${opt.activity_name}', '${opt.shop_id}', '${opt.goods_url}',
         '${opt.goods_id}', '${opt.goods_img}', '${opt.goods_price}', '${opt.short_link}', '${opt.expenditure}', '${opt.after_event}',
         '${opt.start_time}', '${opt.end_time}', '${opt.credits}', '${opt.participation}', '${opt.total}', '${opt.flow}', '${opt.notes}')`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return
}

/**********************************************
* 活动无人参加，重新编辑
* Author：mjq
* Date：2018-10-29
*/
interface IUpdateRebateMallActivity {
    id: number
    activity_name: string       //活动名称，不能重复
    shop_id: string
    goods_url: string           //淘宝链接， 可为空
    goods_id: string            //商品id
    goods_img: string           //商品图片【{}， {}， {}】
    goods_price: string         //商品价格的区间
    short_link: string          //淘口令
    expenditure: number              //返利金额
    after_event: string         //返利规则
    start_time: string          //活动开始时间
    end_time: string            //活动结束时间
    credits: number             //活动赠送的积分， 默认0
    participation: string       //参与活动条件，{新用户， 积分满X, 一个用户只能参与X次}
    total: number               //活动次数
    flow: string                //活动流程
    notes: string               //注意事项
}
export async function updateRebateMall(db_name: string, opt: IUpdateRebateMallActivity): Promise<void> {
    opt = replaceStr3(opt)

    //活动结束时间必须大于当前时间， 更改时不检查开始时间
    let end_time = new Date(opt.end_time)
    let now = new Date()
    if (!(end_time > now)) {
        throw new Error('活动结束时间必须大于当前时间')
    }

    //检查活动名称是否重复，
    let sql = `select id from ${db_name}.rebate_mall where activity_name = '${opt.activity_name}' and id <> '${opt.id}'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('活动名称重复')
    }

    //检查商品id是否与未过期的活动重复
    sql = `select id from ${db_name}.rebate_mall where goods_id = '${opt.goods_id}' and end_time > now() and id <> '${opt.id}'`
    rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('该商品已有未下线活动，请勿重复')
    }

    //检查是否有人参与活动
    sql = `select id from ${db_name}.rebate_mall_record where activity_id = '${opt.id}'`
    rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('活动已有人参加，只可更改结束时间和次数')
    }

    sql = `update ${db_name}.rebate_mall set activity_name = '${opt.activity_name}', shop_id = '${opt.shop_id}', goods_url = '${opt.goods_url}',
    goods_id = '${opt.goods_id}', goods_img = '${opt.goods_img}', goods_price = '${opt.goods_price}', short_link = '${opt.short_link}',
    expenditure = '${opt.expenditure}', after_event = '${opt.after_event}', start_time = '${opt.start_time}', end_time = '${opt.end_time}',
    credits = '${opt.credits}', participation = '${opt.participation}', total = '${opt.total}', flow = '${opt.flow}', notes = '${opt.notes}'
    where id = '${opt.id}'`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return
}

/**********************************************
* 活动已有人参加，可延长活动结束时间和更改活动次数
* Author：mjq
* Date：2018-10-29
*/
export async function updateRebateMall2(db_name: string, opt: { end_time: string, id: number, total: number }): Promise<void> {
    //活动结束时间必须大于当前时间， 更改时不检查开始时间
    let end_time = new Date(opt.end_time)
    let now = new Date()
    if (!(end_time > now)) {
        throw new Error('结束时间必须大于当前时间')
    }

    let sql = `select count(*) as num from ${db_name}.rebate_mall_record where activity_id = ${opt.id}`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (opt.total > rows[0].num) {
        sql = `update ${db_name}.rebate_mall set end_time = '${opt.end_time}', total = '${opt.total}' where id = '${opt.id}'`
        await getConnectionAsync(async conn => await conn.queryAsync(sql))
        return
    }
    throw new Error('活动次数必须大于已参与次数')
}

/**********************************************
* 删除
* Author：mjq
* Date：2018-10-29
*/
export async function delRebateMall(db_name: string, id: number): Promise<void> {
    //检查是否有人参与活动
    let sql = `select id from ${db_name}.rebate_mall_record where activity_id = '${id}'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 !== rows.length) {
        throw new Error('活动已有人参加，请勿更改')
    }
    sql = `delete from ${db_name}.rebate_mall where id = '${id}' and end_time > now()`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
}

/**********************************************
* 查询活动参与信息
* Author：mjq
* Date：2018-10-29
*/
interface selectRecord {
    start: number               //开始位置
    length: number              //查询个数
    order_status: string        //订单状态
    status: string              //记录状态
    activity_name: string       //活动名称
    tid: string                 //交易订单号
    receiver_mobile: string     //收货人手机号
    created_1: string           //创建时间范围1
    created_2: string           //创建时间范围2
    rebate_time_1: string       //转账时间范围1
    rebate_time_2: string       //转账时间范围2
}
export async function selectRebateMallRecords(db_name: string, account_id: number, opt: selectRecord): Promise<any> {
    let shop_power = await getShopPowerByAccountId(db_name, account_id)
    if (!shop_power) {
        return {
            recordsFiltered: 0,
            recordsTotal: 0,
            data: new Array()
        }
    }

    let tid = replaceStr2(opt.tid)
    let activity_name = replaceStr2(opt.activity_name)
    let receiver_mobile = replaceStr2(opt.receiver_mobile)

    let wh_status = opt.status ? `and c.status = '${opt.status}'` : ''
    let wh_activity_name = opt.activity_name ? `and a.activity_name LIKE '%${activity_name.valistr}%' ESCAPE '${activity_name.rplStr}'` : ''
    let wh_tid = opt.tid ? `and c.tid LIKE '%${tid.valistr}%' ESCAPE '${tid.rplStr}'` : ''
    let wh_created_1 = opt.created_1 ? `and c.created > '${opt.created_1}'` : ''
    let wh_created_2 = opt.created_2 ? `and c.created < '${opt.created_2}'` : ''
    let wh_rebate_time_1 = opt.rebate_time_1 ? `and c.rebate_time > '${opt.rebate_time_1}'` : ''
    let wh_rebate_time_2 = opt.rebate_time_2 ? `and c.rebate_time < '${opt.rebate_time_2}'` : ''

    let wh_receiver_mobile = opt.receiver_mobile ? `and receiver_mobile LIKE '%${receiver_mobile.valistr}%' ESCAPE '${receiver_mobile.rplStr}'` : ''
    let wh_order_status = opt.order_status ? `and order_status = '${opt.order_status}'` : ''

    let tb_1 = `select a.activity_name, a.expenditure, b.nickname, c.id, c.tid, c.rebate, c.created, c.rebate_time, c.status from ${db_name}.rebate_mall a, ${db_name}.user b, ${db_name}.rebate_mall_record c where a.shop_id in (${shop_power}) and a.id = c.activity_id and b.user_id = c.user_id ${wh_status} ${wh_activity_name} ${wh_tid}  ${wh_created_1} ${wh_created_2} ${wh_rebate_time_1} ${wh_rebate_time_2}`

    let tb_2 = `select a2.activity_name, a2.expenditure, a2.nickname, a2.id, a2.tid, a2.rebate, a2.created, a2.rebate_time, a2.status, b2.price, b2.receiver_mobile, b2.status as order_status from(${tb_1}) a2 LEFT JOIN ${db_name}.trade_order b2 ON b2.oid = a2.tid`

    let sql_num = `select count(*) as num from (${tb_2}) a3 where 1 ${wh_receiver_mobile} ${wh_order_status}`
    let sql_data = `select * from (${tb_2}) a3 where 1 ${wh_receiver_mobile} ${wh_order_status} order by id desc limit ${opt.start}, ${opt.length}`

    let rows_num = await getConnectionAsync(async conn => conn.queryAsync(sql_num))
    let rows_data = await getConnectionAsync(async conn => conn.queryAsync(sql_data))
    for (let v of rows_data) {
        for (let k in v) {
            v[k] = v[k] ? v[k] : '--'
        }
    }

    return {
        recordsFiltered: rows_num[0].num,
        recordsTotal: rows_num[0].num,
        data: rows_data
    }
}

/**********************************************
* 更改记录状态, 重新返利
* Author：mjq
* Date：2018-10-29
*/
export async function updateRebateMallDetails(db_name: string, ids: string): Promise<void> {
    ids = JSON.parse(ids)
    let param = `(${ids.toString()})`

    //更改参与记录状态
    let sql = `update ${db_name}.rebate_mall_record set status = 2 where id in ${param} and status = 3`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))

    for (let id of ids) {
        sql = `select mall.expenditure, mall.goods_id, mall.after_event, mall.credits, record.id, record.activity_id, record.tid, user.openid, user.phone from ${db_name}.rebate_mall mall, ${db_name}.user, ${db_name}.rebate_mall_record record where record.id = '${id}' and mall.id = record.activity_id and user.user_id = record.user_id`
        let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
        if (0 === rows.length) {
            throw new Error('数据异常')
        }
        let { openid, expenditure, price, credits } = rows[0]

        //插入转账记录表
        sql = `insert into ${db_name}.transfer (activity, record_id, openid, rebate, credits, transfer_time ) value ('rebate_mall', '${id}', '${openid}', '${expenditure + price}', '${credits}', now())`
        await getConnectionAsync(async conn => await conn.queryAsync(sql))
    }
    return
}

/**********************************************
* 删除待下单的记录
* Author：mjq
* Date：2018-10-29
*/
export async function delRebateMallDetails(db_name: string, id: number): Promise<void> {
    let sql = `delete from ${db_name}.rebate_mall_record where id = '${id}' and status = 1`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return
}

/**********************************************
* 获取商城配置
* Author：mjq
* Date：2018-10-31
*/
export async function getRebateMallCarousel(db_name: string): Promise<any> {
    let sql = `select v from ${db_name}.kv where k = 'rebate_mall_carousel'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        return { "time": "3", "pic": [] }
    }
    return JSON.parse(rows[0].v)
}

/**********************************************
* 设置商城配置
* Author：mjq
* Date：2018-10-31
*/
export async function setRebateMallCarousel(db_name: string, data: string): Promise<void> {
    let sql = `select v from ${db_name}.kv where k = 'rebate_mall_carousel'`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        sql = `insert into ${db_name}.kv (k, v) value ('rebate_mall_carousel', '${data}')`
    } else {
        sql = `update ${db_name}.kv set v = '${data}' where k = 'rebate_mall_carousel'`
    }
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
}

/**********************************************
* 返回商家授权的appid
* Author：mjq
* Date：2018-10-31
*/
export async function getWxConf(db_name: string, account_id: number): Promise<any> {
    let sql = `select appid from ec_platform.wx_config where company_id = '${account_id}' limit 1`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (0 === rows.length) {
        throw new Error('公众号未授权')
    }
    return rows[0].appid
}