import { getConnectionAsync } from "../../lib/mysqlpool";
import { formatDate3, formatDate4, sleep, formatDate2 } from "../../lib/utils";

interface Icredits {
    date: string,
    obitian_credits: number,
    spend_credits: number,
    change_credits: number
}

/**********************************************
 * 格式化兑换记录天数据
 * Author：何语漫
 * Date：2018-11-01
 */
function formatExchangeDataDay(startdate: string, enddate: string, result: any, giftname: string) {
    let statistic_time = new Date(startdate).setHours(0)
    let table = new Array()
    let i = 0

    for (let v of result) {
        let temp_time = new Date(v.date).setHours(0)
        console.log(new Date(v.date))
        if (statistic_time < temp_time) {
            for (statistic_time; statistic_time < temp_time; statistic_time += 24 * 60 * 60 * 1000) {
                let result_date = formatDate3(new Date(statistic_time))
                table[i++] = {
                    id: i, date: result_date, gift_name: giftname, num: 0
                }
                console.log(statistic_time)
            }
        }
        console.log(temp_time)
        console.log(statistic_time)
        if (statistic_time === temp_time) {
            table[i++] = {
                id: i, date: v.date, gift_name: giftname, num: v.num
            }
            console.log(table[i])
            statistic_time += 24 * 60 * 60 * 1000
        }
    }

    for (statistic_time; statistic_time < new Date(enddate).setHours(0); statistic_time += 24 * 60 * 60 * 1000) {
        let result_date = formatDate3(new Date(statistic_time))
        table[i++] = {
            id: i, date: result_date, gift_name: giftname, num: 0
        }
    }
    return { recordsFiltered: table.length, recordsTotal: table.length, data: table }
}

/**********************************************
 * 格式化兑换记录年数据
 * Author：何语漫
 * Date：2018-11-01
 */
function formatExchangeDataYear(startdate: string, enddate: string, result: any, giftname: string) {
    let statistic_time = new Date(startdate)
    let table = new Array()
    let i = 0
    for (let v of result) {
        let temp_time = new Date(v.date)
        console.log(new Date(v.date))
        if (statistic_time < temp_time) {
            for (statistic_time; statistic_time < temp_time; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                let result_date
                result_date = formatDate4(new Date(statistic_time))
                table[i++] = {
                    id: i, date: result_date, gift_name: giftname, num: 0
                }
                console.log(statistic_time)
            }
        }
        console.log(temp_time)
        console.log(statistic_time)
        if (statistic_time.getTime() == temp_time.getTime()) {
            table[i++] = {
                id: i, date: v.date, gift_name: giftname, num: v.num
            }
            console.log(table[i])
            statistic_time.setMonth(statistic_time.getMonth() + 1)
        }
    }

    for (statistic_time; statistic_time < new Date(enddate); statistic_time.setMonth(statistic_time.getMonth() + 1)) {
        let result_date = formatDate4(new Date(statistic_time))
        table[i++] = {
            id: i, date: result_date, gift_name: giftname, num: 0
        }
        console.log(formatDate2(statistic_time))
        console.log(formatDate2(new Date(enddate)))
    }
    return { recordsFiltered: table.length, recordsTotal: table.length, data: table }
}

/**********************************************
 * 格式化签到天数据
 * Author：何语漫
 * Date：2018-11-01
 */
function formatAttendenceDataDay(startdate: string, enddate: string, attendence_result: any, user_result: any, credits: number) {
    let total_user_num = 0
    let statistic_time = new Date(startdate).setHours(0)
    let table = new Array()
    let i = 0
    let j = 0
    for (let v of attendence_result) {
        let temp_time = new Date(v.date).setHours(0)
        console.log(new Date(v.date))
        if (statistic_time < temp_time) {
            for (statistic_time; statistic_time < temp_time; statistic_time += 24 * 60 * 60 * 1000) {
                let result_date = formatDate3(new Date(statistic_time))
                table[i++] = {
                    id: i, date: result_date, num: 0, credits: 0, attendence_rate: 0
                }
                console.log(result_date)
            }
        }
        console.log(temp_time)
        console.log(statistic_time)
        if (statistic_time === temp_time) {
            for (; j < user_result.length; j++) {
                if (new Date(user_result[j].date).setHours(0) <= statistic_time) {
                    console.log(user_result[j].user_num)
                    total_user_num += user_result[j].user_num
                    console.log(total_user_num)
                } else {
                    break
                }
            }
            table[i++] = {
                id: i, date: v.date, num: v.num, credits: v.num * credits, attendence_rate: (v.num / total_user_num).toFixed(2)
            }
            //console.log(table[i])
            statistic_time += 24 * 60 * 60 * 1000
        }
    }
    for (statistic_time; statistic_time < new Date(enddate).setHours(0); statistic_time += 24 * 60 * 60 * 1000) {
        let result_date = formatDate3(new Date(statistic_time))
        table[i++] = {
            id: i, date: result_date, num: 0, credits: 0, attendence_rate: 0
        }

    }
    return { recordsFiltered: table.length, recordsTotal: table.length, data: table }
}

/**********************************************
 * 格式化签到年数据
 * Author：何语漫
 * Date：2018-11-01
 */
function formatAttendenceDataYear(startdate: string, enddate: string, attendence_result: any, user_result: any, credits: number) {
    let total_user_num = 0
    let statistic_time = new Date(startdate)
    let table = new Array()
    let i = 0
    let j = 0
    for (let v of attendence_result) {
        let temp_time = new Date(v.date)
        console.log(new Date(v.date), ' v.date')
        if (statistic_time < temp_time) {
            for (statistic_time; statistic_time < temp_time; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                let result_date = formatDate4(new Date(statistic_time))
                table[i++] = {
                    id: i, date: result_date, num: 0, credits: 0, attendence_rate: 0
                }
                console.log(result_date, 'result_date')
            }
        }
        console.log(temp_time, 'temp_time')
        console.log(statistic_time, 'statistic_time')
        if (statistic_time.getTime() === temp_time.getTime()) {
            for (; j < user_result.length; j++) {
                if (new Date(user_result[j].date) <= statistic_time) {
                    console.log(user_result[j].user_num)
                    total_user_num += user_result[j].user_num
                    console.log(total_user_num)
                } else {
                    break
                }
            }
            table[i++] = {
                id: i, date: v.date, num: v.num, credits: v.num * credits, attendence_rate: (v.num / total_user_num).toFixed(2)
            }
            //console.log(table[i])
            statistic_time.setMonth(statistic_time.getMonth() + 1)
        }
    }
    for (statistic_time; statistic_time < new Date(enddate); statistic_time.setMonth(statistic_time.getMonth() + 1)) {
        let result_date = formatDate4(new Date(statistic_time))
        table[i++] = {
            id: i, date: result_date, num: 0, credits: 0, attendence_rate: 0
        }
        console.log(formatDate2(statistic_time))
        console.log(formatDate2(new Date(enddate)))
    }
    return { recordsFiltered: table.length, recordsTotal: table.length, data: table }
}

/**********************************************
 * 格式化积分天数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomatCreditsDataDay(startdate: string, enddate: string, result_obitain: any, result_spend: any) {
    let index = 0
    let j = 0
    let i = 0
    let result_credits = new Array<Icredits>()
    let statistic_time = new Date(startdate).setHours(0)
    while (i < result_obitain.length && j < result_spend.length) {
        let obitain_date = new Date(result_obitain[i].date).setHours(0)
        let spend_date = new Date(result_spend[j].date).setHours(0)
        for (; obitain_date < spend_date;) {
            if (statistic_time < obitain_date) {
                for (statistic_time; statistic_time < obitain_date; statistic_time += 24 * 60 * 60 * 1000) {
                    let result_date = formatDate3(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '   1')
                }
            }
            if (statistic_time == obitain_date) {
                result_credits[index] = {
                    date: result_obitain[i].date,
                    spend_credits: 0,
                    obitian_credits: result_obitain[i].credits,
                    change_credits: result_obitain[i].credits
                }
                index++
                i++
                statistic_time += 24 * 60 * 60 * 1000
            }
            if (i >= result_obitain.length) {
                break
            }
            obitain_date = new Date(result_obitain[i].date).setHours(0)
        }
        for (; obitain_date > spend_date;) {
            if (statistic_time < spend_date) {
                for (statistic_time; statistic_time < spend_date; statistic_time += 24 * 60 * 60 * 1000) {
                    let result_date = formatDate3(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '    2')
                }
            }
            if (statistic_time == spend_date) {
                result_credits[index] = {
                    date: result_spend[j].date,
                    spend_credits: result_spend[j].credits,
                    obitian_credits: 0,
                    change_credits: result_spend[j].credits
                }
                console.log(result_credits[index])
                j++
                index++
                statistic_time += 24 * 60 * 60 * 1000
            }
            if (j >= result_spend.length) {
                break
            }
            spend_date = new Date(result_spend[j].date).setHours(0)
        }
        if (obitain_date == spend_date) {
            console.log('there!')
            if (statistic_time < obitain_date) {
                for (statistic_time; statistic_time < obitain_date; statistic_time += 24 * 60 * 60 * 1000) {
                    let result_date = formatDate3(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '   1')
                }
            }
            result_credits[index] = {
                date: result_obitain[i].date,
                spend_credits: result_spend[j].credits,
                obitian_credits: result_obitain[i].credits,
                change_credits: result_obitain[i].credits + result_spend[j].credits
            }
            console.log(result_credits[index])
            index++
            j++
            i++
            statistic_time += 24 * 60 * 60 * 1000
        }
    }
    for (; j < result_spend.length; j++) {
        let spend_date = new Date(result_spend[i].date).setHours(0)
        if (statistic_time < spend_date) {
            for (statistic_time; statistic_time < spend_date; statistic_time += 24 * 60 * 60 * 1000) {
                let result_date = formatDate3(new Date(statistic_time))
                result_credits[index] = {
                    date: result_date,
                    spend_credits: 0,
                    obitian_credits: 0,
                    change_credits: 0
                }
                index++
                console.log(result_date + '3')
            }
        }
        if (statistic_time == spend_date) {
            result_credits[index] = {
                date: result_spend[j].date,
                spend_credits: result_spend[j].credits,
                obitian_credits: 0,
                change_credits: result_spend[j].credits
            }
            index++
            statistic_time += 24 * 60 * 60 * 1000
        }
    }
    for (; i < result_obitain.length; i++) {
        let obitain_date = new Date(result_obitain[i].date).setHours(0)
        if (statistic_time < obitain_date) {
            for (statistic_time; statistic_time < obitain_date; statistic_time += 24 * 60 * 60 * 1000) {
                let result_date = formatDate3(new Date(statistic_time))
                result_credits[index] = {
                    date: result_date,
                    spend_credits: 0,
                    obitian_credits: 0,
                    change_credits: 0
                }
                index++
                console.log(result_date + '4')
            }
        }
        if (statistic_time == obitain_date) {
            result_credits[index] = {
                date: result_obitain[i].date,
                spend_credits: 0,
                obitian_credits: result_obitain[i].credits,
                change_credits: result_obitain[i].credits
            }
            index++
            statistic_time += 24 * 60 * 60 * 1000
        }
    }
    for (statistic_time; statistic_time < new Date(enddate).setHours(0); statistic_time += 24 * 60 * 60 * 1000) {
        let result_date = formatDate3(new Date(statistic_time))
        result_credits[index] = {
            date: result_date,
            spend_credits: 0,
            obitian_credits: 0,
            change_credits: 0
        }
        index++
        console.log(result_date + '5')
    }
    console.log(result_credits.length)
    return { recordsFiltered: result_credits.length, recordsTotal: result_credits.length, data: result_credits }
}

/**********************************************
 * 格式化积分年数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomatCreditsDataYear(startdate: string, enddate: string, result_obitain: any, result_spend: any) {
    let index = 0
    let j = 0
    let i = 0
    console.log(startdate)
    console.log(enddate)
    let result_credits = new Array<Icredits>()
    let statistic_time = new Date(new Date(startdate).setHours(0))
    while (i < result_obitain.length && j < result_spend.length) {
        let obitain_date = new Date(new Date(result_obitain[i].date).setHours(0))
        let spend_date = new Date(new Date(result_spend[j].date).setHours(0))
        console.log(obitain_date)
        console.log(spend_date, 'dsd')
        for (; obitain_date < spend_date;) {
            if (statistic_time < obitain_date) {
                for (statistic_time; statistic_time < obitain_date; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                    let result_date = formatDate4(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '   1')
                }
            }
            if (statistic_time.getTime() == obitain_date.getTime()) {
                result_credits[index] = {
                    date: result_obitain[i].date,
                    spend_credits: 0,
                    obitian_credits: result_obitain[i].credits,
                    change_credits: result_obitain[i].credits
                }
                index++
                i++
                statistic_time.setMonth(statistic_time.getMonth() + 1)
            }
            if (i >= result_obitain.length) {
                break
            }
            obitain_date = new Date(new Date(result_obitain[i].date).setHours(0))
        }
        for (; obitain_date > spend_date;) {
            console.log(spend_date)
            if (statistic_time < spend_date) {
                for (statistic_time; statistic_time < spend_date; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                    let result_date = formatDate4(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '    2')
                }
            }
            if (statistic_time.getTime() == spend_date.getTime()) {
                result_credits[index] = {
                    date: result_spend[j].date,
                    spend_credits: result_spend[j].credits,
                    obitian_credits: 0,
                    change_credits: result_spend[j].credits
                }
                console.log(result_credits[index])
                j++
                index++
                statistic_time.setMonth(statistic_time.getMonth() + 1)
            }
            if (j >= result_spend.length) {
                break
            }
            spend_date = new Date(new Date(result_spend[j].date).setHours(0))
        }
        if (obitain_date.getTime() == spend_date.getTime()) {
            console.log('there!')
            if (statistic_time < obitain_date) {
                for (statistic_time; statistic_time < obitain_date; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                    let result_date = formatDate4(new Date(statistic_time))
                    result_credits[index++] = {
                        date: result_date,
                        spend_credits: 0,
                        obitian_credits: 0,
                        change_credits: 0
                    }
                    console.log(result_date + '   1')
                }
            }
            result_credits[index] = {
                date: result_obitain[i].date,
                spend_credits: result_spend[j].credits,
                obitian_credits: result_obitain[i].credits,
                change_credits: result_obitain[i].credits + result_spend[j].credits
            }
            console.log(result_credits[index])
            index++
            j++
            i++
            statistic_time.setMonth(statistic_time.getMonth() + 1)
        }
    }
    for (; j < result_spend.length; j++) {
        let spend_date = new Date(new Date(result_spend[j].date).setHours(0))
        if (statistic_time < spend_date) {
            for (statistic_time; statistic_time < spend_date; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                let result_date = formatDate4(new Date(statistic_time))
                result_credits[index] = {
                    date: result_date,
                    spend_credits: 0,
                    obitian_credits: 0,
                    change_credits: 0
                }
                index++
                console.log(result_date + '3')
            }
        }
        if (statistic_time.getTime() == spend_date.getTime()) {
            result_credits[index] = {
                date: result_spend[j].date,
                spend_credits: result_spend[j].credits,
                obitian_credits: 0,
                change_credits: result_spend[j].credits
            }
            index++
            statistic_time.setMonth(statistic_time.getMonth() + 1)
        }
    }
    for (; i < result_obitain.length; i++) {
        let obitain_date = new Date(new Date(result_obitain[i].date).setHours(0))
        if (statistic_time < obitain_date) {
            for (statistic_time; statistic_time < obitain_date; statistic_time.setMonth(statistic_time.getMonth() + 1)) {
                let result_date = formatDate4(new Date(statistic_time))
                result_credits[index] = {
                    date: result_date,
                    spend_credits: 0,
                    obitian_credits: 0,
                    change_credits: 0
                }
                index++
                console.log(formatDate2(statistic_time) + '4')
            }
        }
        if (statistic_time.getTime() == obitain_date.getTime()) {
            result_credits[index] = {
                date: result_obitain[i].date,
                spend_credits: 0,
                obitian_credits: result_obitain[i].credits,
                change_credits: result_obitain[i].credits
            }
            index++
            statistic_time.setMonth(statistic_time.getMonth() + 1)
        }
    }
    console.log(formatDate2(statistic_time) + '6')
    for (statistic_time; statistic_time.getTime() < new Date(enddate).setHours(0); statistic_time.setMonth(statistic_time.getMonth() + 1)) {
        let result_date = formatDate4(new Date(statistic_time))
        result_credits[index] = {
            date: result_date,
            spend_credits: 0,
            obitian_credits: 0,
            change_credits: 0
        }
        index++
        console.log(formatDate2(statistic_time) + '5')
        console.log(formatDate2(new Date(enddate)))
    }
    console.log(result_credits.length)
    return { recordsFiltered: result_credits.length, recordsTotal: result_credits.length, data: result_credits }
}

/**********************************************
 * 格式化按天统计日期开始数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomaatStartDateDay(startdate: string) {
    let date = new Date()
    date.setHours(0)
    date.setMilliseconds(0)
    if (startdate == '') {
        startdate = formatDate3(new Date(date.setDate(date.getDate() - 15)))
    } else {
        let date = new Date(startdate)
        date.setHours(0)
        date.setMilliseconds(0)
        startdate = formatDate3(date)
    }
    return startdate
}

/**********************************************
 * 格式化按年统计日期开始数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomaatStartDateYear(startdate: string) {
    let date = new Date()
    date.setHours(0)
    date.setMilliseconds(0)
    if (startdate == '') {
        date.setMonth(date.getMonth() - 15)
        date.setDate(1)
        startdate = formatDate3(date)
    } else {
        console.log(formatDate2(new Date(startdate)))
        startdate = formatDate3(new Date(startdate))
    }
    return startdate
}

/**********************************************
 * 格式化按天统计日期结束数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomaatEndDateDay(enddate: string) {
    let date = new Date()
    date.setHours(0)
    date.setMilliseconds(0)
    if (enddate == '') {
        enddate = formatDate3(new Date(date.setDate(date.getDate())))
    } else {
        let date = new Date(enddate)
        date.setHours(0)
        date.setMilliseconds(0)
        enddate = formatDate3(new Date(date.setDate(date.getDate() + 1)))
    }
    return enddate
}

/**********************************************
 * 格式化按年统计日期结束数据
 * Author：何语漫
 * Date：2018-11-01
 */
function fomatEndDateYear(enddate: string) {
    let date = new Date()
    date.setHours(0)
    date.setMilliseconds(0)
    if (enddate == '') {
        date.setDate(1)
        enddate = formatDate3(date)
    } else {
        let true_date = new Date(enddate)
        true_date.setHours(0)
        true_date.setMilliseconds(0)
        enddate = formatDate3(new Date(true_date.setMonth(true_date.getMonth() + 1)))
    }
    return enddate
}

/**********************************************
 * 签到统计
 * Author：何语漫
 * Date：2018-11-01
 */
export async function attendence(database: string, way: number, startdate: string, enddate: string) {
    let sql_attendence = ''
    let sql_user = ''
    let attendence_result: any
    let user_result: any
    let sql = `SELECT v FROM ${database}.kv WHERE k = 'attendence_rule'`
    let result_credits = await getConnectionAsync(async conn => conn.queryAsync(sql))
    let credits = 0
    if (result_credits.length != 0) {
        credits = result_credits[0].v
    }
    if (way == 0) {
        enddate = fomaatEndDateDay(enddate)
        startdate = fomaatStartDateDay(startdate)
        sql_attendence = `SELECT DATE_FORMAT(attendence_date,'%Y-%m-%d') AS date, COUNT(attendence_id) AS num FROM ${database}.attendence  WHERE attendence_date BETWEEN '${startdate}' AND '${enddate}' GROUP BY  DATE_FORMAT(attendence_date,'%Y-%m-%d') `
        sql_user = `SELECT DATE_FORMAT(create_time,'%Y-%m-%d') AS date, COUNT(user_id) AS user_num FROM ${database}.user GROUP BY DATE_FORMAT(create_time,'%Y-%m-%d')`
        attendence_result = await getConnectionAsync(async conn => conn.queryAsync(sql_attendence))
        user_result = await getConnectionAsync(async conn => conn.queryAsync(sql_user))
        let result = formatAttendenceDataDay(startdate, enddate, attendence_result, user_result, credits)
        return result
    } else if (way == 1) {
        startdate = fomaatStartDateYear(startdate)
        enddate = fomatEndDateYear(enddate)
        sql_attendence = `SELECT DATE_FORMAT(attendence_date,'%Y-%m') AS date, COUNT(attendence_id) AS num FROM ${database}.attendence  WHERE attendence_date BETWEEN '${startdate}' AND '${enddate}' GROUP BY  DATE_FORMAT(attendence_date,'%Y-%m')`
        sql_user = `SELECT DATE_FORMAT(create_time,'%Y-%m') AS date, COUNT(user_id) AS user_num FROM ${database}.user GROUP BY DATE_FORMAT(create_time,'%Y-%m')`
        attendence_result = await getConnectionAsync(async conn => conn.queryAsync(sql_attendence))
        user_result = await getConnectionAsync(async conn => conn.queryAsync(sql_user))
        let result = formatAttendenceDataYear(startdate, enddate, attendence_result, user_result, credits)
        return result
    }
    console.log(sql_attendence)
}

/**********************************************
 * 兑换统计
 * Author：何语漫
 * Date：2018-11-01
 */
export async function exchange(database: string, way: number, gift_name: string, startdate: string, enddate: string) {
    let sql = ''
    let result: any
    let search = ''
    let giftname = '--'
    let group_sql = ''
    if (gift_name !== '' && gift_name !== undefined) {
        search = ` AND gift_name = '${gift_name}'`
        giftname = gift_name
        group_sql = `, a.gift_id`
    }
    if (way == 0) {
        enddate = fomaatEndDateDay(enddate)
        startdate = fomaatStartDateDay(startdate)
        sql = `SELECT DATE_FORMAT(a.modify_time,'%Y-%m-%d') AS date, b.gift_name, COUNT(a.num) AS num FROM ${database}.gift_record AS a , ${database}.gift AS b WHERE a.gift_id = b.gift_id AND a.status = 2 AND b.is_del = 0 AND a.is_del = 0 ${search} AND a.modify_time BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(a.modify_time,'%Y-%m-%d') ${group_sql}`
        console.log(sql)
        result = await getConnectionAsync(async conn => conn.queryAsync(sql))
        return formatExchangeDataDay(startdate, enddate, result, giftname)
    } else if (way == 1) {
        startdate = fomaatStartDateYear(startdate)
        enddate = fomatEndDateYear(enddate)
        sql = `SELECT DATE_FORMAT(a.modify_time,'%Y-%m') AS date, b.gift_name, COUNT(a.num) AS num FROM ${database}.gift_record AS a , ${database}.gift AS b WHERE a.gift_id = b.gift_id AND a.status = 2 AND b.is_del = 0 AND a.is_del = 0 ${search} AND a.modify_time BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(a.modify_time,'%Y-%m') ${group_sql} `
        console.log(sql)
        result = await getConnectionAsync(async conn => conn.queryAsync(sql))
        return formatExchangeDataYear(startdate, enddate, result, giftname)
    }
}

/**********************************************
 * 积分统计
 * Author：何语漫
 * Date：2018-11-01
 */
export async function creditsRecord(database: string, way: number, startdate: string, enddate: string) {
    let sql_obitain = ''
    let sql_spend = ''
    let result_obitain: any
    let result_spend: any
    console.log(startdate)
    console.log(enddate)
    if (way == 0) {
        enddate = fomaatEndDateDay(enddate)
        startdate = fomaatStartDateDay(startdate)
        sql_obitain = `SELECT SUM(credits) AS credits, DATE_FORMAT(modify_time,'%Y-%m-%d') AS date FROM ${database}.credits WHERE reason <= 1 AND modify_time  BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(modify_time,'%Y-%m-%d')`
        sql_spend = `SELECT credits, DATE_FORMAT(modify_time,'%Y-%m-%d') AS date FROM ${database}.credits WHERE reason = 2 AND modify_time BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(modify_time,'%Y-%m-%d')`
        console.log(sql_obitain)
        console.log(sql_spend)
        result_obitain = await getConnectionAsync(async conn => conn.queryAsync(sql_obitain))
        result_spend = await getConnectionAsync(async conn => conn.queryAsync(sql_spend))
        let result = fomatCreditsDataDay(startdate, enddate, result_obitain, result_spend)
        return result
    } else if (way == 1) {
        startdate = fomaatStartDateYear(startdate)
        console.log(startdate)
        enddate = fomatEndDateYear(enddate)
        console.log(enddate)
        sql_obitain = `SELECT SUM(credits) AS credits, DATE_FORMAT(modify_time,'%Y-%m') AS date FROM ${database}.credits  WHERE reason <= 1 AND modify_time BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(modify_time,'%Y-%m')`
        sql_spend = `SELECT credits, DATE_FORMAT(modify_time,'%Y-%m') AS date FROM ${database}.credits WHERE reason = 2  AND modify_time BETWEEN '${startdate}' AND '${enddate}' GROUP BY DATE_FORMAT(modify_time,'%Y-%m')`
        result_obitain = await getConnectionAsync(async conn => conn.queryAsync(sql_obitain))
        result_spend = await getConnectionAsync(async conn => conn.queryAsync(sql_spend))
        console.log(sql_obitain)
        console.log(sql_spend)
        let result = fomatCreditsDataYear(startdate, enddate, result_obitain, result_spend)
        return result
    }
}