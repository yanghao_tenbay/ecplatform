/**********************************************
* 公司账号注册
* Author：鲁欣欣
* Date：2018-09-21
*/
import { getConnectionAsync } from "../../lib/mysqlpool"
import { PcSendCode } from "../../lib/sms";

/**********************************************
* 公司注册时手机获取验证码
* Author：鲁欣欣
* Date：2018-09-21
*/
export async function getCode(phone: string): Promise<any> {
    let sql = `SELECT phone,status FROM company WHERE phone='${phone}' and is_del=0;`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length > 0) {
        if (rows[0].status == 0) {
            throw Error('您已提交过审核')
        }
        if (rows[0].status > 0) {
            throw Error('该手机号已注册')
        }
    }
    else {
        console.log('发送验证码')
        PcSendCode(phone)
    }
    return rows.length
}

/**********************************************
* 公司注册信息写入
* Author：鲁欣欣
* Date：2018-09-21
*/
export async function writeCompany(phone: string, company_name: string, shop: string, province: string, city: string, real_name: string, email: string): Promise<any> {
    let sql1 = `SELECT * FROM company WHERE company_name='${company_name}' and is_del=0;;`
    let rows1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    if (rows1.length > 0) {
        throw Error('该公司名已注册')
    }
    let sql = `INSERT INTO company(phone, company_name, shop, province, city, real_name, email) VALUES('${phone}','${company_name}','${shop}','${province}','${city}','${real_name}','${email}');`
    console.log(sql)
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res.affectedRows
}

/**********************************************
* 获取中国地区一级行政区
* Author：鲁欣欣
* Date：2018-09-30
*/
export async function getProvince(): Promise<any> {
    let sql = `SELECT id,name FROM cc_region WHERE LEVEL=1 and state=1`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}

/**********************************************
* 获取中国地区一级行政区对应的二级行政区
* Author：鲁欣欣
* Date：2018-09-30
*/
export async function getCity(province_id: number): Promise<any> {
    let sql = `SELECT id,name FROM cc_region WHERE parent_id=${province_id} and LEVEL=2 and state=1`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return res
}