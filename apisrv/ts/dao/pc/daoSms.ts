import { getConnectionAsync, transactionAsync } from "../../lib/mysqlpool";
import { PrePayUnifiedOrder } from "../../entity/unifiedorder";
import { WxTrade } from "../../entity/wxtrade";
import { getRedisClientAsync } from "../../lib/redispool";
import { formatDate2 } from "../../lib/utils";

const [tradeDbOpt, tradeTimeout] = [{ db: 2 }, 7200] //redis要改
const mainSQL = 'ec_platform'

/**********************************************
 * 获取短信剩余数量
 * Author：何语漫
 * Date：2018-9-27
 */
export async function getRestSms(company_id: string) {
    let sql = `SELECT sms FROM ${mainSQL}.company WHERE id = '${company_id}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 获取短信套餐
 * Author：何语漫
 * Date：2018-9-27
 */
export async function getSmsPcakge() {
    let sql = `SELECT * FROM sms_package`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 获取短信订单
 * Author：何语漫
 * Date：2018-9-27
 */
export async function getSmsOrder(start: string, length: string, company_id: string, start_time: string, end_time: string, out_trade_no: string) {
    let sql_count = `SELECT COUNT(a.id) AS num FROM ${mainSQL}.smstrade a, sms_package b WHERE b.id = a.package_id AND company_id = '${company_id}' AND chargestate = 'fin' `
    let sql = `SELECT a.id, package, out_trade_no, a.modify_time, b.num, a.total_fee FROM ${mainSQL}.smstrade a, ${mainSQL}.sms_package b WHERE b.id = a.package_id AND company_id = '${company_id}' AND chargestate = 'fin' `
    if ((start_time !== undefined && start_time !== '') || (end_time !== undefined && end_time !== '')) {
        if (end_time === undefined || end_time === '') {
            end_time = formatDate2(new Date())
        }
        sql_count += ` AND a.modify_time BETWEEN '${start_time}' AND '${end_time}' `
        sql += ` AND a.modify_time BETWEEN '${start_time}' AND '${end_time}'`
    }
    if (out_trade_no !== undefined && out_trade_no !== '') {
        sql_count += ` AND out_trade_no LIKE '%${out_trade_no}%'`
        sql += ` AND out_trade_no LIKE '%${out_trade_no}%'`
    }
    sql += ` ORDER BY a.modify_time LIMIT ${start}, ${length}`
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
 * 统一下单之后，生成交易记录
 * Author：何语漫
 * Date：2018-9-27
 */
export async function saveTradeRecord(traderoder: PrePayUnifiedOrder, account_id: string, company_id: string) {
    let sql = `INSERT INTO ${mainSQL}.smstrade ( out_trade_no, account_id, company_id, prepay_id, package_id, appid, mch_id, body, total_fee, code_url,payway )
     VALUES ( '${traderoder.getOutTradeNo()}', '${account_id}', '${company_id}', '${traderoder.getPrepayId()}',
    '${traderoder.getProductId()}', '${traderoder.getAppid()}', '${traderoder.getMchId()}', '${traderoder.getBody()}',
    '${traderoder.getTotalFee() / 100}', '${traderoder.getCode_url()}', '微信支付')`
    await getConnectionAsync(async conn => await conn.queryAsync(sql))
}

/**********************************************
 * 查找所有新充值成功的订单
 * Author：何语漫
 * Date：2018-9-27
 */
export async function selectNewChargeState(): Promise<Array<WxTrade>> {
    let sql = `SELECT * FROM ${mainSQL}.smstrade WHERE state = 'fin' AND chargestate = 'new' ORDER BY modify_time`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as Array<any>
    if (rows.length === 0)
        return new Array<WxTrade>()
    return rows.map(r => new WxTrade(r))
}

/**********************************************
 * 异步收到维信支付回复之后，确认订单是否已经完成
 * Author：何语漫
 * Date：2018-9-27
 */
export async function isTradeFinish(out_trade_no: string) {
    let res = await getRedisClientAsync(async rds => await rds.getAsync(out_trade_no), tradeDbOpt)
    return !!res
}

/**********************************************
 * 设置订单已经完成，并缓存到redis
 * Author：何语漫
 * Date：2018-9-27
 */
export async function finishWxTrade(out_trade_no: string) {
    let selectSql = `SELECT state FROM ${mainSQL}.smstrade WHERE out_trade_no = '${out_trade_no}' limit 1`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(selectSql)) as Array<any>
    if (rows.length === 0) {
        return
    }

    let r = rows[0]
    if (r.state === 'new') {
        let updatesql = `UPDATE ${mainSQL}.smstrade SET state = 'fin' WHERE out_trade_no = '${out_trade_no}'`
        await getConnectionAsync(async conn => await conn.queryAsync(updatesql))
    }

    await getRedisClientAsync(async rds => {
        await rds.setAsync(out_trade_no, new Date().toLocaleString())
        await rds.expireAsync(out_trade_no, tradeTimeout)
    }, tradeDbOpt)
}

/**********************************************
 * 收到微信充值成的通知后，更新订单状态，将充值的短信条数充值到公司账号里
 * Author：何语漫
 * Date：2018-9-27
 */
export async function finishSmsCharge(wxtrade: Array<WxTrade>) {
    for (let wt of wxtrade) {
        try {
            await transactionAsync(async conn => {
                let sql = `SELECT num, price FROM ${mainSQL}.sms_package WHERE id = '${wt.getPackage_id()}' `
                let result = await conn.queryAsync(sql)

                sql = `UPDATE ${mainSQL}.company SET sms = sms + '${result[0].num}' WHERE id = '${wt.getCompany_id()}'`
                await conn.queryAsync(sql)

                //业务处理完成之后，更改微信充值记录的状态
                sql = `UPDATE ${mainSQL}.smstrade SET chargestate = 'fin' WHERE out_trade_no = '${wt.getOut_trade_no()}'`
                await conn.queryAsync(sql)
            })
        } catch (e) {
            console.log(e)
        }
    }
}
