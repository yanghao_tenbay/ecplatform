/**********************************************
* 订单返利
* Author：鲁欣欣
* Date：2018-10-30
*/
import { getConnectionAsync } from "../../lib/mysqlpool"
import { replaceStr } from "../../lib/utils";

/**************************************
 * 新建订单返利活动
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function addEvent(company_id: number, database_name: string, event: string, start_time: string, end_time: string, amount: number, percentage: number, rebate_status: string, days: number, integral: number, newuser: number, integral_num: number, num: number, number: number, commodity: string[], img: string[], shop_id: string[]): Promise<any> {
    /**检查活动名是否重复 */
    let sql1 = `SELECT count(*) as count FROM ${database_name}.order_event WHERE event='${event}' and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    if (res[0].count > 0) {
        throw Error('活动名重复')
    }
    let sql = `INSERT INTO ${database_name}.order_event(event,start_time,end_time,amount,percentage,rebate_status, days,integral,newuser,integral_num,num,number,commodity,img,shop_id) VALUES('${event}','${start_time}','${end_time}',${amount},${percentage},'${rebate_status}',${days},${integral},${newuser},${integral_num},${num},${number},'${commodity}','${img}','${shop_id}') `
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    /**生成推广链接 */
    console.log(rows.insertId)
    let sql2 = `SELECT appid FROM wx_config WHERE company_id=${company_id}`
    let res2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    if (res2.length === 0) {
        throw Error('请先授权，再添加活动！')
    }
    let appid = res2[0].appid
    console.log(appid)
    let sql3 = `SELECT component_appid FROM component_config`
    let res3 = await getConnectionAsync(async conn => await conn.queryAsync(sql3)) as any
    let component_appid = res3[0].component_appid
    console.log(component_appid)
    let sql4 = `SELECT main_url FROM component_config `
    let res4 = await getConnectionAsync(async conn => await conn.queryAsync(sql4)) as any
    let redirect_uri = encodeURIComponent(`${res4[0].main_url}wechat/order/userInfo/${company_id}/${rows.insertId}`)
    console.log(redirect_uri)
    let url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appid}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=STATE&component_appid=${component_appid}#wechat_redirect`
    console.log(url)
    let sql5 = `UPDATE ${database_name}.order_event set url='${url}' where event_id=${rows.insertId} and is_del=0`
    console.log(sql5)
    let res5 = await getConnectionAsync(async conn => await conn.queryAsync(sql5)) as any
    if (res5.affectedRows === 0) {
        throw Error('操作失败')
    }
    return res5.affectedRows
}

/**************************************
 * 查询订单返利活动
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function selectEvent(shop_power: string, database_name: string, start: number, length: number, event: string, start_time: string, end_time: string, status: string): Promise<any> {
    let rplstr = '/'
    if (event.includes('/')) {
        rplstr = '+'
    }
    event = replaceStr(event, rplstr)

    let timesql = ``
    if (start_time !== '' && end_time !== '') {
        timesql = `and ('${end_time}'>=start_time or '${start_time}'<=end_time) `
    }

    let statussql = ``
    if (status === '未上线') {
        statussql = `and now()<start_time `
    }
    if (status === '已上线') {
        statussql = `and now() BETWEEN start_time and end_time `
    }
    if (status === '已下线') {
        statussql = `and now()>end_time `
    }

    let sql = `FROM ${database_name}.order_event WHERE event like '%${event}%' ESCAPE '${rplstr}' ${timesql}${statussql} and is_del=0 `
    /*let sql1 = `SELECT count(*) as count ` + sql
    console.log(sql1)
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数*/

    let sql2 = `select * ` + sql + `ORDER BY create_time DESC `
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))

    let dataarr: any[] = []
    let a = 0
    let shop_power_arr: string[] = []
    let shop: string[] = []
    if (shop_power !== '') {
        shop_power_arr = JSON.parse(`[${shop_power}]`)
        console.log('power:' + shop_power_arr)
    }
    console.log('data.length' + data.length)
    for (let i = 0; i < data.length; i++) {
        shop = JSON.parse(`[${data[i].shop_id}]`)
        console.log(shop)
        for (let j = 0; j < shop.length; j++) {
            if (shop_power_arr.indexOf(shop[j]) === -1) {
                break
            }
            if (j === shop.length - 1) {
                if (a >= start && a <= start + length - 1) {
                    dataarr.push(data[i])
                }
                a++
            }
        }
    }
    console.log(dataarr)
    return { recordsFiltered: a, recordsTotal: a, data: dataarr }
}

/**************************************
 * 编辑订单返利活动
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function updateEvent(database_name: string, event_id: number, event: string, start_time: string, end_time: string, amount: number, percentage: number, rebate_status: string, days: number, integral: number, newuser: number, integral_num: number, num: number, number: number, commodity: string[], img: string[], shop_id: string[]): Promise<any> {
    /**检查活动名是否重复 */
    let sql1 = `SELECT count(*) as count FROM ${database_name}.order_event WHERE event='${event}' and event_id<>${event_id} and is_del=0`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    if (res[0].count > 0) {
        throw Error('活动名重复')
    }
    let sql2 = `UPDATE ${database_name}.order_event set event='${event}',start_time='${start_time}',end_time='${end_time}',percentage=${percentage},amount=${amount},rebate_status='${rebate_status}', days=${days},integral=${integral},newuser=${newuser},integral_num=${integral_num},num=${num},number=${number},commodity='${commodity}',img='${img}',shop_id='${shop_id}' WHERE event_id=${event_id}`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql2)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    return rows.affectedRows
}

/**************************************
 * 编辑晒评价返利活动时间和次数
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function updateEventTime(database_name: string, event_id: number, end_time: string, number: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_event set end_time='${end_time}',number=${number} WHERE event_id=${event_id} and is_del=0`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    return rows.affectedRows
}

/**************************************
 * 删除订单返利活动
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function deleteEvent(database_name: string, event_id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_event set is_del=1 WHERE event_id=${event_id} and is_del=0`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    return rows.affectedRows
}


/**************************************
 * 查看订单返利活动参与记录
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function selectRecord(shop_power: string, database_name: string, start: number, length: number, event: string, order_id: string, nickname: string, status: string, order_status: string, start_time: string, end_time: string, s_time: string, e_time: string): Promise<any> {
    let eventstr = '/'
    if (event.includes('/')) {
        eventstr = '+'
    }
    event = replaceStr(event, eventstr)

    let nicknamestr = '/'
    if (nickname.includes('/')) {
        nicknamestr = '+'
    }
    nickname = replaceStr(nickname, nicknamestr)

    let eventsql
    if (event === '') {
        eventsql = ``
    }
    else {
        eventsql = `and d.event like '%${event}%' ESCAPE '${eventstr}' `
    }
    /**订单状态语句 */
    let order_statussql
    if (order_status === '') {
        order_statussql = ``
    }
    if (order_status === '等待买家付款') {
        order_statussql = `and b.status= 'WAIT_BUYER_PAY' `
    }
    if (order_status === '买家已付款') {
        order_statussql = `and b.status in ('WAIT_SELLER_SEND_GOODS','WAIT_BUYER_CONFIRM_GOODS') `
    }
    if (order_status === '交易成功') {
        order_statussql = `and b.status in ('TRADE_BUYER_SIGNED','TRADE_FINISHED') `
    }

    let nicknamesql
    if (nickname === '') {
        nicknamesql = ``
    }
    else {
        nicknamesql = `and c.nickname like '%${nickname}%' ESCAPE '${nicknamestr}' `
    }

    let ordersql
    if (order_id === '') {
        ordersql = ``
    }
    else {
        ordersql = `and a.order_id like '%${order_id}%' `
    }

    let statussql
    if (status === '') {
        statussql = ``
    }
    if (status === '审核成功') {
        statussql = `and a.status=1 `
    }
    if (status === '自动审核失败') {
        statussql = `and a.status=2 `
    }
    if (status === '人工审核失败') {
        statussql = `and a.status=3 `
    }
    if (status === '返利中') {
        statussql = `and a.status=4 `
    }
    if (status === '已返利') {
        statussql = `and a.status=5 `
    }
    if (status === '返利失败') {
        statussql = `and a.status=6 `
    }

    let create_timesql
    if (start_time === '' && end_time === '') {
        create_timesql = `where a.create_time>d.create_time `
    }
    if (start_time === '' && end_time !== '') {
        create_timesql = `where a.create_time<'${end_time}' `
    }
    if (start_time !== '' && end_time === '') {
        create_timesql = `where a.create_time>'${start_time}' `
    }
    if (start_time !== '' && end_time !== '') {
        create_timesql = `where a.create_time between '${start_time}' and '${end_time}' `
    }

    let rebate_timesql
    if (s_time === '' && e_time === '') {
        rebate_timesql = ``
    }
    if (s_time === '' && e_time !== '') {
        rebate_timesql = `and a.modify_time<'${e_time}' and a.status=5 `
    }
    if (s_time !== '' && e_time === '') {
        rebate_timesql = `and a.modify_time>'${s_time}' and a.status=5 `
    }
    if (s_time !== '' && e_time !== '') {
        rebate_timesql = `and a.modify_time between '${s_time}' and '${e_time}' and a.status=5 `
    }


    let sql = `FROM ${database_name}.order_record as a LEFT JOIN ${database_name}.trade_order as b on a.order_id=b.oid JOIN ${database_name}.user as c on a.user_id=c.user_id JOIN ${database_name}.order_event as d on a.event_id=d.event_id ${create_timesql} ${eventsql}${ordersql}${nicknamesql}${statussql}${order_statussql}${rebate_timesql} `
    /*let sql1 = `SELECT count(*) as count ` + sql
    console.log(sql1)
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数*/

    let sql2 = `SELECT a.id as id,event,headimgurl,nickname,a.create_time as create_time,a.amount,a.status as record_status,a.modify_time,reason,pay_err_desc,b.*,d.shop_id  ` + sql + `ORDER BY a.create_time DESC `
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    console.log('data.length:' + data.length)

    let dataarr: any[] = []
    let a = 0
    let shop_power_arr: string[] = []
    let shop: string[] = []
    if (shop_power !== '') {
        shop_power_arr = JSON.parse(`[${shop_power}]`)
        console.log('power:' + shop_power_arr)
    }
    for (let i = 0; i < data.length; i++) {
        shop = JSON.parse(`[${data[i].shop_id}]`)
        console.log(shop)
        for (let j = 0; j < shop.length; j++) {
            if (shop_power_arr.indexOf(shop[j]) === -1) {
                break
            }
            if (j === shop.length - 1) {
                if (a >= start && a <= start + length - 1) {
                    dataarr.push(data[i])
                }
                a++
            }
        }
    }
    console.log(dataarr)

    for (let v of data) {
        switch (v.status) {
            case 'TRADE_NO_CREATE_PAY':
                v.status = '没有创建支付宝交易'
                break;
            case 'WAIT_BUYER_PAY':
                v.status = '等待买家付款'
                break;
            case 'SELLER_CONSIGNED_PART':
                v.status = '卖家部分发货'
                break;
            case 'WAIT_SELLER_SEND_GOODS':
                v.status = '买家已付款'
                break;
            case 'WAIT_BUYER_CONFIRM_GOODS':
                v.status = '卖家已发货'
                break;
            case 'TRADE_BUYER_SIGNED':
                v.status = '买家已签收,货到付款专用'
                break;
            case 'TRADE_FINISHED':
                v.status = '交易成功'
                break;
            case 'TRADE_CLOSED':
                v.status = '交易自动关闭'
                break;
            case 'TRADE_CLOSED_BY_TAOBAO':
                v.status = '主动关闭交易'
                break;
            case 'PAY_PENDING':
                v.status = '国际信用卡支付付款确认中'
                break;
            case 'WAIT_PRE_AUTH_CONFIRM':
                v.status = '0元购合约中'
                break;
            case 'PAID_FORBID_CONSIGN':
                v.status = '已付款但禁止发货'
                break;
        }
    }
    return { recordsFiltered: a, recordsTotal: a, data: dataarr }
}

/**************************************
 * 审核通过
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function approve(database_name: string, id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_record set status=1 WHERE status=2 and id=${id}`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    return rows.affectedRows
}

/**********************************************
* 审核成功后更改活动已参与次数
* Author：鲁欣欣
* Date：2018-10-30
*/
export async function addEventNum(database_name: string, event_id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_event set used_number=used_number+1 WHERE event_id='${event_id}'`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    return
}

/**************************************
 * 审核确认不通过
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function unapprove(database_name: string, id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_record set status=3 WHERE status=2 and id=${id}`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    return rows.affectedRows
}

/**************************************
 * 人工点击重新返利
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function rebate(database_name: string, id: number): Promise<any> {
    let sql = `UPDATE ${database_name}.order_record set status=4 WHERE status=6 and id=${id}`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('操作失败')
    }
    let sql1 = `INSERT INTO ${database_name}.transfer( activity, activity_name, record_id, openid, rebate, credits, transfer_time) (SELECT 'order_rebate',c.event,a.id,b.openid, a.amount, a.integral,now() FROM ${database_name}.order_record as a,${database_name}.user as b,${database_name}.order_event as c WHERE a.user_id=b.user_id and a.event_id=c.event_id and a.id=${id} );`
    console.log(sql1)
    let rows1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    return rows1.affectedRows
}

/**************************************
 * 批量返利
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function batchRebate(database_name: string, ids: any): Promise<any> {
    let param = `(${ids.toString()})`
    console.log(param)
    let sql = `UPDATE ${database_name}.order_record set status=4 WHERE (status=1 or status=6) and id in ${param}`
    console.log(sql)
    await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    let sql1 = `INSERT INTO ${database_name}.transfer( activity, activity_name, record_id, openid, rebate, credits, transfer_time) (SELECT 'order_rebate',c.event,a.id,b.openid, a.amount, a.integral,now() FROM order_record as a,user as b,order_event as c WHERE a.user_id=b.user_id and a.event_id=c.event_id and a.id in ${param} );`
    console.log(sql1)
    await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    return
}

/**************************************
 * 查看某活动的参与记录
 * Author：鲁欣欣
 * Date：2018-10-30
 */
export async function selectEventRecord(database_name: string, start: number, length: number, event_id: number): Promise<any> {
    let sql = `FROM ${database_name}.order_record as a LEFT JOIN ${database_name}.trade_order as b on a.order_id=b.oid JOIN ${database_name}.user as c on a.user_id=c.user_id JOIN ${database_name}.order_event as d on a.event_id=d.event_id where a.event_id=${event_id} `
    let sql1 = `SELECT count(*) as count ` + sql
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `SELECT a.id as id,event,headimgurl,nickname,a.create_time as create_time,a.amount,a.status as record_status,reason,a.modify_time,b.*  ` + sql + `ORDER BY a.create_time DESC limit ${start} , ${length}`
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    return { recordsFiltered: row, recordsTotal: row, data: data }
}

/**********************************************
* 获取店铺id
* Author：鲁欣欣
* Date：2018-11-10
*/
export async function getShop_id(shop_name: string): Promise<any> {
    let sql = `SELECT id FROM authorization WHERE shop_name='${shop_name}'`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.length === 0) {
        throw Error('未找到该店铺，请确认店铺名是否输入正确或者已授权！')
    }
    return rows[0]
}