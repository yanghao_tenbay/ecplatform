/**********************************************
* 公司内部账号管理
* Author：鲁欣欣
* Date：2018-09-28
*/

import { getConnectionAsync } from "../../lib/mysqlpool"
import { replaceStr } from "../../lib/utils"

/**********************************************
* 查询账号
* Author：鲁欣欣
* Date：2018-09-28
*/
export async function selectAccount(database_name: string, start: number, length: number, account_name: string, real_name: string, position?: string, status?: string): Promise<any> {
    let rplstr = '/'
    if (real_name.includes('/')) {
        rplstr = '+'
    }
    real_name = replaceStr(real_name, rplstr)
    let rplstr1 = '/'
    if (account_name.includes('/')) {
        rplstr1 = '+'
    }
    account_name = replaceStr(account_name, rplstr1)
    let positionsql
    if (position === '') {
        positionsql = ``
    }
    else {
        positionsql = `and position=${position} `
    }
    let statussql
    if (status === '') {
        statussql = ``
    }
    else {
        statussql = `and account.status=${status} `
    }

    let sql = `FROM ${database_name}.account,${database_name}.post WHERE position=id and account.is_del=0 and post.is_del=0 and account_name like '%${account_name}%' ESCAPE '${rplstr1}' and real_name like '%${real_name}%' ESCAPE '${rplstr}' ${positionsql}${statussql} and account_id<>100000 `
    let sql1 = `SELECT count(*) as count ` + sql
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `select account_id,account_name,real_name,post.id as position_id, name as position_name,phone,account.status as status  ` + sql + `ORDER BY account.create_time DESC limit ${start} , ${length}`

    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    return { recordsFiltered: row, recordsTotal: row, data: data }
}

/**********************************************
* 账号编辑页面刷新，获取某id的信息
* Author：鲁欣欣
* Date：2018-10-15
*/
export async function selectInfo(database_name: string, account_id: number): Promise<any> {
    let sql = `SELECT account_name,real_name,phone,post.id as position_id,name as position_name FROM ${database_name}.account,${database_name}.post WHERE position=id and account.is_del=0 and post.is_del=0 and account_id=${account_id}`
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data[0]
}

/**********************************************
* 公司管理者展示所有职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function rootShowPosition(database_name: string): Promise<any> {
    let sql = `SELECT id,name from ${database_name}.post WHERE is_del=0 and id>1`
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**********************************************
* 员工展示自己创建的职位
* Author：鲁欣欣
* Date：2018-09-27
*/
export async function showPosition(account_id: number, database_name: string): Promise<any> {
    let sql = `SELECT id,name from ${database_name}.post WHERE creator=${account_id} and is_del=0 and id>1`
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**********************************************
* 检测添加账号名是否重复
* Author：鲁欣欣
* Date：2018-09-07
*/
export async function testAccountName(database_name: string, account_name: string, phone: string): Promise<any> {
    let sql = `SELECT count(*) as count FROM ${database_name}.account WHERE account_name='${account_name}' and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows[0].count > 0) {
        throw Error('账号名重复')
    }
    let sql1 = `SELECT count(*) as count FROM ${database_name}.account WHERE phone='${phone}' and is_del=0`
    let rows1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    if (rows1[0].count > 0) {
        throw Error('手机号重复')
    }
}

/**********************************************
* 添加账号
* Author：鲁欣欣
* Date：2018-09-28
*/
export async function addAccount(account_name: string, password: string, real_name: string, database_name: string, position: number, phone: string, supervisor: number): Promise<any> {
    let sql = `insert into ${database_name}.account (account_name,password,real_name,supervisor,position,phone) values ('${account_name}','${password}','${real_name}',${supervisor},'${position}','${phone}')`
    console.log(sql)
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('添加失败')
    }
    return rows.affectedRows
}


/**********************************************
* 修改账号
* Author：鲁欣欣
* Date：2018-09-28
*/
export async function updateAccount(database_name: string, account_id: number, real_name: string, position: number, phone: string): Promise<any> {
    let sql = `UPDATE ${database_name}.account set real_name='${real_name}',position=${position},phone='${phone}' WHERE account_id=${account_id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows === 0) {
        throw Error('修改失败')
    }
    return rows.affectedRows
}

/**********************************************
* 删除账号
* Author：鲁欣欣
* Date：2018-09-28
*/
export async function deleteAccount(database_name: string, account_id: number): Promise<any> {
    let sql = `update ${database_name}.account set is_del=1 WHERE account_id=${account_id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    if (rows.affectedRows == 0) {
        throw Error('删除失败')
    }
    return rows.affectedRows
}

/**********************************************
* 限制账号登录
* Author：鲁欣欣
* Date：2018-09-28
*/
export async function restrictAccount(database_name: string, account_id: number, status: number): Promise<any> {
    let sql = `update ${database_name}.account set status=${status} WHERE account_id=${account_id} and is_del=0`
    let rows = await getConnectionAsync(async conn => await conn.queryAsync(sql)) as any
    console.log(rows)
    if (rows.affectedRows == 0) {
        throw Error('修改失败')
    }
    return rows.affectedRows
}
