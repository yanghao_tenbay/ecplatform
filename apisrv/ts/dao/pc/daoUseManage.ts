import { getConnectionAsync, transactionAsync } from "../../lib/mysqlpool";
import { replaceStr, formatDate3 } from "../../lib/utils";

/**********************************************
 * 获取所有标签
 * Author：何语漫
 * Date：2018-9-26
 */
export async function getTap(database_name: number) {
    let sql = `SELECT tap_id, tap_name FROM ${database_name}.tap`
    let row = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return row
}

/**********************************************
 * 获取所有用户
 * Author：何语漫
 * Date：2018-9-26
 */
export async function getUser(start: string, length: string, database_name: number) {
    let sql = `SELECT COUNT(user_id) AS num FROM ${database_name}.user a, ${database_name}.tap b WHERE b.tap_id = a.tap_id`
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql))
    sql = ` SELECT headimgurl, user_id, nickname, phone, tap_name, a.tap_id, city, sex, credits, is_black, a.create_time FROM ${database_name}.user a, ${database_name}.tap b WHERE b.tap_id = a.tap_id LIMIT ${start}, ${length} `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
 * 将用户批量/单独加入黑名单
 * Author：何语漫
 * Date：2018-9-26
 */
export async function joinBlack(user_id: string, database_name: number) {
    user_id = JSON.parse(user_id)
    let param = `(${user_id.toString()})`
    let sql = `UPDATE ${database_name}.user SET is_black = 1 WHERE user_id IN ${param}`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectedRows != 0) {
        return 0
    }
    return 1
}

/**********************************************
 * 解除黑名单
 * Author：何语漫
 * Date：2018-9-26
 */
export async function outBlack(user_id: any, database_name: number) {
    let sql = `UPDATE ${database_name}.user SET is_black = 0 WHERE user_id = '${user_id}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectedRows != 0) {
        return 0
    }
    return 1
}

/**********************************************
 * 查找用户信息
 * Author：何语漫
 * Date：2018-9-26
 */
export async function searchUser(nickname: string, phone: string, tap_name: string, start: string, length: string, database_name: number) {
    let sql = `SELECT headimgurl, user_id, nickname, phone, tap_name, city, sex, credits, is_black, a.create_time FROM ${database_name}.user a, ${database_name}.tap b WHERE a.tap_id = b.tap_id  `
    let sql_count = `SELECT COUNT(*) AS num FROM ${database_name}.user a, ${database_name}.tap b WHERE a.tap_id = b.tap_id `
    if (nickname !== '' && nickname !== undefined) {
        let rplstr = '/'
        if (nickname.includes('/')) {
            rplstr = '+'
        }
        nickname = replaceStr(nickname, rplstr)
        sql += ` AND nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
        sql_count += ` AND nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
    }
    if (phone !== '' && phone !== undefined) {
        sql += ` AND phone LIKE '%${phone}%'`
        sql_count += ` AND phone LIKE '%${phone}%'`
    }
    if (tap_name !== '' && tap_name !== undefined) {
        let rplstr = '/'
        if (tap_name.includes('/')) {
            rplstr = '+'
        }
        tap_name = replaceStr(tap_name, rplstr)
        sql += ` AND tap_name LIKE '%${tap_name}%'  ESCAPE '${rplstr}'`
        sql_count += ` AND tap_name LIKE '%${tap_name}%'  ESCAPE '${rplstr}'`
    }
    sql += ` LIMIT ${start}, ${length} `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
 * 获取用户详细信息
 * Author：何语漫
 * Date：2018-9-26
 */
export async function userInfo(user_id: string, database_name: number) {
    let sql = `SELECT headimgurl, user_id, nickname, phone, tap_name, city, province, sex, is_black, credits, a.create_time FROM ${database_name}.user a, ${database_name}.tap b WHERE user_id = '${user_id}' AND a.tap_id = b.tap_id`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 批量/单独打标签
 * Author：何语漫
 * Date：2018-9-26
 */
export async function joinTap(user_id: any, tap_id: string, database_name: number) {
    await transactionAsync(async conn => {
        user_id = JSON.parse(user_id)
        let param = `(${user_id.toString()})`
        let sql = `UPDATE ${database_name}.user SET tap_id = '${tap_id}' WHERE user_id IN ${param} `
        let sql_tapid = `SELECT tap_id,COUNT(user_id) num FROM ${database_name}.user WHERE user_id IN ${param} GROUP BY tap_id`
        let result = await conn.queryAsync(sql_tapid)
        let num = 0
        if (result.length == 0) {
            return 1
        } else {
            let value = ''
            let param = ''
            for (let i = 0; i < result.length; i++) {
                value += ` WHEN ${result[i].tap_id} THEN user_num - ${result[i].num} `
                param += ` ${result[i].tap_id},`
                num += result[i].num
            }
            param = param.substring(0, param.length - 1)
            sql_tapid = `UPDATE ${database_name}.tap  SET user_num = CASE tap_id ${value} END WHERE tap_id IN ( ${param} )`
            await conn.queryAsync(sql_tapid)
        }
        result = await conn.queryAsync(sql)
        if (result.affectedRows == 0) {
            return 1
        }
        sql = `UPDATE ${database_name}.tap SET user_num = user_num + ${num} WHERE tap_id = ${tap_id}`
        await conn.queryAsync(sql)
    })
    return 0
}

/**********************************************
 * 新建标签
 * Author：何语漫
 * Date：2018-9-26
 */
export async function addTap(tap_name: string, action_name: string, database_name: number) {
    tap_name = tap_name.replace(/'/g, "''")
    tap_name = tap_name.replace(/\\/g, "\\\\")
    action_name = action_name.replace(/'/g, "''")
    action_name = action_name.replace(/\\/g, "\\\\")
    let sql = `INSERT INTO ${database_name}.tap(tap_name, user_num, account_name) VALUES('${tap_name}', 0, '${action_name}')`
    await await getConnectionAsync(async conn => conn.queryAsync(sql))
    sql = `SELECT tap_id FROM ${database_name}.tap order by tap_id DESC limit 1`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 更新标签
 * Author：何语漫
 * Date：2018-9-26
 */
export async function updateTap(tap_id: string, newTap_name: string, action_name: string, database_name: number) {
    newTap_name = newTap_name.replace(/'/g, "''")
    newTap_name = newTap_name.replace(/\\/g, "\\\\")
    let sql = `UPDATE ${database_name}.tap SET tap_name = '${newTap_name}', account_name = '${action_name}' WHERE tap_id = '${tap_id}'  `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectRows == 0) {
        return 1
    }
    return 0
}

/**********************************************
 * 删除标签
 * Author：何语漫
 * Date：2018-9-26
 */
export async function delTap(tap_id: string, database_name: number) {
    await transactionAsync(async conn => {
        let sql = `DELETE FROM ${database_name}.tap WHERE tap_id = '${tap_id}'`
        let result = await conn.queryAsync(sql)
        sql = `UPDATE ${database_name}.user SET tap_id = '1' WHERE tap_id = '${tap_id}' `
        let num = await conn.queryAsync(sql)
        sql = `UPDATE ${database_name}.tap SET user_num = user_num + ${num.affectedRows} WHERE tap_id = '1'`
        await conn.queryAsync(sql)
        if (result.affectRows == 0) {
            return 1
        }
    })
    return 0
}

/**********************************************
 * 获取标签列表，更改时间降序排序
 * Author：何语漫
 * Date：2018-9-26
 */
export async function getTapInfo(database_name: number, start: string, length: string, search: string) {
    let sql: string
    let sql_count: string
    if (search !== '' && search !== undefined) {
        let rplstr = '/'
        if (search.includes('/')) {
            rplstr = '+'
        }
        search = replaceStr(search, rplstr)
        sql = `SELECT * FROM ${database_name}.tap WHERE tap_name LIKE '%${search}%' ESCAPE '${rplstr}' AND tap_id > 1 ORDER BY modify_time DESC LIMIT ${start}, ${length}`
        sql_count = `SELECT COUNT(tap_id) as num FROM ${database_name}.tap WHERE tap_name LIKE '%${search}%' AND tap_id > 1 ESCAPE '${rplstr}' `
    } else {
        sql = ` SELECT * FROM ${database_name}.tap WHERE tap_id > 1 ORDER BY modify_time DESC LIMIT ${start}, ${length}`
        sql_count = `SELECT COUNT(tap_id) as num FROM ${database_name}.tap WHERE tap_id > 1`
    }
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

export async function getComment(user_id: string, database_name: string, start: string, length: string) {
    let sql_count = `SELECT COUNT(order_id) AS num FROM  ${database_name}.comment_record as a , ${database_name}.comment_event as b, ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.event_id = a.event_id AND c.oid = a.order_id AND a.status = 5`
    let sql = ` SELECT order_id, event, screenshot, amount, c.result, a.create_time FROM  ${database_name}.comment_record as a , ${database_name}.comment_event as b, ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.event_id = a.event_id AND c.oid = a.order_id AND a.status = 5 ORDER BY create_time DESC LIMIT ${start}, ${length}`
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    for (let i = 0; i < result.length; i++) {
        //good(好评), neutral(中评), bad(差评)
        if (result[i].result == 'good') {
            result[i].result = '好评'
        } else if (result[i].result == 'neutral') {
            result[i].result = '中评'
        } else if (result[i].result == 'bad') {
            result[i].result = '差评'
        }
    }
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

export async function getOrder(user_id: string, database_name: string, start: string, length: string) {
    let sql_count = `SELECT  COUNT(order_id) AS num FROM ${database_name}.order_record as a, ${database_name}.order_event as b, ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.event_id = a.event_id AND c.oid = a.order_id AND a.status = 5`
    let sql = `SELECT order_id , event, a.amount, a.modify_time FROM ${database_name}.order_record as a, ${database_name}.order_event as b, ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.event_id = a.event_id AND c.oid = a.order_id AND a.status = 5 ORDER BY a.create_time DESC LIMIT ${start}, ${length}`
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}
/**********************************************
 * 获取用户订单详情
 * Author：何语漫
 * Date：2018-10-09
 */
export async function getRebate(user_id: string, database_name: string, start: string, length: string) {
    let sql_count = `SELECT COUNT(a.tid) AS num FROM ${database_name}.rebate_mall_record as a, ${database_name}.rebate_mall as b , ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.id = a.activity_id AND c.oid = a.tid AND a.status = 4 `
    let sql = `SELECT a.tid, activity_name, rebate, a.modified FROM ${database_name}.rebate_mall_record as a, ${database_name}.rebate_mall as b , ${database_name}.trade_order as c, ${database_name}.user as d WHERE d.user_id = ${user_id} AND a.user_id = d.user_id AND b.id = a.activity_id AND c.oid = a.tid AND a.status = 4 ORDER BY create_time DESC LIMIT ${start}, ${length}`
    console.log(sql_count)
    console.log(sql)
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
 * 获取用户积分记录
 * Author：何语漫
 * Date：2018-10-09
 */
export async function getCredit(databasename: string, nickname: string, phone: string, rule: string, start_date: string, end_date: string, start: string, length: string) {
    let sql = `SELECT a.credits_id, b.nickname, b.headimgurl, b.phone, a.reason, a.credits, a.modify_time FROM ${databasename}.credits AS a, ${databasename}.user as b WHERE b.user_id = a.user_id`
    let sql_count = `SELECT COUNT(a.credits_id) AS num FROM ${databasename}.credits AS a, ${databasename}.user AS b WHERE b.user_id = a.user_id`

    if (nickname !== '' && nickname !== undefined) {
        let rplstr = '/'
        if (nickname.includes('/')) {
            rplstr = '+'
        }
        nickname = replaceStr(nickname, rplstr)
        sql += ` AND b.nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
        sql_count += ` AND b.nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
    }

    if (phone !== '' && phone !== undefined) {
        sql += ` AND b.phone LIKE '%${phone}%'`
        sql_count += ` AND b.phone LIKE '%${phone}%'`
    }

    if (rule == '签到获得') {
        sql += ` AND a.reason = 0`
        sql_count += ' AND a.reason = 0'
    } else if (rule == '积分消耗') {
        sql += ` AND a.reason = 2 `
        sql_count += ' AND a.reason = 2'
    } else if (rule == '订单获得') {
        sql += ` AND a.reason = 1 `
        sql_count += ' AND a.reason = 1 '
    }

    if ((start_date !== '' && start_date !== undefined) || (end_date !== '' && end_date !== undefined)) {
        if (end_date == '' || end_date == undefined) {
            let date = new Date().setDate(new Date().getDate() + 1)
            end_date = formatDate3((new Date(date)))
        }
        sql += ` AND a.modify_time BETWEEN '${start_date}' AND '${end_date}'`
        sql_count += ` AND a.modify_time BETWEEN '${start_date}' AND '${end_date}'`
    }
    sql += ` ORDER BY a.modify_time DESC LIMIT ${start}, ${length}`
    console.log(sql)
    console.log(sql_count)
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    for (let i = 0; i < result.length; i++) {
        if (result[i].reason == 2) {
            result[i].reason = '积分消耗'
        } else if (result[i].reason == 0) {
            result[i].reason = '签到获得'
        } else if (result[i].reason == 1) {
            result[i].reason = '订单获得'
        }
    }
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}