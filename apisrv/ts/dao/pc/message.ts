/**********************************************
* 消息通知
* Author：鲁欣欣
* Date：2018-11-14
*/

import { getConnectionAsync } from "../../lib/mysqlpool"

/**************************************
 * 查询店铺
 * Author：鲁欣欣
 * Date：2018-11-14
 */
export async function checkShop(company_id: string, shop_power: string): Promise<any> {
    let sql = `select shop_name,TIMESTAMPDIFF(DAY,now(),ADDDATE(auth_time,INTERVAL 31 DAY)) as time from authorization where company_id=${company_id} and now() BETWEEN ADDDATE(auth_time,INTERVAL 26 DAY) and ADDDATE(auth_time,INTERVAL 31 DAY) and id in (${shop_power}) `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**************************************
 * 查询短信剩余条数
 * Author：鲁欣欣
 * Date：2018-11-14
 */
export async function checkSms(company_id: string): Promise<any> {
    let sql = `SELECT sms FROM company WHERE id=${company_id} and sms<50 `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**************************************
 * 查询公司是否到期
 * Author：鲁欣欣
 * Date：2018-11-14
 */
export async function checkCompany(company_id: string): Promise<any> {
    let sql = `SELECT TIMESTAMPDIFF(DAY,now(),end_time) as time FROM  company WHERE id=${company_id} and now() BETWEEN SUBDATE(end_time,INTERVAL 30 DAY) AND end_time `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data
}

/**************************************
 * 百度IP定位配置ak
 * Author：鲁欣欣
 * Date：2018-11-16
 */
export async function getAk(): Promise<any> {
    let sql = `SELECT ak FROM baidu_config `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (data.length === 0) {
        throw Error('未获取到百度ip定位api的配置信息，请联系平台管理员')
    }
    return data[0].ak
}

/**************************************
 * 读取登录信息
 * Author：鲁欣欣
 * Date：2018-11-16
 */
export async function getLoginAddress(database_name: string, account_id: number): Promise<any> {
    let sql = `SELECT login_address FROM ${database_name}.account WHERE account_id=${account_id} `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return data[0].login_address
}

/**************************************
 * 更新登录信息
 * Author：鲁欣欣
 * Date：2018-11-16
 */
export async function updateLoginAddress(database_name: string, account_id: number, newlogin: string): Promise<any> {
    let sql = `UPDATE ${database_name}.account set login_address='${newlogin}' WHERE account_id=${account_id} `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return
}