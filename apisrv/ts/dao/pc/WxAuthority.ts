import { getAuthorityConf, getCreateManueUrl, getPreAuthcode } from "../../lib/compconfig";
import { postAsync } from "../../lib/request";
import { getConnectionAsync } from "../../lib/mysqlpool";
import { setAccessToken } from "../../lib/wxconfig";
import { getRedisClientAsync } from "../../lib/redispool";
import { log_pc as log } from '../../lib/log'
const [wxauthDbOpt, wxauthTimeout] = [{ db: 6 }, 1800]

/**********************************************
 * 用户扫码授权后，保存接口凭证，保存用户公众号信息
 * Author：何语漫
 * Date：2018-09-25
 */
export async function authority(authorization_code: string, company_id: string) {
    let auth = await getAuthorityConf()
    let postOpt = {
        url: `https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=${auth.compAccessToken}`,
        body: JSON.stringify({
            component_appid: auth.compAppid,
            authorization_code: authorization_code
        })
    }
    let result = await postAsync(postOpt)
    let rMap = await JSON.parse(result)
    log.debug('/授权信息： '+JSON.stringify(rMap))
    let func_info = new Array()
    for (let v of rMap.authorization_info.func_info) {
        func_info.push(v.funcscope_category.id)
    }
    //保存 authorizer_access_token authorizer_refresh_token
    await setAccessToken(rMap.authorization_info.authorizer_appid, rMap.authorization_info.authorizer_access_token, rMap.authorization_info.authorizer_refresh_token, company_id, func_info.toString())
    //添加公众账号信息
    await insertInfo(company_id, rMap.authorization_info.authorizer_appid)
    await WxAuthFinish(company_id)
    await creatManue(rMap.authorization_info.authorizer_appid, auth.compAppid)
}

/**********************************************
 * 创建菜单
 * Author：何语漫
 * Date：2018-09-25
 */
export async function creatManue(appid: string, component_appid: string) {
    //    let access_token = await getaccess_token(appid)
    let redirect_uri = (await getCreateManueUrl()).createManue_url
    redirect_uri = encodeURIComponent(redirect_uri)
    let url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appid}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=STATE&component_appid=${component_appid}#wechat_redirect`
    /*    let str = {
            "button": [{
                "type": "view",
                "name": "个人中心",
                "url": `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appid}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=STATE&component_appid=${component_appid}#wechat_redirect`
            }]
        }
        let sql = `UPDATE wx_config SET manul_url = '${str.button[0].url}' WHERE appid = '${appid}'`
        await getConnectionAsync(async conn => conn.queryAsync(sql))
        let postOpt = {
            url: `https://api.weixin.qq.com/cgi-bin/menu/create?access_token=${access_token}`,
            body: JSON.stringify(str)
        }
        await postAsync(postOpt)*/
    let sql = `UPDATE wx_config SET manul_url = '${url}' WHERE appid = '${appid}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
}

/**********************************************
 * 微信授权成功
 * Author：何语漫
 * Date：2018-10-18
 */
export async function WxAuthFinish(company_id: string) {
    await getRedisClientAsync(async rds => {
        await rds.setAsync(company_id, new Date().toLocaleString())
        await rds.expireAsync(company_id, wxauthTimeout)
    }, wxauthDbOpt)
}

/**********************************************
 * 微信授权是否成功
 * Author：何语漫
 * Date：2018-10-18
 */
export async function isWxAuthFinish(company_id: string) {
    let res = await getRedisClientAsync(async rds => await rds.getAsync(company_id), wxauthDbOpt)
    return !!res
}

/**********************************************
 * 保存用户公众号信息
 * Author：何语漫
 * Date：2018-09-25
 */
export async function insertInfo(company_id: string, appid: string) {
    let auth = await getAuthorityConf()
    let postOpt = {
        url: `https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=${auth.compAccessToken}`,
        body: JSON.stringify({
            component_appid: auth.compAppid,
            authorizer_appid: appid
        })
    }
    let result = await postAsync(postOpt)
    let rMap = JSON.parse(result)
    let sql = `UPDATE ec_platform.wx_config SET nick_name = '${rMap.authorizer_info.nick_name}', head_img = '${rMap.authorizer_info.head_img}', user_name = '${rMap.authorizer_info.user_name}', principal_name = '${rMap.authorizer_info.principal_name}', qrcode_url =  '${rMap.authorizer_info.qrcode_url}' WHERE appid = '${appid}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
    //保存appid进入company表
    sql = `UPDATE ec_platform.company SET appid = '${appid}' WHERE id = '${company_id}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
    return {
        nick_name: `${rMap.authorizer_info.nick_name}`,
        head_img: `${rMap.authorizer_info.head_img}`,
        user_name: `${rMap.authorizer_info.user_name}`, principal_name: `${rMap.authorizer_info.principal_name}`, qrcode_url: `${rMap.authorizer_info.qrcode_url}`
    }
}

/**********************************************
 * 获取用户公众号信息
 * Author：何语漫
 * Date：2018-09-25
 */
export async function getAuthInfo(company_id: string) {
    let sql = `SELECT appid, nick_name, head_img, user_name, principal_name, qrcode_url, manul_url FROM ec_platform.wx_config WHERE company_id = '${company_id}'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    return result
}

/**********************************************
 * 获取授权二维码
 * Author：何语漫
 * Date：2018-09-25
 */
export async function getAuthCodeUrl(company_id: string) {
    //获取pre_auth_code,com_appid
    let auth = await getAuthorityConf()
    let preAuthCode = await getPreAuthcode()
    let encodeurl = await encodeURIComponent(auth.notifyUrl + `/${company_id}`)
    let codeUrl = `https://mp.weixin.qq.com/safe/bindcomponent?action=bindcomponent&auth_type=3&no_scan=1&component_appid=${auth.compAppid}&pre_auth_code=${preAuthCode.preAuthCode}&redirect_uri=${encodeurl}&auth_type=1#wechat_redirect`
    return codeUrl
}

/**********************************************
 * 取消授权
 * Author：何语漫
 * Date：2018-09-25
 */
export async function cancelAuth(appid: string) {
    let sql = `UPDATE ec_platform.company SET appid = '' WHERE appid = '${appid}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
    sql = `DELETE FROM ec_platform.wx_config WHERE appid = '${appid}'`
    await getConnectionAsync(async conn => conn.queryAsync(sql))
}
