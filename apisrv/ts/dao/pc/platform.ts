/**********************************************
* 平台上账号管理
* Author：鲁欣欣
* Date：2018-09-28
*/

import { getConnectionAsync } from "../../lib/mysqlpool"

/**********************************************
* 平台管理账号修改密码
* Author：鲁欣欣
* Date：2018-09-08
*/
export async function updPasswd(account_id: number, passwd: string): Promise<any> {
    let sql = `UPDATE account set password='${passwd}' WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (res.affectedRows === 0) {
        throw Error('修改失败')
    }
    return res.affectedRows
}

/**********************************************
* 原密码验证
* Author：鲁欣欣
* Date：2018-09-26
*/
export async function verifyPasswd(account_id: number): Promise<any> {
    let sql = `SELECT password FROM account WHERE account_id=${account_id} and is_del=0;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return res[0].password
}

/**********************************************
* 数据统计
* Author：鲁欣欣
* Date：2018-09-26
*/
export async function statistics(): Promise<any> {
    let arr1 = []
    let arr2 = []
    let arr3 = []
    let arr4 = []
    let arr5 = []
    let arr6 = []
    let sql = `SELECT id,company_name,database_name FROM company WHERE is_del=0 and status=1;`
    let res = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    console.log(res)
    for (let i = 0; i < res.length; i++) {
        /**昨日新增粉丝数 */
        let sql1 = `SELECT count(*) as count FROM ${res[i].database_name}.user WHERE date(create_time)=CURDATE()-INTERVAL 1 DAY`
        let res1 = await getConnectionAsync(async conn => await conn.queryAsync(sql1))
        res[i].yesterday_users = res1[0].count
        /**昨日转帐数 */
        let sql2 = `SELECT count(*) as count FROM( SELECT id FROM ${res[i].database_name}.comment_record WHERE status=5 and date(modify_time)=CURDATE()-INTERVAL 1 DAY UNION SELECT id FROM ${res[i].database_name}.order_record WHERE status=5 and date(modify_time)=CURDATE()-INTERVAL 1 DAY UNION SELECT id FROM  ${res[i].database_name}.rebate_mall_record WHERE status=4 and date(modified)=CURDATE()-INTERVAL 1 DAY) as countt`
        let res2 = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
        res[i].yesterday_transfer = res2[0].count
        /**昨日销量 */
        let sql3 = `SELECT count(*) as count FROM ${res[i].database_name}.trade_order WHERE status='TRADE_FINISHED' and date(end_time)=CURDATE()-INTERVAL 1 DAY`
        let res3 = await getConnectionAsync(async conn => await conn.queryAsync(sql3))
        res[i].yesterday_sales = res3[0].count
        /**总粉丝数 */
        let sql4 = `SELECT count(*) as count FROM ${res[i].database_name}.user`
        let res4 = await getConnectionAsync(async conn => await conn.queryAsync(sql4))
        res[i].users = res4[0].count
        /**总转帐数 */
        let sql5 = `SELECT count(*) as count FROM( SELECT id FROM ${res[i].database_name}.comment_record WHERE status=5 UNION SELECT id FROM ${res[i].database_name}.order_record WHERE status=5 UNION SELECT id FROM  ${res[i].database_name}.rebate_mall_record WHERE status=4 ) as countt`
        let res5 = await getConnectionAsync(async conn => await conn.queryAsync(sql5))
        res[i].transfer_number = res5[0].count
        /**月销量 */
        let sql6 = `SELECT count(*) as count FROM ${res[i].database_name}.trade_order WHERE status='TRADE_FINISHED' and PERIOD_DIFF(date_format(now(),'%Y%m'),date_format(end_time,'%Y%m')) =1`
        let res6 = await getConnectionAsync(async conn => await conn.queryAsync(sql6))
        res[i].sales = res6[0].count
    }
    console.log(1)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].yesterday_users > res[b - 1].yesterday_users) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }

        arr1.push({ no: a + 1, company_name: res[a].company_name, number: res[a].yesterday_users })
    }
    console.log(arr1)
    console.log(2)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].yesterday_transfer > res[b - 1].yesterday_transfer) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }
        arr2.push({ no: a + 1,company_name: res[a].company_name, number: res[a].yesterday_transfer })
    }
    console.log(arr2)
    console.log(3)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].yesterday_sales > res[b - 1].yesterday_sales) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }
        arr3.push({ no: a + 1, company_name: res[a].company_name, number: res[a].yesterday_sales })
    }
    console.log(arr3)
    console.log(4)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].users > res[b - 1].users) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }
        arr4.push({ no: a + 1, company_name: res[a].company_name, number: res[a].users })
    }
    console.log(arr4)
    console.log(5)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].transfer_number > res[b - 1].transfer_number) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }
        arr5.push({ no: a + 1, company_name: res[a].company_name, number: res[a].transfer_number })
    }
    console.log(arr5)
    console.log(6)
    for (let a = 0; a < (res.length > 5 ? 5 : res.length); a++) {
        for (let b = res.length - 1; b > a; b--) {
            if (res[b].sales > res[b - 1].sales) {
                let temp = res[b]
                res[b] = res[b - 1]
                res[b - 1] = temp
            }
        }
        arr6.push({ no: a + 1, company_name: res[a].company_name, number: res[a].sales })
    }
    console.log(arr6)
    return { yesterday_users: arr1, yesterday_transfer: arr2, yesterday_sales: arr3, user: arr4, transfer_number: arr5, sales: arr6 }
}
