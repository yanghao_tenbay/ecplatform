import { getConnectionAsync } from "../../lib/mysqlpool";
import { replaceStr, formatDate3 } from "../../lib/utils";


async function formatData(result: any) {
    let cheackTime = new Date()
    for (let i = 0; i < result.length; i++) {
        if (new Date(result[i].end_time) < cheackTime) {
            result[i].status = '无效'
        } else if (
            result[i].used_num == result[i].total_num) {
            result[i].status = '数量已兑完'
        } else {
            result[i].status = '有效'
        }
        result[i].used_num = result[i].total_num - result[i].used_num
    }
    return result
}

/**********************************************
* 获取礼品名称
* Author：hym
* Date：2018-10-19
*/
export async function getGiftName(database: string) {
    let sql = `SELECT gift_name FROM ${database}.gift WHERE is_del = 0`
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return result
}

/**********************************************
* 获取礼品
* Author：hym
* Date：2018-10-19
*/
export async function getGift(database: string, gift_name: string, startcredits: string, endcredits: string, status: string, start: string, length: string) {
    let sql_count = `SELECT COUNT(gift_id) AS num FROM ${database}.gift WHERE is_del = 0 `
    let sql = `SELECT gift_id, gift_name, gift_type, gift_cove, credits, price, total_num, used_num, create_time, end_time FROM ${database}.gift WHERE is_del = 0 `
    if (gift_name !== '' || startcredits !== '' || status !== '') {
        if (gift_name !== '' && gift_name !== undefined) {
            let rplstr = '/'
            if (gift_name.includes('/')) {
                rplstr = '+'
            }
            gift_name = replaceStr(gift_name, rplstr)
            sql_count += ` AND gift_name LIKE '%${gift_name}%'`
            sql += ` AND gift_name LIKE '%${gift_name}%'`
        }
        if (startcredits !== '' || endcredits !== '') {
            endcredits === '' ? endcredits = '10000' : ''
            startcredits === '' ? startcredits = '0' : ''
            sql_count += ` AND credits BETWEEN ${startcredits} AND ${endcredits}`
            sql += ` AND credits BETWEEN ${startcredits} AND ${endcredits}`
        }
        if (status == '有效') {
            sql_count += ` AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
            sql += ` AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
        } else if (status == '无效') {
            sql_count += ` AND end_time <= '${formatDate3(new Date())}'`
            sql += ` AND end_time <= '${formatDate3(new Date())}'`
        } else if (status == '数量已兑完') {
            sql_count += ` AND total_num = used_num  AND end_time >= '${formatDate3(new Date())}'`
            sql += ` AND total_num = used_num  AND end_time >= '${formatDate3(new Date())}'`
        }
    }
    sql += ` ORDER BY end_time DESC LIMIT ${start}, ${length}`
    console.log(sql_count)
    console.log(sql)
    let num = await getConnectionAsync(async conn => await conn.queryAsync(sql_count))
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    result = await formatData(result)
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

/**********************************************
* 新建礼品
* Author：hym
* Date：2018-10-19
*/
export async function newGift(database: string, gift_name: string, gift_cove: string, gift_type: string, gift_num: string, credits: string, price: number, create_time: string, end_time: string) {
    let sql = `SELECT gift_name FROM ${database}.gift WHERE gift_name = '${gift_name}' AND is_del != 1 `
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (row.length > 0) {
        throw new Error('礼品名称已经存在!')
    }

    if (new Date(create_time) > new Date(end_time)) {
        throw new Error("结束时间大于开始时间！")
    }

    sql = `INSERT INTO ${database}.gift(gift_name, gift_cove, gift_type, total_num, credits, price, create_time, end_time) VALUES('${gift_name}', '${gift_cove}', '${gift_type}', ${gift_num}, ${credits},  ${Number(price).toFixed(2)}, '${create_time}', '${end_time}')`
    console.log(sql)
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        return '新建失败'
    }
    return '新建成功'
}

/**********************************************
* 更新礼品
* Author：hym
* Date：2018-10-19
*/
export async function updGift(database: string, gift_id: string, gift_name: string, gift_cove: string, gift_type: string, gift_num: string, credits: string, price: number, create_time: string, end_time: string) {
    let sql = `SELECT gift_name, used_num FROM ${database}.gift WHERE gift_id != '${gift_id}' AND gift_name = '${gift_name}' AND is_del != 1`
    let row = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (row.length > 0) {
        throw new Error('礼品名称已经存在!')
    }

    if (new Date(create_time) > new Date(end_time)) {
        throw new Error("结束时间大于开始时间！")
    }

    sql = `UPDATE ${database}.gift SET gift_name = '${gift_name}', gift_cove = '${gift_cove}', gift_type = '${gift_type}', total_num = '${gift_num}', credits = '${credits}', price = ${Number(price).toFixed(2)}, create_time = '${create_time}', end_time = '${end_time}' WHERE gift_id = '${gift_id}'`
    console.log(sql)
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        throw new Error('修改失败')
    }
    return '修改成功'
}

/**********************************************
* 删除礼品
* Author：hym
* Date：2018-10-19
*/
export async function delGift(database: string, gift_id: string) {
    gift_id = JSON.parse(gift_id)
    let param = `(${gift_id.toString()})`
    let sql = `UPDATE ${database}.gift SET is_del = 1 WHERE gift_id IN ${param}`
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        return '删除失败'
    }
    return '删除成功'
}

interface giftRecord {
    id: string,
    gift_name: string,
    headimgurl: string,
    nickname: string,
    phone: string,
    num: number,
    credits: number,
    status: string
}

async function formatData1(result: any) {
    let cheackTime = new Date()
    let gift_record = new Array<giftRecord>(result.length)
    for (let i = 0; i < result.length; i++) {
        if (new Date(result[i].end_time) < cheackTime) {
            result[i].status = '无效'
        } else if (
            result[i].used_num == result[i].total_num) {
            result[i].status = '数量已兑完'
        } else {
            result[i].status = '有效'
        }
        gift_record[i] = {
            id: result[i].id,
            gift_name: result[i].gift_name,
            headimgurl: result[i].headimgurl,
            nickname: result[i].nickname,
            phone: result[i].phone,
            num: result[i].num,
            credits: result[i].credits,
            status: result[i].status
        }
    }
    return gift_record
}

/**********************************************
* 获取积分兑换记录
* Author：hym
* Date：2018-10-24
*/
export async function getGiftRecord(database: string, gift_name: string, nickname: string, status: string, start: string, length: string) {
    console.log(status)
    let sql = `SELECT a.id, b.*, c.headimgurl, c.nickname, c.phone, a.num, a.credits FROM ${database}.gift_record AS a, ${database}.gift AS b, ${database}.user AS c WHERE b.gift_id = a.gift_id AND c.user_id = a.user_id AND a.status = 2 `

    let sql_count = `SELECT COUNT(a.id) AS num FROM ${database}.gift_record AS a, ${database}.gift AS b, ${database}.user AS c WHERE b.gift_id = a.gift_id AND c.user_id = a.user_id AND a.status = 2 `

    if (nickname !== '' && nickname !== undefined) {
        let rplstr = '/'
        if (nickname.includes('/')) {
            rplstr = '+'
        }
        nickname = replaceStr(nickname, rplstr)
        sql += ` AND c.nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
        sql_count += ` AND c.nickname LIKE '%${nickname}%' ESCAPE '${rplstr}'`
    }
    if (gift_name !== '' && gift_name !== undefined) {
        let rplstr = '/'
        if (gift_name.includes('/')) {
            rplstr = '+'
        }
        gift_name = replaceStr(gift_name, rplstr)
        sql += ` AND b.gift_name LIKE '%${gift_name}%' ESCAPE '${rplstr}'`
        sql_count += ` AND b.gift_name LIKE '%${gift_name}%' ESCAPE '${rplstr}'`
    }

    if (status == '有效') {
        sql_count += ` AND b.end_time > '${formatDate3(new Date())}' AND total_num > used_num`
        sql += ` AND b.end_time > '${formatDate3(new Date())}' AND total_num > used_num`
    } else if (status == '无效') {
        sql_count += ` AND b.end_time <= '${formatDate3(new Date())}'`
        sql += ` AND b.end_time <= '${formatDate3(new Date())}'`
    } else if (status == '数量已兑完') {
        sql_count += ` AND b.total_num = b.used_num  AND end_time >= '${formatDate3(new Date())}'`
        sql += ` AND b.total_num = b.used_num  AND end_time >= '${formatDate3(new Date())}'`
    }
    sql += ` ORDER BY a.modify_time DESC LIMIT ${start}, ${length}`
    console.log(sql)
    console.log(sql_count)
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    result = await formatData1(result)
    let num = await getConnectionAsync(async conn => conn.queryAsync(sql_count))
    return { recordsFiltered: num[0].num, recordsTotal: num[0].num, data: result }
}

export async function selectPic(database: string) {
    let sql = `SELECT gift_id, gift_name FROM ${database}.gift WHERE is_del = 0 AND end_time > '${formatDate3(new Date())}' AND total_num > used_num`
    let result = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    return result
}

export async function delPic(database: string, data: any) {
    data = JSON.parse(data)
    let sql = `UPDATE ${database}.gift SET shop_set = 0  WHERE gift_id IN (${data.toString()}) `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        throw new Error('删除失败')
    }
    return '删除成功'
}

/**********************************************
* 保存图片
* Author：hym
* Date：2018-10-24
*/
export async function savePic(database: string, data: any, credits: number) {
    data = JSON.parse(data)
    let sql = `UPDATE ${database}.gift SET shop_set = 1  WHERE gift_id IN (${data.toString()}) AND end_time > '${formatDate3(new Date())}' AND total_num > used_num `
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    attendenceRule(database, credits)
    if (result.affectedRows != data.length) {
        return '保存失败'
    }
    return '保存成功'
}

export async function showPic(database: string) {
    let sql = `SELECT * FROM ${database}.gift WHERE shop_set = 1`
    let gift = await getConnectionAsync(async conn => conn.queryAsync(sql))
    gift = await formatData(gift)
    sql = `SELECT v FROM ${database}.kv WHERE k = 'attendence_rule'`
    let credits_result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    console.log(gift)
    let credits = 0
    if (credits_result.length != 0) {
        credits = credits_result[0].v
    }
    return {
        gift: gift,
        credits: credits
    }
}

export async function attendenceRule(database: string, credits: number) {
    let sql = `SELECT v FROM ${database}.kv WHERE k = 'attendence_rule'`
    let result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.length !== 0) {
        sql = `DELETE FROM ${database}.kv WHERE K = 'attendence_rule'`
        await getConnectionAsync(async conn => conn.queryAsync(sql))
    }
    sql = `INSERT INTO ${database}.kv(k, v) VALUES('attendence_rule','${credits}')`
    result = await getConnectionAsync(async conn => conn.queryAsync(sql))
    if (result.affectedRows == 0) {
        throw new Error('保存失败')
    }
    return '保存成功'
}