/**********************************************
* 晒评价统计
* Author：鲁欣欣
* Date：2018-11-1
*/
import { getConnectionAsync } from "../../lib/mysqlpool"

/**************************************
 * 按天统计
 * Author：鲁欣欣
 * Date：2018-11-1
 */
export async function dayStatistics(database_name: string): Promise<any> {
    let sql = `SELECT temp.date,coalesce(u.amount,0) as amount,coalesce(u.number,0) as number from(SELECT adddate(CURDATE()-INTERVAL 1 day, -numlist.id) AS 'date' FROM ( SELECT n1.i + n10.i * 10 + n100.i * 100 AS id FROM ec_platform.num n1 CROSS JOIN ec_platform.num AS n10 CROSS JOIN ec_platform.num AS n100 ) AS numlist WHERE adddate(CURDATE()-INTERVAL 1 day, -numlist.id) >= date_sub(date(now()),interval 15 day)) temp LEFT JOIN  (SELECT date(create_time) AS udate,SUM(amount) as amount,count(amount) as number FROM ${database_name}.comment_record WHERE status=5 GROUP BY udate) as u on temp.date = u.udate order by temp.date `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    let total_amount = 0
    let total_number = 0
    for (let i = 0; i < data.length; i++) {
        total_amount += data[i].amount
        total_number += data[i].number
    }
    return { recordsFiltered: 15, recordsTotal: 15, total_amount: total_amount, total_number: total_number, data: data }
}

/**************************************
 * 按月统计
 * Author：鲁欣欣
 * Date：2018-11-1
 */
export async function monthStatistics(database_name: string): Promise<any> {
    let sql = `SELECT temp.month as date,coalesce(u.amount,0) as amount,coalesce(u.number,0) as number from(SELECT DATE_FORMAT(ADDDATE(now()-INTERVAL 1 month,INTERVAL -numlist.id month),'%Y %m') AS month FROM ( SELECT n1.i + n10.i * 10 + n100.i * 100 AS id FROM ec_platform.num n1 CROSS JOIN ec_platform.num AS n10 CROSS JOIN ec_platform.num AS n100 ) AS numlist WHERE ADDDATE(now()-INTERVAL 1 month,INTERVAL -numlist.id month) >= date_sub(now(),interval 15 month) ) as temp LEFT JOIN (SELECT DATE_FORMAT(create_time,'%Y %m') AS month,SUM(amount) as amount,count(amount) as number FROM ${database_name}.comment_record  WHERE status=5 GROUP BY month) as u on temp.month = u.month ORDER BY temp.month `
    console.log(sql)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql))
    let total_amount = 0
    let total_number = 0
    for (let i = 0; i < data.length; i++) {
        total_amount += data[i].amount
        total_number += data[i].number
    }
    return { recordsFiltered: 15, recordsTotal: 15, total_amount: total_amount, total_number: total_number, data: data }
}

/**************************************
 * 按时间段统计
 * Author：鲁欣欣
 * Date：2018-11-5
 */
export async function timeStatistics(database_name: string, start_time: string, end_time: string, start: number, length: number): Promise<any> {
    let sql = `from(SELECT adddate('${end_time}', -numlist.id) AS 'date' FROM ( SELECT n1.i + n10.i * 10 + n100.i * 100 AS id FROM ec_platform.num n1 CROSS JOIN ec_platform.num AS n10 CROSS JOIN ec_platform.num AS n100 ) AS numlist WHERE adddate('${end_time}', -numlist.id) >= '${start_time}') as temp LEFT JOIN  (SELECT date(create_time) AS udate,SUM(amount) as amount,count(amount) as number FROM ${database_name}.comment_record WHERE status=5 GROUP BY udate) as u on temp.date = u.udate order by temp.date `
    let sql1 = `SELECT count(*) as count ` + sql
    let rowcount = await getConnectionAsync(async conn => await conn.queryAsync(sql1)) as any
    let row: number = rowcount[0].count//总行数

    let sql2 = `SELECT temp.date,coalesce(u.amount,0) as amount,coalesce(u.number,0) as number  ` + sql + `limit ${start} , ${length}`
    console.log(sql2)
    let data = await getConnectionAsync(async conn => await conn.queryAsync(sql2))
    let total_amount = 0
    let total_number = 0
    for (let i = 0; i < data.length; i++) {
        total_amount += data[i].amount
        total_number += data[i].number
    }
    return { recordsFiltered: row, recordsTotal: row, total_amount: total_amount, total_number: total_number, data: data }
}