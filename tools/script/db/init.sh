#!/bin/sh
#修改必看：不能有  "" 符号。
dbname=ec_platform
password=authcloud2014

mysqlcmd="mysql -uroot -p$password -e "
dbcmd="mysql -uroot -p$password $dbname -e "

promise(){
	$dbcmd "$1"
	test $? -eq 0 && return
	echo "execute sql fail $1 \n\r"
	#exit 1
}

check_mysql_alive(){
	while :; do
		$mysqlcmd "select 1 " >/dev/null 2>&1
		test $? -eq 0 && return
		echo "mysql is not running"
		sleep 1
	done
}


init_sql(){
		$mysqlcmd "create DATABASE ec_platform;" >/dev/null 2>&1

		promise "CREATE TABLE IF NOT EXISTS account  (
		  account_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
		  account_name varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号名',
		  password varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'MD5之后的密码',
		  status tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态0：失效；1：有效',
		  create_time timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  is_del tinyint(4) NOT NULL DEFAULT 0,
		  PRIMARY KEY (account_id) USING BTREE
		) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COMMENT = '平台管理员账号表';"

		promise "CREATE TABLE IF NOT EXISTS ali_config (
		  AccessKeyId varchar(255) NOT NULL COMMENT '密钥id',
		  AccessKeySecret varchar(255) NOT NULL COMMENT '密钥',
		  remark varchar(255) DEFAULT '' COMMENT '备注',
		  host varchar(255) DEFAULT NULL COMMENT '解析目的地址',
		  domain varchar(255) DEFAULT NULL COMMENT '域名'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS cc_region (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  name varchar(40) DEFAULT NULL,
		  parent_id int(11) DEFAULT NULL,
		  level int(11) DEFAULT NULL,
		  city_code varchar(20) DEFAULT NULL,
		  zip_code varchar(20) DEFAULT NULL,
		  lng float DEFAULT NULL,
		  lat float DEFAULT NULL,
		  pinyin varchar(100) DEFAULT NULL,
		  state int(11) DEFAULT '1',
		  PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=110000 DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS company (
		  id int(11) NOT NULL AUTO_INCREMENT COMMENT '公司id',
		  phone varchar(255) NOT NULL COMMENT '电话号码即账号',
		  password varchar(255) DEFAULT NULL COMMENT '密码',
		  company_name varchar(255) NOT NULL COMMENT '公司名',
		  shop varchar(255) NOT NULL COMMENT '店铺',
		  province varchar(255) NOT NULL COMMENT '所在地：省',
		  city varchar(255) NOT NULL COMMENT '市',
		  real_name varchar(255) NOT NULL COMMENT '姓名',
		  email varchar(255) NOT NULL COMMENT '邮箱',
		  domain varchar(255) NOT NULL DEFAULT '' COMMENT '域名',
		  recordId varchar(255) NOT NULL DEFAULT '' COMMENT '域名解析记录id',
		  database_name varchar(255) DEFAULT NULL COMMENT '数据库',
		  appid varchar(255) NOT NULL DEFAULT '' COMMENT '公众号id',
		  sms int(11) NOT NULL DEFAULT '0' COMMENT '剩余短信条数',
		  status tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态0：待审核；1：审核通过;2：不通过；3：被拉黑',
		  end_time datetime DEFAULT NULL COMMENT '公司到期时间',
		  create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
		  is_del tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除0：未删除；1：已删除',
		  PRIMARY KEY (id) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='公司账号信息表';"

		promise "CREATE TABLE IF NOT EXISTS component_config (
		  component_appid varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '第三方平台公众号ID',
		  component_appsecret varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '第三方平台密码',
		  component_verify_ticket varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '每10分钟获取一次校验卡',
		  component_access_token varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '第三方接口凭证',
		  pre_auth_code varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '预授权码',
		  token varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '加解密用',
		  encoding_aes_key varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '加解密key值',
		  notify_url varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '授权结果通知回调连接',
		  createmanue_url varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建菜单回调URL',
		  main_url varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '主域名',
		  PRIMARY KEY (component_appid)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS email_config (
		  host varchar(255) DEFAULT NULL COMMENT '主机',
		  port varchar(255) DEFAULT NULL COMMENT '端口',
		  user varchar(255) DEFAULT NULL COMMENT '邮箱号',
		  pass varchar(255) DEFAULT NULL COMMENT '密码'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS mch_config_platform (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  mch_id varchar(255) NOT NULL DEFAULT '' COMMENT '商户平台id',
		  mch_appid varchar(255) NOT NULL DEFAULT '' COMMENT '商户平台对应的公众号appid',
		  mch_key varchar(255) NOT NULL DEFAULT '' COMMENT '支付key',
		  status int(255) NOT NULL DEFAULT '1' COMMENT '状态',
		  notify_url varchar(255) DEFAULT NULL COMMENT '回调连接',
		  PRIMARY KEY (id) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS sms_config (
		  account varchar(255) DEFAULT NULL COMMENT 'API账号',
		  password varchar(255) DEFAULT NULL COMMENT 'API密码',
		  url varchar(255) DEFAULT NULL COMMENT '接口地址',
		  status tinyint(4) DEFAULT '0' COMMENT '0: 未使用， 1：使用',
		  remark varchar(255) DEFAULT NULL COMMENT '备注'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

		promise "CREATE TABLE IF NOT EXISTS sms_package (
		  id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
		  package varchar(255) DEFAULT NULL COMMENT '套餐名',
		  num int(11) DEFAULT NULL COMMENT '包含短信数量',
		  price float(11,2) DEFAULT NULL COMMENT '单价',
		  create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
		  PRIMARY KEY (id) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='短信套餐信息表';"

		promise "CREATE TABLE IF NOT EXISTS smstrade (
		  id int(11) NOT NULL AUTO_INCREMENT COMMENT '递增主键',
		  out_trade_no char(36) DEFAULT NULL COMMENT '商户系统内部的订单号,32个字符内、可包含字母',
		  account_id varchar(128) DEFAULT NULL COMMENT '充值用户',
		  company_id varchar(128) DEFAULT NULL COMMENT '充值公司',
		  prepay_id varchar(64) DEFAULT NULL COMMENT '预支付交易会话标识',
		  package_id int(11) DEFAULT NULL COMMENT '套餐ID',
		  state char(4) DEFAULT 'new' COMMENT '用户是否支付成功，成功为''fin''',
		  chargestate char(4) DEFAULT 'new' COMMENT '是否将金币加入到玩家账号，完成为‘fin’',
		  appid varchar(64) DEFAULT NULL COMMENT '微信分配的公众账号ID',
		  mch_id varchar(20) DEFAULT NULL COMMENT '微信支付分配的商户号',
		  body varchar(64) DEFAULT NULL COMMENT '商品简单描述',
		  total_fee float(11,2) DEFAULT '0.00' COMMENT '订单总金额，单位为分',
		  create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
		  modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
		  code_url varchar(255) DEFAULT NULL COMMENT '二维码链接',
		  payway varchar(255) DEFAULT NULL COMMENT '支付方式',
		  PRIMARY KEY (id) USING BTREE,
		  UNIQUE KEY out_trade_no (out_trade_no) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='微信充值产生的记录';"
		
		promise "CREATE TABLE IF NOT EXISTS wx_config (
		  appid varchar(255) NOT NULL COMMENT '公众号ID',
		  notify_url varchar(255) DEFAULT NULL COMMENT '支付回调的通知地址',
		  status tinyint(4) DEFAULT '0' COMMENT '0:未使用， 1：正在使用',
		  authorizer_access_token varchar(255) DEFAULT NULL COMMENT 'access_token',
		  authorizer_refresh_token varchar(255) DEFAULT NULL COMMENT '刷新',
		  modify_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
		  nick_name varchar(255) DEFAULT NULL COMMENT '公众号昵称',
		  head_img text COMMENT '头像地址',
		  user_name varchar(255) DEFAULT NULL COMMENT '原始ID',
		  principal_name varchar(255) DEFAULT NULL COMMENT '公众号的主体名称',
		  qrcode_url text COMMENT '二维码',
		  company_id int(11) DEFAULT NULL,
		  func_info varchar(255) DEFAULT NULL,
		  manul_url varchar(400) DEFAULT NULL COMMENT 'H5页面菜单',
		  PRIMARY KEY (appid)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
		
		promise "CREATE TABLE IF NOT EXISTS num (i INT) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
		
		promise "CREATE TABLE IF NOT EXISTS authorization  (
		   id  int(11) NOT NULL AUTO_INCREMENT,
		   shop_name  varchar(255) DEFAULT '' COMMENT '授权店铺名',
		   taobao_user_id  varchar(255) DEFAULT NULL COMMENT '淘宝授权者ID',
		   session_key  varchar(255) DEFAULT NULL,
		   refresh_token  varchar(255) DEFAULT NULL COMMENT '刷新用的标识码',
		   first_auth_time  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '店铺首次授权时间',
		   auth_time  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最新授权时间',
		   refresh_time  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最新刷新时间',
		   company_id  int(11) DEFAULT NULL COMMENT '公司ID',
		  PRIMARY KEY ( id ) USING BTREE
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
		
		promise "CREATE TABLE IF NOT EXISTS baidu_config (
		  ak varchar(255) DEFAULT NULL COMMENT '百度地图开放平台ip定位访问应用ak',
		  remark varchar(255) DEFAULT NULL COMMENT '描述'
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ip定位—百度地图api,应用ak配置';"
}

check_mysql_alive
init_sql
sh -x data_init.sh
