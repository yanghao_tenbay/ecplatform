#!/bin/sh

apt-get update
apt-get install lrzsz
apt-get install dos2unix
apt-get install unzip
apt-get install software-properties-common

# 安装mysql8.0
echo "提示更改配置，请选择ok, 默认即可"
echo "提示输入mysql root密码，请输入 authcloud2014"
echo "提示选择密码版本，请选择第二个兼容低版本的"
cd /usr/local/src
wget https://dev.mysql.com/get/mysql-apt-config_0.8.10-1_all.deb
dpkg -i mysql-apt-config_0.8.10-1_all.deb
apt-get update

echo press ENTER to continue
read a
apt-get install mysql-server

# 安装nginx
echo 'start install nginx'
cd /usr/local/src
apt-get install openssl libssl-dev
apt-get install libpcre3 libpcre3-dev
apt-get install zlib1g-dev
wget https://nginx.org/download/nginx-1.14.0.tar.gz
tar -zxvf nginx-1.14.0.tar.gz
cd nginx-1.14.0/
./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module
make && make install

# 启动nginx
cd  /usr/local/nginx/sbin
./nginx
ln -sf /usr/local/nginx/sbin/nginx  /usr/bin/nginx
read -p '确认nginx安装成功，按任意键继续' a

# 安装reids
echo 'start install redis'
apt-get install redis-server
read -p '确认redis安装成功，按任意键继续' a

# 安装node
echo 'start install node'
cd /usr/local/src
wget https://nodejs.org/dist/v10.13.0/node-v10.13.0.tar.gz
tar zxvf node-v10.13.0.tar.gz
cd node-v10.13.0/
./configure
make && make install
read -p '确认node安装成功，按任意键继续' a

# 安装typescript
echo 'start install typescript'
npm install -g cnpm --registry=https://registry.npm.taobao.org
cnpm install typescript --global


#安装git
echo 'start install git'
apt-get install git
read -p '确认git安装成功，按任意键继续' a

read -p "Please enter your git account :" username
read -p "Please enter your git password :" passwd
#下载代码
cd /home
mkdir Ecommerce_git
cd Ecommerce_git
git clone https://${username}:${passwd}@gitlab.com/TenbayServer/rebateplatform.git

echo "git pwd: /home/SmartBox_git/rebateplatform"


# 代码路径
mkdir -p /var/Ecommerce
# 日志路径
mkdir -p /var/log/Ecommerce
# pc端上传证书路径
mkdir -p /var/data/cert
# 上传图片路径
mkdir -p /var/data/uploads/images
