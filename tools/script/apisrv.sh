#!/bin/sh

ps | grep /Ecommerce/apisrv/bin/www | grep -v grep >/dev/null 
test $? -eq 0 && exit 0

errorfile=/var/log/Ecommerce/apisrv.error

test -d /var/log/Ecommerce || mkdir -p /var/log/Ecommerce

while :; do 
	echo "`date` `uptime` start apisrv" >>$errorfile
	node /Ecommerce/apisrv/bin/www >/dev/null 2>>$errorfile
	sleep 2
done
