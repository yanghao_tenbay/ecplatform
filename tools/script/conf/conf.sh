#!/bin/sh
### 更改配置
cp /Ecommerce/tools/script/conf/nginx.conf /usr/local/nginx/conf/nginx.conf

if [ ! -d "/usr/local/nginx/licence" ]; then
  mkdir -p /usr/local/nginx/licence
  cp /Ecommerce/tools/script/conf/licence/https_support.crt /usr/local/nginx/licence
  cp /Ecommerce/tools/script/conf/licence/https_support.key /usr/local/nginx/licence
fi

nginx -s reload