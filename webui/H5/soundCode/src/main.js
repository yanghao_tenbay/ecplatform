// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import fastClick from 'fastclick'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueBetterScroll from 'vue2-better-scroll'
import Toast from './lib/toast/Toast.js'
import loading from 'super-loading'
import 'swiper/dist/css/swiper.css'
import anmated from 'animate.css'
import vueCropper from 'vue-cropper'
import gallery from 'img-vuer'
import VModal from 'vue-js-modal'
import $http from '@/services/api.js'
import store from './store'
// import utils from './lib/utils'

require('styles/reset.css')
require('styles/border.css')
require('styles/layer.css')
require('@/assets/js/layer.js')
Vue.config.productionTip = false
fastClick.attach(document.body)
Vue.use(VueAwesomeSwiper)
Vue.use(VueBetterScroll)
Vue.use(Toast)
Vue.use(loading)
Vue.use(anmated)
Vue.use(vueCropper)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.config.devtools = true
Vue.use(gallery, {
  swipeThreshold: 150
})

window.onload = function () {
  var deviceWidth = document.documentElement.clientWidth;
  if (deviceWidth > 750) deviceWidth = 750;
  document.documentElement.style.fontSize = deviceWidth / 7.5 + 'px';
}
// 利用bus总线向兄弟组件传值
Vue.prototype.bus = new Vue();
Vue.prototype.$http = $http;

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title;
  }
  next();
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
