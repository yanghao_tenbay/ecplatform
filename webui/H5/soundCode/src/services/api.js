import withAxios from './http.js'
const apiConfig = [
  {
    name: 'getUserCredits',
    url: '/wechat/creditShop/getUserCredits',
    method: 'post'
  }, {
    name: 'getShopPic',
    url: '/creditShop/getShopPic',
    method: 'post'
  }, {
    name: 'getGift',
    url: '/wechat/creditShop/getGift',
    method: 'post'
  }, {
    name: 'showCart',
    url: '/wechat/creditShop/showCart',
    method: 'post'
  }, {
    name: 'addCart',
    url: '/wechat/creditShop/addCart',
    method: 'post'
  }, {
    name: 'addCartNum',
    url: '/wechat/creditShop/addCartNum',
    method: 'post'
  }, {
    name: 'redCartNum',
    url: '/wechat/creditShop/redCartNum',
    method: 'post'
  }, {
    name: 'exchange',
    url: '/wechat/creditShop/exchange',
    method: 'post'
  }, {
    name: 'getCash',
    url: '/wechat/creditShop/getCash', // 展示兑换
    method: 'post'
  }, {
    name: 'getAttendence',
    url: '/wechat/wxapp/getAttendence', // 获取用户签到日期
    method: 'post'
  }, {
    name: 'setAttendence',
    url: '/wechat/wxapp/setAttendence', // 签到
    method: 'post'
  }, {
    name: 'delCart',
    url: '/wechat/creditShop/delCart', // 购物车删除记录
    method: 'post'
  }, {
    name: 'imageUpload',
    url: '/wechat/comment/image_upload', // 晒评价上传图片
    method: 'post'
  },
  // 返利商城接口
  {
    name: 'mallGetCarousel',
    url: '/wechat/rebate_mall/get_carousel',
    method: 'post'
  },
  {
    name: 'mallSelect',
    url: '/wechat/rebate_mall/select',
    method: 'post'
  },
  {
    name: 'mallRecord',
    url: '/wechat/rebate_mall/record',
    method: 'post'
  },
  {
    name: 'mallRob',
    url: '/wechat/rebate_mall/rob',
    method: 'post'
  },
  {
    name: 'mallSingleDetails',
    url: '/wechat/rebate_mall/single_details',
    method: 'post'
  },
  // 订单返利接口
  {
    name: 'orderGetEvent',
    url: '/wechat/order/get_event',
    method: 'post'
  },
  {
    name: 'orderSubmit',
    url: '/wechat/order/submit',
    method: 'post'
  },
  {
    name: 'orderMyrebate',
    url: '/wechat/order/myrebate',
    method: 'post'
  },
  // 晒评价接口
  {
    name: 'evaluationGetEvent',
    url: '/wechat/comment/get_event',
    method: 'post'
  },
  {
    name: 'evaluationSubmit',
    url: '/wechat/comment/submit',
    method: 'post'
  },
  {
    name: 'evaluationMyrebate',
    url: '/wechat/comment/myrebate',
    method: 'post'
  },
  // 个人中心接口
  {
    // 发送验证码
    name: 'sendCheckCode',
    url: '/wechat/wxapp/sendCheckCode',
    method: 'post'
  }, {
    // 绑定手机号
    name: 'savePhone',
    url: '/wechat/wxapp/savePhone',
    method: 'post'
  }, {
    // 查看订单
    name: 'showMyTrade',
    url: '/wechat/wxapp/myTrade',
    method: 'post'
  }, {
    //  查看用户信息
    name: 'showUserinfoDetail',
    url: '/wechat/wxapp/userInfoDetail',
    method: 'post'
  }, {
    //  我的积分
    name: 'personalGetCrdit',
    url: '/wechat/creditShop/getCrdit',
    method: 'post'
  }
]

export default withAxios(apiConfig);
