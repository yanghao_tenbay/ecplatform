import Vue from 'vue';
import Vuex from 'vuex'
Vue.use(Vuex);

const pointsShop = {
  namespaced: true,
  state: {
    points: 0
  },
  mutations: {
    setPoints (state, points) {
      state.points = points;
    }
  }
}

const mall = {
  namespaced: true,
  state: {
    short_link: null
  },
  mutations: {
    setShortLink (state, val) {
      state.short_link = val;
    }
  }
}

export default new Vuex.Store({
  modules: {
    pointsShop,
    mall
  }
})
