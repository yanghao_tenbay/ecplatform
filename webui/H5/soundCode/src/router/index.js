import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Evaluation from '@/components/pages/Evaluation'
import Myrebate from '@/components/pages/Myrebate'
import Personal from '@/components/pages/Personal'
import Bindtel from '@/components/pages/Bindtel'
import OrderRebate from '@/components/pages/OrderRebate'
import Toast from '@/lib/toast/Toast'
import Detail from '@/components/pages/Detail'
import test from '@/components/pages/test'
import PointsCart from '@/components/PointsMall/PointsCart'
import error from '@/components/pages/error'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: '返利商城'
      }
    },
    {
      path: '/evaluation',
      name: 'Evaluation',
      component: Evaluation,
      meta: {
        title: '评价管理'
      }
    },
    {
      path: '/evaluation/error',
      name: 'EvaluationTips',
      component: () => import('@/components/EvaluationTips'),
      meta: {
        title: '评价管理'
      }
    },
    {
      path: '/toast',
      name: 'Toast',
      component: Toast
    },
    {
      path: '/myrebate',
      name: 'Myrebate',
      component: Myrebate,
      meta: {
        title: '我的返利'
      }
    },
    {
      path: '/detail',
      name: 'Detail',
      component: Detail,
      meta: {
        title: '商品详情'
      }
    },
    {
      path: '/personal',
      name: 'Personal',
      component: Personal
    },
    {
      path: '/personalIntergal',
      name: 'Personal',
      component: () => import('@/components/PersonalIntegral')
    },
    {
      path: '/bindtel',
      name: 'Bindtel',
      component: Bindtel,
      meta: {
        title: '绑定手机号'
      }
    },
    {
      path: '/error',
      name: 'error',
      component: error,
      meta: {
        title: '黑名单'
      }
    },
    {
      path: '/orderrebate',
      name: 'OrderRebate',
      component: OrderRebate,
      meta: {
        title: '订单返利'
      }
    },
    {
      path: '/test',
      name: 'test',
      component: test
    },
    {
      path: '/points',
      name: 'Points',
      redirect: '/points/pointsMall',
      component: () => import('@/components/PointsMall/Points'),
      meta: {
        title: '积分商城',
        keepAlive: true
      },
      children: [{
        path: 'pointsMall',
        name: 'PointsMall',
        component: () => import('@/components/PointsMall/PointsMall'),
        meta: {
          title: '积分商城',
          keepAlive: true
        }
      }, {
        path: 'pointsCart',
        name: 'PointsCart',
        // component: () => import('@/components/PointsMall/PointsCart'),
        component: PointsCart,
        meta: {
          title: '购物车',
          keepAlive: true
        }
      }, {
        path: 'pointsDetail',
        name: 'PointsDetail',
        component: () => import('@/components/PointsMall/PointsDetail'),
        meta: {
          title: '商品详情',
          keepAlive: true
        }
      }, {
        path: 'pointsExchange',
        name: 'pointsExchange',
        component: () => import('@/components/PointsMall/PointsExchange'),
        meta: {
          title: '我的兑换',
          keepAlive: true
        }
      }, {
        path: 'pointsSign',
        name: 'PointsSign',
        component: () => import('@/components/PointsMall/PointsSign'),
        meta: {
          title: '积分签到',
          keepAlive: true
        }
      }, {
        path: 'singleDetail',
        name: 'PointsSingleDetail',
        component: () => import('@/components/PointsMall/SingleDetail'),
        meta: {
          title: '商品详情',
          keepAlive: true
        }
      }]
    }
  ]
})
