/*
utils
  reg
    testTel() 测试是否为手机号
  countDown() 倒计时
  judege
    isArray() 是否为数组
    isFunction() 是否问函数
    isRegExp() 是否问函数
*/
/* eslint-disable */
let utils = (function () {
  let reg = (function () {
    let reg = {
      telPhone: /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/
    }
    let msg = {
      telPhone: '您输入的手机号码有误，请重新输入'
    }

    /**
     * 功能：验证手机号码
     * @param telPhone 验证的手机号码
     * @param errorMsg 出错提示信息
     * @param callback 成功后的回调
     */
    function testTel (telPhone, errorMsg, callback) {
      var telPhone = telPhone || undefined;
      var errorMsg = errorMsg || msg.telPhone;
      var callback = callback || function () {
      }
      if (telPhone && telPhone !== null) {
        if (reg.telPhone.test(telPhone)) {
          callback();
        } else {
          layer.open({
            content: '您输入的手机号不正确，请重新输入',
            skin: 'msg',
            time: 2 //2秒后自动关闭
          })
        }
      }
    }
    return {
      testTel
    }
  })();
  /**
   * 发送短信倒计时
   * @param dom dom节点
   * @param count 倒计时秒数
   */
  function countDown (dom, count) {
    let num = count;
    function f () {
      if (num === 0) {
        dom.removeAttribute('disabled');
        dom.innerHTML = '获取验证码';
        dom.style.background = '#FF5000';
        return false;
      } else {
        dom.setAttribute('disabled', true);
        dom.style.background = '#D8D8D8';
        dom.innerHTML = num + 'S后重新发送';
        num--;
      }
      setTimeout(() => {
        f();
      }, 1000)
    }
    f();
  }

  let judge = (function () {
    /**
     * 判断是否是数组
     * @param value
     * @returns {boolean}
     */
    function isArray (value) {
      return Object.prototype.toString.call(value) === '[object Array]';
    }
    /**
     * 判断会否为函数
     * @param value
     * @returns {boolean}
     */
    function isFunction (value) {
      return Object.prototype.toString.call(value) === '[object Function]';
    }
    function isRegExp (value) {
      return Object.prototype.toString.call(value) === '[object RegExp]';
    }
    return {
      isArray,
      isFunction,
      isRegExp
    }
  })()
  return {
    countDown,
    reg,
    judge
  }
})();

let layer = {
  msg (msg) {
    window.layer.open({
      content: msg,
      skin: 'msg',
      time: 3 //2秒后自动关闭
    });
  }
}

let fnTimeCountDown = function(d, o){
  var f = {
    zero: function(n){
      var n = parseInt(n, 10);
      if(n > 0){
        if(n <= 9){
          n = "0" + n;
        }
        return String(n);
      }else{
        return "00";
      }
    },
    dv: function(){
      d = d || Date.UTC(2050, 0, 1); //如果未定义时间，则我们设定倒计时日期是2050年1月1日
      var future = new Date(d), now = new Date();
      //现在将来秒差值
      var dur = Math.round((future.getTime() - now.getTime()) / 1000) + future.getTimezoneOffset() * 60, pms = {
        sec: "00",
        mini: "00",
        hour: "00",
        day: "00",
        month: "00",
        year: "0"
      };
      if(dur > 0){
        pms.sec = f.zero(dur % 60);
        pms.mini = Math.floor((dur / 60)) > 0? f.zero(Math.floor((dur / 60)) % 60) : "00";
        pms.hour = Math.floor((dur / 3600)) > 0? f.zero(Math.floor((dur / 3600)) % 24) : "00";
        pms.day = Math.floor((dur / 86400)) > 0? f.zero(Math.floor((dur / 86400)) % 30) : "00";
        //月份，以实际平均每月秒数计算
        pms.month = Math.floor((dur / 2629744)) > 0? f.zero(Math.floor((dur / 2629744)) % 12) : "00";
        //年份，按按回归年365天5时48分46秒算
        pms.year = Math.floor((dur / 31556926)) > 0? Math.floor((dur / 31556926)) : "0";
      }
      return pms;
    },
    ui: function(){
      if(o.sec){
        o.sec.innerHTML = f.dv().sec;
      }
      if(o.mini){
        o.mini.innerHTML = f.dv().mini;
      }
      if(o.hour){
        o.hour.innerHTML = f.dv().hour;
      }
      if(o.day){
        o.day.innerHTML = f.dv().day;
      }
      if(o.month){
        o.month.innerHTML = f.dv().month;
      }
      if(o.year){
        o.year.innerHTML = f.dv().year;
      }
      setTimeout(f.ui, 1000);
    }
  };
  f.ui();
};

export {
  utils,
  layer,
  fnTimeCountDown,
}
