import ToastsComponent from './Toast.vue';

let $vm;

export default {
  install (Vue, options) {
    if (!$vm) {
      const ToastsPlugin = Vue.extend(ToastsComponent);
      $vm = new ToastsPlugin({
        el: document.createElement('div')
      })
      document.body.appendChild($vm.$el);
    }
    $vm.show = false;
    function merge (target) {
      for (let i = 1, j = arguments.length; i < j; i++) {
        let source = arguments[i] || {};
        for (let prop in source) {
          if (source.hasOwnProperty(prop)) {
            let value = source[prop];
            if (value !== undefined) {
              target[prop] = value;
            }
          }
        }
      }
    }
    let toasts = {
      show () {
        // Vue.set($vm.show, true)
        $vm.show = true;
      },
      hide () {
        $vm.show = false;
      },
      confirm (options) {
        merge($vm.$data, options);
        $vm.msg = $vm.$data.msg;
        $vm.show = true;
        // return new Promise((resolve, reject) => {
        //   let success = $vm.success;
        //   let cancel = $vm.cancle;
        //   $vm.show = true;
        //   $vm.success = () => {
        //     success();
        //     resolve('ok');
        //   }
        //   $vm.cancel = () => {
        //     cancel();
        //     reject(new Error('something wrong'));
        //   }
        // })
      }
    }
    if (!Vue.$toasts) {
      Vue.$toasts = toasts;
    }

    Vue.mixin({
      created () {
        this.$toasts = Vue.$toasts;
      }
    })
  }
}
