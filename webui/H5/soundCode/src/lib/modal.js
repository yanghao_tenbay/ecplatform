let modal = {
  data () {
    return {
    }
  },
  methods: {
    showConfirm: function (data, submitFn, beforeCloseFn) {
      var beforeCloseFn = beforeCloseFn || function () {}
      var submitFn = submitFn || function () {}
      this.$modal.show({
        template: `
                <div>
                  <div class="modal_body_tips">
                    <div>{{ text }}</div>
                  </div>
                  <div class="modal_footer_confirm">
                    <a href="javascript:void(0);" @click="submit">确定</a>
                  </div>
                </div>
              `,
        data () {
          return {
            text: data
          }
        },
        methods: {
          submit () {
            submitFn();
            this.$emit('close')
          }
        }
      }, {
        text: 'daa'
      }, {
        width: '345px',
        height: '180px',
        classes: 'v-modal'
      }, {
        'before-close': (event) => {
          beforeCloseFn();
        }
      });
    }
  }
}

export {
  modal
}
