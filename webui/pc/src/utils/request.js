import axios from 'axios'
import {
  Message
} from 'element-ui'
import store
from
'@/store'

// create an axios instance
const service = axios.create({
  baseURL: 'http://120.79.161.66:8089', // api的base_url
  // baseURL: 'http://192.168.50.189:8080', // 本地接口
  timeout: 5000, // request timeout
  withCredentials: true //设置cookies
})

// request interceptor
service.interceptors.request.use(config => {
  // Do something before request is sent

  config.headers['Content-Type'] = 'application/json; charset=UTF-8'

  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone interceptor
// http response 服务器响应拦截器，这里拦截401错误，并重新跳入登页重新获取token
service.interceptors.response.use(
  response => {
    return response;
  },
  /**
   * 下面的注释为通过response自定义code来标示请求状态，当code返回如下情况为权限有问题，登出并返回到登录页
   * 如通过xmlhttprequest 状态码标识 逻辑可写在下面error中
   */
  error => {
    console.log('err' + error) // for debug

    const res = response.data;

    if (res.code == 13) {
      MessageBox.confirm('账户未登录，请进行登录', '确定登出', {
        confirmButtonText: '登录',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        store.dispatch('FedLogOut').then(() => {
          // location.reload(); // 为了重新实例化vue-router对象 避免bug

        });
      })
    } else {
      return Promise.reject('error');
    }

    //error
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  })

export default service
