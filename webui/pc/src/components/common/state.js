

import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const modelOperate = {
  namespaced: true,
  state: {
    operate: false,
    userTag: '',
    chargeInfo: {},
  },
  mutations: {
    changeOperate(state, val) {
      state.operate = val;
    },
    changeUserTag(state, val) {
      state.userTag = val;
    },
    changeChargeInfo(state, val) {
      state.chargeInfo = val;
    },
  },
};
const company = {
  namespaced: true,
  state: {
    obj: {
      name: '',
      domain: '',
      user: '',
      password: '',
      email: '',
    },
    data: {
      id: '',
      phone: '',
      real_name: '',
    },
  },
  actions: {
    changeData(ctx, data) {
      ctx.commit('changeData', data);
    },
  },
  mutations: {
    changeObj(state, val) {
      state.obj = val;
    },
    changeData(state, data) {
      state.data = data;
    },
  },
};

export default new Vuex.Store({
  modules: {
    modelOperate,
    company,
  },
});
