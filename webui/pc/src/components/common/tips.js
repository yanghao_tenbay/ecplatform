const mixin = {
  data() {
    return {};
  },
  methods: {
    // 显示提示
    showConfirm(data, obj) {
      // eslint-disable-next-line no-param-reassign
      obj = obj || {};
      const width = obj.width || '360px';
      const height = obj.height || '220px';
      const item = obj.item || {};
      const submitFn = obj.submitFn;
      this.$modal.show({
        template: `
        <div>
          <div class="modal_head">提示</div>
          <div class="modal_body_tips">
            <div>{{text}}</div>
          </div>
          <div class="modal_footer_confirm">
            <a href="javascript:void(0);" @click="submit">确定</a>
          </div>
        </div>
      `,
        props: ['text'],
        methods: {
          submit() {
            if (submitFn) {
              submitFn(item);
              this.$emit('close');
            } else {
              this.$emit('close');
            }
          },
        },
      }, {
        text: data,
      }, {
        width,
        height,
        pivotX: 0.6,
        pivotY: 0.3,
      });
    },
    // 设置cookie
    setCookie(name, value, day) {
      if (day !== 0 && day !== undefined) {
        const expires = day * 24 * 60 * 60 * 1000;
        const date = new Date(+new Date() + expires);
        document.cookie = `${name}=${value};expires=${date.toUTCString()}`;
      } else {
        document.cookie = `${name}=${value}`;
      }
    },
    // 获取cookie
    getCookie(name) {
      const reg = new RegExp(`(^| )${name}=([^;]*)(;|$)`);
      const arr = document.cookie.match(reg);
      if (arr) {
        return decodeURIComponent(arr[2]);
      }
      return null;
    },
    // 删除cookie
    delCookie(name) {
      this.setCookie(name, '', -1);
    },
  },
};
export default mixin;
