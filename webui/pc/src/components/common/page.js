const page = {
  data() {
    return {
      page: {
        total: 0,
        curPage: 1,
        pageCount: 0,
        prePage: 10,
        pageSelect: [10, 20, 50, 100],
      },
    };
  },
  methods: {
    /**
     * 获取列表数据
     * @param api
     * @param obj， 请求的post
     * @param succCallback, 返回的数据回调，用于处理数据
     * @param title， 用于导出数据
     */
    getListData(api, obj, succCallback) {
      this.$http(api, obj)
        .then((res) => {
          if (res.status === 0) {
            this.data = [];
            this.page.total = parseInt(res.recordsTotal, 10);
            this.page.pageCount = Math.ceil(this.page.total / this.page.prePage);
            let data;
            if (typeof res.data === 'string') {
              data = JSON.parse(res.data);
            } else if (res.data.data) {
              data = res.data.data;
            } else {
              data = res.data;
            }
            if (succCallback) {
              succCallback(data);
            }
            for (let i = 0, len = data.length; i < len; i += 1) {
              data[i].no = i + 1 + obj.start;
              this.data.push(data[i]);
            }
            // if (title) { // 导出数据
            //   console.log(data);
            //   return;
            //   this.tableToExcel(title, data);
            // }
          } else {
            this.showConfirm(res.msg || res.data);
          }
        }).catch(() => {
          this.data = [];
        });
    },
    // 分页
    changePage(val, len) {
      this.page.curPage = val;
      this.page.prePage = len || this.page.prePage;
      this.getList();
    },
    // eslint-disable-next-line consistent-return
    tableToExcel(title, jsonData) {
      function isIE() {
        if (!!window.ActiveXObject || 'ActiveXObject' in window) {
          return true;
        }
        return false;
      }
      if (isIE()) {
        this.showConfirm('导出功能站不支持ie内核浏览器，请使用其他浏览器或其他浏览器模式！');
        return false;
      }
      // title 形式 如 `姓名,电话,邮箱\n`;
      // 列标题，逗号隔开，每一个逗号就是隔开一个单元格
      let str = title;
      for (let i = 0; i < jsonData.length; i += 1) {
        // eslint-disable-next-line guard-for-in,no-restricted-syntax
        for (const item in jsonData[i]) {
          str += (`${jsonData[i][item]}\t,`);
        }
        str += '\n';
      }
      const uri = `data:text/csv;charset=utf-8,\ufeff${encodeURIComponent(str)}`;
      // 通过创建a标签实现
      const link = document.createElement('a');
      link.href = uri;
      link.download = 'json数据表.csv';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    },
    /**
     * 导出数据
     * @param api
     * @param obj
     * @param title '公司名称,店铺名称,运营者姓名,省份,城市,手机号,状态\n'
     * @param keyArr [event, status] 数据的key值
     */
    exportDatas(api, obj, title, keyArr) {
      this.$http(this.$api, obj)
        .then((res) => {
          if (res.data === 0) {
            const items = res.data || [];
            const data = [];
            for (let i = 0; i < items.length; i += 1) {
              const arr = [];
              for (let j = 0; j < keyArr.length; j += 1) {
                arr.push(items[i][keyArr[j]]);
              }
              data.push(arr);
            }
            this.tableToExcel(title, data);
          }
        });
    },
  },
};
export default page;
