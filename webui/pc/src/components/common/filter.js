/* eslint-disable */
const coinFormat = (value) => {
  const price = value.toString();
  if(price && price.toString().indexOf('.') > -1){
    let double = price.split('.')[1];
    if(double.length > 1) {
      return Math.round(price*100)/100;
    } else {
      return price + '0';
    }
  }
  return price + '.00';
};
const timeFormat = (value) => {
  return value>9 ? value : `0${value}`
};
const timeNormalFormat = (value) => {
  // 2018/12/13 12:12 转 2018-12-13 12:12
  return value.toString().replace(/\//g, '-');
};
export { coinFormat, timeFormat, timeNormalFormat };
