const serviceModule = {
  login: {
    url: 'login',
    method: 'post',
  },
  rootLogin: {
    url: 'rootlogin',
    method: 'post',
  },
  getImg: {
    url: 'image',
    method: 'post',
  },
  logOut: {
    url: 'logout',
    method: 'post',
  },
  getCode: {
    url: 'regist/get_code',
    method: 'post',
  },
  checkCode: {
    url: 'regist/check_code',
    method: 'post',
  },
  company: {
    url: 'regist/write_company',
    method: 'post',
  },
  province: {
    url: 'regist/get_province',
    method: 'post',
  },
  cities: {
    url: 'regist/get_city',
    method: 'post',
  },
  backPwd: {
    url: 'forgot_passwd',
    method: 'post',
  },
  accountSelect: {
    url: 'account/account_select',
    method: 'post',
  },
  accountSelectInfo: {
    url: 'account/select_info',
    method: 'post',
  },
  positions: {
    url: 'account/position_show',
    method: 'post',
  },
  accountUpdate: {
    url: 'account/account_update',
    method: 'post',
  },
  accountAdd: {
    url: 'account/account_add',
    method: 'post',
  },
  accountDelete: {
    url: 'account/account_delete',
    method: 'post',
  },
  accountRestrict: {
    url: 'account/account_restrict',
    method: 'post',
  },
  positionSelect: {
    url: 'position/position_select',
    method: 'post',
  },
  positionSelectShop: {
    url: 'position/select_shop',
    method: 'post',
  },
  positionSelectInfo: {
    url: 'position/position_info',
    method: 'post',
  },
  positionStatus: {
    url: 'position/position_status',
    method: 'post',
  },
  positionUpdate: {
    url: 'position/position_update',
    method: 'post',
  },
  positionAdd: {
    url: 'position/position_add',
    method: 'post',
  },
  positionDelete: {
    url: 'position/position_delete',
    method: 'post',
  },
  getTap: {
    url: 'userManage/getTap',
    method: 'post',
  },
  getUser: {
    url: 'userManage/getUser',
    method: 'post',
  },
  joinBlack: {
    url: 'userManage/joinBlack',
    method: 'post',
  },
  userInfo: {
    url: 'userManage/userInfo',
    method: 'post',
  },
  newTap: {
    url: 'userManage/newTap',
    method: 'post',
  },
  updTap: {
    url: 'userManage/updTap',
    method: 'post',
  },
  joinTap: {
    url: 'userManage/joinTap',
    method: 'post',
  },
  getTapInfo: {
    url: 'userManage/getTapInfo',
    method: 'post',
  },
  userGetComment: {
    url: 'userManage/getComment',
    method: 'post',
  },
  userGetOrder: {
    url: 'userManage/getOrder',
    method: 'post',
  },
  userGetRebate: {
    url: 'userManage/getRebate',
    method: 'post',
  },
  getPhone: {
    url: 'forgot_passwd',
    method: 'post',
  },
  resetPass: {
    url: 'reset_passwd',
    method: 'post',
  },
  delTap: {
    url: 'userManage/delTap',
    method: 'post',
  },
  codePassword: {
    url: 'receive_code',
    method: 'post',
  },
  getCodeUrl: {
    url: 'wxapp/getCodeUrl',
    method: 'post',
  },
  mchinfo: {
    url: 'mch_conf/mchinfo',
    method: 'post',
  },
  mchSet: {
    url: 'mch_conf/set_mch',
    method: 'post',
  },
  mchCert: {
    url: 'mch_conf/cert',
    method: 'post',
  },
  outBlack: {
    url: 'userManage/outBlack',
    method: 'post',
  },
  myTrade: {
    url: 'userManage/myTrade',
    method: 'post',
  },
  smsOrder: {
    url: 'sms/smsOrder',
    method: 'post',
  },
  smsPackage: {
    url: 'sms/smsPackage',
    method: 'post',
  },
  restSms: {
    url: 'sms/restSms',
    method: 'post',
  },
  goods: {
    url: 'sms/good',
    method: 'post',
  },
  judgePay: {
    url: 'sms/judgePay',
    method: 'post',
  },
  tradeList: {
    url: 'trade_history/select',
    method: 'post',
  },
  tradeDel: {
    url: 'trade_history/del',
    method: 'post',
  },
  checkPhone: {
    url: 'trade_history/user_info',
    method: 'post',
  },
  tradeAdd: {
    url: 'trade_history/add',
    method: 'post',
  },
  tradeUpdate: {
    url: 'trade_history/update',
    method: 'post',
  },
  tradeTransfer: {
    url: 'trade_history/transfer',
    method: 'post',
  },
  tradeAudit: {
    url: 'trade_history/audit',
    method: 'post',
  },
  tradeBatch: {
    url: 'trade_history/batch',
    method: 'post',
  },
  newPassword: {
    url: 'personal_homepage/personal_passwd',
    method: 'post',
  },

  // 公司平台管理api
  changePassword: {
    url: 'platform/personal_passwd', // 更改过plateform - by yh
    method: 'post',
  },
  // 公司信息查询
  selectCompany: {
    url: 'check/select_company',
    method: 'post',
  },
  companyDelete: {
    url: 'check/company_delete',
    method: 'post',
  },
  // 审核不通过
  checkNot: {
    url: 'check/check_not',
    method: 'post',
  },
  checkCompany: {
    url: 'check/check_company',
    method: 'post',
  },
  companyBlack: {
    url: 'check/company_black',
    method: 'post',
  },
  companyWhite: {
    url: 'check/company_white',
    method: 'post',
  },
  editeCompany: {
    url: 'check/edite_company',
    method: 'post',
  },
  getProvince: {
    url: 'regist/get_province',
    method: 'post',
  },
  getCity: {
    url: 'regist/get_city',
    method: 'post',
  },
  // 1.1新增接口
  // 返利策略-订单信息
  orderSelectOrderInfo: {
    url: 'order_info/select_orderinfo',
    method: 'post',
  },
  // 返利策略-订单返利
  rebateAddEvent: {
    url: 'order_rebate/add_event',
    method: 'post',
  },
  rebateSelectEvent: {
    url: 'order_rebate/select_event',
    method: 'post',
  },
  rebateUpdateEvent: {
    url: 'order_rebate/update_event',
    method: 'post',
  },
  rebateUpdateEventTime: {
    url: 'order_rebate/update_event_time',
    method: 'post',
  },
  rebateDeleteEvent: {
    url: 'order_rebate/delete_event',
    method: 'post',
  },
  rebateSelectRecord: {
    url: 'order_rebate/select_record',
    method: 'post',
  },
  rebateApprove: {
    url: 'order_rebate/approve',
    method: 'post',
  },
  rebateUnapprove: {
    url: 'order_rebate/unapprove',
    method: 'post',
  },
  rebateRebate: {
    url: 'order_rebate/rebate',
    method: 'post',
  },
  rebateBatchRebate: {
    url: 'order_rebate/batch_rebate',
    method: 'post',
  },
  rebateEventRecord: {
    url: 'order_rebate/event_record',
    method: 'post',
  },
  rebateGetImage: {
    url: 'order_rebate/get_image',
    method: 'post',
  },
  // 返利策略-晒评价
  evaluateSelectEvent: {
    url: 'comment_rebate/select_event',
    method: 'post',
  },
  evaluateAddEvent: {
    url: 'comment_rebate/add_event',
    method: 'post',
  },
  evaluateUpdateEvent: {
    url: 'comment_rebate/update_event',
    method: 'post',
  },
  evaluateDeleteEvent: {
    url: 'comment_rebate/delete_event',
    method: 'post',
  },
  evaluateSelectRecord: {
    url: 'comment_rebate/select_record',
    method: 'post',
  },
  evaluateSelectShop: {
    url: 'comment_rebate/select_shop',
    method: 'post',
  },
  evaluateUpdateEventTime: {
    url: 'comment_rebate/update_event_time',
    method: 'post',
  },
  evaluateApprove: {
    url: 'comment_rebate/approve',
    method: 'post',
  },
  evaluateUnapprove: {
    url: 'comment_rebate/unapprove',
    method: 'post',
  },
  evaluateRebate: {
    url: 'comment_rebate/rebate',
    method: 'post',
  },
  evaluateBatchRebate: {
    url: 'comment_rebate/batch_rebate',
    method: 'post',
  },
  evaluateCommentRebate: {
    url: 'comment_rebate/comment_rebate',
    method: 'post',
  },
  // 积分商城管理
  getGiftName: {
    url: 'creditsManage/getGiftName',
    method: 'post',
  },
  getGift: {
    url: 'creditsManage/getGift',
    method: 'post',
  },
  newGift: {
    url: 'creditsManage/newGift',
    method: 'post',
  },
  updGift: {
    url: 'creditsManage/updGift',
    method: 'post',
  },
  delGift: {
    url: 'creditsManage/delGift',
    method: 'post',
  },
  getGiftRecord: {
    url: 'creditsManage/getGiftRecord',
    method: 'post',
  },
  delGiftRecord: {
    url: 'creditsManage/delGiftRecord',
    method: 'post',
  },
  savePic: {
    url: 'creditsManage/savePic',
    method: 'post',
  },
  image_upload: {
    url: 'creditsManage/image_upload',
    method: 'post',
  },
  getCredit: {
    url: 'userManage/getCredit',
    method: 'post',
  },
  saveRule: {
    url: 'creditsManage/saveRule',
    method: 'post',
  },
  showPic: {
    url: 'creditsManage/showPic',
    method: 'post',
  },
  selectPic: {
    url: 'creditsManage/selectPic',
    method: 'post',
  },
  delPic: {
    url: 'creditsManage/delPic',
    method: 'post',
  },
  // 数据统计接口
  attendence: {
    url: 'staticts/attendence',
    method: 'post',
  },
  credits: {
    url: 'staticts/credits',
    method: 'post',
  },
  exchange: {
    url: 'staticts/exchange',
    method: 'post',
  },
  countDayStatistics: {
    url: 'comment_statistics/day_statistics', // 晒评价按天
    method: 'post',
  },
  countMonthStatistics: {
    url: 'comment_statistics/month_statistics', // 晒评价按月
    method: 'post',
  },
  countTimeStatistics: {
    url: 'comment_statistics/time_statistics',
    method: 'post',
  },
  countOrderDayStatistics: {
    url: 'order_statistics/day_statistics',
    method: 'post',
  },
  countOrderMonthStatistics: {
    url: 'order_statistics/month_statistics',
    method: 'post',
  },
  countOrderTimeStatistics: {
    url: 'order_statistics/time_statistics',
    method: 'post',
  },
  // 返利商城接口
  mallSelect: {
    url: 'rebate_mall/select',
    method: 'post',
  },
  mallInsert: {
    url: 'rebate_mall/insert',
    method: 'post',
  },
  mallUpdate: {
    url: 'rebate_mall/update',
    method: 'post',
  },
  mallDel: {
    url: 'rebate_mall/del',
    method: 'post',
  },
  mallLink: {
    url: 'rebate_mall/link',
    method: 'post',
  },
  mallSelectRecord: {
    url: 'rebate_mall/select_record',
    method: 'post',
  },
  mallDelRecord: {
    url: 'rebate_mall/del_record',
    method: 'post',
  },
  mallGetCarousel: {
    url: 'rebate_mall/get_carousel',
    method: 'post',
  },
  mallSetCarousel: {
    url: 'rebate_mall/set_carousel',
    method: 'post',
  },
  // 店铺管理
  shopGetAuthUrl: {
    url: 'taobaoAuth/getAuthUrl',
    method: 'post',
  },
  shopShop: {
    url: 'taobaoAuth/shop',
    method: 'post',
  },
  checkCompanyStart: {
    url: 'check/company_stat',
    method: 'post',
  },
  checkCompanyTime: {
    url: 'check/company_time',
    method: 'post',
  },
  // 平台首页接口 check/company_stat
  platformDataStatistics: {
    url: 'platform/data_statistics',
    method: 'post',
  },
  // 消息通知接口
  messageGetMessage: {
    url: 'message/get_message',
    method: 'post',
  },
};

const ApiSetting = { ...serviceModule };

export default ApiSetting;
