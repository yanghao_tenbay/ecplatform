import Vue from 'vue';
import Router from 'vue-router';

import promote from '../components/lib/promote';

import login from '../components/login/login';
import register from '../components/login/register';
import backPwd from '../components/login/backPassword';

import evaluate from '../components/tactics/evaluate';
import evaluateList from '../components/tactics/evaluate/list';
import evaluateDetail from '../components/tactics/evaluate/listDetail';
import evaluateInfo from '../components/tactics/evaluate/record';

import order from '../components/tactics/order';
import orderList from '../components/tactics/order/order';
import orderLogistics from '../components/tactics/order/logistics';

import rebate from '../components/tactics/rebate';
import rebateList from '../components/tactics/rebate/list';
import rebateDetail from '../components/tactics/rebate/listDetail';
import rebateInfo from '../components/tactics/rebate/info';

import mall from '../components/mall/mall';
import mallList from '../components/mall/mallList';
import mallListConfig from '../components/mall/mallListConfig';
import mallInfo from '../components/mall/mallInfo';
import mallConfig from '../components/mall/mallConfig';

import integral from '../components/integral/integral';
import integralList from '../components/integral/integralList';
import integralListConfig from '../components/integral/integralListConfig';
import integralInfo from '../components/integral/integralInfo';
import integralConfig from '../components/integral/integralConfig';

import account from '../components/account/account';
import accountManage from '../components/account/accountManage';
import accountWork from '../components/account/accountWork';
import accountConfig from '../components/account/accountConfig';
import workConfig from '../components/account/workConfig';

import user from '../components/user/user';
import userInfo from '../components/user/userInfo';
import userTag from '../components/user/userTag';
import userDetail from '../components/user/userDetail';
import userRecords from '../components/user/records';
import userRule from '../components/user/rule';

import evaluateCount from '../components/count/evaluate';
import exchangeCount from '../components/count/exchange';
import integralCount from '../components/count/integral';
import rebateCount from '../components/count/rebate';
import signCount from '../components/count/sign';

import systemConfig from '../components/system/systemConfig';
import shop from '../components/system/shop';
import smsRecharge from '../components/system/smsRecharge';
import modifyPwd from '../components/system/modifyPwd';
import example from '../components/system/example';

import home from '../components/home/home';

import homeTable from '../components/company/homeTable';
import company from '../components/company/company';

import password from '../components/password/password';

import help from '../components/notice/help';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/dologin',
    },
    {
      path: '/dologin',
      name: '登录',
      component: login,
    },
    {
      path: '/doregister',
      name: '注册',
      component: register,
    },
    {
      path: '/dobackPwd',
      name: '密码',
      component: backPwd,
    },
    {
      path: '/evaluate',
      name: 'evaluate',
      component: evaluate,
      redirect: '/evaluate/list',
      children: [{
        path: 'list',
        name: '活动列表',
        component: evaluateList,
      }, {
        path: 'detail',
        name: '活动信息',
        component: evaluateDetail,
      }, {
        path: 'record',
        name: '活动参与记录',
        component: evaluateInfo,
      }],
    },
    {
      path: '/order',
      name: 'order',
      component: order,
      redirect: '/order/order',
      children: [{
        path: 'order',
        name: '订单信息',
        component: orderList,
      }, {
        path: 'logistics',
        name: '物流信息',
        component: orderLogistics,
      }],
    },
    {
      path: '/rebate',
      name: 'rebate',
      component: rebate,
      redirect: '/rebate/list',
      children: [{
        path: 'list',
        name: '活动列表',
        component: rebateList,
      }, {
        path: 'detail',
        name: '活动信息',
        component: rebateDetail,
      }, {
        path: 'promote',
        name: '活动推广',
        component: promote,
      }, {
        path: 'info',
        name: '活动参与记录',
        component: rebateInfo,
      }],
    },
    {
      path: '/mall',
      name: 'mall',
      component: mall,
      redirect: '/mall/list',
      children: [{
        path: 'list',
        name: '活动列表',
        component: mallList,
      }, {
        path: 'detail',
        name: '活动推广',
        component: promote,
      }, {
        path: 'listInfo',
        name: '活动信息',
        component: mallListConfig,
      }, {
        path: 'info',
        name: '活动参与记录',
        component: mallInfo,
      }, {
        path: 'config',
        name: '商城配置',
        component: mallConfig,
      }],
    },
    {
      path: '/integral',
      name: 'integral',
      component: integral,
      redirect: '/integral/list',
      children: [{
        path: 'list',
        name: '活动列表',
        component: integralList,
      }, {
        path: 'edit',
        name: '活动列表',
        component: () => import('../components/integral/giftEdit'),
      }, {
        path: 'detail',
        name: '活动推广',
        component: promote,
      }, {
        path: 'listInfo',
        name: '活动信息',
        component: integralListConfig,
      }, {
        path: 'info',
        name: '活动参与记录',
        component: integralInfo,
      }, {
        path: 'config',
        name: '商城配置',
        component: integralConfig,
      }],
    },
    {
      path: '/account',
      name: '账号管理',
      component: account,
      redirect: '/account/accountManage',
      children: [{
        path: 'accountManage',
        name: '账号管理',
        component: accountManage,
      }, {
        path: 'accountWork',
        name: '职位管理',
        component: accountWork,
      }, {
        path: 'accountConfig/:id',
        name: '账号信息',
        component: accountConfig,
      }, {
        path: 'workConfig/:id',
        name: '职位信息',
        component: workConfig,
      }],
    },
    {
      path: '/user',
      name: '用户管理',
      component: user,
      redirect: '/user/userInfo',
      children: [{
        path: 'userInfo',
        name: '用户信息',
        component: userInfo,
      }, {
        path: 'userTag',
        name: '用户标签',
        component: userTag,
      }, {
        path: 'userDetail/:id',
        name: '用户详情',
        component: userDetail,
      }, {
        path: 'userRecords',
        name: '积分记录',
        component: userRecords,
      }, {
        path: 'userRule',
        name: '签到规则',
        component: userRule,
      }],
    },
    {
      path: '/evaluateCount',
      name: '晒评价',
      component: evaluateCount,
    },
    {
      path: '/rebateCount',
      name: '订单返利',
      component: rebateCount,
    },
    {
      path: '/signCount',
      name: '签到',
      component: signCount,
    },
    {
      path: '/exchangeCount',
      name: '兑换记录',
      component: exchangeCount,
    },
    {
      path: '/integralCount',
      name: '积分记录',
      component: integralCount,
    },
    {
      path: '/systemConfig',
      name: '公众号管理',
      component: systemConfig,
    },
    {
      path: '/shop',
      name: '店铺管理',
      component: shop,
    },
    {
      path: '/smsRecharge',
      name: '短信充值',
      component: smsRecharge,
    },
    {
      path: '/modifyPwd',
      name: '密码修改',
      component: modifyPwd,
    },
    {
      path: '/doexample/:ind',
      name: '示例',
      component: example,
    },
    {
      path: '/home',
      name: '公司管理',
      component: home,
    },
    {
      path: '/company',
      name: '公司管理',
      component: homeTable,
    },
    {
      path: '/help',
      name: '帮助中心',
      component: help,
    },
    // 平台账号登录 start
    {
      path: '/companyDetail',
      name: '公司管理',
      component: company,
    },
    {
      path: '/password',
      name: '密码修改',
      component: password,
    }],
});
