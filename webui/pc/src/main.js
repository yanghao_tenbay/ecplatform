import Vue from 'vue';
import VModal from 'vue-js-modal';
import dateTimePicker from 'vue-date-time-m';
import App from './App';
import router from './router';
import http from './components/common/http';
import store from './components/common/state';
import vTable from './components/common/table';
import api from './components/common/apis';
import mixin from './components/common/tips';
import VueBetterScroll from 'vue2-better-scroll';
import { timeNormalFormat } from './components/common/filter.js';
import { Scrollbar, Progress } from 'element-ui';
import './css/el.css'
import '@/css/index.scss'
import  $ from  'jquery'
window.$=$
import  tinymce from 'tinymce'
import 'tinymce/themes/modern'
import 'tinymce/themes/mobile'
import 'tinymce/plugins/noneditable'
import 'tinymce/plugins/preview'//
import 'tinymce/plugins/colorpicker'//
import 'tinymce/plugins/textcolor'
import 'tinymce/plugins/fullpage' //
import 'tinymce/plugins/insertdatetime' //
import 'tinymce/plugins/save' //
import 'tinymce/plugins/table'//
import 'tinymce/plugins/emoticons'
import 'tinymce/plugins/importcss'//
import 'tinymce/plugins/image'  //图片
import 'tinymce/plugins/imagetools'//
import 'tinymce/plugins/fullscreen' //
import 'tinymce/plugins/wordcount'
import 'tinymce/plugins/imagetools'
import 'tinymce/plugins/lists'  //
import 'tinymce/plugins/autoresize'
import 'tinymce/plugins/visualblocks'//
import 'tinymce/plugins/advlist'
import 'tinymce/plugins/hr'//
import 'tinymce/plugins/paste'//
import 'tinymce/plugins/code'//
import 'tinymce/plugins/link'//
import 'tinymce/plugins/help'//
import 'tinymce/skins/lightgray/skin.min.css'
import '../static/zh_CN'

const VueUploadComponent = require('vue-upload-component');

Vue.config.productionTip = false;

Vue.use(VModal, { dynamic: true });

Vue.use(VueBetterScroll);

Vue.component('data-time-picker', dateTimePicker);

Vue.mixin(mixin);

Vue.component('v-table', vTable);

Vue.component('file-upload', VueUploadComponent);

Vue.filter('timeNormalFormat', timeNormalFormat);

import ElementUI from 'element-ui'
Vue.use(ElementUI)
Vue.use(Scrollbar);
Vue.use(Progress);

Vue.prototype.$http = http;
Vue.prototype.$api = api;
Vue.prototype.bus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
