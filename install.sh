#!/bin/sh

### 关闭进程
/etc/init.d/apisrv stop

git_dir=`pwd`
echo "git_dir =  ${git_dir}"

dir_name=Ecommerce_`date +%Y%m%d%H%M%S`
echo $dir_name

cp -rf ${git_dir} /var/Ecommerce/${dir_name}/
rm -rf /Ecommerce
ln -sf /var/Ecommerce/${dir_name} /Ecommerce

sh -x /Ecommerce/dos2unix.sh

### 更改配置
# cd /Ecommerce/tools/script/conf
# sh -x conf.sh

### db
# cd /Ecommerce/tools/script/db
# sh -x init.sh

### 编译ts
cd /Ecommerce/apisrv
cnpm install
tsc -p .

### 加权限
chmod 777 /Ecommerce/tools/script/etc_init.d/apisrv
chmod 777 /Ecommerce/tools/script/apisrv.sh

### /etc/init.d
ln -sf /Ecommerce/tools/script/etc_init.d/apisrv /etc/init.d/

### 重启进程
/etc/init.d/apisrv start

### 自启动
# echo "setup startup for apps"
update-rc.d -f apisrv remove >/dev/null 2>&1
update-rc.d apisrv defaults 50 >/dev/null 2>&1